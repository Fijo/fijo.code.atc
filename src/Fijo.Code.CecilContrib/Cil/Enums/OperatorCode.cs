﻿using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.CecilContrib.Cil.Enums {
	[Flags]
	public enum OperatorCode {
		Unsigned = 1,
		OverflowCheck = 2,
		HasRightValue = 4,
		Not = 8,
		Equal = 16 | HasRightValue,
		NotEqual = Equal | Not,
		Greater = 32 | HasRightValue,
		GreaterUnsigned = Greater | Unsigned,
		GreaterOrEqual = Greater | Equal,
		GreaterOrEqualUnsigned = Greater | Equal | Unsigned,
		Less = 64 | HasRightValue,
		LessUnsigned = Less | Unsigned,
		LessOrEqual = Less | Equal,
		LessOrEqualUnsigned = Less | Equal | Unsigned,
		Divide = 128 | HasRightValue,
		DivideOverflowCheck = Divide | OverflowCheck,
		DivideUnsigned = Divide | Unsigned,
		DivideUnsignedOverflowCheck = Divide | Unsigned | OverflowCheck,
		Multiply = 256 | HasRightValue,
		MultiplyOverflowCheck = Multiply | OverflowCheck,
		MultiplyUnsigned = Multiply | Unsigned,
		MultiplyUnsignedOverflowCheck = Multiply | Unsigned | OverflowCheck,
		Add = 512 | HasRightValue,
		AddOverflowCheck = Add | OverflowCheck,
		AddUnsigned = Add | Unsigned,
		AddUnsignedOverflowCheck = Add | OverflowCheck | Unsigned,
		Sub = 1024 | HasRightValue,
		SubOverflowCheck = Sub | OverflowCheck,
		SubUnsigned = Sub | Unsigned,
		SubUnsignedOverflowCheck = Sub | OverflowCheck | Unsigned,
		[About("Rem")]
		Mod = 2048 | HasRightValue,
		ModUnsigned = Mod | Unsigned,
		BitwiseNot = 4096,
		BitwiseAnd = 8192 | HasRightValue,
		BitwiseOr = 16384 | HasRightValue,
		BitwiseXOr = 32768 | HasRightValue,
		BitwiseShiftLeft = 65536 | HasRightValue,
		BitwiseShiftRight = 131072 | HasRightValue,
		BitwiseShiftRightUnsigned = BitwiseShiftRight | Unsigned,
		Negate = 262144
	}
}