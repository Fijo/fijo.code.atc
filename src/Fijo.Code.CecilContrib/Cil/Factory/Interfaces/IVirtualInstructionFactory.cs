﻿using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Factory.Interfaces {
	public interface IVirtuaTreatelInstructionFactory {
		IEnumerable<TreateInstruction> CreateOverflowCheck(Instruction source);
	}
}