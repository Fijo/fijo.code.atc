﻿using Fijo.Code.CecilContrib.Cil.Model.Expr;

namespace Fijo.Code.CecilContrib.Cil.Factory.Interfaces {
	public interface IExpressions {
		Expression Equal { get; }
		Expression Greater { get; }
		Expression GreaterOrEqual { get; }
		Expression GreaterOrEqualUnsigned { get; }
		Expression GreaterUnsigned { get; }
		Expression Less { get; }
		Expression LessOrEqual { get; }
		Expression LessOrEqualUnsigned { get; }
		Expression LessUnsigned { get; }
		Expression NotEqual { get; }
		Expression IsFalse { get; }
		Expression IsTrue { get; }
		Expression Add { get; }
		Expression AddOverflowCheck { get; }
		Expression AddUnsigned { get; }
		Expression AddUnsignedOverflowCheck { get; }
		Expression Sub { get; }
		Expression SubOverflowCheck { get; }
		Expression SubUnsigned { get; }
		Expression SubUnsignedOverflowCheck { get; }
		Expression Divide { get; }
		Expression DivideOverflowCheck { get; }
		Expression DivideUnsigned { get; }
		Expression DivideUnsignedOverflowCheck { get; }
		Expression Multiply { get; }
		Expression MultiplyOverflowCheck { get; }
		Expression MultiplyUnsigned { get; }
		Expression MultiplyUnsignedOverflowCheck { get; }
		Expression Mod { get; }
		Expression ModUnsigned { get; }
		Expression BitwiseNot { get; }
		Expression BitwiseAnd { get; }
		Expression BitwiseOr { get; }
		Expression BitwiseXOr { get; }
		Expression BitwiseShiftLeft { get; }
		Expression BitwiseShiftRight { get; }
		Expression BitwiseShiftRightUnsigned { get; }
		Expression Negate { get; }
	}
}