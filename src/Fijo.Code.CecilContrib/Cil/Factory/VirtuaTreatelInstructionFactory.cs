﻿using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Factory.Interfaces;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Factory {
	public class VirtuaTreatelInstructionFactory : IVirtuaTreatelInstructionFactory {
		#region Implementation of IVirtuaTreatelInstructionFactory
		public IEnumerable<TreateInstruction> CreateOverflowCheck(Instruction source) {
			yield return new OverflowCheckInstruction(source);
		}
		#endregion
	}
}