﻿using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Factory.Interfaces;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Enums;

namespace Fijo.Code.CecilContrib.Cil.Factory {
	public class Expressions : IExpressions {
		public Expression Equal { get; protected set; }
		public Expression Greater { get; protected set; }
		public Expression GreaterOrEqual { get; protected set; }
		public Expression GreaterOrEqualUnsigned { get; protected set; }
		public Expression GreaterUnsigned { get; protected set; }
		public Expression Less { get; protected set; }
		public Expression LessOrEqual { get; protected set; }
		public Expression LessOrEqualUnsigned { get; protected set; }
		public Expression LessUnsigned { get; protected set; }
		public Expression NotEqual { get; protected set; }
		public Expression IsFalse { get; protected set; }
		public Expression IsTrue { get; protected set; }
		public Expression Add { get; protected set; }
		public Expression AddOverflowCheck { get; protected set; }
		public Expression AddUnsigned { get; protected set; }
		public Expression AddUnsignedOverflowCheck { get; protected set; }
		public Expression Sub { get; protected set; }
		public Expression SubOverflowCheck { get; protected set; }
		public Expression SubUnsigned { get; protected set; }
		public Expression SubUnsignedOverflowCheck { get; protected set; }
		public Expression Divide { get; protected set; }
		public Expression DivideOverflowCheck { get; protected set; }
		public Expression DivideUnsigned { get; protected set; }
		public Expression DivideUnsignedOverflowCheck { get; protected set; }
		public Expression Multiply { get; protected set; }
		public Expression MultiplyOverflowCheck { get; protected set; }
		public Expression MultiplyUnsigned { get; protected set; }
		public Expression MultiplyUnsignedOverflowCheck { get; protected set; }
		public Expression Mod { get; protected set; }
		public Expression ModUnsigned { get; protected set; }
		public Expression BitwiseNot { get; protected set; }
		public Expression BitwiseAnd { get; protected set; }
		public Expression BitwiseOr { get; protected set; }
		public Expression BitwiseXOr { get; protected set; }
		public Expression BitwiseShiftLeft { get; protected set; }
		public Expression BitwiseShiftRight { get; protected set; }
		public Expression BitwiseShiftRightUnsigned { get; protected set; }
		public Expression Negate { get; protected set; }

		public Expressions() {
			Equal = Create(OperatorCode.Equal);
			Greater = Create(OperatorCode.Greater);
			GreaterOrEqual = Create(OperatorCode.GreaterOrEqual);
			GreaterOrEqualUnsigned = Create(OperatorCode.GreaterOrEqualUnsigned);
			GreaterUnsigned = Create(OperatorCode.GreaterUnsigned);
			Less = Create(OperatorCode.Less);
			LessOrEqual = Create(OperatorCode.LessOrEqual);
			LessOrEqualUnsigned = Create(OperatorCode.LessOrEqualUnsigned);
			LessUnsigned = Create(OperatorCode.LessUnsigned);
			NotEqual = Create(OperatorCode.NotEqual);
			IsFalse = Create(OperatorCode.Equal, 0, CoreTypes.Int32);
			IsTrue = Create(OperatorCode.NotEqual, 0, CoreTypes.Int32);
			Add = Create(OperatorCode.Add);
			AddOverflowCheck = Create(OperatorCode.AddOverflowCheck);
			AddUnsigned = Create(OperatorCode.AddUnsigned);
			AddUnsignedOverflowCheck = Create(OperatorCode.AddUnsignedOverflowCheck);
			Sub = Create(OperatorCode.Sub);
			SubOverflowCheck = Create(OperatorCode.SubOverflowCheck);
			SubUnsigned = Create(OperatorCode.SubUnsigned);
			SubUnsignedOverflowCheck = Create(OperatorCode.SubUnsignedOverflowCheck);
			Divide = Create(OperatorCode.Divide);
			DivideOverflowCheck = Create(OperatorCode.DivideOverflowCheck);
			DivideUnsigned = Create(OperatorCode.DivideUnsigned);
			DivideUnsignedOverflowCheck = Create(OperatorCode.DivideUnsignedOverflowCheck);
			Multiply = Create(OperatorCode.Multiply);
			MultiplyOverflowCheck = Create(OperatorCode.MultiplyOverflowCheck);
			MultiplyUnsigned = Create(OperatorCode.MultiplyUnsigned);
			MultiplyUnsignedOverflowCheck = Create(OperatorCode.MultiplyUnsignedOverflowCheck);
			Mod = Create(OperatorCode.Mod);
			ModUnsigned = Create(OperatorCode.ModUnsigned);
			BitwiseNot = Create(OperatorCode.BitwiseNot);
			BitwiseAnd = Create(OperatorCode.BitwiseAnd);
			BitwiseOr = Create(OperatorCode.BitwiseOr);
			BitwiseXOr = Create(OperatorCode.BitwiseXOr);
			BitwiseShiftLeft = Create(OperatorCode.BitwiseShiftLeft);
			BitwiseShiftRight = Create(OperatorCode.BitwiseShiftRight);
			BitwiseShiftRightUnsigned = Create(OperatorCode.BitwiseShiftRightUnsigned);
			Negate = Create(OperatorCode.Negate);
		}

		private Expression Create(OperatorCode code, object rightValue = null, CoreTypes rightValueType = default(CoreTypes)) {
			return new Expression(code, rightValue, rightValueType);
		}
	}
}