using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Extract a value-type from obj, its boxed representation.")]
	public class UnboxInstruction : TreateInstruction<TypeDefinition> {
		public UnboxInstruction(TypeDefinition target, Instruction source) : base(TreatedCode.Unbox, target, source) {}
	}
}