using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("End finally clause of an exception block.")]
	public class EndFinallyInstruction : SimpleTreateInstruction {
		public EndFinallyInstruction(Instruction source) : base(TreatedCode.EndFinally, source) {}
	}
}