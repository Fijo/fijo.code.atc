using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Indirect load value of spec type as spec type on the stack.")]
	public class LdIndInstruction : TreateInstruction<TypeReference> {
		public readonly TypeReference AsType;
		public LdIndInstruction(TypeReference type, TypeReference asType, Instruction source) : base(TreatedCode.LdInd, type, source) {
			AsType = asType;
		}
	}
}