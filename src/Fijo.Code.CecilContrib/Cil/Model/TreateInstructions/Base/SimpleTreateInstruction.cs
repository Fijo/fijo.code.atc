using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base {
	[About("TargetlessTreateInstruction")]
	public abstract class SimpleTreateInstruction : TreateInstruction {
		protected SimpleTreateInstruction(TreatedCode code, Instruction source) : base(code, source) {}
	}
}