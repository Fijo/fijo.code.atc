using Fijo.Code.CecilContrib.Cil.Enums;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base {
	public class TreateInstruction {
		public readonly TreatedCode Code;
		public readonly Instruction Source;

		public TreateInstruction(TreatedCode code, Instruction source) {
			Code = code;
			Source = source;
		}
	}

	public abstract class TreateInstruction<T> : TreateInstruction {
		public readonly T Target;

		protected TreateInstruction(TreatedCode code, T target, Instruction source) : base(code, source) {
			Target = target;
		}
	}
}