using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push the type token stored in a typed reference.")]
	public class RefAnyTypeInstruction : SimpleTreateInstruction {
		public RefAnyTypeInstruction(Instruction source) : base(TreatedCode.RefAnyType, source) {}
	}
}