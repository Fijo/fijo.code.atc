using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push address of virtual method method on the stack.")]
	public class LdVirtualFtnInstruction : TreateInstruction<MethodReference> {
		public LdVirtualFtnInstruction(MethodReference target, Instruction source) : base(TreatedCode.LdVirtualFtn, target, source) {}
	}
}