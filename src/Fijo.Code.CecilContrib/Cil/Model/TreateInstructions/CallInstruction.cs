using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Call method described by method.")]
	public class CallInstruction : TreateInstruction<IMethodSignature> {
		public CallInstruction(IMethodSignature target, Instruction source) : base(TreatedCode.Call, target, source) {}
	}
}