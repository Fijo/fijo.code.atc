using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Exit a protected region of code.")]
	public class LeaveInstruction : TreateInstruction<Instruction> {
		public LeaveInstruction(Instruction target, Instruction source) : base(TreatedCode.Leave, target, source) {}
	}
}