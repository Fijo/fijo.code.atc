using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Sets an element of the spec type at index (on the stack) as value at an index using an indexer (of an array).")]
	public class StElemInstruction : TreateInstruction<TypeReference> {
		public StElemInstruction(TypeReference type, Instruction source) : base(TreatedCode.StElem, type, source) {}
	}
}