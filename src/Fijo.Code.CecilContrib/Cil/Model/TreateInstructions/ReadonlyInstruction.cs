using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Specify that the subsequent array address operation performs no type check at runtime, and that it returns a controlled-mutability managed pointer")]
	[RelatedLink("https://www.simple-talk.com/blogs/2010/11/11/subterranean-il-generics-and-array-covariance/", "http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.readonly.aspx")]
	public class ReadonlyInstruction : SimpleTreateInstruction {
		public ReadonlyInstruction(Instruction source) : base(TreatedCode.Readonly, source) {}
	}
}