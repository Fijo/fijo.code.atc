using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push a typed reference to ptr of type class onto the stack.")]
	public class MkRefAnyInstruction : TreateInstruction<TypeReference> {
		public MkRefAnyInstruction(TypeReference target, Instruction source) : base(TreatedCode.MkRefAny, target, source) {}
	}
}