using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Pop a value from stack into local variable index.")]
	public class StLocInstruction : TreateInstruction<VariableDefinition> {
		public StLocInstruction(VariableDefinition target, Instruction source) : base(TreatedCode.StLoc, target, source) {}
	}
}