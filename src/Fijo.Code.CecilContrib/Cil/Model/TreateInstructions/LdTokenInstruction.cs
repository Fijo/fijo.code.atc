using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[RelatedLink("https://www.simple-talk.com/blogs/2012/05/31/postsharp-obfuscation-and-il/")]
	public class LdTokenInstruction : TreateInstruction<MemberReference> {
		public LdTokenInstruction(MemberReference target, Instruction source) : base(TreatedCode.LdToken, target, source) {}
	}
}