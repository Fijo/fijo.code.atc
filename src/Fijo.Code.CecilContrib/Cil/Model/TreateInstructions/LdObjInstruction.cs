using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Copy the value stored at address src to the stack.")]
	public class LdObjInstruction : TreateInstruction<TypeReference> {
		public LdObjInstruction(TypeReference type, Instruction source) : base(TreatedCode.LdObj, type, source) {}
	}
}