using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Return argument list handle for the current method.")]
	public class ArgListInstruction : SimpleTreateInstruction {
		public ArgListInstruction(Instruction source) : base(TreatedCode.ArgList, source) {}
	}
}