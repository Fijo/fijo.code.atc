using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("End an exception handling filter clause.")]
	public class EndFilterInstruction : SimpleTreateInstruction {
		public EndFilterInstruction(Instruction source) : base(TreatedCode.EndFilter, source) {}
	}
}