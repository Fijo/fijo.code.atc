using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Return from method, possibly with a value.")]
	public class RetInstruction : SimpleTreateInstruction {
		public RetInstruction(Instruction source) : base(TreatedCode.Ret, source) {}
	}
}