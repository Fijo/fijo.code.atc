using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Do nothing.")]
	public class NopInstruction : SimpleTreateInstruction {
		public NopInstruction(Instruction source) : base(TreatedCode.Nop, source) {}
	}
}