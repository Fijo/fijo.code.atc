using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Load the address of element at index onto the top of the stack.")]
	public class LdElemaInstruction : TreateInstruction<TypeReference> {
		public LdElemaInstruction(TypeReference target, Instruction source) : base(TreatedCode.LdElema, target, source) {}
	}
}