using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push the address of field of object obj on the stack.")]
	public class LdFldaInstruction : TreateInstruction<FieldDefinition> {
		public LdFldaInstruction(FieldDefinition target, Instruction source) : base(TreatedCode.LdFlda, target, source) {}
	}
}