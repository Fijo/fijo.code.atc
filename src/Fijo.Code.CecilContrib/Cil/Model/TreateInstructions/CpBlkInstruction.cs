using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Copy data from memory to memory.")]
	public class CpBlkInstruction : SimpleTreateInstruction {
		public CpBlkInstruction(Instruction source) : base(TreatedCode.CpBlk, source) {}
	}
}