using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Load local variable of index indx onto stack.")]
	public class LdLocInstruction : TreateInstruction<VariableDefinition> {
		public LdLocInstruction(VariableDefinition target, Instruction source) : base(TreatedCode.LdLoc, target, source) {}
	}
}