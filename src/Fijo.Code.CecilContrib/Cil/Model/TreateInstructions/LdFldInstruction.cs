using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push the value of field of object (or value type) obj, onto the stack.")]
	public class LdFldInstruction : TreateInstruction<FieldReference> {
		public LdFldInstruction(FieldReference target, Instruction source) : base(TreatedCode.LdFld, target, source) {}
	}
}