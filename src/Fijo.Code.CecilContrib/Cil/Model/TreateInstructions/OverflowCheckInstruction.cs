using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Intefaces;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	public class OverflowCheckInstruction : SimpleTreateInstruction, IVirtualInstruction {
		public OverflowCheckInstruction(Instruction source) : base(TreatedCode.OverflowCheck, source) {}
	}
}