using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push the address stored in a typed reference.")]
	public class RefAnyValInstruction : TreateInstruction<TypeReference> {
		public RefAnyValInstruction(TypeReference target, Instruction source) : base(TreatedCode.RefAnyVal, target, source) {}
	}
}