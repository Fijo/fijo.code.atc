using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push the size, in bytes, of a type as an unsigned int32.")]
	public class SizeOfInstruction : TreateInstruction<TypeReference> {
		public SizeOfInstruction(TypeReference target, Instruction source) : base(TreatedCode.SizeOf, target, source) {}
	}
}