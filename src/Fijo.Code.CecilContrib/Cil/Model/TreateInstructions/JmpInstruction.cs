using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Exit current method and jump to the specified method.")]
	public class JmpInstruction : TreateInstruction<TypeDefinition> {
		public JmpInstruction(TypeDefinition target, Instruction source) : base(TreatedCode.Jmp, target, source) {}
	}
}