using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Fetch the address of argument argNum.")]
	public class LdArgaInstruction : TreateInstruction<ParameterDefinition> {
		public LdArgaInstruction(ParameterDefinition target, Instruction source) : base(TreatedCode.LdArga, target, source) {}
	}
}