using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Store value of spec type into memory at address")]
	public class StIndInstruction : TreateInstruction<TypeReference> {
		public StIndInstruction(TypeReference type, Instruction source) : base(TreatedCode.LdInd, type, source) {}
	}
}