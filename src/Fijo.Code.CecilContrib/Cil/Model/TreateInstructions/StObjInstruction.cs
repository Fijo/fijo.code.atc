using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Store a value of type typeTok at an address.")]
	public class StObjInstruction : TreateInstruction<TypeReference> {
		public StObjInstruction(TypeReference target, Instruction source) : base(TreatedCode.StObj, target, source) {}
	}
}