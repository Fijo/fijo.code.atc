using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push a pointer to a method referenced by method, on the stack.")]
	public class LdFtnInstruction : TreateInstruction<MethodReference> {
		public LdFtnInstruction(MethodReference target, Instruction source) : base(TreatedCode.LdFtn, target, source) {}
	}
}