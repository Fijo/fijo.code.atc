using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Rethrow the current exception.")]
	public class ThrowInstruction : TreateInstruction {
		public ThrowInstruction(Instruction source) : base(TreatedCode.Throw, source) {}
	}
}