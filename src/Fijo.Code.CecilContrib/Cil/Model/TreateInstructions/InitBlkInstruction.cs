using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Set all bytes in a block of memory to a given byte value.")]
	public class InitBlkInstruction : SimpleTreateInstruction {
		public InitBlkInstruction(Instruction source) : base(TreatedCode.InitBlk, source) {}
	}
}