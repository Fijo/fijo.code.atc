using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Cast obj to class.")]
	public class CastClassInstruction : TreateInstruction<TypeDefinition> {
		public CastClassInstruction(TypeDefinition target, Instruction source) : base(TreatedCode.CastClass, target, source) {}
	}
}