using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Copy a value type from src to dest.")]
	public class CpObjInstruction : TreateInstruction<TypeDefinition> {
		public CpObjInstruction(TypeDefinition target, Instruction source) : base(TreatedCode.CpObj, target, source) {}
	}
}