using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push the length (of type native unsigned int) of array on the stack.")]
	public class LdLenInstruction : SimpleTreateInstruction {
		public LdLenInstruction(Instruction source) : base(TreatedCode.LdLen, source) {}
	}
}