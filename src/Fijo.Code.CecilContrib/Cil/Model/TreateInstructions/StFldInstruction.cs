using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Replace the value of field of the object obj with value.")]
	public class StFldInstruction : TreateInstruction<FieldReference> {
		public StFldInstruction(FieldReference target, Instruction source) : base(TreatedCode.StFld, target, source) {}
	}
}