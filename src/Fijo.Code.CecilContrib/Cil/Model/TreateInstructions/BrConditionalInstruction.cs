using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Branch to target if condition is true.")]
	public class BrConditionalInstruction : TreateInstruction<Instruction> {
		public readonly Expression Expression;

		public BrConditionalInstruction(Expression expression, Instruction target, Instruction source) : base(TreatedCode.BrConditional, target, source) {
			Expression = expression;
		}
	}
}