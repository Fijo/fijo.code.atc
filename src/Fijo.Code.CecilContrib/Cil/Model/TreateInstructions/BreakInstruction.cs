using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Inform a debugger that a breakpoint has been reached.")]
	public class BreakInstruction : SimpleTreateInstruction {
		public BreakInstruction(Instruction source) : base(TreatedCode.Break, source) {}
	}
}