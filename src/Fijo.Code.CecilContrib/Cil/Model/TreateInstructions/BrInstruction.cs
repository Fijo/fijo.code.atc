using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Branch to target. Like �goto�.")]
	public class BrInstruction : TreateInstruction<Instruction> {
		public BrInstruction(Instruction target, Instruction source) : base(TreatedCode.Br, target, source) {}
	}
}