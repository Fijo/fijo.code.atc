using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Throw ArithmeticException if value is not a finite number.")]
	public class CkFiniteInstruction : SimpleTreateInstruction {
		public CkFiniteInstruction(Instruction source) : base(TreatedCode.CkFinite, source) {}
	}
}