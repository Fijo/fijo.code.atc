using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Subsequent call terminates current method.")]
	[RelatedLink("http://blogs.msdn.com/b/abhinaba/archive/2007/07/27/tail-recursion-on-net.aspx")]
	public class TailInstruction : SimpleTreateInstruction {
		public TailInstruction(Instruction source) : base(TreatedCode.Tail, source) {}
	}
}