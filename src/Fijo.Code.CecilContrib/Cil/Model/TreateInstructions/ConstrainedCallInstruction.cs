using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Call a virtual method on a type constrained to be type T")]
	[Note("This obj contains only information about the type contrain. You may find information about the Method to call on the stack.")]
	public class ConstrainedCallInstruction : TreateInstruction<TypeDefinition> {
		public ConstrainedCallInstruction(TypeDefinition target, Instruction source) : base(TreatedCode.ConstrainedCall, target, source) {}
	}
}