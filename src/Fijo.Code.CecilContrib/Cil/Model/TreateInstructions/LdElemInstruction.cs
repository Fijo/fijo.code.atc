using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Loads an element of the spec type at index (on the stack) onto the top of the stack as a spec type.")]
	[Note("Example" +
	      "ldloc.0 // load local variable (asserting that this is an int[100] with 5 at [0]" +
	      "ldc.i4.0 // query index 0" +
	      "ldelem.i4 // {AsType= int32} query the value from the indexer with the index 0 from the array and push the result to the stack." +
	      "call void [mscorlib]System.Console::WriteLine(int32) // should output 5.")]
	public class LdElemInstruction : TreateInstruction<TypeReference> {
		public readonly TypeReference AsType;

		public LdElemInstruction(TypeReference type, TypeReference asType, Instruction source) : base(TreatedCode.LdElem, type, source) {
			AsType = asType;
		}
	}
}