using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Pop value from the stack.")]
	public class PopInstruction : SimpleTreateInstruction {
		public PopInstruction(Instruction source) : base(TreatedCode.Pop, source) {}
	}
}