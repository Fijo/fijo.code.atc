using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Load argument numbered num onto the stack.")]
	public class LdArgInstruction : TreateInstruction<ParameterDefinition> {
		public LdArgInstruction(ParameterDefinition target, Instruction source) : base(TreatedCode.LdArg, target, source) {}
	}
}