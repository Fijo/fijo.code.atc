using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Condition Expresssion. If the condition returns true push 1 (of type int32) else push 0.")]
	public class ConditionInstruction : TreateInstruction {
		public readonly Expression Expression;

		public ConditionInstruction(Expression expression, Instruction source) : base(TreatedCode.Condition, source) {
			Expression = expression;
		}
	}
}