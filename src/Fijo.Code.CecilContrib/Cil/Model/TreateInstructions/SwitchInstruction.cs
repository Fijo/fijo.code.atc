using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Jump to one of n values.")]
	public class SwitchInstruction : TreateInstruction<IList<Instruction>> {
		public SwitchInstruction(IList<Instruction> target, Instruction source) : base(TreatedCode.Switch, target, source) {}
	}
}