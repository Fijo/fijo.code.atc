using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Store value to the argument numbered num.")]
	public class StArgInstruction : TreateInstruction<ParameterDefinition> {
		public StArgInstruction(ParameterDefinition target, Instruction source) : base(TreatedCode.StArg, target, source) {}
	}
}