using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Convert a boxable value to its boxed form.")]
	public class BoxInstruction : TreateInstruction<TypeReference> {
		public BoxInstruction(TypeReference target, Instruction source) : base(TreatedCode.Box, target, source) {}
	}
}