using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Push a constant to the stack.")]
	public class LdcInstruction : TreateInstruction<object> {
		public readonly TypeDefinition Type;

		public LdcInstruction(TypeDefinition type, object value, Instruction source) : base(TreatedCode.Ldc, value, source) {
			Type = type;
		}
	}
}