using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Initialize the value at address dest.")]
	public class InitObjInstruction : TreateInstruction<TypeDefinition> {
		public InitObjInstruction(TypeDefinition target, Instruction source) : base(TreatedCode.InitObj, target, source) {}
	}
}