using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Test if obj is an instance of class, returning null or an instance of that class or interface.")]
	[Note("Like the �as� keyword in C#.")]
	public class IsInstInstruction : TreateInstruction<TypeDefinition> {
		public IsInstInstruction(TypeDefinition target, Instruction source) : base(TreatedCode.IsInst, target, source) {}
	}
}