using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Allocate an uninitialized object or value type and call ctor.")]
	public class NewObjInstruction : TreateInstruction<MethodReference> {
		public NewObjInstruction(MethodReference target, Instruction source) : base(TreatedCode.NewObj, target, source) {}
	}
}