using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Duplicate the value on the top of the stack.")]
	public class DupInstruction : SimpleTreateInstruction {
		public DupInstruction(Instruction source) : base(TreatedCode.Dup, source) {}
	}
}