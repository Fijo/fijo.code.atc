using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Create a new array with elements of type etype.")]
	public class NewArrInstruction : TreateInstruction<TypeReference> {
		public NewArrInstruction(TypeReference target, Instruction source) : base(TreatedCode.NewArr, target, source) {}
	}
}