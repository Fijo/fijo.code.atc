﻿using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Intefaces {
	[Desc("Marks a something as virtual instruction what means, that it is not realy contained in the Common Intermediate Language.")]
	public interface IVirtualInstruction {}
}