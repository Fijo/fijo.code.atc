using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Convert to a form a type, pushing a result of another type on stack.")]
	public class ConvInstruction : TreateInstruction {
		public readonly TypeDefinition SourceType;
		public readonly bool SourceTypeIsUnsigned;
		public readonly TypeDefinition TargetType;
		public bool CheckOverflow;

		public ConvInstruction(Instruction source, TypeDefinition sourceType, bool sourceTypeIsUnsigned, TypeDefinition targetType, bool checkOverflow) : base(TreatedCode.Conv, source) {
			SourceType = sourceType;
			SourceTypeIsUnsigned = sourceTypeIsUnsigned;
			TargetType = targetType;
			CheckOverflow = checkOverflow;
		}
	}
}