using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Allocate space from the local memory pool.")]
	public class LocAllocInstruction : SimpleTreateInstruction {
		public LocAllocInstruction(Instruction source) : base(TreatedCode.LocAlloc, source) {}
	}
}