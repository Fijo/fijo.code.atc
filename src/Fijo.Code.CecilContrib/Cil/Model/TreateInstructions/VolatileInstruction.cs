using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Subsequent pointer reference is volatile.")]
	[RelatedLink("http://social.msdn.microsoft.com/Forums/en/csharpgeneral/thread/65f312ba-c7f1-49c8-b4b5-e1e0f3da64b1")]
	public class VolatileInstruction : SimpleTreateInstruction {
		public VolatileInstruction(Instruction source) : base(TreatedCode.Volatile, source) {}
	}
}