using System.Diagnostics;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Subsequent pointer instruction might be unaligned.")]
	[RelatedLink("http://books.google.de/books?id=oAcCRKd6EZgC&pg=PA279&lpg=PA279&dq=unaligned+instruction+il&source=bl&ots=KAVaaGrSat&sig=-UhuzfTFDl2_xeVsCIaM_MqqI_c&hl=de&sa=X&ei=cBIpUeXJOsiGswbw84Bo&ved=0CDMQ6AEwAA#v=onepage&q=Unaligned&f=false")]
	[RelatedLink("http://en.wikipedia.org/wiki/Data_structure_alignment")]
	public class UnalignedInstruction : TreateInstruction<byte> {
		public UnalignedInstruction(byte alignment, Instruction source) : base(TreatedCode.Unaligned, alignment, source) {
			Debug.Assert(alignment.IsIn<byte>(1, 2, 4), "Only 1, 2 and 4 are valid alignments for the Unaligned Instruction.");
		}
	}
}