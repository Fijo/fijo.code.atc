using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("Load address of local variable with index indx.")]
	public class LdLocaInstruction : TreateInstruction<VariableDefinition> {
		public LdLocaInstruction(VariableDefinition target, Instruction source) : base(TreatedCode.LdLoca, target, source) {}
	}
}