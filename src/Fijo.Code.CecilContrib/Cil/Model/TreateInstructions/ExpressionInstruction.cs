using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.TreateInstructions {
	[Desc("An expression with one or two values as input the do an operation returns the result.")]
	public class ExpressionInstruction : TreateInstruction {
		public readonly Expression Exprssion;

		public ExpressionInstruction(Expression exprssion, Instruction source) : base(TreatedCode.Expression, source) {
			Exprssion = exprssion;
		}
	}
}