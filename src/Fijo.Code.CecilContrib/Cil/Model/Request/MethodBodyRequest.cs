using Mono.Cecil;
using MethodBody = Mono.Cecil.Cil.MethodBody;

namespace Fijo.Code.CecilContrib.Cil.Model.Request {
	public class MethodBodyRequest<T> {
		public readonly MethodBody Body;
		public readonly ModuleDefinition Module;
		public readonly T Source;

		public MethodBodyRequest(MethodBody body, ModuleDefinition module, T source) {
			Body = body;
			Module = module;
			Source = source;
		}
	}
}