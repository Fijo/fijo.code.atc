using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Model.Request {
	public class InstructionRequest : MethodBodyRequest<Instruction> {
		public InstructionRequest(MethodBody body, ModuleDefinition module, Instruction source) : base(body, module, source) {}
	}
}