﻿using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;

namespace Fijo.Code.CecilContrib.Cil.Model.Expr {
	public class Expression {
		public readonly OperatorCode Code;
		public readonly object RightValue;
		public readonly CoreTypes RightValueType;

		public bool IsUnsigned { get { return Code.HasFlag(OperatorCode.Unsigned); } }
		public bool HasOverflowCheck { get { return Code.HasFlag(OperatorCode.OverflowCheck); } }
		public bool IsNot { get { return Code.HasFlag(OperatorCode.Not); } }
		public bool IsEqual { get { return Code.HasFlag(OperatorCode.Equal); } }
		public bool IsGreater { get { return Code.HasFlag(OperatorCode.Greater); } }
		public bool IsLess { get { return Code.HasFlag(OperatorCode.Less); } }
		public bool IsAdd { get { return Code.HasFlag(OperatorCode.Add); } }
		public bool IsSub { get { return Code.HasFlag(OperatorCode.Sub); } }
		public bool IsMod { get { return Code.HasFlag(OperatorCode.Mod); } }
		public bool IsMultiply { get { return Code.HasFlag(OperatorCode.Multiply); } }
		public bool IsDivide { get { return Code.HasFlag(OperatorCode.Divide); } }
		public bool IsBitwiseNot { get { return Code.HasFlag(OperatorCode.BitwiseNot); } }
		public bool IsBitwiseAnd { get { return Code.HasFlag(OperatorCode.BitwiseAnd); } }
		public bool IsBitwiseOr { get { return Code.HasFlag(OperatorCode.BitwiseOr); } }
		public bool IsBitwiseXOr { get { return Code.HasFlag(OperatorCode.BitwiseXOr); } }
		public bool IsBitwiseShiftLeft { get { return Code.HasFlag(OperatorCode.BitwiseShiftLeft); } }
		public bool IsBitwiseShiftRight { get { return Code.HasFlag(OperatorCode.BitwiseShiftRight); } }
		public bool IsNegate { get { return Code.HasFlag(OperatorCode.Negate); } }
		public bool HasRightValue { get { return Code.HasFlag(OperatorCode.HasRightValue); } }
		public bool HasConstantRightValue { get { return RightValue != null; } }

		public Expression(OperatorCode code, object rightValue = null, CoreTypes rightValueType = default(CoreTypes)) {
			Code = code;
			RightValue = rightValue;
			RightValueType = rightValueType;
		}

		#region Equality
		protected bool Equals(Expression other) {
			return Code == other.Code && Equals(RightValue, other.RightValue) && RightValueType == other.RightValueType;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<Expression>(obj, Equals);
		}

		public override int GetHashCode() {
			return Code.GetHashCode().GetHashCodeAlgorithm(RightValue.NullableGetHashCode(), RightValueType.GetHashCode());
		}
		#endregion
	}
}