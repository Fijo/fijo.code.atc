﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Cil.Services.Interfaces;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Services {
	public class InstructionTreadService : IInstructionTreadService {
		private readonly ICGAllHandlingProcessor<InstructionRequest, IEnumerable<TreateInstruction>> _instructionTreadProcessor;

		public InstructionTreadService(ICGAllHandlingProcessor<InstructionRequest, IEnumerable<TreateInstruction>> instructionTreadProcessor) {
			_instructionTreadProcessor = instructionTreadProcessor;
		}

		#region Implementation of IInstructionTreadService
		public IEnumerable<TreateInstruction> Get(MethodBody methodBody, ModuleDefinition module, Instruction instruction) {
			var treateInstructions = _instructionTreadProcessor.Get(GetInstructionRequest(methodBody, module, instruction)).Flat();
// ReSharper disable PossibleMultipleEnumeration
			#if DEBUG
			treateInstructions = treateInstructions.Execute();
			Debug.Assert(treateInstructions.Any());
			#endif
			return treateInstructions;
// ReSharper restore PossibleMultipleEnumeration
		}

		private InstructionRequest GetInstructionRequest(MethodBody methodBody, ModuleDefinition module, Instruction instruction) {
			return new InstructionRequest(methodBody, module, instruction);
		}
		#endregion
	}
}