﻿using System.Collections.Generic;
using Fijo.Code.CecilContrib.Dto;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Services.Interfaces {
	public interface IBodyMetaCodeService {
		IEnumerable<HandledExceptionBlock> GetHandledExceptionBlocks(MethodBody methodBody);
	}
}