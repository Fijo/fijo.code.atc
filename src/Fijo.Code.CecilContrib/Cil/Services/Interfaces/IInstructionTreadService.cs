﻿using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Services.Interfaces {
	public interface IInstructionTreadService {
		IEnumerable<TreateInstruction> Get(MethodBody methodBody, ModuleDefinition module, Instruction instruction);
	}
}