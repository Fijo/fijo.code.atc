﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.CecilContrib.Cil.Services.Interfaces;
using Fijo.Code.CecilContrib.Dto;
using Mono.Cecil.Cil;
using ExceptionHandler = Mono.Cecil.Cil.ExceptionHandler;

namespace Fijo.Code.CecilContrib.Cil.Services {
	public class BodyMetaCodeService : IBodyMetaCodeService {
		 public IEnumerable<HandledExceptionBlock> GetHandledExceptionBlocks(MethodBody methodBody) {
			 var exceptionHandlers = methodBody.ExceptionHandlers;
			 return exceptionHandlers.GroupBy(x => x.TryStart).Select(x => GetHandledExceptionBlock(x.ToArray()));
		 }

		private HandledExceptionBlock GetHandledExceptionBlock(ICollection<ExceptionHandler> handlerGroup) {
			var exceptionHandler = handlerGroup.First();
			Debug.Assert(handlerGroup.All(x => x.TryEnd == exceptionHandler.TryEnd));
			return new HandledExceptionBlock(GetExceptionHandlers(handlerGroup).ToArray(),
			                                 exceptionHandler.TryStart, exceptionHandler.TryEnd);
		}

		private IEnumerable<Dto.ExceptionHandler> GetExceptionHandlers(IEnumerable<ExceptionHandler> handlerGroup) {
			return handlerGroup.Select(x => new Dto.ExceptionHandler(x.CatchType, x.FilterStart, x.HandlerStart, x.HandlerEnd, x.HandlerType));
		}
	}
}