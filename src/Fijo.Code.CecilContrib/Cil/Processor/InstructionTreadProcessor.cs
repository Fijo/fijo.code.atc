﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;

namespace Fijo.Code.CecilContrib.Cil.Services {
	public class InstructionTreadProcessor : CGAllHandlingProcessor<InstructionRequest, IEnumerable<TreateInstruction>> {
		public InstructionTreadProcessor(IParallelizableHandlingProvider<ICGAllHandling<InstructionRequest, IEnumerable<TreateInstruction>>> handlingRepository) : base(handlingRepository) {}

		protected override void Validation() {
			base.Validation();
			#if DEBUG
			var primaryInstructionTreats = Handlings.Cast<InstructionTreat>().Where(x => !(x is IAdditionalInstructionTreat));
			var handledCodes = primaryInstructionTreats.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism).SelectMany(x => x.GetUsedForCodes()).Execute();
			var codes = Enum.GetValues(typeof (Mono.Cecil.Cil.Code)).Cast<Mono.Cecil.Cil.Code>();
			var unhandledCodes = codes.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism).Where(x => !handledCodes.Contains(x)).Execute();
			Debug.Assert(unhandledCodes.None(), string.Format("Not all ILCodes are handled in all your primary InstructionTreat(s). Unhandled codes: {0}", string.Join(", ", unhandledCodes)));
			Debug.Assert(handledCodes.IsUnique(), "There are ILCodes that are handled more than one time by primary InstructionTreat(s)");
			#endif
		}
	}
}