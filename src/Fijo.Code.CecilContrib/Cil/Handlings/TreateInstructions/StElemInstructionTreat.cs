using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class StElemInstructionTreat : TypeInstructionTreat<TypeReference> {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.Stelem_Any;
			yield return Mono.Cecil.Cil.Code.Stelem_I;
			yield return Mono.Cecil.Cil.Code.Stelem_I1;
			yield return Mono.Cecil.Cil.Code.Stelem_I2;
			yield return Mono.Cecil.Cil.Code.Stelem_I4;
			yield return Mono.Cecil.Cil.Code.Stelem_I8;
			yield return Mono.Cecil.Cil.Code.Stelem_R4;
			yield return Mono.Cecil.Cil.Code.Stelem_R8;
			yield return Mono.Cecil.Cil.Code.Stelem_Ref;
		}
		#endregion

		#region Overrides of TypeInstructionTreat<TypeReference>
		protected override IDictionary<Mono.Cecil.Cil.Code, TypeReference> CreateLoadingTypes(TypeSystem typeSystem) {
			return new Dictionary<Mono.Cecil.Cil.Code, TypeReference>
			{
				{Mono.Cecil.Cil.Code.Stelem_I, typeSystem.Int32},
				{Mono.Cecil.Cil.Code.Stelem_I1, typeSystem.SByte},
				{Mono.Cecil.Cil.Code.Stelem_I2, typeSystem.Int16},
				{Mono.Cecil.Cil.Code.Stelem_I4, typeSystem.Int32},
				{Mono.Cecil.Cil.Code.Stelem_I8, typeSystem.Int64},
				{Mono.Cecil.Cil.Code.Stelem_R4, typeSystem.Single},
				{Mono.Cecil.Cil.Code.Stelem_R8, typeSystem.Double},
				{Mono.Cecil.Cil.Code.Stelem_Ref, typeSystem.Object},
			};
		}

		protected override TreateInstruction GetInstruction(TypeReference loadingType, Instruction instruction) {
			return new StElemInstruction(loadingType, instruction);
		}

		protected override TypeReference GetLoadingTypeToReturn(Mono.Cecil.Cil.Code code, Instruction instruction, InstructionRequest request) {
			switch (code) {
				case Mono.Cecil.Cil.Code.Stelem_Any:
					return (TypeReference) instruction.Operand;
				default:
					return base.GetLoadingTypeToReturn(code, instruction, request);
			}
		}
		#endregion
	}
}