using System;
using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;
using Mono.Cecil.Cil;
using ILCode = Mono.Cecil.Cil.Code;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class LdcInstructionTreat : TypeInstructionTreat<LdcInstructionTreat.ConstVal> {
		[OnlyPublicFor("be used as an generic parameter of baseType TypeInstructionTreat<>")]
		public class ConstVal {
			public readonly TypeDefinition AsType;
			public readonly object Value;

			public ConstVal(TypeDefinition asType, object value = null) {
				AsType = asType;
				Value = value;
			}
		}

		protected override IDictionary<ILCode, ConstVal> CreateLoadingTypes(TypeSystem typeSystem) {
			return new Dictionary<ILCode, ConstVal>
			{
				{ILCode.Ldc_I4, CreateConstVal(typeSystem.Int32)},
				{ILCode.Ldc_I4_0, CreateConstVal(typeSystem.Int32, 0)},
				{ILCode.Ldc_I4_1, CreateConstVal(typeSystem.Int32, 1)},
				{ILCode.Ldc_I4_2, CreateConstVal(typeSystem.Int32, 2)},
				{ILCode.Ldc_I4_3, CreateConstVal(typeSystem.Int32, 3)},
				{ILCode.Ldc_I4_4, CreateConstVal(typeSystem.Int32, 4)},
				{ILCode.Ldc_I4_5, CreateConstVal(typeSystem.Int32, 5)},
				{ILCode.Ldc_I4_6, CreateConstVal(typeSystem.Int32, 6)},
				{ILCode.Ldc_I4_7, CreateConstVal(typeSystem.Int32, 7)},
				{ILCode.Ldc_I4_8, CreateConstVal(typeSystem.Int32, 8)},
				{ILCode.Ldc_I4_M1, CreateConstVal(typeSystem.Int32, -1)},
				{ILCode.Ldc_I4_S, CreateConstVal(typeSystem.Int32)},
				{ILCode.Ldc_I8, CreateConstVal(typeSystem.Int64)},
				{ILCode.Ldc_R4, CreateConstVal(typeSystem.Single)},
				{ILCode.Ldc_R8, CreateConstVal(typeSystem.Double)},
				{ILCode.Ldnull, CreateConstVal(typeSystem.Object)},
				{ILCode.Ldstr, CreateConstVal(typeSystem.String)}
			};
		}

		protected ConstVal CreateConstVal(TypeReference asType, object value = null) {
			return new ConstVal(asType.Resolve(), value);
		}

		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			return new[]
			{
				Mono.Cecil.Cil.Code.Ldc_I4,
				Mono.Cecil.Cil.Code.Ldc_I4_0,
				Mono.Cecil.Cil.Code.Ldc_I4_1,
				Mono.Cecil.Cil.Code.Ldc_I4_2,
				Mono.Cecil.Cil.Code.Ldc_I4_3,
				Mono.Cecil.Cil.Code.Ldc_I4_4,
				Mono.Cecil.Cil.Code.Ldc_I4_5,
				Mono.Cecil.Cil.Code.Ldc_I4_6,
				Mono.Cecil.Cil.Code.Ldc_I4_7,
				Mono.Cecil.Cil.Code.Ldc_I4_8,
				Mono.Cecil.Cil.Code.Ldc_I4_M1,
				Mono.Cecil.Cil.Code.Ldc_I4_S,
				Mono.Cecil.Cil.Code.Ldc_I8,
				Mono.Cecil.Cil.Code.Ldc_R4,
				Mono.Cecil.Cil.Code.Ldc_R8,
				Mono.Cecil.Cil.Code.Ldnull,
				Mono.Cecil.Cil.Code.Ldstr,
			};
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			var code = instruction.OpCode.Code;
			var constVal = GetLoadingType(code, instruction, request);
			var usedValue = GetUsedConstVal(code, instruction, constVal);
			yield return new LdcInstruction(constVal.AsType, usedValue, instruction);
		}
		#endregion

		protected override TreateInstruction GetInstruction(ConstVal loadingType, Instruction instruction) {
			throw new NotSupportedException();
		}

		private object GetUsedConstVal(ILCode code, Instruction instruction, ConstVal constVal) {
			switch (code) {
				case ILCode.Ldc_I4:
				case ILCode.Ldc_I4_S:
				case ILCode.Ldc_I8:
				case ILCode.Ldc_R4:
				case ILCode.Ldc_R8:
				case ILCode.Ldstr:
					return instruction.Operand;
				default:
					return constVal.Value;
			}
		}
	}
}