using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class StObjInstructionTreat : SingleInstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<TreateInstruction> Map(Instruction instruction) {
			yield return new StObjInstruction((TypeReference) instruction.Operand, instruction);
		}

		protected override Mono.Cecil.Cil.Code UsedForCode() {
			return Mono.Cecil.Cil.Code.Stobj;
		}
		#endregion
	}
}