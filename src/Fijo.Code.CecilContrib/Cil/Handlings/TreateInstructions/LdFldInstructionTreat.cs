using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class LdFldInstructionTreat : InstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.Ldfld;
			yield return Mono.Cecil.Cil.Code.Ldsfld;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			yield return new LdFldInstruction((FieldReference) instruction.Operand, instruction);
		}
		#endregion
	}
}