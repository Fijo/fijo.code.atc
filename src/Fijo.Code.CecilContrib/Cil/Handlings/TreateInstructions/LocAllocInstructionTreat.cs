using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class LocAllocInstructionTreat : SimpleInstructionTreat {
		#region Overrides of SingleInstructionTreat
		protected override TreateInstruction MapSingle(Instruction instruction) {
			return new LocAllocInstruction(instruction);
		}

		protected override Mono.Cecil.Cil.Code UsedForCode() {
			return Mono.Cecil.Cil.Code.Localloc;
		}
		#endregion
	}
}