using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Extentions.TypeReference;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using Mono.Cecil;
using Mono.Cecil.Cil;
using ILCode = Mono.Cecil.Cil.Code;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class ConvInstructionTreat : TypeInstructionTreat<ConvInstructionTreat.ConverterDef> {
		[OnlyPublicFor("be used as an generic parameter of baseType TypeInstructionTreat<>")]
		public class ConverterDef {
			public readonly TypeDefinition SourceType;
			public readonly bool SourceTypeIsUnsigned;
			public readonly TypeDefinition TargetType;
			public readonly TypeDefinition ResultAsType;
			public readonly bool CheckOverflow;

			public ConverterDef(TypeDefinition sourceType, bool sourceTypeIsUnsigned, TypeDefinition targetType, bool checkOverflow, TypeDefinition resultAsType = null) {
				SourceTypeIsUnsigned = sourceTypeIsUnsigned;
				TargetType = targetType;
				ResultAsType = resultAsType ?? targetType;
				CheckOverflow = checkOverflow;
				SourceType = sourceType;
			}
		}

		protected override IDictionary<ILCode, ConverterDef> CreateLoadingTypes(TypeSystem typeSystem) {
			return new Dictionary<ILCode, ConverterDef>
			{
				{ILCode.Conv_I, CreateConverter(null, false, typeSystem.Int32, false)},
				{ILCode.Conv_I1, CreateConverter(null, false, typeSystem.SByte, false, typeSystem.Int32)},
				{ILCode.Conv_I2, CreateConverter(null, false, typeSystem.Int16, false, typeSystem.Int32)},
				{ILCode.Conv_I4, CreateConverter(null, false, typeSystem.Int32, false)},
				{ILCode.Conv_I8, CreateConverter(null, false, typeSystem.Int64, false)},
				{ILCode.Conv_Ovf_I, CreateConverter(null, false, typeSystem.Int32, true)},
				{ILCode.Conv_Ovf_I_Un, CreateConverter(null, true, typeSystem.Int32, true)},
				{ILCode.Conv_Ovf_I1, CreateConverter(null, false, typeSystem.SByte, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_I1_Un, CreateConverter(null, true, typeSystem.SByte, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_I2, CreateConverter(null, false, typeSystem.Int16, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_I2_Un, CreateConverter(null, true, typeSystem.Int16, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_I4, CreateConverter(null, false, typeSystem.Int32, true)},
				{ILCode.Conv_Ovf_I4_Un, CreateConverter(null, false, typeSystem.Int32, true)},
				{ILCode.Conv_Ovf_I8, CreateConverter(null, false, typeSystem.Int64, true)},
				{ILCode.Conv_Ovf_I8_Un, CreateConverter(null, false, typeSystem.Int64, true)},
				{ILCode.Conv_Ovf_U, CreateConverter(null, false, typeSystem.UInt32, true)},
				{ILCode.Conv_Ovf_U_Un, CreateConverter(null, true, typeSystem.UInt32, true)},
				{ILCode.Conv_Ovf_U1, CreateConverter(null, false, typeSystem.Byte, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_U1_Un, CreateConverter(null, true, typeSystem.Byte, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_U2, CreateConverter(null, false, typeSystem.UInt16, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_U2_Un, CreateConverter(null, true, typeSystem.UInt16, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_U4, CreateConverter(null, false, typeSystem.UInt32, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_U4_Un, CreateConverter(null, false, typeSystem.UInt32, true, typeSystem.Int32)},
				{ILCode.Conv_Ovf_U8, CreateConverter(null, false, typeSystem.UInt64, true, typeSystem.Int64)},
				{ILCode.Conv_Ovf_U8_Un, CreateConverter(null, false, typeSystem.UInt64, true, typeSystem.Int64)},
				{ILCode.Conv_R_Un, CreateConverter(typeSystem.UInt32, true, typeSystem.Single, false)},
				{ILCode.Conv_R4, CreateConverter(null, false, typeSystem.Single, false)},
				{ILCode.Conv_R8, CreateConverter(null, false, typeSystem.Double, false)},
				{ILCode.Conv_U, CreateConverter(null, false, typeSystem.UInt32, false, typeSystem.Int32)},
				{ILCode.Conv_U1, CreateConverter(null, false, typeSystem.Byte, false, typeSystem.Int32)},
				{ILCode.Conv_U2, CreateConverter(null, false, typeSystem.UInt16, false, typeSystem.Int32)},
				{ILCode.Conv_U4, CreateConverter(null, false, typeSystem.UInt32, false, typeSystem.Int32)},
				{ILCode.Conv_U8, CreateConverter(null, false, typeSystem.UInt64, false, typeSystem.Int64)},
			};
		}

		protected ConverterDef CreateConverter(TypeReference sourceType, bool sourceTypeIsUnsigned, TypeReference targetType, bool checkOverflow, TypeReference resultAsType = null) {
			return new ConverterDef(sourceType.WhenNotNull(x => x.Resolve()), sourceTypeIsUnsigned,
			                        targetType.WhenNotNull(x => x.Resolve()), checkOverflow,
			                        resultAsType.WhenNotNull(x => x.Resolve()));
		}

		#region Overrides of InstructionTreat
		protected override IEnumerable<ILCode> UsedForCodes() {
			yield return ILCode.Conv_I;
			yield return ILCode.Conv_I1;
			yield return ILCode.Conv_I2;
			yield return ILCode.Conv_I4;
			yield return ILCode.Conv_I8;
			yield return ILCode.Conv_Ovf_I;
			yield return ILCode.Conv_Ovf_I_Un;
			yield return ILCode.Conv_Ovf_I1;
			yield return ILCode.Conv_Ovf_I1_Un;
			yield return ILCode.Conv_Ovf_I2;
			yield return ILCode.Conv_Ovf_I2_Un;
			yield return ILCode.Conv_Ovf_I4;
			yield return ILCode.Conv_Ovf_I4_Un;
			yield return ILCode.Conv_Ovf_I8;
			yield return ILCode.Conv_Ovf_I8_Un;
			yield return ILCode.Conv_Ovf_U;
			yield return ILCode.Conv_Ovf_U_Un;
			yield return ILCode.Conv_Ovf_U1;
			yield return ILCode.Conv_Ovf_U1_Un;
			yield return ILCode.Conv_Ovf_U2;
			yield return ILCode.Conv_Ovf_U2_Un;
			yield return ILCode.Conv_Ovf_U4;
			yield return ILCode.Conv_Ovf_U4_Un;
			yield return ILCode.Conv_Ovf_U8;
			yield return ILCode.Conv_Ovf_U8_Un;
			yield return ILCode.Conv_R_Un;
			yield return ILCode.Conv_R4;
			yield return ILCode.Conv_R8;
			yield return ILCode.Conv_U;
			yield return ILCode.Conv_U1;
			yield return ILCode.Conv_U2;
			yield return ILCode.Conv_U4;
			yield return ILCode.Conv_U8;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			var converter = GetLoadingTypeToReturn(instruction.OpCode.Code, instruction, request);
			
			var targetType = converter.TargetType;
			yield return new ConvInstruction(instruction, converter.SourceType, converter.SourceTypeIsUnsigned, targetType, converter.CheckOverflow);
			var resultAsType = converter.ResultAsType;
			if (resultAsType.Equals(targetType)) yield break;
			if (resultAsType.IsAssignableFrom(targetType)) yield return new BoxInstruction(resultAsType, instruction);
			else {
				Debug.Assert(resultAsType.IsValueType);
				yield return new UnboxInstruction(resultAsType, instruction);
			}
		}
		#endregion

		protected override TreateInstruction GetInstruction(ConverterDef loadingType, Instruction instruction) {
			throw new NotSupportedException();
		}
	}
}