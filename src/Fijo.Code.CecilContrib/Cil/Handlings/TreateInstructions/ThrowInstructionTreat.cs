using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class ThrowInstructionTreat : InstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.Rethrow;
			yield return Mono.Cecil.Cil.Code.Throw;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			yield return new ThrowInstruction(request.Source);
		}
		#endregion
	}
}