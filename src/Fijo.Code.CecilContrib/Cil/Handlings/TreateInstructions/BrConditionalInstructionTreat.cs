using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Factory.Interfaces;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil.Cil;
using ILCode = Mono.Cecil.Cil.Code;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class BrConditionalInstructionTreat : InstructionTreat {
		private readonly IDictionary<ILCode, Expression> _conditions;

		public BrConditionalInstructionTreat(IExpressions expressions) {
			_conditions = new Dictionary<ILCode, Expression>
			{
				{ILCode.Beq, expressions.Equal},
				{ILCode.Beq_S, expressions.Equal},
				{ILCode.Bge, expressions.GreaterOrEqual},
				{ILCode.Bge_S, expressions.GreaterOrEqual},
				{ILCode.Bge_Un, expressions.GreaterOrEqualUnsigned},
				{ILCode.Bge_Un_S, expressions.GreaterOrEqualUnsigned},
				{ILCode.Bgt, expressions.Greater},
				{ILCode.Bgt_S, expressions.Greater},
				{ILCode.Bgt_Un, expressions.GreaterUnsigned},
				{ILCode.Bgt_Un_S, expressions.GreaterUnsigned},
				{ILCode.Ble, expressions.LessOrEqual},
				{ILCode.Ble_S, expressions.LessOrEqual},
				{ILCode.Ble_Un, expressions.LessOrEqualUnsigned},
				{ILCode.Ble_Un_S, expressions.LessOrEqualUnsigned},
				{ILCode.Blt, expressions.Less},
				{ILCode.Blt_S, expressions.Less},
				{ILCode.Blt_Un, expressions.LessUnsigned},
				{ILCode.Blt_Un_S, expressions.LessUnsigned},
				{ILCode.Bne_Un, expressions.NotEqual},
				{ILCode.Bne_Un_S, expressions.NotEqual},
				{ILCode.Brfalse, expressions.IsFalse},
				{ILCode.Brfalse_S, expressions.IsFalse},
				{ILCode.Brtrue, expressions.IsTrue},
				{ILCode.Brtrue_S, expressions.IsTrue},
			};
		}

		#region Overrides of InstructionTreat
		protected override IEnumerable<ILCode> UsedForCodes() {
			yield return ILCode.Beq;
			yield return ILCode.Beq_S;
			yield return ILCode.Bge;
			yield return ILCode.Bge_S;
			yield return ILCode.Bge_Un;
			yield return ILCode.Bge_Un_S;
			yield return ILCode.Bgt;
			yield return ILCode.Bgt_S;
			yield return ILCode.Bgt_Un;
			yield return ILCode.Bgt_Un_S;
			yield return ILCode.Ble;
			yield return ILCode.Ble_S;
			yield return ILCode.Ble_Un;
			yield return ILCode.Ble_Un_S;
			yield return ILCode.Blt;
			yield return ILCode.Blt_S;
			yield return ILCode.Blt_Un;
			yield return ILCode.Blt_Un_S;
			yield return ILCode.Bne_Un;
			yield return ILCode.Bne_Un_S;
			yield return ILCode.Brfalse;
			yield return ILCode.Brfalse_S;
			yield return ILCode.Brtrue;
			yield return ILCode.Brtrue_S;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			yield return new BrConditionalInstruction(GetCondition(instruction), (Instruction) instruction.Operand, instruction);
		}

		private Expression GetCondition(Instruction instruction) {
			return _conditions[instruction.OpCode.Code];
		}
		#endregion
	}
}