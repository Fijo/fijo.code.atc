using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class StArgInstructionTreat : InstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.Starg;
			yield return Mono.Cecil.Cil.Code.Starg_S;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			yield return new StArgInstruction((ParameterDefinition) request.Source.Operand, request.Source);
		}
		#endregion
	}
}