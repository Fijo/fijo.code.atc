using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class LdArgaInstructionTreat : InstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.Ldarga;
			yield return Mono.Cecil.Cil.Code.Ldarga_S;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			yield return new LdArgaInstruction((ParameterDefinition) instruction.Operand, instruction);
		}
		#endregion
	}
}