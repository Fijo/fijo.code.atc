using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class StFldInstructionTreat : InstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.Stfld;
			yield return Mono.Cecil.Cil.Code.Stsfld;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			yield return new StFldInstruction((FieldReference) instruction.Operand, instruction);
		}
		#endregion
	}
}