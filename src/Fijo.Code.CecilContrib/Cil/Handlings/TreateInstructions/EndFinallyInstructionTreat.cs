using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class EndFinallyInstructionTreat : SimpleInstructionTreat {
		#region Overrides of SingleInstructionTreat
		protected override TreateInstruction MapSingle(Instruction instruction) {
			return new EndFinallyInstruction(instruction);
		}

		protected override Mono.Cecil.Cil.Code UsedForCode() {
			return Mono.Cecil.Cil.Code.Endfinally;
		}
		#endregion
	}
}