using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public abstract class TypeInstructionTreat<T> : InstructionTreat {
		protected readonly IDictionary<ModuleDefinition, IDictionary<Mono.Cecil.Cil.Code, T>> LoadingTypeByModule = new Dictionary<ModuleDefinition, IDictionary<Mono.Cecil.Cil.Code, T>>();

		protected abstract IDictionary<Mono.Cecil.Cil.Code, T> CreateLoadingTypes(TypeSystem module);

		protected IDictionary<Mono.Cecil.Cil.Code, T> GetLoadingTypes(ModuleDefinition module) {
			// ToDo FixMe delete the entries after a while or if a module is disposed delete entry with that module ... currently this will never release the memory again
			return LoadingTypeByModule.GetOrCreate(module, () => CreateLoadingTypes(module.TypeSystem));
		}

		#region Overrides of InstructionTreat
		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			var code = instruction.OpCode.Code;
			var loadingType = GetLoadingTypeToReturn(code, instruction, request);
			yield return GetInstruction(loadingType, instruction);
		}

		protected abstract TreateInstruction GetInstruction(T loadingType, Instruction instruction);

		[About("GetLoadingTypeNoOperand")]
		protected virtual T GetLoadingTypeToReturn(Mono.Cecil.Cil.Code code, Instruction instruction, InstructionRequest request) {
			Debug.Assert(instruction.Operand == null, string.Format("Asserted that there is no operand to handle, but there is one. At the deveoper of �{0}�: You should handle him.", GetType().Name));
			return GetLoadingType(code, instruction, request);
		}
		
		protected virtual T GetLoadingType(Mono.Cecil.Cil.Code code, Instruction instruction, InstructionRequest request) {
			return GetLoadingTypes(request.Module)[code];
		}
		#endregion
	}
}