using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class LdIndInstructionTreat : LoadTypeInstructionTreat {
		protected override IDictionary<Mono.Cecil.Cil.Code, LoadingType> CreateLoadingTypes(TypeSystem typeSystem) {
			return new Dictionary<Mono.Cecil.Cil.Code, LoadingType>
			{
				{Mono.Cecil.Cil.Code.Ldind_I, CreateLoadingType(typeSystem.Int32)},
				{Mono.Cecil.Cil.Code.Ldind_I1, CreateLoadingType(typeSystem.SByte, typeSystem.Int32)},
				{Mono.Cecil.Cil.Code.Ldind_I2, CreateLoadingType(typeSystem.Int16, typeSystem.Int32)},
				{Mono.Cecil.Cil.Code.Ldind_I4, CreateLoadingType(typeSystem.Int32)},
				{Mono.Cecil.Cil.Code.Ldind_I8, CreateLoadingType(typeSystem.Int64)},
				{Mono.Cecil.Cil.Code.Ldind_R4, CreateLoadingType(typeSystem.Single)},
				{Mono.Cecil.Cil.Code.Ldind_R8, CreateLoadingType(typeSystem.Double)},
				{Mono.Cecil.Cil.Code.Ldind_U1, CreateLoadingType(typeSystem.Byte, typeSystem.Int32)},
				{Mono.Cecil.Cil.Code.Ldind_U2, CreateLoadingType(typeSystem.UInt16, typeSystem.Int32)},
				{Mono.Cecil.Cil.Code.Ldind_U4, CreateLoadingType(typeSystem.UInt32, typeSystem.Int32)},
				{Mono.Cecil.Cil.Code.Ldind_Ref, CreateLoadingType(typeSystem.Object)},
			};
		}

		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			return new[]
			{
				Mono.Cecil.Cil.Code.Ldind_I,
				Mono.Cecil.Cil.Code.Ldind_I1,
				Mono.Cecil.Cil.Code.Ldind_I2,
				Mono.Cecil.Cil.Code.Ldind_I4,
				Mono.Cecil.Cil.Code.Ldind_I8,
				Mono.Cecil.Cil.Code.Ldind_R4,
				Mono.Cecil.Cil.Code.Ldind_R8,
				Mono.Cecil.Cil.Code.Ldind_Ref,
				Mono.Cecil.Cil.Code.Ldind_U1,
				Mono.Cecil.Cil.Code.Ldind_U2,
				Mono.Cecil.Cil.Code.Ldind_U4,
			};
		}

		protected override TreateInstruction GetInstruction(LoadingType loadingType, Instruction instruction) {
			return new LdIndInstruction(loadingType.Type, loadingType.Type, instruction);
		}
		#endregion
	}
}