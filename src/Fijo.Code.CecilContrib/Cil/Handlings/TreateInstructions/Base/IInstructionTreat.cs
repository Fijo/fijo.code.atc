using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public interface IInstructionTreat : IMapping<InstructionRequest, IEnumerable<TreateInstruction>>, ICGAllHandling<InstructionRequest, IEnumerable<TreateInstruction>> {}
}