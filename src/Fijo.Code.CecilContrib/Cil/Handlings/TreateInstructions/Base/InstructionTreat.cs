using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public abstract class InstructionTreat : IInstructionTreat {
		private readonly HashSet<Mono.Cecil.Cil.Code> _usedForCodes;

		protected InstructionTreat() {
			var usedForCodes = UsedForCodes();
// ReSharper disable PossibleMultipleEnumeration
			#if DEBUG
			usedForCodes = usedForCodes.Execute();
			Debug.Assert(usedForCodes.IsUnique(), string.Format("Your InstructionTreat �{0}� seems to handle the same ILCode more than one time.", GetType()));
			#endif
			_usedForCodes = new HashSet<Mono.Cecil.Cil.Code>(usedForCodes);
// ReSharper restore PossibleMultipleEnumeration
		}

		#region Implementation of ISupportsParallelization
		public IEnumerable<TreateInstruction> Get(InstructionRequest source) {
			return Map(source);
		}

		public virtual bool IsUsed(InstructionRequest source) {
			return _usedForCodes.Contains(source.Source.OpCode.Code);
		}

		public bool SupportParallelization { get { return true; } }
		#endregion

		protected abstract IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes();

		#region Implementation of IMapping<in OpCode,out TreateInstruction>
		public abstract IEnumerable<TreateInstruction> Map(InstructionRequest request);
		#endregion

		#if DEBUG
		[EditorBrowsable(EditorBrowsableState.Never)]
		internal IEnumerable<Mono.Cecil.Cil.Code> GetUsedForCodes() {
			return UsedForCodes();
		}
		#endif
	}
}