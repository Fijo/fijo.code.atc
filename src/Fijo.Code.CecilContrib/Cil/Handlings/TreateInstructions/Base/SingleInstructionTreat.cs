using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	[About("SingleILCodeInstructionTreat")]
	public abstract class SingleInstructionTreat : InstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			return UsedForCode().IntoEnumerable();
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			return Map(request.Source);
		}
		#endregion

		protected abstract IEnumerable<TreateInstruction> Map(Instruction instruction);

		protected abstract Mono.Cecil.Cil.Code UsedForCode();
	}
}