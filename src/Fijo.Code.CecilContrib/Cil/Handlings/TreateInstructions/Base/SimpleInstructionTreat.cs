using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	[About("SimpleILCodeInstructionTreat")]
	[About("TargetlessInstructionTreat")]
	public abstract class SimpleInstructionTreat : SingleInstructionTreat {
		protected override IEnumerable<TreateInstruction> Map(Instruction instruction) {
			#region PreCondition
			Debug.Assert(instruction.Operand == null, "The Instruction is not targetless as expected");
			#endregion
			yield return MapSingle(instruction);
		}

		protected abstract TreateInstruction MapSingle(Instruction instruction);
	}
}