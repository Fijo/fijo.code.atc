using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	[About("INonPrimaryInstructionTreat")]
	public interface IAdditionalInstructionTreat {}
}