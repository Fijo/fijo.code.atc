using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public abstract class LoadTypeInstructionTreat : TypeInstructionTreat<LoadTypeInstructionTreat.LoadingType> {
		[OnlyPublicFor("be used as an generic parameter of baseType TypeInstructionTreat<>")]
		public class LoadingType {
			public readonly TypeReference Type;
			public readonly TypeReference AsType;

			public LoadingType(TypeReference type, TypeReference asType = null) {
				Type = type;
				AsType = asType ?? type;
			}
		}

		protected virtual LoadingType CreateLoadingType(TypeReference type, TypeReference asType = null) {
			return new LoadingType(type, asType);
		}
	}
}