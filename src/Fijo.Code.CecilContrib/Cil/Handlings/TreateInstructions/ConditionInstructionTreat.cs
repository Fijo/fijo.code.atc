using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Factory.Interfaces;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using ILCode = Mono.Cecil.Cil.Code;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class ConditionInstructionTreat : InstructionTreat {
		private readonly IDictionary<ILCode, Expression> _conditions;

		public ConditionInstructionTreat(IExpressions expressions) {
			_conditions = new Dictionary<ILCode, Expression>
			{
				{ILCode.Ceq, expressions.Equal},
				{ILCode.Cgt, expressions.Greater},
				{ILCode.Cgt_Un, expressions.GreaterUnsigned},
				{ILCode.Clt, expressions.Less},
				{ILCode.Clt_Un, expressions.LessUnsigned},
			};
		}

		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.Ceq;
			yield return Mono.Cecil.Cil.Code.Cgt;
			yield return Mono.Cecil.Cil.Code.Cgt_Un;
			yield return Mono.Cecil.Cil.Code.Clt;
			yield return Mono.Cecil.Cil.Code.Clt_Un;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			yield return new ConditionInstruction(_conditions[instruction.OpCode.Code], instruction);
		}
		#endregion
	}
}