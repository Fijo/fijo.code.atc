using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Factory.Interfaces;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using ILCode = Mono.Cecil.Cil.Code;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class ExpressionInstructionTreat : InstructionTreat {
		private readonly IDictionary<ILCode, Expression> _expressions;

		public ExpressionInstructionTreat(IExpressions expressions) {
			_expressions = new Dictionary<ILCode, Expression>
			{
				{ILCode.Add, expressions.Add},
				{ILCode.Add_Ovf, expressions.AddOverflowCheck},
				{ILCode.Add_Ovf_Un, expressions.AddUnsignedOverflowCheck},
				{ILCode.And, expressions.BitwiseAnd},
				{ILCode.Div, expressions.Divide},
				{ILCode.Div_Un, expressions.DivideUnsigned},
				{ILCode.Mul, expressions.Multiply},
				{ILCode.Mul_Ovf, expressions.MultiplyOverflowCheck},
				{ILCode.Mul_Ovf_Un, expressions.MultiplyUnsignedOverflowCheck},
				{ILCode.Neg, expressions.Negate},
				{ILCode.Not, expressions.BitwiseNot},
				{ILCode.Or, expressions.BitwiseOr},
				{ILCode.Rem, expressions.Mod},
				{ILCode.Rem_Un, expressions.ModUnsigned},
				{ILCode.Shl, expressions.BitwiseShiftLeft},
				{ILCode.Shr, expressions.BitwiseShiftRight},
				{ILCode.Shr_Un, expressions.BitwiseShiftRightUnsigned},
				{ILCode.Sub, expressions.Sub},
				{ILCode.Sub_Ovf, expressions.SubOverflowCheck},
				{ILCode.Sub_Ovf_Un, expressions.SubUnsignedOverflowCheck},
				{ILCode.Xor, expressions.BitwiseXOr},
			};
			
		}

		#region Overrides of InstructionTreat
		protected override IEnumerable<ILCode> UsedForCodes() {
			yield return ILCode.Add;
			yield return ILCode.Add_Ovf;
			yield return ILCode.Add_Ovf_Un;
			yield return ILCode.And;
			yield return ILCode.Div;
			yield return ILCode.Div_Un;
			yield return ILCode.Mul;
			yield return ILCode.Mul_Ovf;
			yield return ILCode.Mul_Ovf_Un;
			yield return ILCode.Neg;
			yield return ILCode.Not;
			yield return ILCode.Or;
			yield return ILCode.Rem;
			yield return ILCode.Rem_Un;
			yield return ILCode.Shl;
			yield return ILCode.Shr;
			yield return ILCode.Shr_Un;
			yield return ILCode.Sub;
			yield return ILCode.Sub_Ovf;
			yield return ILCode.Sub_Ovf_Un;
			yield return ILCode.Xor;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			var instruction = request.Source;
			yield return new ExpressionInstruction(_expressions[instruction.OpCode.Code], instruction);
		}
		#endregion
	}
}