using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class SwitchInstructionTreat : SingleInstructionTreat {
		#region Overrides of SingleInstructionTreat
		protected override IEnumerable<TreateInstruction> Map(Instruction instruction) {
			yield return new SwitchInstruction((IList<Instruction>) instruction.Operand, instruction);
		}

		protected override Mono.Cecil.Cil.Code UsedForCode() {
			return Mono.Cecil.Cil.Code.Switch;
		}
		#endregion
	}
}