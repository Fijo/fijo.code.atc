using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class NotUsedInstructionTreat : InstructionTreat {
		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			yield return Mono.Cecil.Cil.Code.No;
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			Debug.Assert(false, string.Format("Asserted that the ILCode �{0}� would not be used, but it had been used. If you�re the developer of �Fijo.Code.CecilContrib� you should impl a InstructionTreat for that ILCode.", request.Source.OpCode.Code));
// ReSharper disable HeuristicUnreachableCode
			yield break;
// ReSharper restore HeuristicUnreachableCode
		}
		#endregion
	}
}