using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Extentions;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions {
	public class StLocInstructionTreat : InstructionTreat {
		private readonly IDictionary<Mono.Cecil.Cil.Code, ushort> _indexByCode = new Dictionary<Mono.Cecil.Cil.Code, ushort>
		{
			{Mono.Cecil.Cil.Code.Stloc_0, 0},
			{Mono.Cecil.Cil.Code.Stloc_1, 1},
			{Mono.Cecil.Cil.Code.Stloc_2, 2},
			{Mono.Cecil.Cil.Code.Stloc_3, 3},
		};

		#region Overrides of InstructionTreat
		protected override IEnumerable<Mono.Cecil.Cil.Code> UsedForCodes() {
			return new[]
			{
				Mono.Cecil.Cil.Code.Stloc,
				Mono.Cecil.Cil.Code.Stloc_0,
				Mono.Cecil.Cil.Code.Stloc_1,
				Mono.Cecil.Cil.Code.Stloc_2,
				Mono.Cecil.Cil.Code.Stloc_3,
				Mono.Cecil.Cil.Code.Stloc_S
			};
		}

		public override IEnumerable<TreateInstruction> Map(InstructionRequest request) {
			yield return new StLocInstruction(GetTarget(request), request.Source);
		}
		#endregion
		
		private VariableDefinition GetTarget(InstructionRequest request) {
			var instruction = request.Source;
			var code = instruction.OpCode.Code;
			switch (code) {
				case Mono.Cecil.Cil.Code.Stloc:
				case Mono.Cecil.Cil.Code.Stloc_S:
					return (VariableDefinition) instruction.Operand;
				default:
					return request.Body.GetVariable(_indexByCode[code]);
			}
		}
	}
}