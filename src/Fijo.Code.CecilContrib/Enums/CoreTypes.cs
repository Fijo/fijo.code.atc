﻿using System;
using System.Runtime.Serialization;

namespace Fijo.Code.CecilContrib.Enums {
	[Serializable, DataContract]
	public enum CoreTypes : byte {
		Object,
		Void,
		Bool,
		Char,
		SByte,
		Byte,
		Int16,
		UInt16,
		Int32,
		UInt32,
		Int64,
		UInt64,
		Single,
		Double,
		IntPtr,
		UIntPtr,
		String,
		TypeRef
	}
}