﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public abstract class Token {}

	[Serializable, DataContract]
	public class MetadataToken : Token {
		[DataMember(Order = 1)]
		public Mono.Cecil.MetadataToken Value { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public MetadataToken() {
			
		}

		public MetadataToken(Mono.Cecil.MetadataToken value) {
			Value = value;
		}

		#region Equality
		protected bool Equals(MetadataToken other) {
			return Value.Equals(other.Value);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<MetadataToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return Value.GetHashCode();
		}
		#endregion
	}

	[Serializable, DataContract]
	public class FullToken {
		[DataMember(Order = 1)]
		public Token MetadataToken { get; set; }

		// ToDo impove that types (the could be smaller)
		[DataMember(Order = 2)]
		public string Module { get; set; }

		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public FullToken() {}

		public FullToken(Token metadataToken, string module) {
			MetadataToken = metadataToken;
			Module = module;
		}

		//[UsedImplicitly]
		//public FullToken(SerializationInfo info, StreamingContext context) {
		//	MetadataToken = info.GetValue("a", typeof(object));
		//	Module = info.GetString("b");
		//}

		#region Equality
		protected bool Equals(FullToken other) {
			return MetadataToken.Equals(other.MetadataToken) && Module == other.Module;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<FullToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return MetadataToken.GetHashCode().GetHashCodeAlgorithm(Module.NullableGetHashCode());
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", MetadataToken);
		//	info.AddValue("b", Module);
		//}
		//#endregion
	}
}