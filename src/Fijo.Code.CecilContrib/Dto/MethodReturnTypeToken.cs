﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class MethodReturnTypeToken : Token {
		[DataMember(Order = 1)]
		public Token MethodMetadataToken { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public MethodReturnTypeToken() {}

		public MethodReturnTypeToken(Token methodMetadataToken) {
			MethodMetadataToken = methodMetadataToken;
		}

		//[UsedImplicitly]
		//public MethodReturnTypeToken(SerializationInfo info, StreamingContext context) {
		//	MethodMetadataToken = info.GetValue("a", typeof(object));
		//}

		#region Equality
		protected bool Equals(MethodReturnTypeToken other) {
			return MethodMetadataToken.Equals(other.MethodMetadataToken);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<MethodReturnTypeToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return MethodMetadataToken.GetHashCode();
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", MethodMetadataToken);
		//}
		//#endregion
	}
}