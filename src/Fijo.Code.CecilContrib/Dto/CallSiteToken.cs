﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class CallSiteToken : Token {
		[DataMember(Order = 1)]
		public MetadataToken ReturnTypeMetadataToken { get; set; }
		[DataMember(Order = 2)]
		public ICollection<Token> ParameterMetadataToken { get; set; }
		[DataMember(Order = 3)]
		public string Name { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public CallSiteToken() {}

		public CallSiteToken(MetadataToken returnTypeMetadataToken, ICollection<Token> parameterMetadataToken, string name) {
			ReturnTypeMetadataToken = returnTypeMetadataToken;
			ParameterMetadataToken = parameterMetadataToken;
			Name = name;
		}

		//[UsedImplicitly]
		//public CallSiteToken(SerializationInfo info, StreamingContext context) {
		//	ReturnTypeMetadataToken = (MetadataToken) info.GetValue("a", typeof (MetadataToken));
		//	ParameterMetadataToken = (ICollection<object>) info.GetValue("b", typeof (ICollection<object>));
		//	Name = (string) info.GetValue("c", typeof (string));
		//}

		#region Equality
		protected bool Equals(CallSiteToken other) {
			return Name == other.Name && ReturnTypeMetadataToken.Equals(other.ReturnTypeMetadataToken) && ParameterMetadataToken.SequenceEqual(other.ParameterMetadataToken);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<CallSiteToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return Name.GetHashCode().GetHashCodeAlgorithm(ReturnTypeMetadataToken.GetHashCode(), ParameterMetadataToken.GetSequenceHashCode());
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", ReturnTypeMetadataToken);
		//	info.AddValue("b", ParameterMetadataToken);
		//	info.AddValue("c", Name);
		//}
		//#endregion
	}
}