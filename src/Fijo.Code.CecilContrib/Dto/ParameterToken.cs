﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class ParameterToken : Token {
		[DataMember(Order = 1)]
		public Token MethodMetadataToken { get; set; }
		[DataMember(Order = 2)]
		public int Index { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public ParameterToken() {}

		public ParameterToken(Token methodMetadataToken, int index) {
			MethodMetadataToken = methodMetadataToken;
			Index = index;
		}



		//[UsedImplicitly]
		//public ParameterToken(SerializationInfo info, StreamingContext context) {
		//	MethodMetadataToken = info.GetValue("a", typeof(object));
		//	Index = info.GetInt32("b");
		//}

		#region Equality
		protected bool Equals(ParameterToken other) {
			return MethodMetadataToken.Equals(other.MethodMetadataToken) && Index == other.Index;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<ParameterToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return MethodMetadataToken.GetHashCode().GetHashCodeAlgorithm(Index);
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", MethodMetadataToken);
		//	info.AddValue("b", Index);
		//}
		//#endregion
	}
}