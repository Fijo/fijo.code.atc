﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class HandledExceptionBlock {
		[DataMember(Order = 1)]
		public ICollection<ExceptionHandler> ExceptionHandler { get; set; }
		[DataMember(Order = 2)]
		public Instruction TryStart { get; set; }
		[DataMember(Order = 3)]
		public Instruction TryEnd { get; set; }

		public HandledExceptionBlock([NotNull] ICollection<ExceptionHandler> exceptionHandler, [NotNull] Instruction tryStart, [NotNull] Instruction tryEnd) {
			ExceptionHandler = exceptionHandler;
			TryStart = tryStart;
			TryStart = tryStart;
		}
	}
}