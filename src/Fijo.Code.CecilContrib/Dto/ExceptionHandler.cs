﻿using System;
using System.Runtime.Serialization;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class ExceptionHandler {
		[DataMember(Order = 1)]
		public TypeReference CatchType { get; set; }
		[DataMember(Order = 2)]
		public Instruction FilterStart { get; set; }
		[DataMember(Order = 3)]
		public Instruction HandlerStart { get; set; }
		[DataMember(Order = 4)]
		public Instruction HandlerEnd { get; set; }
		[DataMember(Order = 5)]
		public ExceptionHandlerType HandlerType { get; set; }

		public ExceptionHandler(TypeReference catchType, Instruction filterStart, Instruction handlerStart, Instruction handlerEnd, ExceptionHandlerType handlerType) {
			CatchType = catchType;
			FilterStart = filterStart;
			HandlerStart = handlerStart;
			HandlerEnd = handlerEnd;
			HandlerType = handlerType;
		}
	}
}