﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class CustomAttributeToken : Token {
		[DataMember(Order = 1)]
		public Token ProviderMetadataToken { get; set; }
		[DataMember(Order = 2)]
		public int AttrIndex { get; set; }
		
		protected CustomAttributeToken() {}

		public CustomAttributeToken(Token providerMetadataToken, int attrIndex) {
			ProviderMetadataToken = providerMetadataToken;
			AttrIndex = attrIndex;
		}

		//[UsedImplicitly]
		//public CustomAttributeToken(SerializationInfo info, StreamingContext context) {
		//	AttrIndex = info.GetInt32("a");
		//	ProviderMetadataToken = info.GetValue("b", typeof (object));
		//}

		#region Equality
		protected bool Equals(CustomAttributeToken other) {
			return AttrIndex == other.AttrIndex && ProviderMetadataToken.Equals(other.ProviderMetadataToken);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<CustomAttributeToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return AttrIndex.GetHashCodeAlgorithm(ProviderMetadataToken.GetHashCode());
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", AttrIndex);
		//	info.AddValue("b", ProviderMetadataToken);
		//}
		//#endregion
	}
}