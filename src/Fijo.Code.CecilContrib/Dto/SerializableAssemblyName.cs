﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class SerializableAssemblyName : Token {
		[DataMember(Order = 1)]
		public string FullName { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public SerializableAssemblyName() {}

		public SerializableAssemblyName(string fullName) {
			FullName = fullName;
		}

		//[UsedImplicitly]
		//public SerializableAssemblyName(SerializationInfo info, StreamingContext context) {
		//	FullName = info.GetString("a");
		//}

		#region Equality
		protected bool Equals(SerializableAssemblyName other) {
			return FullName == other.FullName;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<SerializableAssemblyName>(obj, Equals);
		}

		public override int GetHashCode() {
			return FullName.GetHashCode();
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", FullName);
		//}
		//#endregion
	}
}