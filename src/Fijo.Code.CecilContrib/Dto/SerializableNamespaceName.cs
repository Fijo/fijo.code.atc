﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class SerializableNamespaceName : Token {
		[DataMember(Order = 1)]
		public string Namespace { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public SerializableNamespaceName() {}

		public SerializableNamespaceName(string @namespace) {
			Namespace = @namespace;
		}

		//[UsedImplicitly]
		//public SerializableNamespaceName(SerializationInfo info, StreamingContext context) {
		//	Namespace = info.GetString("a");
		//}

		#region Equality
		protected bool Equals(SerializableNamespaceName other) {
			return Namespace == other.Namespace;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<SerializableNamespaceName>(obj, Equals);
		}

		public override int GetHashCode() {
			return Namespace.GetHashCode();
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", Namespace);
		//}
		//#endregion
	}
}