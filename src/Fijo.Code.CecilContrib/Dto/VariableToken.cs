﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public class VariableToken : Token {
		[DataMember(Order = 1)]
		public MetadataToken MethodMetadataToken { get; set; }
		[DataMember(Order = 2)]
		public int Index { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public VariableToken() {}

		public VariableToken(MetadataToken methodMetadataToken, int index) {
			MethodMetadataToken = methodMetadataToken;
			Index = index;
		}

		//[UsedImplicitly]
		//public VariableToken(SerializationInfo info, StreamingContext context) {
		//	MethodMetadataToken = (MetadataToken) info.GetValue("a", typeof (MetadataToken));
		//	Index = info.GetInt32("b");
		//}

		#region Equality
		protected bool Equals(VariableToken other) {
			return MethodMetadataToken.Equals(other.MethodMetadataToken) && Index == other.Index;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<VariableToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return MethodMetadataToken.GetHashCode().GetHashCodeAlgorithm(Index);
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", MethodMetadataToken);
		//	info.AddValue("b", Index);
		//}
		//#endregion
	}
}