﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Dto {
	[Serializable, DataContract]
	public enum MemberType {
		Field,
		Method
	}

	[Serializable, DataContract]
	public class MemberToken : Token {
		[DataMember(Order = 1)]
		public Token ProviderMetadataToken { get; set; }
		[DataMember(Order = 2)]
		public MemberType MemberType { get; set; }
		[DataMember(Order = 3)]
		public int MemberIndex { get; set; }
		
		[UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never), Note("Do not use manualy. Only used Serialization."), OnlyPublicFor("Serialization precompiler")]
		public MemberToken() {}

		public MemberToken(Token providerMetadataToken, MemberType memberType, int memberIndex) {
			ProviderMetadataToken = providerMetadataToken;
			MemberType = memberType;
			MemberIndex = memberIndex;
		}

		//[UsedImplicitly]
		//public MemberToken(SerializationInfo info, StreamingContext context) {
		//	MemberIndex = info.GetInt32("a");
		//	ProviderMetadataToken = info.GetValue("b", typeof (object));
		//}

		#region Equality
		protected bool Equals(MemberToken other) {
			return MemberIndex == other.MemberIndex && MemberType == other.MemberType && ProviderMetadataToken.Equals(other.ProviderMetadataToken);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<MemberToken>(obj, Equals);
		}

		public override int GetHashCode() {
			return MemberIndex.GetHashCodeAlgorithm(MemberType.GetHashCode(), ProviderMetadataToken.GetHashCode());
		}
		#endregion

		//#region Implementation of ISerializable
		//public void GetObjectData(SerializationInfo info, StreamingContext context) {
		//	info.AddValue("a", MemberIndex);
		//	info.AddValue("b", ProviderMetadataToken);
		//}
		//#endregion
	}
}