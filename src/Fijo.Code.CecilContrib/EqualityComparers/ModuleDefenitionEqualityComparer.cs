using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.CoreSerializer;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.EqualityComparers {
	[Serializable]
	public class ModuleDefenitionEqualityComparer : IEqualityComparer<ModuleDefinition>, ISerializable {
		private readonly IEqualityComparer<AssemblyNameReference> _assemblyNameReferenceEqualityComparer;

		public ModuleDefenitionEqualityComparer(IEqualityComparer<AssemblyNameReference> assemblyNameReferenceEqualityComparer) {
			_assemblyNameReferenceEqualityComparer = assemblyNameReferenceEqualityComparer;
		}
		#region Implementation of IEqualityComparer<in ModuleDefinition>
		public bool Equals(ModuleDefinition x, ModuleDefinition y) {
			var xAsssembly = x.Assembly;
			var yAssembly = y.Assembly;
			var isAssemblyNull = xAsssembly == null;
			var bothAssembliesAreLoadedEqually = isAssemblyNull == (yAssembly == null);
			return bothAssembliesAreLoadedEqually &&
			       (isAssemblyNull
				        ? x.FullyQualifiedName == y.FullyQualifiedName
				        : _assemblyNameReferenceEqualityComparer.Equals(xAsssembly.Name, yAssembly.Name));
		}

		public int GetHashCode(ModuleDefinition obj) {
			var assembly = obj.Assembly;
			return assembly == null
				       ? obj.FullyQualifiedName.GetHashCode()
				       : _assemblyNameReferenceEqualityComparer.GetHashCode(assembly.Name);
		}
		#endregion
		
		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (InjectionSerializer<IEqualityComparer<ModuleDefinition>>));
		}
		#endregion
	}
}