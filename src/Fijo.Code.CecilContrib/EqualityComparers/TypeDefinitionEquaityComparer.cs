using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Fijo.Code.CecilContrib.Extentions.TypeDefinition;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using FijoCore.Infrastructure.LightContrib.Module.CoreSerializer;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.EqualityComparers {
	[UsedImplicitly, Serializable]
	public class TypeDefinitionEquaityComparer : IEqualityComparer<TypeDefinition>, ISerializable {
		private readonly IEqualityComparer<ModuleDefinition> _moduleDefenitionEqualityComparer;

		public TypeDefinitionEquaityComparer(IEqualityComparer<ModuleDefinition> moduleDefenitionEqualityComparer) {
			_moduleDefenitionEqualityComparer = moduleDefenitionEqualityComparer;
		}
		#region Implementation of IEqualityComparer<in TypeDefinition>
		public bool Equals(TypeDefinition x, TypeDefinition y) {
			return x.Name == y.Name && x.Namespace == y.Namespace &&
			       _moduleDefenitionEqualityComparer.Equals(x.Module, y.Module) &&
			       x.GetNestedParents().SequenceEqual(y.GetNestedParents(), this);
		}

		public int GetHashCode(TypeDefinition obj) {
			return obj.Name.GetHashCode().GetHashCodeAlgorithm(obj.Namespace.GetHashCode(),
			                                                   _moduleDefenitionEqualityComparer.GetHashCode(obj.Module).GetHashCode(),
			                                                   obj.GetNestedParents().Select(GetHashCode).GetSequenceHashCode());
		}
		#endregion

		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (InjectionSerializer<IEqualityComparer<TypeDefinition>>));
		}
		#endregion
	}
}