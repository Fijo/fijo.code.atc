using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.CoreSerializer;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.EqualityComparers {
	[Serializable]
	public class AssemblyNameDefinitionEqualityComparer : IEqualityComparer<AssemblyNameDefinition>, ISerializable {
		private readonly IEqualityComparer<AssemblyNameReference> _assemblyNameReferenceEqualityComparer;

		public AssemblyNameDefinitionEqualityComparer(IEqualityComparer<AssemblyNameReference> assemblyNameReferenceEqualityComparer) {
			_assemblyNameReferenceEqualityComparer = assemblyNameReferenceEqualityComparer;
		}

		public bool Equals(AssemblyNameDefinition x, AssemblyNameDefinition y) {
			return _assemblyNameReferenceEqualityComparer.Equals(x, y);
		}

		public int GetHashCode(AssemblyNameDefinition obj) {
			return _assemblyNameReferenceEqualityComparer.GetHashCode(obj);
		}
		
		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (InjectionSerializer<IEqualityComparer<AssemblyNameDefinition>>));
		}
		#endregion
	}
}