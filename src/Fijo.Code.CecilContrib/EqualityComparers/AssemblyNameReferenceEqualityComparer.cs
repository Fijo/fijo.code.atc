using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode;
using FijoCore.Infrastructure.LightContrib.Module.CoreSerializer;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.EqualityComparers {
	[Serializable]
	public class AssemblyNameReferenceEqualityComparer : IEqualityComparer<AssemblyNameReference>, ISerializable {
		#region Implementation of IEqualityComparer<in AssemblyNameReference>
		public bool Equals(AssemblyNameReference x, AssemblyNameReference y) {
			return x.FullName == y.FullName;
		}

		public int GetHashCode(AssemblyNameReference obj) {
			return obj.FullName.NullableGetHashCode();
		}
		#endregion

		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (InjectionSerializer<IEqualityComparer<AssemblyNameReference>>));
		}
		#endregion
	}
}