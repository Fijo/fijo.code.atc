using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.CoreSerializer;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.EqualityComparers {
	[Serializable]
	public class AssemblyDefinitionEqualityComparer : IEqualityComparer<AssemblyDefinition>, ISerializable {
		private readonly IEqualityComparer<AssemblyNameDefinition> _assemblyNameDefinitionEqualityComparer;

		public AssemblyDefinitionEqualityComparer(IEqualityComparer<AssemblyNameDefinition> assemblyNameDefinitionEqualityComparer) {
			_assemblyNameDefinitionEqualityComparer = assemblyNameDefinitionEqualityComparer;
		}

		public bool Equals(AssemblyDefinition x, AssemblyDefinition y) {
			return _assemblyNameDefinitionEqualityComparer.Equals(x.Name, y.Name);
		}

		public int GetHashCode(AssemblyDefinition obj) {
			return _assemblyNameDefinitionEqualityComparer.GetHashCode(obj.Name);
		}

		#region Implementation of ISerializable
		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			info.SetType(typeof (InjectionSerializer<IEqualityComparer<AssemblyDefinition>>));
		}
		#endregion
	}
}