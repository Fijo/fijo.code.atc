﻿using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Properties {
	[PublicAPI]
	public class CecilContribInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
			kernel.Load(new CecilContribCilInjectionModule());
			kernel.Load(new CecilContribEqualityComparerInjectionModule());
		}
	}
}