﻿using System.Collections.Generic;
using Fijo.Code.CecilContrib.Cil.Factory;
using Fijo.Code.CecilContrib.Cil.Factory.Interfaces;
using Fijo.Code.CecilContrib.Cil.Handlings.TreateInstructions;
using Fijo.Code.CecilContrib.Cil.Model.Request;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Cil.Services;
using Fijo.Code.CecilContrib.Cil.Services.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Provider;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Properties {
	[PublicAPI]
	public class CecilContribCilInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IExpressions>().To<Expressions>().InSingletonScope();

			//kernel.Bind<ICGAllHandlingProcessor<InstructionRequest, IEnumerable<TreateInstruction>>>().To<InstructionTreadProcessor>().InSingletonScope();
			kernel.Bind<IParallelizableHandlingProvider<ICGAllHandling<InstructionRequest, IEnumerable<TreateInstruction>>>>().To<InjectionParallelizableHandlingProvider<ICGAllHandling<InstructionRequest, IEnumerable<TreateInstruction>>, IInstructionTreat>>().InSingletonScope();

			kernel.Bind<IInstructionTreat>().To<ArgListInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<BoxInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<BrConditionalInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<BreakInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<BrInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<CallInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<CalliInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<CastClassInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<CkFiniteInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<ConditionInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<ConstrainedInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<ConvInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<CpObjInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<CpBlkInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<DupInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<EndFilterInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<EndFinallyInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<ExpressionInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<InitBlkInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<InitObjInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<IsInstInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<JmpInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdArgaInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdArgInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdcInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdElemaInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdElemInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdFldaInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdFldInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdFtnInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdIndInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdLenInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdLocaInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdLocInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdObjInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdTokenInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LdVirtualFtnInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LeaveInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<LocAllocInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<MkRefAnyInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<NewArrInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<NewObjInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<NopInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<PopInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<ReadonlyInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<RefAnyTypeInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<RefAnyValInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<RetInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<SizeOfInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<StArgInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<StElemInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<StFldInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<StIndInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<StLocInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<StObjInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<SwitchInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<TailInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<ThrowInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<UnalignedInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<UnboxInstructionTreat>().InSingletonScope();
			kernel.Bind<IInstructionTreat>().To<VolatileInstructionTreat>().InSingletonScope();
			#if DEBUG
			kernel.Bind<IInstructionTreat>().To<NotUsedInstructionTreat>().InSingletonScope();
			#endif

			kernel.Bind<IVirtuaTreatelInstructionFactory>().To<VirtuaTreatelInstructionFactory>().InSingletonScope();
			kernel.Bind<IInstructionTreadService>().To<InstructionTreadService>().InSingletonScope();

			kernel.Bind<IBodyMetaCodeService>().To<BodyMetaCodeService>().InSingletonScope();
		}
	}
}