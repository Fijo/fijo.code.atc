﻿using System.Collections.Generic;
using Fijo.Code.ATC.DotNet.EqualityComparers;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Properties {
	[PublicAPI]
	public class CecilContribEqualityComparerInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IEqualityComparer<AssemblyNameReference>>().To<AssemblyNameReferenceEqualityComparer>().InSingletonScope();
			kernel.Bind<IEqualityComparer<AssemblyNameDefinition>>().To<AssemblyNameDefinitionEqualityComparer>().InSingletonScope();
			kernel.Bind<IEqualityComparer<AssemblyDefinition>>().To<AssemblyDefinitionEqualityComparer>().InSingletonScope();
			kernel.Bind<IEqualityComparer<ModuleDefinition>>().To<ModuleDefenitionEqualityComparer>().InSingletonScope();
			kernel.Bind<IEqualityComparer<TypeDefinition>>().To<TypeDefinitionEquaityComparer>().InSingletonScope();
		}
	}
}