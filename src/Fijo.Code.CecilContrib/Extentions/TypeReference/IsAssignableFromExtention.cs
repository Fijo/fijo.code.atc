﻿using System.Diagnostics;
using System.Linq;
using Fijo.Code.CecilContrib.Extentions.TypeDefinition;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Extentions.TypeReference {
	[PublicAPI]
	public static class IsAssignableFromExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool IsAssignableFrom([NotNull] this Mono.Cecil.TypeReference me, [CanBeNull] Mono.Cecil.TypeDefinition fromType) {
			if (fromType == null) return false;
			if (me.Equals(fromType)) return true;
			var def = me.Resolve();
			return def.ImplementsType(fromType) ||
			       me.IsGenericParameter &&
			       ((GenericParameter) me).Constraints.All(type => IsAssignableFrom(type, fromType));
		}
	}
}