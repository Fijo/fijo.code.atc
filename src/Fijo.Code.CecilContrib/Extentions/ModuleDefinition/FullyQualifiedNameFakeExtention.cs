﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Extentions.ModuleDefinition {
	#if FakeTools
	public static class FullyQualifiedNameFakeExtention {
		private static readonly FieldInfo FqNameField;

		static FullyQualifiedNameFakeExtention() {
			var type = typeof (Mono.Cecil.ModuleDefinition);
			FqNameField = type.GetField("fq_name", BindingFlags.Instance | BindingFlags.NonPublic);
		}

		[DebuggerStepThrough]
		[Desc("Sets the internal ´fq_name´ field of the ´Mono.Cecil.ModuleDefinition´")]
		public static void SetFullyQualifiedName([NotNull] this Mono.Cecil.ModuleDefinition me, string value) {
			FqNameField.SetValue(me, value);
		}
	}
	#endif
}