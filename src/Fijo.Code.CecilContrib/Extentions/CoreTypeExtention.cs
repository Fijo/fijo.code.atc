﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.CecilContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.CecilContrib.Extentions {
	[PublicAPI]
	public static class CoreTypeExtention {
		private static readonly IDictionary<CoreTypes, Func<TypeSystem, Mono.Cecil.TypeReference>> CoreTypeSelectors = new Dictionary<CoreTypes, Func<TypeSystem, Mono.Cecil.TypeReference>>
		{
			{CoreTypes.Bool, x => x.Boolean},
			{CoreTypes.Byte, x => x.Byte},
			{CoreTypes.Char, x => x.Char},
			{CoreTypes.Double, x => x.Double},
			{CoreTypes.Int16, x => x.Int16},
			{CoreTypes.Int32, x => x.Int32},
			{CoreTypes.Int64, x => x.Int64},
			{CoreTypes.IntPtr, x => x.IntPtr},
			{CoreTypes.Object, x => x.Object},
			{CoreTypes.SByte, x => x.SByte},
			{CoreTypes.Single, x => x.Single},
			{CoreTypes.String, x => x.String},
			{CoreTypes.TypeRef, x => x.TypedReference},
			{CoreTypes.UInt16, x => x.UInt16},
			{CoreTypes.UInt32, x => x.UInt32},
			{CoreTypes.UInt64, x => x.UInt64},
			{CoreTypes.UIntPtr, x => x.UIntPtr},
			{CoreTypes.Void, x => x.Void}
		};

		private static readonly IDictionary<Mono.Cecil.ModuleDefinition, IDictionary<Mono.Cecil.TypeReference, CoreTypes>> CoreType = new Dictionary<Mono.Cecil.ModuleDefinition, IDictionary<Mono.Cecil.TypeReference, CoreTypes>>();
		
		private static IDictionary<Mono.Cecil.TypeReference, CoreTypes> CreateCoreType(TypeSystem typeSystem) {
			return CoreTypeSelectors.ToDict(x => x.Value(typeSystem), x => x.Key);
		}

		private static IDictionary<Mono.Cecil.TypeReference, CoreTypes> GetCoreType(Mono.Cecil.ModuleDefinition module) {
			// ToDo FixMe delete the entries after a while or if a module is disposed delete entry with that module ... currently this will never release the memory again
			return CoreType.GetOrCreate(module, () => CreateCoreType(module.TypeSystem));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool IsCoreType([NotNull] this Mono.Cecil.TypeReference me) {
			return GetCoreType(me.Module).ContainsKey(me);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static bool IsCoreType([NotNull] this Mono.Cecil.TypeReference me, CoreTypes type) {
			return IsCoreType(me, GetSelectorByCode(type));
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static CoreTypes GetCoreType([NotNull] this Mono.Cecil.TypeReference me) {
			return GetCoreType(me.Module)[me];
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static Mono.Cecil.TypeReference GetCoreType([NotNull] this Mono.Cecil.ModuleDefinition me, CoreTypes type) {
			return GetCoreType(me.TypeSystem, type);
		}

		[Pure, PublicAPI, DebuggerStepThrough]
		public static Mono.Cecil.TypeReference GetCoreType([NotNull] this TypeSystem me, CoreTypes type) {
			return GetSelectorByCode(type)(me);
		}
		
		[NotNull, Pure, DebuggerStepThrough]
		private static Func<TypeSystem, Mono.Cecil.TypeReference> GetSelectorByCode(CoreTypes type) {
			return CoreTypeSelectors.GetOrThrow(type, () => new ArgumentException("You used an invalid Enum value please use only correct values of defined in the Enum ´CoreTypes´.", "type"));
		}

		[Pure, DebuggerStepThrough]
		private static bool IsCoreType([NotNull] Mono.Cecil.TypeReference type, Func<TypeSystem, Mono.Cecil.TypeReference> selector) {
			return selector(type.Module.TypeSystem).Equals(type);
		}
	}
}