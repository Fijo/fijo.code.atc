﻿using System;
using System.Linq;
using System.Reflection;

namespace Fijo.Code.CecilContrib.Extentions {
	internal static class CecilExtentionHelper {
		internal static Assembly MainAssembly { get; private set; }

		static CecilExtentionHelper() {
			var publicKeyToken = new byte[] {7, 56, 235, 159, 19, 46, 215, 86};
			MainAssembly = AppDomain.CurrentDomain.GetAssemblies().Single(x => {
				var name = x.GetName();
				return name.Name == "Mono.Cecil" && name.GetPublicKeyToken().SequenceEqual(publicKeyToken);
			});
		}
	}
}