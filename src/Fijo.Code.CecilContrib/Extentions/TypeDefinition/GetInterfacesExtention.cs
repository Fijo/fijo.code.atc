﻿using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;
using System.Linq;

namespace Fijo.Code.CecilContrib.Extentions.TypeDefinition {
	[PublicAPI]
	public static class GetInterfacesExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("gets all interface that are implemented by a type.")]
		public static IEnumerable<Mono.Cecil.TypeReference> GetInterfaces([NotNull] this Mono.Cecil.TypeDefinition me) {
			return me.GetBaseTypes().AddReturn(me).SelectMany(x => x.Interfaces);
		}
	}
}