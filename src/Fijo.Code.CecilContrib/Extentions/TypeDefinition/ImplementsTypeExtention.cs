﻿using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Extentions.TypeDefinition {
	[PublicAPI]
	public static class ImplementsTypeExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this type implements a given type")]
		public static bool ImplementsType([NotNull] this Mono.Cecil.TypeDefinition me, [NotNull] Mono.Cecil.TypeDefinition baseType) {
			return baseType.IsInterface ? me.ImplementsInteface(baseType) : me.ImplementsClass(baseType);
		}
	}
}