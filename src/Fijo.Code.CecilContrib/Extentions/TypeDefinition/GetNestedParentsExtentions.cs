﻿using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Extentions.TypeDefinition {
	[PublicAPI]
	public static class GetNestedParentsExtentions {
		[NotNull, Pure, PublicAPI, DebuggerStepThrough]
		[About("GetParentsForNestedTypes")]
		public static IEnumerable<Mono.Cecil.TypeDefinition> GetNestedParents([NotNull] this Mono.Cecil.TypeDefinition typeDefenition) {
			while(typeDefenition.IsNested)
				yield return typeDefenition = typeDefenition.DeclaringType;
		}
	}
}