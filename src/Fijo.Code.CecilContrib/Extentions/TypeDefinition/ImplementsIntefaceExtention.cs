﻿using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Extentions.TypeDefinition {
	[PublicAPI]
	public static class ImplementsIntefaceExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this implements the given interface")]
		public static bool ImplementsInteface([NotNull] this Mono.Cecil.TypeDefinition me, [NotNull] Mono.Cecil.TypeDefinition @interface) {
			Debug.Assert(@interface.IsInterface, "@interface.IsInterface");
			return me.Interfaces.Contains(@interface);
		}
	}
}