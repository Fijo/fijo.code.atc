﻿using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Extentions.TypeDefinition {
	[PublicAPI]
	public static class ImplementsClassExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("returns if this class type implements a given class type")]
		public static bool ImplementsClass([NotNull] this Mono.Cecil.TypeDefinition me, [NotNull] Mono.Cecil.TypeDefinition baseType) {
			return InternalImplementsClass(me, baseType);
		}

		[Pure, DebuggerStepThrough]
		private static bool InternalImplementsClass([NotNull] Mono.Cecil.TypeDefinition type, [NotNull] Mono.Cecil.TypeDefinition baseType) {
			var currentBaseTypeRef = type.BaseType;
			Mono.Cecil.TypeDefinition currentBaseTypeDef;
			return currentBaseTypeRef != null && (currentBaseTypeDef = currentBaseTypeRef.Resolve()) != null &&
			       (currentBaseTypeDef.Equals(baseType) || InternalImplementsClass(currentBaseTypeDef, baseType));
		}
	}
}