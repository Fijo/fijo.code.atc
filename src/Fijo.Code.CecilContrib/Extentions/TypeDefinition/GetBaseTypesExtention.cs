﻿using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Code.CecilContrib.Extentions.TypeDefinition {
	[PublicAPI]
	public static class GetBaseTypesExtention {
		[Pure, PublicAPI, DebuggerStepThrough]
		[Desc("gets all baseTypes from a type.")]
		public static IEnumerable<Mono.Cecil.TypeDefinition> GetBaseTypes([NotNull] this Mono.Cecil.TypeDefinition me) {
			Mono.Cecil.TypeReference baseTypeRef;
			while((baseTypeRef = me.BaseType) != null && (me = baseTypeRef.Resolve()) != null)
				yield return me;
		}
	}
}