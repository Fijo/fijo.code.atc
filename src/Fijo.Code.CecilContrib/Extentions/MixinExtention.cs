﻿using System.Reflection;
using Mono.Cecil;
using Mono.Cecil.Cil;
using MethodBody = Mono.Cecil.Cil.MethodBody;

namespace Fijo.Code.CecilContrib.Extentions {
	public static class MixinExtention {
		private static readonly MethodInfo _getParameter;
		private static readonly MethodInfo _getVariable;

		static MixinExtention() {
			var mixin = CecilExtentionHelper.MainAssembly.GetType("Mono.Cecil.Mixin", true);
			_getParameter = mixin.GetMethod("GetParameter", new[] {typeof (MethodBody), typeof (int)});
			_getVariable = mixin.GetMethod("GetVariable", new[] {typeof (MethodBody), typeof (int)});
		}

		public static ParameterDefinition GetParameter(this MethodBody me, int index) {
			return (ParameterDefinition) _getParameter.Invoke(null, new object[] {me, index});
		}

		public static VariableDefinition GetVariable (this MethodBody me, int index)
		{
			return (VariableDefinition) _getVariable.Invoke(null, new object[] {me, index});
		} 
	}
}