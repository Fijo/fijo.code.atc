﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.CecilContrib.Dto;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;
using Mono.Cecil;
using Mono.Cecil.Cil;
using MetadataToken = Fijo.Code.CecilContrib.Dto.MetadataToken;

namespace Fijo.Code.CecilContrib.Extentions {
	[PublicAPI]
	public static class FullTokenExtention {

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this VariableDefinition me, [NotNull] MethodDefinition method) {
			#region PreCondition
			Debug.Assert(method.Body != null);
			Debug.Assert(method.Body.Variables.Contains(me));
			Debug.Assert(method.Body.Variables.Select(x => x.Index).IsUnique());
			Debug.Assert(method.Body.Variables.IndexOf(me) == me.Index);
			#endregion
			return GetFullToken(new VariableToken(GetMetadataToken(method.MetadataToken), me.Index), method.Module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this Mono.Cecil.ModuleDefinition me) {
			return GetFullToken(me, me);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this AssemblyDefinition me) {
			return GetFullToken(new SerializableAssemblyName(me.FullName), default(Mono.Cecil.ModuleDefinition));
		}

		// may improve this
		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this GenericParameter me) {
			return GetFullToken(me, me.Module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this FieldDefinition me) {
			return FullTokenBase(me, MemberType.Field, x => x.DeclaringType, x => x.Fields);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this MethodDefinition me) {
			return FullTokenBase(me, MemberType.Method, x => x.DeclaringType, x => x.Methods);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this CallSite me) {
			return GetFullToken(new CallSiteToken(GetMetadataToken(me.ReturnType.MetadataToken), me.Parameters.Select(x => GetSpecFullToken(x.ParameterType)).Cast<Token>().ToArray(), me.Name), me.Module);
		}

		private static FullToken FullTokenBase<T>(T me, MemberType memberType, Func<T, Mono.Cecil.TypeDefinition> declaringTypeResolver, Func<Mono.Cecil.TypeDefinition, ICollection<T>> collectionOfMeResolver) {
			var typeDefinition = declaringTypeResolver(me);
			return GetFullToken(new MemberToken(GetMetadataToken(typeDefinition.MetadataToken), memberType, collectionOfMeResolver(typeDefinition).IndexOf(me)), typeDefinition.Module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this FunctionPointerType me) {
			return GetFullToken(me, me.Module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this Mono.Cecil.TypeDefinition me) {
			return GetFullToken(me, me.Module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this CustomAttribute me, [NotNull] ICustomAttributeProvider provider) {
			var providerFullToken = GetSpecFullToken(provider);
			Debug.Assert(provider != null);
			return GetFullToken(new CustomAttributeToken(providerFullToken.MetadataToken, provider.CustomAttributes.IndexOf(me)),
			                    providerFullToken.Module);
		}

		// ToDo solve issues
		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this IMetadataTokenProvider me) {
			var methodDefinition = me as MethodDefinition;
			if (methodDefinition != null) return methodDefinition.GetFullToken();
			var fieldDefinition = me as FieldDefinition;
			if (fieldDefinition != null) return fieldDefinition.GetFullToken();
			var functionPointerType = me as FunctionPointerType;
			if (functionPointerType != null) return functionPointerType.GetFullToken();
			return GetFullToken(me.MetadataToken, null);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this ParameterDefinition me, IMethodSignature method) {
			var methodFullToken = method.GetFullToken();
			return GetFullToken(new ParameterToken(methodFullToken.MetadataToken, method.Parameters.IndexOf(me)), methodFullToken.Module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this MethodReturnType me) {
			var methodFullToken = me.Method.GetFullToken();
			return GetFullToken(new MethodReturnTypeToken(methodFullToken.MetadataToken), methodFullToken.Module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this IMetadataTokenProvider me, [NotNull] Mono.Cecil.ModuleDefinition module) {
			return GetFullToken(me.MetadataToken, module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this Mono.Cecil.MetadataToken me, [NotNull] Mono.Cecil.ModuleDefinition module) {
			Debug.Assert(me != null);
			return GetFullToken(GetMetadataToken(me), module);
		}

		[NotNull, Pure, PublicAPI]
		public static FullToken GetFullToken([NotNull] this Token metadataToken, [CanBeNull] Mono.Cecil.ModuleDefinition module) {
			return GetFullToken(metadataToken, GetModule(module));
		}
		
		[NotNull, Pure, PublicAPI]
		private static FullToken GetFullToken([NotNull] Token metadataToken, [CanBeNull] string moduleStr) {
			return new FullToken(metadataToken, moduleStr);
		}

		private static MetadataToken GetMetadataToken(Mono.Cecil.MetadataToken me) {
			return new MetadataToken(me);
		}

		private static string GetModule(Mono.Cecil.ModuleDefinition module) {
			return module.WhenNotNull(x => x.FullyQualifiedName);
		}

		private static FullToken GetSpecFullToken(object obj) {
			var assemblyDefinition = obj as AssemblyDefinition;
			if (assemblyDefinition != null) return assemblyDefinition.GetFullToken();
			var genericParameter = obj as GenericParameter;
			if (genericParameter != null) return genericParameter.GetFullToken();
			var typeDefinition = obj as Mono.Cecil.TypeDefinition;
			if (typeDefinition != null) return typeDefinition.GetFullToken();
			var methodDefinition = obj as MethodDefinition;
			if (methodDefinition != null) return methodDefinition.GetFullToken();
			var metadataTokenProvider = obj as IMetadataTokenProvider;
			if (metadataTokenProvider != null) return metadataTokenProvider.GetFullToken();
			return default(FullToken);
		}
	}
}