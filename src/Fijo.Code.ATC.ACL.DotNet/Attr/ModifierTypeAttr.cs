using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[Serializable, DataContract]
	[DisplayName("ModifierTypeAttr: {Type}")]
	public class ModifierTypeAttr : SpecialTypeAttr {
		[DataMember(Order = 1)]
		public Type ConstrainModifierType { get; set; }
		[DataMember(Order = 2)]
		public bool IsOptional { get; set; }

		public ModifierTypeAttr() {
			Type = SpecialTypes.ModifierType;
		}

		public override string ToString() {
			return string.Format("ModifierTypeAttr: {0}", Type);
		}
	}
}