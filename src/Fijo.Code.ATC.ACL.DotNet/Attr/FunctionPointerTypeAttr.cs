using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[Serializable, DataContract]
	[DisplayName("FunctionPointerTypeAttr: {Target.Name}")]
	public class FunctionPointerTypeAttr : SpecialTypeAttr {
		[DataMember(Order = 1)]
		public Method Target { get; set; }

		public FunctionPointerTypeAttr() {
			Type = SpecialTypes.FunctionPointer;
		}

		public override string ToString() {
			return string.Format("FunctionPointerTypeAttr: {0}", Target.Name);
		}
	}
}