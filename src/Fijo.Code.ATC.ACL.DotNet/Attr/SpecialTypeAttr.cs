using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[Serializable, DataContract]
	[DisplayName("SpecialTypeAttr: {Type}")]
	public class SpecialTypeAttr : OopAttr, IVirtualAttr {
		[DataMember(Order = 1)]
		public SpecialTypes Type { get; set; }

		public override string ToString() {
			return string.Format("SpecialTypeAttr: {0}", Type);
		}

		public class Validation : AttrValidation<SpecialTypeAttr> {
			public override bool IsValidFor(Type codeObjType) {
				return base.IsValidFor(codeObjType) && codeObjType.IsAssignableFrom(typeof (Model.Oop.Type));
			}
		}
	}
}