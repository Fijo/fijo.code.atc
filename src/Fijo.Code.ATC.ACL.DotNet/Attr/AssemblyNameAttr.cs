using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Oop;
using Type = System.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[Serializable, DataContract]
	[DebuggerDisplay("AssemblyNameAttr: {Name.FullName}")]
	public class AssemblyNameAttr : OopAttr {
		[DataMember(Order = 1)]
		public AssemblyName Name { get; set; }

		public override string ToString() {
			return string.Format("AssemblyNameAttr: {0}", Name.FullName);
		}

		public class Validation : AttrValidation<AssemblyNameAttr> {
			public override bool IsValidFor(Type codeObjType) {
				return base.IsValidFor(codeObjType) && codeObjType.IsAssignableFrom(typeof (Assembly));
			}
		}
	}
}