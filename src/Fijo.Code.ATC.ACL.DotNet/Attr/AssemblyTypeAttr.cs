using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Oop;
using Type = System.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[Serializable, DataContract]
	[DisplayName("AssemblyTypeAttr: {Type}")]
	public class AssemblyTypeAttr : OopAttr {
		[DataMember(Order = 1)]
		public AssemblyType Type { get; set; }

		public override string ToString() {
			return string.Format("AssemblyTypeAttr: {0}", Type);
		}
		
		public class Validation : AttrValidation<AssemblyTypeAttr> {
			public override bool IsValidFor(Type codeObjType) {
				return base.IsValidFor(codeObjType) && codeObjType.IsAssignableFrom(typeof (Assembly));
			}
		}
	}
}