using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[Serializable, DataContract]
	[DisplayName("ArrayTypeAttr: <{Dimensions.Count}>")]
	public class ArrayTypeAttr : SpecialTypeAttr {
		[Note("should be at least 1")]
		[DataMember(Order = 1)]
		public ICollection<ArrayDimension> Dimensions { get; set; }

		public ArrayTypeAttr() {
			Type = SpecialTypes.Array;
		}
	}
}