using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Cloning;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Mono.Cecil;
using Newtonsoft.Json;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[PublicAPI]
	public static class Util {
		[NotNull, Pure, PublicAPI]
		public static string GetDisplayNameForTypeParams([NotNull] this IEnumerable<Type> me) {
			return string.Format("<{0}>", string.Join(", ", me.Select(x => x.Name)));
		}
	}

	[Serializable, DataContract]
	[DebuggerDisplay("SpecificGenericTypeAttr: {GenericType.Name}")]
	public class SpecificGenericTypeAttr : OopAttr {
		[DataMember(Order = 1)]
		//[IgnoreDataMember]
		public Types GenericTypeParams { get; set; }
		[DataMember(Order = 2)]
		//[IgnoreDataMember]
		public Type GenericType { get; set; }

		#region for DebuggerDisplay
		#if DEBUG
		[DebuggerHidden, UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never)]
		[JsonIgnore]
		[IgnoreDataMember]
		public string TypesDisplayName { get { return GenericTypeParams.GetDisplayNameForTypeParams(); } }
		#endif
		#endregion

		public override string ToString() {
			return string.Format("SpecificGenericTypeAttr: {0}", GenericType.Name);
		}
	}

	[Serializable, DataContract]
	[DebuggerDisplay("GenericParamsAttr: {TypesDisplayName}")]
	[About("GenericTypeParamsAttr")]
	public class GenericParamsAttr : OopAttr {
		[DataMember(Order = 1)]
		public Types Types { get; set; }
		[DataMember(Order = 2)]
		private Types _specificTypeImpls;
		[IgnoreDataMember]
		[JsonIgnore]
		public Types SpecificTypeImpls { get { return _specificTypeImpls ?? (_specificTypeImpls = new Types()); } set { _specificTypeImpls = value; } }

		#region for DebuggerDisplay
		#if DEBUG
		[DebuggerHidden, UsedImplicitly, EditorBrowsable(EditorBrowsableState.Never)]
		[IgnoreDataMember]
		[JsonIgnore]
		public string TypesDisplayName { get { return Types.GetDisplayNameForTypeParams(); } } 
		#endif
		#endregion

		public override string ToString() {
			return string.Format("GenericParamsAttr: {0}", TypesDisplayName);
		}

		public class Validation : AttrValidation<GenericParamsAttr> {
			private readonly IList<System.Type> _allowedTypes = new[]
			{
				typeof (Type),
				typeof (Member)
			};

			public override bool IsValidFor(System.Type codeObjType) {
				return base.IsValidFor(codeObjType) && _allowedTypes.Any(codeObjType.IsAssignableFrom);
			}
		}
	}

	public interface IGenericTypeService {
		Type CreateSpecificType(ACLContextContainer context, Type genericType, IEnumerable<Type> types);
		void AdjustSpecificTypes(ACLContextContainer context);
		void AdjustSpecificType(Type specificType);
	}

	public class GenericTypeService : IGenericTypeService {
		private readonly IDotNetCodeFactory _codeFactory;
		private readonly ICloneService _cloneService;
		private readonly IPairFactory _pairFactory;

		public GenericTypeService(IDotNetCodeFactory codeFactory, ICloneService cloneService, IPairFactory pairFactory) {
			_codeFactory = codeFactory;
			_cloneService = cloneService;
			_pairFactory = pairFactory;
		}

		public Type CreateSpecificType(ACLContextContainer context, Type genericType, IEnumerable<Type> types) {
			#region PreCondition
			Debug.Assert(genericType != null);
			#endregion
			AddGenericType(context, genericType);
			return CreateSpecificType(genericType, types);
		}

		private void AddGenericType(ACLContextContainer context, Type genericType) {
			#region PreCondition
			Debug.Assert(genericType != null);
			#endregion
			var genericTypes = context.OfType<DotNetACLContext>().Single().GenericTypes;
			lock(genericTypes) genericTypes.Add(genericType);
		}

		private Type CreateSpecificType(Type genericType, IEnumerable<Type> types) {
			var typeCollection = types.ToArray();
			var genericParamsAttr = genericType.OopAttrs.OfType<GenericParamsAttr>().Single();
			lock (genericParamsAttr) {
				var templateTypes = genericParamsAttr.Types;
				#region CheckTemplateType Count
				var templateTypesCount = templateTypes.Count;
				var typeCount = typeCollection.Length;
				if (typeCount != templateTypesCount) throw new ArgumentException(string.Format("You have to use exactly the same count of typeParams as used in the generic type, to create a specific type from the generic type. wantedCount: {0}, actualCount: {1}", templateTypesCount, typeCount), "types");
				#endregion
				if (genericParamsAttr.Types.SequenceEqual(typeCollection)) return genericType;
				return GetOrCreateSpecificType(genericType, genericParamsAttr, typeCollection);
			}
		}

		private Type GetOrCreateSpecificType(Type genericType, GenericParamsAttr genericParamsAttr, IList<Type> typeCollection) {
			var specificTypeImpls = genericParamsAttr.SpecificTypeImpls;
			var specificType = specificTypeImpls.SingleOrDefault(x => x.OopAttrs.OfType<SpecificGenericTypeAttr>().Single().GenericTypeParams.SequenceEqual(typeCollection));
			if (specificType != null) return specificType;
			specificType = CreateSpecificType(genericType, specificTypeImpls, typeCollection);
			specificTypeImpls.Add(specificType);
			return specificType;
		}

		private Type CreateSpecificType(Type genericType, Types specificTypeImpls, IEnumerable<Type> typeCollection) {
			var specificType = new Type
			{
				Name = string.Format("{0}�{1}", genericType.Name, specificTypeImpls.Count + 1),
			};
			specificType.OopAttrs.Add(GetSpecificGenericTypeAttr(genericType, typeCollection));
			return specificType;
		}

		private SpecificGenericTypeAttr GetSpecificGenericTypeAttr(Type genericType, IEnumerable<Type> typeCollection) {
			return new SpecificGenericTypeAttr {GenericType = genericType, GenericTypeParams = new Types(typeCollection)};
		}


		public void AdjustSpecificTypes(ACLContextContainer context) {
			GetSpecificTypes(context).ForEach(AdjustSpecificType);
		}

		private IEnumerable<Type> GetSpecificTypes(ACLContextContainer context) {
			var genericTypes = context.OfType<DotNetACLContext>().Single().GenericTypes;
			lock(genericTypes) return genericTypes.SelectMany(x => x.OopAttrs.OfType<GenericParamsAttr>().Single().SpecificTypeImpls).Execute();
		}

		public void AdjustSpecificType(Type specificType) {
			var specificTypeAttr = specificType.OopAttrs.OfType<SpecificGenericTypeAttr>().Single();
			var genericType = specificTypeAttr.GenericType;
			var genericTypeAttr = genericType.OopAttrs.OfType<GenericParamsAttr>().Single();
			var typeReplacements = GetTypeReplacements(genericTypeAttr.Types, specificTypeAttr.GenericTypeParams);

			specificType.Members = GetMembers(genericType, specificType, typeReplacements);
		}

		private IDictionary<Type, Type> GetTypeReplacements(IEnumerable<Type> genericTypeParams, IList<Type> specificTypeParams) {
			return genericTypeParams.Select((x, i) => _pairFactory.KeyValuePair(x, specificTypeParams[i])).ToDict();
		}

		private Members GetMembers(Type genericType, Type specificType, IDictionary<Type, Type> typeReplacements) {
			return new Members(genericType.Members.Select(x => GetMember(specificType, typeReplacements, x)));
		}

		#region GetMember
		private Member GetMember(Type specificType, IDictionary<Type, Type> typeReplacements, Member genericMember) {
			var specificMember = _cloneService.CloneDyn(genericMember);
			var specificMethod = specificMember as Method;
			if (specificMethod != null) AdjustMethod(specificMethod, (Method) genericMember, specificType, typeReplacements);
			AdjustMember(specificMember, genericMember, typeReplacements);
			return specificMember;
		}

		private void AdjustMember(Member specificMember, Member genericMember, IDictionary<Type, Type> typeReplacements) {
			specificMember.Type = GetReplacedType(genericMember.Type, typeReplacements);
		}

		private void AdjustMethod(Method specificMethod, Method genericMethod, Type specificType, IDictionary<Type, Type> typeReplacements) {
			if (genericMethod.Name == ".ctor") specificMethod.Type = specificType;
			specificMethod.ReturnType = GetReturnType(genericMethod, typeReplacements);
			specificMethod.Parameters = new Parameters(GetParamters(genericMethod.Parameters, typeReplacements));
		}
		#endregion

		private ReturnType GetReturnType(Method genericMethod, IDictionary<Type, Type> typeReplacements) {
			return _codeFactory.CreateReturnType(GetReplacedType(genericMethod.ReturnType.Type, typeReplacements));
		}

		private IEnumerable<Parameter> GetParamters(IEnumerable<Parameter> parameters, IDictionary<Type, Type> typeReplacements) {
			return parameters.Select(parameter => _codeFactory.CreateParameter(parameter.Name, GetReplacedType(parameter.Type, typeReplacements)));
		}

		private Type GetReplacedType(Type type, IDictionary<Type, Type> typeReplacements) {
			var genericParamsAttr = type.OopAttrs.OfType<GenericParamsAttr>().SingleOrDefault();
			//if (genericParamsAttr != null) return CreateSpecificType(type, genericParamsAttr.Types.AsParallel().WithExecutionMode(ParallelExecutionMode.ForceParallelism).Select(x => GetReplacedType(x, typeReplacements)));
			return type;
		}
	}
}