using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.DotNet.Attr {
	[Serializable, DataContract]
	[AboutName("Param", "Parameter")]
	[Desc("A �flag� for all generic types that marks them as generic type")]
	public class GenericTypeParamAttr : OopAttr, IVirtualAttr {
		public class Validation : AttrValidation<GenericTypeParamAttr> {
			[Note("Wenn ein generischer type generische Typen constrains habe k�nnte oder von einem generischen Typen erben kann, kann es m�glich sein dass der generische Typ in einem generischen Typen verwendet wird.")]
			public override bool IsValidFor(Type codeObjType) {
				return base.IsValidFor(codeObjType) && codeObjType.IsAssignableFrom(typeof (Model.Oop.Type));
			}
		}
	}
}