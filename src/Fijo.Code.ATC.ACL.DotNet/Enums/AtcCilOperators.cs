using System;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.DotNet.Enums {
	[Serializable, DataContract]
	public enum AtcCilOperators {
		Not,
		Equal,
		NotEqual,
		Greater,
		GreaterUnsigned,
		GreaterThan,
		GreaterThanUnsigned,
		Less,
		LessUnsigned,
		LessThan,
		LessThanUnsigned
	}
}