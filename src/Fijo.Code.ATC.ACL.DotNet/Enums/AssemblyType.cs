using System;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.DotNet.Enums {
	[Serializable, DataContract]
	public enum AssemblyType {
		[About("Container")]
		Assembly,
		Module
	}
}