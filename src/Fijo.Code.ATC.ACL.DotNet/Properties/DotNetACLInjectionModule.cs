﻿using Fijo.Code.ATC.ACL.AdvancedOop.Properties;
using Fijo.Code.ATC.ACL.Common.Properties;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Mapping;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.CecilContrib.Properties;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;
using Mono.Cecil;
using ArrayDimension = Fijo.Code.ATC.ACL.DotNet.Dto.ArrayDimension;

namespace Fijo.Code.ATC.ACL.DotNet.Properties {
	[PublicAPI]
	public class DotNetACLInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
			kernel.Load(new AdvancedOopACLInjectionModule());
			kernel.Load(new CecilContribEqualityComparerInjectionModule());
		}
		
		public override void OnLoad(IKernel kernel) {
			kernel.Bind<AttrValidation>().To<AssemblyNameAttr.Validation>().InSingletonScope();
			kernel.Bind<AttrValidation>().To<GenericParamsAttr.Validation>().InSingletonScope();
			kernel.Bind<AttrValidation>().To<GenericTypeParamAttr.Validation>().InSingletonScope();
			kernel.Bind<AttrValidation>().To<SpecialTypeAttr.Validation>().InSingletonScope();
			kernel.Bind<AttrValidation>().To<AssemblyTypeAttr.Validation>().InSingletonScope();

			kernel.Bind<IMapping<Mono.Cecil.ArrayDimension, ArrayDimension>>().To<ArrayDimensionMapping>().InSingletonScope();
			kernel.Bind<IMapping<ArrayDimension, Mono.Cecil.ArrayDimension>>().To<CecilArrayDimensionMapping>().InSingletonScope();
			kernel.Bind<IMapping<AssemblyName, AssemblyNameDefinition>>().To<AssemblyNameDefinitionMapping>().InSingletonScope();
			kernel.Bind<IMapping<AssemblyNameDefinition, AssemblyName>>().To<AssemblyNameMapping>().InSingletonScope();

			kernel.Bind<IDotNetDefaultFactory>().To<DotNetDefaultFactory>().InSingletonScope();
			kernel.Bind<IDotNetACLContextFactory>().To<DotNetACLContextFactory>().InSingletonScope();

			kernel.Bind<IDotNetACLContextService>().To<DotNetACLContextService>().InSingletonScope();

			kernel.Bind<IDotNetAttributeFactory>().To<DotNetAttributeFactory>().InSingletonScope();

			kernel.Bind<IGenericTypeService>().To<GenericTypeService>().InSingletonScope();

			kernel.Bind<ICilTypeSystemService>().To<CilTypeSystemService>().InSingletonScope();

			kernel.Bind<IDotNetCodeFactory>().To<DotNetCodeFactory>().InSingletonScope();
		}

		public override void Init(IKernel kernel) {
			AssemblyName.Init();
		}
	}
}