using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface {
	public interface IDotNetDefaultFactory {
		Type GenericArrayType(IEnumerable<ArrayDimension> dimensions);
		Type GenericByReferenceType();
		Type GenericPointerType();
		Type GenericPinnedType();
		[Note("provides a type that targets to a method")]
		Type FunctionPointerType(Method method);
		[Note("provides additional constain data to as contract for �type�. For example �System.Runtime.CompilerServices.IsLong�.")]
		Type ModifierType(Type type, Type constainType, bool isOptional);
	}
}