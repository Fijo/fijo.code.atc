﻿using System.Collections.Generic;
using Fijo.Code.ATC.ACL.AdvancedOop.Dto;
using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Data;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.ACL.Model.Oop;
using Expression = Fijo.Code.ATC.ACL.Model.Expressions.Expression;
using GotoExpression = Fijo.Code.ATC.ACL.Model.Expressions.Statements.GotoExpression;
using SwitchExpression = Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition.SwitchExpression;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface {
	public interface IDotNetCodeFactory {
		Assembly CreateAssembly(AssemblyName name, Assemblies module);
		Assembly CreateModule(string name, Assemblies assemblies, Namespaces namespaces = null);
		Namespace CreateNamespace(string name, Assembly assembly = null, Types types = null);
		Type CreateType(Modifier modifier, string name, Namespace ns = null, Members members = null, Types baseTypes = null);
		Field CreateField(string name, Type type = null);
		Method CreateMethod(string name, Parameters parameters = null, Expression body = null, ReturnType returnType = null);
		Variable CreateVariable(string name, Type type = null);
		Parameter CreateParameter(string name, Type type = null);
		ReturnType CreateReturnType(Type type = null);
		Attribute Attribute(Type type = null, CallExpression callExpression = null);
		CallExpression CreateCallExpression(Method target, Expressions argumentInstances);
		CaseExpression CreateCaseExpression(Expression key, Expression body);
		ContainerExpression CreateContainerExpression(IEnumerable<Expression> expressions = null);
		DoWhileExpression CreateDoWhileExpression(Expression condition, Expression body);
		FieldExpression CreateFieldExpression(Field target, Expression instance = null);
		ForExpression CreateForExpression(Expression initialization, Expression condition, Expression afterThroughtOperation, Expression body);
		IfExpression CreateIfExpression(Expression condition, Expression trueBody, Expression falseBody = null);
		ReturnExpression CreateReturnExpression(Expression value);
		ThrowExpression CreateThrowExpression(Expression value);
		TryExpression CreateTryExpression(Expression body, Catches catches, Expression @finally);
		CatchExpression CreateCatchExpression(Type exceptionType, Expression body);
		SetExpression CreateSetExpression(Expression target, Expression value);
		SimpleForExpression CreateSimpleForExpression(Expression condition, Expression afterThroughtOperation, Expression body);
		SwitchExpression CreateSwitchExpression(Expression input, Cases cases);
		GotoExpression GotoExpression(Expression target);
		ValueExpression CreateValueExpression(Instance instance);
		VariableExpression CreateVariableExpression(Variable variable);
		WhileExpression CreateWhileExpression(Expression condition, Expression body);
		Instance CreateInstance(Type type, MemberInstances memberInstances);
		Instance CreateNativeInstance(Type type, object value);
		MemberInstance CreateMemberInstance(Member member, Instance instance);
		VariableInstance CreateVariableInstance(Variable variable, Instance instance);
	}
}