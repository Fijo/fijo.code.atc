using Fijo.Code.ATC.ACL.DotNet.Context;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface {
	public interface IDotNetACLContextFactory {
		DotNetACLContext Create();
	}
}