using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Mono.Cecil;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface {
	public interface IDotNetAttributeFactory {
		AssemblyNameAttr AssemblyNameAttr(AssemblyName assemblyName);
		AssemblyTypeAttr AssemblyTypeAttr(AssemblyType type);
	}
}