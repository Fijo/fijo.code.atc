﻿using System.Collections.Generic;
using Fijo.Code.ATC.ACL.AdvancedOop.Dto;
using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Data;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.ACL.Model.Oop;
using Mono.Cecil;
using Expression = Fijo.Code.ATC.ACL.Model.Expressions.Expression;
using GotoExpression = Fijo.Code.ATC.ACL.Model.Expressions.Statements.GotoExpression;
using SwitchExpression = Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition.SwitchExpression;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory {
	public class DotNetCodeFactory : IDotNetCodeFactory {
		private readonly ICodeFactory _codeFactory;
		private readonly IDotNetAttributeFactory _attributeFactory;

		public DotNetCodeFactory(ICodeFactory codeFactory, IDotNetAttributeFactory attributeFactory) {
			_codeFactory = codeFactory;
			_attributeFactory = attributeFactory;
		}
		#region Implementation of IDotNetCodeFactory
		public Assembly CreateAssembly(AssemblyName name, Assemblies module) {
			var assembly = _codeFactory.CreateAssembly(name.Name, module);
			assembly.OopAttrs.AddRange(GetAssemblyOopAttrs(name));
			return assembly;
		}

		private IEnumerable<OopAttr> GetAssemblyOopAttrs(AssemblyName assemblyName) {
			yield return _attributeFactory.AssemblyNameAttr(assemblyName);
			yield return _attributeFactory.AssemblyTypeAttr(AssemblyType.Module);
		}
		
		public Assembly CreateModule(string name, Assemblies assemblies, Namespaces namespaces = null) {
			var module = _codeFactory.CreateAssembly(name, assemblies, namespaces);
			module.OopAttrs.AddRange(GetModuleOopAttrs());
			return module;
		}

		private IEnumerable<OopAttr> GetModuleOopAttrs() {
			yield return _attributeFactory.AssemblyTypeAttr(AssemblyType.Module);
		}

		public Namespace CreateNamespace(string name, Assembly assembly = null, Types types = null) {
			return _codeFactory.CreateNamespace(name, assembly, types);
		}

		public Type CreateType(Modifier modifier, string name, Namespace ns = null, Members members = null, Types baseTypes = null) {
			var type = _codeFactory.CreateType(name, ns, members, baseTypes);
			type.OopAttrs.AddRange(GetTypeOopAttrs(modifier));
			return type;
		}

		private IEnumerable<OopAttr> GetTypeOopAttrs(Modifier modifier) {
			yield return GetModifierAttr(modifier);
		}

		private ModifierAttr GetModifierAttr(Modifier modifier) {
			return new ModifierAttr {Modifier = modifier};
		}

		public Field CreateField(string name, Type type = null) {
			return _codeFactory.CreateField(name, type);
		}

		public Method CreateMethod(string name, Parameters parameters = null, Expression body = null, ReturnType returnType = null) {
			return _codeFactory.CreateMethod(name, parameters, body, returnType);
		}

		public Variable CreateVariable(string name, Type type = null) {
			return _codeFactory.CreateVariable(name, type);
		}

		public Parameter CreateParameter(string name, Type type = null) {
			return _codeFactory.CreateParameter(name, type);
		}

		public ReturnType CreateReturnType(Type type = null) {
			return _codeFactory.CreateReturnType(type);
		}

		public Attribute Attribute(Type type = null, CallExpression callExpression = null) {
			return new Attribute {Type = type, CallExpression = callExpression};
		}

		public CallExpression CreateCallExpression(Method target, Expressions argumentInstances) {
			return _codeFactory.CreateCallExpression(target, argumentInstances);
		}

		public CaseExpression CreateCaseExpression(Expression key, Expression body) {
			return _codeFactory.CreateCaseExpression(key, body);
		}

		public ContainerExpression CreateContainerExpression(IEnumerable<Expression> expressions = null) {
			return _codeFactory.CreateContainerExpression(expressions);
		}

		public DoWhileExpression CreateDoWhileExpression(Expression condition, Expression body) {
			return _codeFactory.CreateDoWhileExpression(condition, body);
		}

		public FieldExpression CreateFieldExpression(Field target, Expression instance = null) {
			return _codeFactory.CreateFieldExpression(target, instance);
		}

		public ForExpression CreateForExpression(Expression initialization, Expression condition, Expression afterThroughtOperation, Expression body) {
			return _codeFactory.CreateForExpression(initialization, condition, afterThroughtOperation, body);
		}

		public IfExpression CreateIfExpression(Expression condition, Expression trueBody, Expression falseBody = null) {
			return _codeFactory.CreateIfExpression(condition, trueBody, falseBody);
		}

		public ReturnExpression CreateReturnExpression(Expression value) {
			return _codeFactory.CreateReturnExpression(value);
		}

		public ThrowExpression CreateThrowExpression(Expression value) {
			return _codeFactory.CreateThrowExpression(value);
		}

		public TryExpression CreateTryExpression(Expression body, Catches catches, Expression @finally) {
			return _codeFactory.CreateTryExpression(body, catches, @finally);
		}

		public CatchExpression CreateCatchExpression(Type exceptionType, Expression body) {
			return _codeFactory.CreateCatchExpression(exceptionType, body);
		}

		public SetExpression CreateSetExpression(Expression target, Expression value) {
			return _codeFactory.CreateSetExpression(target, value);
		}

		public SimpleForExpression CreateSimpleForExpression(Expression condition, Expression afterThroughtOperation, Expression body) {
			return _codeFactory.CreateSimpleForExpression(condition, afterThroughtOperation, body);
		}

		public SwitchExpression CreateSwitchExpression(Expression input, Cases cases) {
			return _codeFactory.CreateSwitchExpression(input, cases);
		}

		public GotoExpression GotoExpression(Expression target) {
			return _codeFactory.GotoExpression(target);
		}

		public ValueExpression CreateValueExpression(Instance instance) {
			return _codeFactory.CreateValueExpression(instance);
		}

		public VariableExpression CreateVariableExpression(Variable variable) {
			return _codeFactory.CreateVariableExpression(variable);
		}

		public WhileExpression CreateWhileExpression(Expression condition, Expression body) {
			return _codeFactory.CreateWhileExpression(condition, body);
		}

		public Instance CreateInstance(Type type, MemberInstances memberInstances) {
			return _codeFactory.CreateInstance(type, memberInstances);
		}

		public Instance CreateNativeInstance(Type type, object value) {
			var instance = _codeFactory.CreateInstance(type, null);
			instance.DataAttrs.Add(new NativeInstanceAttr {Value = value});
			return instance;
		}

		public MemberInstance CreateMemberInstance(Member member, Instance instance) {
			return _codeFactory.CreateMemberInstance(member, instance);
		}

		public VariableInstance CreateVariableInstance(Variable variable, Instance instance) {
			return _codeFactory.CreateVariableInstance(variable, instance);
		}
		#endregion
	}
}