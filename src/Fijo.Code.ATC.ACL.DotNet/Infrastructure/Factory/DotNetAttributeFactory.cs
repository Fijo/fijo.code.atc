using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory {
	public class DotNetAttributeFactory : IDotNetAttributeFactory {
		public AssemblyNameAttr AssemblyNameAttr(AssemblyName assemblyName) {
			return new AssemblyNameAttr {Name = assemblyName};
		}

		public AssemblyTypeAttr AssemblyTypeAttr(AssemblyType type) {
			return new AssemblyTypeAttr {Type = type};
		}
	}
}