using System;
using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Oop;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory {
	public class DotNetDefaultFactory : IDotNetDefaultFactory {
		#region GenericArrayType
		public Type GenericArrayType(IEnumerable<ArrayDimension> dimensions) {
			var genericArrayType = new Type{Name = "[]"};
			genericArrayType.BaseTypes = new Types();
			genericArrayType.Members = new Members();
			genericArrayType.Namespace = new Namespace();
			genericArrayType.OopAttrs.AddRange(GetGenericArrayTypeOopAttrs(dimensions));
			return genericArrayType;
		}

		private IEnumerable<OopAttr> GetGenericArrayTypeOopAttrs(IEnumerable<ArrayDimension> dimensions) {
			var dimensionCollection = dimensions.Execute();
			#region PreCondition
			if(dimensionCollection.Count <= 0) throw new ArgumentOutOfRangeException("dimensions", dimensions, "have to be at least 1");
			#endregion
			yield return new ArrayTypeAttr {Dimensions = dimensionCollection};
			yield return GetGenericArrayTypeOopAttrs();
		}
		#endregion
		
		#region GenericSimpleSpecialType
		#region GenericByReferenceType
		public Type GenericByReferenceType() {
			var byReferenceType = new Type{Name = "&"};
			byReferenceType.BaseTypes = new Types();
			byReferenceType.Members = new Members();
			byReferenceType.Namespace = new Namespace();
			byReferenceType.OopAttrs.AddRange(GetGenericSimpleSpecialTypeOopAttrs(SpecialTypes.ByReference));
			return byReferenceType;
		}
		#endregion
		
		#region GenericByReferenceType
		public Type GenericPointerType() {
			var pointerType = new Type{Name = "*"};
			pointerType.BaseTypes = new Types();
			pointerType.Members = new Members();
			pointerType.Namespace = new Namespace();
			pointerType.OopAttrs.AddRange(GetGenericSimpleSpecialTypeOopAttrs(SpecialTypes.Pointer));
			return pointerType;
		}
		#endregion

		#region GenericPinnedType
		public Type GenericPinnedType() {
			var pointerType = new Type{Name = "&"};
			pointerType.BaseTypes = new Types();
			pointerType.Members = new Members();
			pointerType.Namespace = new Namespace();
			pointerType.OopAttrs.AddRange(GetGenericSimpleSpecialTypeOopAttrs(SpecialTypes.Pinned));
			return pointerType;
		}
		#endregion

		private IEnumerable<OopAttr> GetGenericSimpleSpecialTypeOopAttrs(SpecialTypes type) {
			yield return new SpecialTypeAttr {Type = type};
			yield return GetGenericArrayTypeOopAttrs();
		}
		#endregion
		
		#region FunctionPointerType
		public Type FunctionPointerType(Method method) {
			var pointerType = new Type{Name = "FunctionPointer"};
			pointerType.OopAttrs.AddRange(GetFunctionPointerTypeOopAttrs(method));
			return pointerType;
		}

		private IEnumerable<OopAttr> GetFunctionPointerTypeOopAttrs(Method method) {
			yield return new FunctionPointerTypeAttr {Target = method};
		}
		#endregion

		#region ModifierType
		public Type ModifierType(Type type, Type constainType, bool isOptional) {
			var modifierType = new Type {Name = "ModifierType"};
			modifierType.BaseTypes.Add(type);
			modifierType.OopAttrs.AddRange(GetModifierTypeOopAttrs(constainType, isOptional));
			return modifierType;
		}

		private IEnumerable<OopAttr> GetModifierTypeOopAttrs(Type constainType, bool isOptional) {
			yield return new ModifierTypeAttr {ConstrainModifierType = constainType, IsOptional = isOptional};
		}
		#endregion

		private GenericParamsAttr GetGenericArrayTypeOopAttrs() {
			return new GenericParamsAttr {Types = new Types(GetGenericTypes())};
		}

		private IEnumerable<Type> GetGenericTypes() {
			var types = new Type {Name = "T"};
			types.OopAttrs.Add(new GenericTypeParamAttr());
			yield return types;
		}
	}
}