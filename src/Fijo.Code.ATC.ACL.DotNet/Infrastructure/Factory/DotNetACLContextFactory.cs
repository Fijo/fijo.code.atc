using System;
using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory {
	public class DotNetACLContextFactory : IDotNetACLContextFactory {
		private readonly IDotNetDefaultFactory _defaultFactory;
		private readonly IEqualityComparer<IEnumerable<ArrayDimension>> _equalityComparer;

		public DotNetACLContextFactory(IEqualityComparerFactory equalityComparerFactory, IDotNetDefaultFactory defaultFactory) {
			_defaultFactory = defaultFactory;
			_equalityComparer = equalityComparerFactory.Sequential<ArrayDimension>();
		}
		#region Implementation of IDotNetACLContextFactory
		public DotNetACLContext Create() {
			return new DotNetACLContext
			{
				ArrayTypes = new Dictionary<ArrayDimension[], Type>(_equalityComparer),
				ByReferenceType = _defaultFactory.GenericByReferenceType(),
				PointerType = _defaultFactory.GenericPointerType(),
				PinnedType = _defaultFactory.GenericPinnedType(),
				FunctionPointerType = new Dictionary<Method, Type>(),
				ModifierType = new Dictionary<ModifierTypeRequest, Type>(),
				GenericTypes = new HashSet<Type>()
			};
		}
		#endregion
	}
}