﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.CecilContrib.Enums;
using Fijo.Code.CecilContrib.Extentions;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;
using Mono.Cecil;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service {
	[PublicAPI]
	public class CilTypeSystemService : ICilTypeSystemService {
		#region Implementation of ICilTypeSystem
		public void Adjust(ACLContextContainer context, ModuleDefinition moduleDef, System.Func<TypeReference, Type> getType) {
			context.OfType<DotNetACLContext>().Single().TypeSystem = new CilTypeSystem(GetTypeDict(moduleDef, getType));
		}

		private IDictionary<CoreTypes, Type> GetTypeDict(ModuleDefinition moduleDef, System.Func<TypeReference, Type> getType) {
			return GetTypes().ToDict(x => x, x => getType(moduleDef.GetCoreType(x)));
		}

		private IEnumerable<CoreTypes> GetTypes() {
			return Enum.GetValues(typeof (CoreTypes)).Cast<CoreTypes>();
		}

		public CilTypeSystem Get(ACLContextContainer context) {
			var typeSystem = context.OfType<DotNetACLContext>().Single().TypeSystem;
			Debug.Assert(typeSystem != null);
			return typeSystem;
		}
		#endregion
	}
}