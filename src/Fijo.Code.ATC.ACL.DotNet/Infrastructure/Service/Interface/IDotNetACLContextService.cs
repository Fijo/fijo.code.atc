using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface {
	public interface IDotNetACLContextService {
		Type GetArrayType(DotNetACLContext context, IEnumerable<ArrayDimension> dimensions);
		Type GetRequiredModifierType(DotNetACLContext context, Type type, Type requiredType, bool isOptional);
		Type GetFunctionPointerType(DotNetACLContext context, Method method);
	}
}