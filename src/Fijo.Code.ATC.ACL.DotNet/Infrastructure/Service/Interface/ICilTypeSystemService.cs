﻿using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.CecilContrib.Enums;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface {
	[About("ICoreTypes"), Service]
	public interface ICilTypeSystemService {
		void Adjust(ACLContextContainer context, ModuleDefinition moduleDef, System.Func<TypeReference, Type> getType);
		CilTypeSystem Get(ACLContextContainer context);
	}
}