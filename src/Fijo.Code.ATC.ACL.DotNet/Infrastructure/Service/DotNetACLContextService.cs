﻿using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using ArrayDimension = Fijo.Code.ATC.ACL.DotNet.Dto.ArrayDimension;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service {
	public class DotNetACLContextService : IDotNetACLContextService {
		private readonly IDotNetDefaultFactory _defaultFactory;

		public DotNetACLContextService(IDotNetDefaultFactory defaultFactory) {
			_defaultFactory = defaultFactory;
		}

		#region Implementation of IDotNetACLContextService
		public Type GetArrayType(DotNetACLContext context, IEnumerable<ArrayDimension> dimensions) {
			var dimensionCollection = dimensions.ToArray();
			return context.ArrayTypes.GetOrCreate(dimensionCollection, () => _defaultFactory.GenericArrayType(dimensionCollection));
		}

		public Type GetRequiredModifierType(DotNetACLContext context, Type type, Type requiredType, bool isOptional) {
			return context.ModifierType.GetOrCreate(new ModifierTypeRequest(type, requiredType, isOptional), () => _defaultFactory.ModifierType(type, requiredType, isOptional));
		}

		public Type GetFunctionPointerType(DotNetACLContext context, Method method) {
			return context.FunctionPointerType.GetOrCreate(method, () => _defaultFactory.FunctionPointerType(method));
		}
		#endregion
	}
}