﻿using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Mapping {
	public class AssemblyNameDefinitionMapping : IMapping<AssemblyName, AssemblyNameDefinition> {
		#region Implementation of IMapping<in AssemblyName,out AssemblyNameDefinition>
		public AssemblyNameDefinition Map(AssemblyName source) {
			return source.InnerNameDef;
		}
		#endregion
	}
}