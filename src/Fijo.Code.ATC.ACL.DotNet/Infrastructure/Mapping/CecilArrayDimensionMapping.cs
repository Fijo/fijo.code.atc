using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Infrastructure.DesignPattern.Mapping;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Mapping {
	public class CecilArrayDimensionMapping : IMapping<ArrayDimension,Mono.Cecil.ArrayDimension> {
		#region Implementation of IMapping<in ArrayDimension,out ArrayDimension>
		public Mono.Cecil.ArrayDimension Map(ArrayDimension source) {
			return new Mono.Cecil.ArrayDimension(source.LowerBound, source.UpperBound);
		}
		#endregion
	}
}