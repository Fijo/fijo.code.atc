﻿using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Mapping {
	public class AssemblyNameMapping : IMapping<AssemblyNameDefinition, AssemblyName> {
		#region Implementation of IMapping<in AssemblyNameDefinition,out AssemblyName>
		public AssemblyName Map(AssemblyNameDefinition source) {
			return new AssemblyName(source);
		}
		#endregion
	}
}