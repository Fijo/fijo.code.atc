﻿using Fijo.Infrastructure.DesignPattern.Mapping;
using ArrayDimension = Fijo.Code.ATC.ACL.DotNet.Dto.ArrayDimension;

namespace Fijo.Code.ATC.ACL.DotNet.Infrastructure.Mapping {
	public class ArrayDimensionMapping : IMapping<Mono.Cecil.ArrayDimension,ArrayDimension> {
		#region Implementation of IMapping<in ArrayDimension,out ArrayDimension>
		public ArrayDimension Map(Mono.Cecil.ArrayDimension source) {
			return new ArrayDimension(source.LowerBound, source.UpperBound);
		}
		#endregion
	}
}