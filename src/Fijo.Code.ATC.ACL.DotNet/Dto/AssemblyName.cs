using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using Mono.Cecil;
using Newtonsoft.Json;

namespace Fijo.Code.ATC.ACL.DotNet.Dto {
	[Serializable, DataContract]
	public class AssemblyName {
		protected static IEqualityComparer<AssemblyNameDefinition> AssemblyNameDefinitionEqualityComparer;

		internal static void Init() {
			AssemblyNameDefinitionEqualityComparer = Kernel.Resolve<IEqualityComparer<AssemblyNameDefinition>>();
		}

		[NonSerialized, IgnoreDataMember, JsonIgnore]
		internal protected readonly AssemblyNameDefinition InnerNameDef;

		protected AssemblyName() {}

		public AssemblyName(AssemblyNameDefinition innerNameDef) {
			InnerNameDef = innerNameDef;
		}

		public AssemblyAttributes Attributes { get { return InnerNameDef.Attributes; } set { InnerNameDef.Attributes = value; } }
		public string Name { get { return InnerNameDef.Name; } set { InnerNameDef.Name = value; } }
		public string Culture { get { return InnerNameDef.Culture; } set { InnerNameDef.Culture = value; } }
		public string FullName { get { return InnerNameDef.FullName; } }
		public byte[] Hash { get { return InnerNameDef.Hash; } set { InnerNameDef.Hash = value; } }
		public AssemblyHashAlgorithm HashAlgorithm { get { return InnerNameDef.HashAlgorithm; } set { InnerNameDef.HashAlgorithm = value; } }
		public byte[] PublicKey { get { return InnerNameDef.PublicKey; } set { InnerNameDef.PublicKey = value; } }
		public byte[] PublicKeyToken { get { return InnerNameDef.PublicKeyToken; } set { InnerNameDef.PublicKeyToken = value; } }
		public Version Version { get { return InnerNameDef.Version; } set { InnerNameDef.Version = value; } }

		#region Equality
		protected bool Equals(AssemblyName other) {
			#region PreCondition
			Debug.Assert(AssemblyNameDefinitionEqualityComparer != null, "Init should be called before using this");
			#endregion
			return AssemblyNameDefinitionEqualityComparer.Equals(InnerNameDef, other.InnerNameDef);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<AssemblyName>(obj, Equals);
		}

		public override int GetHashCode() {
			#region PreCondition
			Debug.Assert(AssemblyNameDefinitionEqualityComparer != null, "Init should be called before using this");
			#endregion
			return AssemblyNameDefinitionEqualityComparer.GetHashCode(InnerNameDef);
		}
		#endregion
	}
}