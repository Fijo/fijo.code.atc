using System;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Fijo.Code.ATC.ACL.DotNet.Dto {
	[Serializable, DataContract]
	public class ArrayDimension {
		[DataMember(Order = 1)]
		public int? LowerBound { get; private set; }
		[DataMember(Order = 2)]
		public int? UpperBound { get; private set; }
		[IgnoreDataMember]
		[JsonIgnore]
		public bool IsSized { get { return LowerBound.HasValue || UpperBound.HasValue; } }

		[UsedImplicitly]
		[Note("only used for Serialisation")]
		protected ArrayDimension() {}

		public ArrayDimension(int? lowerBound = null, int? upperBound = null) {
			LowerBound = lowerBound;
			UpperBound = upperBound;
		}

		public override string ToString() {
			return IsSized
				       ? string.Format("{0}..{1}", LowerBound, UpperBound)
				       : string.Empty;
		}
	}
}