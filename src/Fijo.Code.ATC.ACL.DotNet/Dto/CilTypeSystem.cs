using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Code.CecilContrib.Enums;
using JetBrains.Annotations;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Dto {
	[Serializable, DataContract]
	[About("CilCoreTypes")]
	public class CilTypeSystem : Dictionary<CoreTypes, Type> {
		public CilTypeSystem() {}
		public CilTypeSystem(IDictionary<CoreTypes, Type> dictionary) : base(dictionary) {}
		[UsedImplicitly]
		public CilTypeSystem(SerializationInfo info, StreamingContext context) : base(info, context) {}
	}
}