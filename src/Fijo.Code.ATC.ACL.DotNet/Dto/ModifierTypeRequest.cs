using System;
using System.Runtime.Serialization;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Dto {
	[Serializable, DataContract]
	public class ModifierTypeRequest {
		[DataMember(Order = 1)]
		public readonly Type Type;
		[DataMember(Order = 2)]
		public readonly Type RequiredType;
		[DataMember(Order = 3)]
		public readonly bool IsOptional;

		public ModifierTypeRequest(Type type, Type requiredType, bool isOptional) {
			Type = type;
			RequiredType = requiredType;
			IsOptional = isOptional;
		}

		#region Equality
		protected bool Equals(ModifierTypeRequest other) {
			return Type.Equals(other.Type) && RequiredType.Equals(other.RequiredType) && IsOptional.Equals(other.IsOptional);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<ModifierTypeRequest>(obj, Equals);
		}

		public override int GetHashCode() {
			return Type.GetHashCode().GetHashCodeAlgorithm(RequiredType.GetHashCode(), IsOptional.GetHashCode());
		}
		#endregion
	}
}