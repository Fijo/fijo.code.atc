﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.CecilContrib.Enums;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.DotNet.Context {
	[Serializable, DataContract]
	public class DotNetACLContext : ACLContext {
		[Note("SpecialType")]
		[DataMember(Order = 1)]
		public IDictionary<ArrayDimension[], Type> ArrayTypes { get; set; }
		[Note("SpecialType")]
		[DataMember(Order = 2)]
		public Type ByReferenceType { get; set; }
		[Note("SpecialType")]
		[DataMember(Order = 3)]
		public Type PointerType { get; set; }
		[Note("PinnedType")]
		[DataMember(Order = 4)]
		public Type PinnedType { get; set; }
		[Note("SpecialType")]
		[DataMember(Order = 5)]
		public IDictionary<Method, Type> FunctionPointerType { get; set; }
		[Note("SpecialType")]
		[DataMember(Order = 6)]
		public IDictionary<ModifierTypeRequest, Type> ModifierType { get; set; }
		[Note("Only the ones that from that have been created specific Type(s).")]
		[DataMember(Order = 7)]
		public HashSet<Type> GenericTypes { get; set; }
		[About("CoreTypes")]
		[DataMember(Order = 8)]
		public CilTypeSystem TypeSystem { get; set; }
		[About("AtcCoreAssembly")]
		[DataMember(Order = 9)]
		public Assembly CoreAssembly { get; set; }
		[About("CoreOperatorMethods")]
		[DataMember(Order = 10)]
		public IDictionary<AtcCilOperators, Method> OperatorMethods { get; set; }


		
		[DataMember(Order = 11)]
		public IDictionary<IEnumerable<string>, Type> Content { get; set; }
	}
}