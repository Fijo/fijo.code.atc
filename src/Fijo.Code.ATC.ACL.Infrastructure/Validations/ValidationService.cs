﻿using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Dtos;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Interfaces;
using Fijo.Infrastructure.Model.Validation;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Conditional.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations {
	public class ValidationService : IValidationService {
		private readonly IChildrenService _childrenService;
		private readonly IGAllHandlingProcessor<ValidationSource, IEnumerable<ValidationResult>> _validationHandlers;
		private readonly IValidationObjFactory _validationObjFactory;

		public ValidationService(IChildrenService childrenService, IGAllHandlingProcessor<ValidationSource, IEnumerable<ValidationResult>> validationHandlers) {
			_childrenService = childrenService;
			_validationHandlers = validationHandlers;
		}

		#region Implementation of IValidationService
		public IEnumerable<ValidationResult> GetValidationResult(Model.Base.Code code) {
			return InternGetValidationResult(code);
		}

		private IEnumerable<ValidationResult> InternGetValidationResult(Model.Base.Code code) {
			return _validationHandlers.Get(GetValidationSource(code)).Flat();
		}

		private ValidationSource GetValidationSource(Model.Base.Code code) {
			return new ValidationSource {Code = code};
		}
		#endregion
	}
}