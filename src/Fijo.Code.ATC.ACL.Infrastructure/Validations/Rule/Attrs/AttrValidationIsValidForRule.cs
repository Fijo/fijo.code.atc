using Fijo.Code.ATC.ACL.Infrastructure.Enums.ValidationRules;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Dtos;
using Fijo.Infrastructure.Model.Validation;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Rule.Attrs {
	public class AttrValidationIsValidForRule : AttrValidationRuleHandler {
		public AttrValidationIsValidForRule(IValidationObjFactory validationObjFactory) : base(validationObjFactory) {}
		#region Overrides of ValidationRuleHandler<AttrValidation,AttrValidationRule>
		protected override AttrValidationRule Rule { get { return AttrValidationRule.IsValidFor; } }
		protected override bool IsValid(ValidationContext<ValidationSource, AttrValidation> source) {
			return source.Validatior.IsValidFor(source.Source.Code.GetType());
		}
		#endregion
	}
}