using Fijo.Code.ATC.ACL.Infrastructure.Enums.ValidationRules;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Rule.Attrs {
	public abstract class AttrValidationRuleHandler : ValidationRuleHandler<AttrValidation, AttrValidationRule> {
		protected AttrValidationRuleHandler(IValidationObjFactory validationObjFactory) : base(validationObjFactory) {}
	}
}