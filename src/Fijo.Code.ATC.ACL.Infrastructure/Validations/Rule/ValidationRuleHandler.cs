using Fijo.Code.ATC.ACL.Infrastructure.Validations.Dtos;
using Fijo.Infrastructure.Model.Validation;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Rule {
	public abstract class ValidationRuleHandler<TValidator, TRule> : IGAllHandling<ValidationContext<ValidationSource, TValidator>, ValidationResult<ValidationSource, TValidator, TRule>> {
		private readonly IValidationObjFactory _validationObjFactory;

		protected ValidationRuleHandler(IValidationObjFactory validationObjFactory) {
			_validationObjFactory = validationObjFactory;
		}

		protected abstract TRule Rule { get; }

		#region Implementation of ISupportsParallelization
		public bool SupportParallelization { get { return true; } }

		public ValidationResult<ValidationSource, TValidator, TRule> Get(ValidationContext<ValidationSource, TValidator> source) {
			return _validationObjFactory.CreateResult(source, Rule, IsValid(source));
		}

		protected abstract bool IsValid(ValidationContext<ValidationSource, TValidator> source);
		#endregion
	}
}