using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Infrastructure.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Enums.ValidationRules;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Dtos;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.Model.Validation;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Handlers {
	public class AttrValidationHandler : ValidationHandler {
		private readonly IFinder<AttrValidation, Type> _attrValidationFinder;
		private readonly IGAllHandlingProcessor<ValidationContext<ValidationSource, AttrValidation>, ValidationResult<ValidationSource, AttrValidation, AttrValidationRule>> _validationRuleHandling;
		private readonly IAttrService _attrService;

		public AttrValidationHandler(IValidationObjFactory validationObjFactory, IFinder<AttrValidation, Type> attrValidationFinder, IGAllHandlingProcessor<ValidationContext<ValidationSource, AttrValidation>, ValidationResult<ValidationSource, AttrValidation, AttrValidationRule>> validationRuleHandling, IAttrService attrService) : base(validationObjFactory) {
			_attrValidationFinder = attrValidationFinder;
			_validationRuleHandling = validationRuleHandling;
			_attrService = attrService;
		}
		#region Overrides of ValidationHandler
		protected override IEnumerable<ValidationResult> GetResults(ValidationSource source) {
			var code = source.Code;
			var attrs = _attrService.GetAttrs(code);
			return attrs.SelectMany(attr => {
				var attrValidation = _attrValidationFinder.Get(attr.GetType());
				if (attrValidation.IsFallback) return Enumerable.Empty<ValidationResult>();
				var context = ValidationObjFactory.CreateContext(source, attrValidation);
				return _validationRuleHandling.Get(context);
			});
		}
		#endregion
	}
}