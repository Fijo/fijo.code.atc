using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Dtos;
using Fijo.Infrastructure.Model.Validation;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Handlers {
	public abstract class ValidationHandler : IGAllHandling<ValidationSource, IEnumerable<ValidationResult>> {
		protected readonly IValidationObjFactory ValidationObjFactory;

		protected ValidationHandler(IValidationObjFactory validationObjFactory) {
			ValidationObjFactory = validationObjFactory;
		}
		#region Implementation of ISupportsParallelization
		public IEnumerable<ValidationResult> Get(ValidationSource source) {
			return GetResults(source);
		}

		protected abstract IEnumerable<ValidationResult> GetResults(ValidationSource source);

		public bool SupportParallelization { get { return true; } }
		#endregion
	}
}