using System.Collections.Generic;
using Fijo.Infrastructure.Model.Validation;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Interfaces {
	public interface IValidationService {
		IEnumerable<ValidationResult> GetValidationResult(Model.Base.Code code);
	}
}