using System;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs {
	public class AttrValidationFallback : AttrValidation {
		#region Overrides of AttrValidation
		public override Type ForAttr { get { return null; } }
		#endregion
	}
}