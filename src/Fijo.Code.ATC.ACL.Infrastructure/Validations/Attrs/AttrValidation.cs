using System;
using System.Diagnostics;
using Fijo.Code.ATC.ACL.Model.Attrs;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs {
	public abstract class AttrValidation {
		public abstract Type ForAttr { get; }

		public virtual bool IsValidFor([NotNull] Type codeObjType) {
			#region PreCondition
			Debug.Assert(codeObjType.ImplementsType<Model.Base.Code>(), "only object types that implement �Code� are allowed for �codeObjType�.");
			#endregion
			return true;
		}

		public bool IsFallback{get { return ForAttr == null; }}
	}

	public abstract class AttrValidation<TAttr> : AttrValidation
		where TAttr : Attr {
		public override Type ForAttr {
			get { return typeof (TAttr); }
		}
	}
}