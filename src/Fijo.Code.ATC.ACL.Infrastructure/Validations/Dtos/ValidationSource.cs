namespace Fijo.Code.ATC.ACL.Infrastructure.Validations.Dtos {
	public class ValidationSource {
		public Model.Base.Code Code { get; set; }
	}
}