﻿using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Attrs;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Attrs {
	public class AttrService : IAttrService {
		private readonly ICGAllHandlingProcessor<Model.Base.Code, IEnumerable<Attr>> _attrProviderProcessor;

		public AttrService(ICGAllHandlingProcessor<Model.Base.Code, IEnumerable<Attr>> attrProviderProcessor) {
			_attrProviderProcessor = attrProviderProcessor;
		}
		#region Implementation of IAttrService
		public IEnumerable<Attr> GetAttrs(Model.Base.Code code) {
			return _attrProviderProcessor.Get(code).Flat();
		}
		#endregion
	}
}