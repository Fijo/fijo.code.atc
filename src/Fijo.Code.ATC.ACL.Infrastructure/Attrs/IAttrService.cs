using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Attrs;

namespace Fijo.Code.ATC.ACL.Infrastructure.Attrs {
	public interface IAttrService {
		IEnumerable<Attr> GetAttrs(Model.Base.Code code);
	}
}