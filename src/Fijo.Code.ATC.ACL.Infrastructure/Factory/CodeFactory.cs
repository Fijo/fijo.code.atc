﻿using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Data;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.ACL.Model.Oop;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;

namespace Fijo.Code.ATC.ACL.Infrastructure.Factory {
	public class CodeFactory : ICodeFactory {
		#region Implementation of ICodeFactory
		public Assembly CreateAssembly(string name, Assemblies assemblies, Namespaces namespaces = null) {
			return new Assembly {Name = name, Assemblies = assemblies, Namespaces = namespaces};
		}

		public Namespace CreateNamespace(string name, Assembly assembly = null, Types types = null) {
			return new Namespace {Name = name, Assembly = assembly, Types = types};
		}

		public Type CreateType(string name, Namespace ns = null, Members members = null, Types baseTypes = null) {
			return new Type {Name = name, Namespace = ns, Members = members, BaseTypes = baseTypes};
		}

		public Field CreateField(string name, Type type = null) {
			return new Field {Name = name, Type = type};
		}

		public Method CreateMethod(string name, Parameters parameters = null, Expression body = null, ReturnType returnType = null) {
			return new Method {Name = name, Parameters = parameters, Body = body, ReturnType = returnType};
		}

		public Variable CreateVariable(string name, Type type = null) {
			return new Variable {Name = name, Type = type};
		}

		public Parameter CreateParameter(string name, Type type = null) {
			return new Parameter {Name = name, Type = type};
		}

		public ReturnType CreateReturnType(Type type = null) {
			return new ReturnType {Type = type};
		}

		public CallExpression CreateCallExpression(Method target, Expressions argumentInstances) {
			return new CallExpression {Target = target, ArgumentInstances = argumentInstances};
		}

		public CaseExpression CreateCaseExpression(Expression key, Expression body) {
			return new CaseExpression {Key = key, Body = body};
		}

		public ContainerExpression CreateContainerExpression(IEnumerable<Expression> expressions = null) {
			var containerExpression = new ContainerExpression();
			if(expressions != null) containerExpression.AddRange(expressions);
			return containerExpression;
		}

		public DoWhileExpression CreateDoWhileExpression(Expression condition, Expression body) {
			return new DoWhileExpression {Condition = condition, Body = body};
		}

		public FieldExpression CreateFieldExpression(Field target, Expression instance = null) {
			return new FieldExpression {Target = target, Instance = instance};
		}

		public ForExpression CreateForExpression(Expression initialization, Expression condition, Expression afterThroughtOperation, Expression body) {
			return new ForExpression {Initialization = initialization, Condition = condition, AfterThroughtOperation = afterThroughtOperation, Body = body};
		}

		public IfExpression CreateIfExpression(Expression condition, Expression trueBody, Expression falseBody = null) {
			return new IfExpression {Condition = condition, TrueBody = trueBody, FalseBody = falseBody};
		}

		public ReturnExpression CreateReturnExpression(Expression value) {
			return new ReturnExpression {Value = value};
		}

		public ThrowExpression CreateThrowExpression(Expression value) {
			return new ThrowExpression {Value = value};
		}

		public TryExpression CreateTryExpression(Expression body, Catches catches, Expression @finally) {
			return new TryExpression {Body = body, Catches = catches, Finally = @finally};
		}

		public CatchExpression CreateCatchExpression(Type exceptionType, Expression body) {
			return new CatchExpression {ExceptionType = exceptionType, Body = body};
		}

		public SetExpression CreateSetExpression(Expression target, Expression value) {
			return new SetExpression {Target = target, Value = value};
		}

		public SimpleForExpression CreateSimpleForExpression(Expression condition, Expression afterThroughtOperation, Expression body) {
			return new SimpleForExpression {Condition = condition, AfterThroughtOperation = afterThroughtOperation, Body = body};
		}

		public SwitchExpression CreateSwitchExpression(Expression input, Cases cases) {
			return new SwitchExpression {Input = input, Cases = cases};
		}

		public GotoExpression GotoExpression(Expression target) {
			return new GotoExpression {Target = target};
		}

		public ValueExpression CreateValueExpression(Instance instance) {
			return new ValueExpression {Instance = instance};
		}

		public VariableExpression CreateVariableExpression(Variable variable) {
			return new VariableExpression {Variable = variable};
		}

		public WhileExpression CreateWhileExpression(Expression condition, Expression body) {
			return new WhileExpression {Condition = condition, Body = body};
		}

		public Instance CreateInstance(Type type, MemberInstances memberInstances) {
			return new Instance {Type = type, MemberInstances = memberInstances};
		}

		public MemberInstance CreateMemberInstance(Member member, Instance instance) {
			return new MemberInstance {Member = member, Instance = instance};
		}

		public VariableInstance CreateVariableInstance(Variable variable, Instance instance) {
			return new VariableInstance {Variable = variable, Instance = instance};
		}
		#endregion
	}
}