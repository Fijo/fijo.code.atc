﻿using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Infrastructure.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Enums.ValidationRules;
using Fijo.Code.ATC.ACL.Infrastructure.Factory;
using Fijo.Code.ATC.ACL.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs.Impl;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Data;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Oop;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Predicates;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Data;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Interface;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Oop;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Validations;
using Fijo.Code.ATC.ACL.Infrastructure.Validations;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Dtos;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Handlers;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Interfaces;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Rule.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Data;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Infrastructure.Model.Validation;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Factories;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.GAll.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Interfaces;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Parallelizable.Provider;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;
using SType = System.Type;

namespace Fijo.Code.ATC.ACL.Infrastructure.Properties {
	[PublicAPI]
	public class ACLInfrastructureInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			BindChildrenIteration(kernel);

			BindAttr(kernel);

			BindValidation(kernel);

			BindClone(kernel);

			kernel.Bind<ICodeFactory>().To<CodeFactory>().InSingletonScope();
		}

		#region BindClone
		private static void BindClone(IKernel kernel) {
			// ToDo finish clone stuff (lists are missing and may other stuff too...)
			BindClonerCreators(kernel);
			BindCloners(kernel);
		}

		private static void BindClonerCreators(IKernel kernel) {
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Assemblies>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Cases>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Catches>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<DataAttrs>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<ExpressionAttrs>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Expressions>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<GeneralAttrs>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<MemberInstances>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Members>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Namespaces>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<OopAttrs>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Parameters>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Types>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<VariableInstances>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Variables>>().InSingletonScope();

			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Instance>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<MemberInstance>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<VariableInstance>>().InSingletonScope();

			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<CallExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<CatchExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<ContainerExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<SetExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<GotoExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<ReturnExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<FieldExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<ValueExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<VariableExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<CaseExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<IfExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<SwitchExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<ThrowExpression>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<TryExpression>>().InSingletonScope();

			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Assembly>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Field>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Member>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Method>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Module>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Namespace>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Parameter>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<ReturnType>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Type>>().InSingletonScope();
			kernel.Bind<IClonerCreator>().To<DefaultClonerCreator<Variable>>().InSingletonScope();
		}

		private static void BindCloners(IKernel kernel) {
			kernel.Bind<ICloner>().To<InstanceCloner>().InSingletonScope();

			kernel.Bind<ICloner>().To<CallExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<CaseExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<CatchExpressionCloner>().InSingletonScope();
			//kernel.Bind<ICloner>().To<ContainerExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<FieldExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<ForExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<GotoExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<IfExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<SetExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<SimpleForExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<SwitchExpressionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<TryExpressionCloner>().InSingletonScope();

			kernel.Bind<ICloner>().To<HaveAssembliesCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveBodyCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveConditionCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveInstanceCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveMemberCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveNamespaceCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveNamespacesCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveTypeCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveTypesCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveValueCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<HaveVariableCloner>().InSingletonScope();

			kernel.Bind<ICloner>().To<MethodCloner>().InSingletonScope();
			kernel.Bind<ICloner>().To<TypeCloner>().InSingletonScope();
		}
		#endregion

		#region BindChildrenIteration
		private static void BindChildrenIteration(IKernel kernel) {
			//kernel.Bind<ICGAllHandlingProcessor<Model.Base.Code, IEnumerable<Model.Base.Code>>>().To<CGAllHandlingProcessor<Model.Base.Code, IEnumerable<Model.Base.Code>>>().InSingletonScope();
			kernel.Bind<IParallelizableHandlingProvider<ICGAllHandling<Model.Base.Code, IEnumerable<Model.Base.Code>>>>().To<InjectionParallelizableHandlingProvider<ICGAllHandling<Model.Base.Code, IEnumerable<Model.Base.Code>>, IChildrenProvider>>().InSingletonScope();

			BindChildrenProvider(kernel);


			kernel.Bind<IterationIgnorePredicates>().To<IterationIgnorePredicates>().InSingletonScope();
			kernel.Bind<IIterationIgnorePredicatesService>().To<IterationIgnorePredicatesService>().InSingletonScope();

			kernel.Bind<IChildrenService>().To<ChildrenService>().InSingletonScope();
		}

		private static void BindChildrenProvider(IKernel kernel) {
			kernel.Bind<IChildrenProvider>().To<InstanceChildrenProvider>().InSingletonScope();

			kernel.Bind<IChildrenProvider>().To<CallExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<CaseExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<CatchExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<ContainerExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<FieldExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<ForExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<GotoExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<IfExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<SetExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<SimpleForExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<SwitchExpressionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<TryExpressionChildrenProvider>().InSingletonScope();

			kernel.Bind<IChildrenProvider>().To<HaveAssembliesChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveBodyChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveConditionChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveInstanceChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveMemberChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveNamespaceChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveNamespacesChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveTypeChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveTypesChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveValueChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<HaveVariableChildrenProvider>().InSingletonScope();

			kernel.Bind<IChildrenProvider>().To<MethodChildrenProvider>().InSingletonScope();
			kernel.Bind<IChildrenProvider>().To<TypeChildrenProvider>().InSingletonScope();
		}
		#endregion

		#region BindAttr
		private static void BindAttr(IKernel kernel) {
			//kernel.Bind<ICGAllHandlingProcessor<Model.Base.Code, IEnumerable<Attr>>>().To<CGAllHandlingProcessor<Model.Base.Code, IEnumerable<Attr>>>().InSingletonScope();
			kernel.Bind<IParallelizableHandlingProvider<ICGAllHandling<Model.Base.Code, IEnumerable<Attr>>>>().To<InjectionParallelizableHandlingProvider<ICGAllHandling<Model.Base.Code, IEnumerable<Attr>>, IAttrsProvider>>().InSingletonScope();
			BindAttrsProvider(kernel);
			kernel.Bind<IAttrService>().To<AttrService>().InSingletonScope();
		}

		private static void BindAttrsProvider(IKernel kernel) {
			kernel.Bind<IAttrsProvider>().To<CodeAttrsProvider>().InSingletonScope();
			kernel.Bind<IAttrsProvider>().To<DataCodeAttrsProvider>().InSingletonScope();
			kernel.Bind<IAttrsProvider>().To<ExpressionAttrsProvider>().InSingletonScope();
			kernel.Bind<IAttrsProvider>().To<OopCodeAttrsProvider>().InSingletonScope();
		}
		#endregion

		#region BindValidation
		private static void BindValidation(IKernel kernel) {
			kernel.Bind<IAlgorithmProvider<AttrValidation, SType>>().To<AttrValidationProvider>().InSingletonScope();
			//kernel.Bind<IFinder<AttrValidation, SType>>().To<Finder<AttrValidation, SType>>().InSingletonScope();
			BindAttrValidation(kernel);

			//kernel.Bind<IGAllHandlingProcessor<ValidationContext<ValidationSource, AttrValidation>, ValidationResult<ValidationSource, AttrValidation, AttrValidationRule>>>().To<GAllHandlingProcessor<ValidationContext<ValidationSource, AttrValidation>, ValidationResult<ValidationSource, AttrValidation, AttrValidationRule>>>().InSingletonScope();
			kernel.Bind<IParallelizableHandlingProvider<IGAllHandling<ValidationContext<ValidationSource, AttrValidation>, ValidationResult<ValidationSource, AttrValidation, AttrValidationRule>>>>().To<InjectionParallelizableHandlingProvider<IGAllHandling<ValidationContext<ValidationSource, AttrValidation>, ValidationResult<ValidationSource, AttrValidation, AttrValidationRule>>, AttrValidationRuleHandler>>().InSingletonScope();
			BindAttrValidationRuleHandlers(kernel);

			//kernel.Bind<IGAllHandlingProcessor<ValidationSource, IEnumerable<ValidationResult>>>().To<GAllHandlingProcessor<ValidationSource, IEnumerable<ValidationResult>>>().InSingletonScope();
			kernel.Bind<IParallelizableHandlingProvider<IGAllHandling<ValidationSource, IEnumerable<ValidationResult>>>>().To<InjectionParallelizableHandlingProvider<IGAllHandling<ValidationSource, IEnumerable<ValidationResult>>, ValidationHandler>>().InSingletonScope();
			BindAttrValidationHandlers(kernel);

			kernel.Bind<IValidationService>().To<ValidationService>().InSingletonScope();
		}

		private static void BindAttrValidation(IKernel kernel) {
			kernel.Bind<AttrValidation>().To<AttrValidationFallback>().InSingletonScope();
		}

		private static void BindAttrValidationRuleHandlers(IKernel kernel) {
			kernel.Bind<AttrValidationRuleHandler>().To<AttrValidationIsValidForRule>().InSingletonScope();
		}

		private static void BindAttrValidationHandlers(IKernel kernel) {
			kernel.Bind<ValidationHandler>().To<AttrValidationHandler>().InSingletonScope();
		}
		#endregion
	}
}