using Fijo.Code.ATC.ACL.Model.Attrs;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs {
	public interface IAttrsProvider : IChildrenProviderHandling<Model.Base.Code, Attr> {}
}