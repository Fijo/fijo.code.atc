using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs.Impl {
	public class OopCodeAttrsProvider : AttrsProvider<OopCode> {
		#region Overrides of ChildrenProviderHandling<OopCode,Code,Attr>
		protected override OopCode Cast(Model.Base.Code source) {
			return (OopCode) source;
		}

		protected override IEnumerable<Attr> GetChildren(OopCode obj) {
			return obj.OopAttrs;
		}
		#endregion
	}
}