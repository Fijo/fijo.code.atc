using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Attrs;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs.Impl {
	public class CodeAttrsProvider : AttrsProvider<Model.Base.Code> {
		#region Overrides of ChildrenProviderHandling<Code,Code,Attr>
		protected override Model.Base.Code Cast(Model.Base.Code source) {
			return source;
		}

		protected override IEnumerable<Attr> GetChildren(Model.Base.Code obj) {
			return obj.GeneralAttrs;
		}
		#endregion
	}
}