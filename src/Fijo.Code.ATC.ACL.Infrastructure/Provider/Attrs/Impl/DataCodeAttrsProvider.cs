using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Data;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs.Impl {
	public class DataCodeAttrsProvider : AttrsProvider<DataCode> {
		#region Overrides of ChildrenProviderHandling<DataCode,Code,Attr>
		protected override DataCode Cast(Model.Base.Code source) {
			return (DataCode) source;
		}

		protected override IEnumerable<Attr> GetChildren(DataCode obj) {
			return obj.DataAttrs;
		}
		#endregion
	}
}