using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Expressions;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs.Impl {
	public class ExpressionAttrsProvider : AttrsProvider<Expression> {
		#region Overrides of ChildrenProviderHandling<Expression,Code,Attr>
		protected override Expression Cast(Model.Base.Code source) {
			return (Expression) source;
		}

		protected override IEnumerable<Attr> GetChildren(Expression obj) {
			return obj.ExpressionAttrs;
		}
		#endregion
	}
}