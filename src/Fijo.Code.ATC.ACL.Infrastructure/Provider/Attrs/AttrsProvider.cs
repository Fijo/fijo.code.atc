using Fijo.Code.ATC.ACL.Model.Attrs;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Attrs {
	public abstract class AttrsProvider<T> : ChildrenProviderHandling<T, Model.Base.Code, Attr>, IAttrsProvider {}
}