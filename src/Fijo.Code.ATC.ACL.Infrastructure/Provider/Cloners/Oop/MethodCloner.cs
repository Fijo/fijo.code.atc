﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Oop;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Oop {
	public class MethodCloner : PropertyCloner<Method> {
		#region Overrides of PropertyCloner<Method>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<Method>(x => x.Parameters,
			                                 x => x.ReturnType);
		}
		#endregion
	}
}