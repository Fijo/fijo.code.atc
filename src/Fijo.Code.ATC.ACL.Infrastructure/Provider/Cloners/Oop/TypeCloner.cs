﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Oop;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Oop {
	public class TypeCloner : PropertyCloner<Type> {
		#region Overrides of PropertyCloner<Type>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<Type>(x => x.BaseTypes,
			                               x => x.Members);
		}
		#endregion
	}
}