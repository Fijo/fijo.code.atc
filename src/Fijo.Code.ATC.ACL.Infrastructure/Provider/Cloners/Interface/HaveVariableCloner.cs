using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Interface {
	public class HaveVariableCloner : PropertyCloner<IHaveVariable> {
		#region Overrides of PropertyCloner<IHaveVariable>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<IHaveVariable>(x => x.Variable);
		}
		#endregion
	}
}