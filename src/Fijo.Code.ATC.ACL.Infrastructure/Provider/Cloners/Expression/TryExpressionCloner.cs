﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class TryExpressionCloner : PropertyCloner<TryExpression> {
		#region Overrides of PropertyCloner<TryExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<TryExpression>(x => x.Catches,
			                                        x => x.Finally);
		}
		#endregion
	}
}