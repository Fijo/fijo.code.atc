﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class SwitchExpressionCloner : PropertyCloner<SwitchExpression> {
		#region Overrides of Cloner<SwitchExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<SwitchExpression>(x => x.Cases,
			                                           x => x.Input);
		}
		#endregion
	}
}