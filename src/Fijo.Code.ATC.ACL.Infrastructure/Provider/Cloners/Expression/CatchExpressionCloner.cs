﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class CatchExpressionCloner : PropertyCloner<CatchExpression> {
		#region Overrides of Cloner<CatchExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<CatchExpression>(x => x.ExceptionType);
		}
		#endregion
	}
}