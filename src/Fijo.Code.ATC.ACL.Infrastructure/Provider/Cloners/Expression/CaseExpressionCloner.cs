﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class CaseExpressionCloner : PropertyCloner<CaseExpression> {
		#region Overrides of Cloner<CaseExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<CaseExpression>(x => x.Key);
		}
		#endregion
	}
}