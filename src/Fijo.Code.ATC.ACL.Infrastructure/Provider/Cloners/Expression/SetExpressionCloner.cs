﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class SetExpressionCloner : PropertyCloner<SetExpression> {
		#region Overrides of Cloner<SetExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<SetExpression>(x => x.Target);
		}
		#endregion
	}
}