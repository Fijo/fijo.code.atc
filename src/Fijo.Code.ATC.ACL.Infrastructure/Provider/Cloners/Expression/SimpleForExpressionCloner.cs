﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class SimpleForExpressionCloner : PropertyCloner<SimpleForExpression> {
		#region Overrides of Cloner<SimpleForExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<SimpleForExpression>(x => x.AfterThroughtOperation);
		}
		#endregion
	}
}