﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class IfExpressionCloner : PropertyCloner<IfExpression> {
		#region Overrides of Cloner<IfExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<IfExpression>(x => x.TrueBody,
			                                       x => x.FalseBody);
		}
		#endregion
	}
}