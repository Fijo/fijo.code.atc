﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class FieldExpressionCloner : PropertyCloner<FieldExpression> {
		#region Overrides of Cloner<FieldExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<FieldExpression>(x => x.Instance,
			                                          x => x.Target);
		}
		#endregion
	}
}