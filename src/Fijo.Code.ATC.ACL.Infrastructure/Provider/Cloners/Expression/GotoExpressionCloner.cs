﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class GotoExpressionCloner : PropertyCloner<GotoExpression> {
		#region Overrides of Cloner<GotoExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<GotoExpression>(x => x.Target);
		}
		#endregion
	}
}