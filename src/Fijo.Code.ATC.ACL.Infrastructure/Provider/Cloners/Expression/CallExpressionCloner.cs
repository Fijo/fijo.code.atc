﻿using System.Collections.Generic;
using System.Reflection;
using Fijo.Code.ATC.ACL.Model.Expressions;
using FijoCore.Infrastructure.LightContrib.Default.Service.Runtime;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Expression {
	public class CallExpressionCloner : PropertyCloner<CallExpression> {
		#region Overrides of Cloner<CallExpression>
		public override IEnumerable<PropertyInfo> GetProperties() {
			return Run.GetProperties<CallExpression>(x => x.ArgumentInstances,
			                                         x => x.Target);
		}
		#endregion
	}
}