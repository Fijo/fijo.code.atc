﻿using Fijo.Code.ATC.ACL.Model.Data;
using FijoCore.Infrastructure.LightContrib.Module.Cloning.Cloners;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Cloners.Data {
	public class InstanceCloner : Cloner<Instance> {
		#region Overrides of Cloner<Instance>
		public override void Copy(Instance source, Instance result, ICloning cloning) {
			result.MemberInstances = cloning.Pass(source.MemberInstances);
		}
		#endregion
	}
}