using System;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Validations {
	public class AttrValidationProvider : FallbackInjectionAlgorithmProvider<AttrValidation, Type> {
		#region Overrides of InjectionAlgorithmProviderBase<AttrValidation,Type>
		protected override Type KeySelector(AttrValidation algorithm) {
			return algorithm.ForAttr;
		}
		#endregion
		#region Overrides of FallbackInjectionAlgorithmProvider<AttrValidation,Type>
		protected override bool IsFallback(AttrValidation algorithm) {
			return algorithm.IsFallback;
		}
		#endregion
	}
}