using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children {
	public interface IChildrenProvider : IChildrenProviderHandling<Model.Base.Code, Model.Base.Code> {}
}