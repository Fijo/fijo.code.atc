﻿using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Impl;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children {
	public abstract class ChildrenProvider<T> : ChildrenProviderHandling<T, Model.Base.Code, Model.Base.Code>, IChildrenProvider {}
}