namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Predicates {
	public interface IIterationIgnorePredicatesService {
		bool Required(Model.Base.Code code);
	}
}