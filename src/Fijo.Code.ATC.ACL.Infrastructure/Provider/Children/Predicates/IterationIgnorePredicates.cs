using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Predicates {
	[PublicAPI]
	public class IterationIgnorePredicates {
		public virtual bool Default(Model.Base.Code code) {
			return false;
		}
		public virtual bool Required(Model.Base.Code code) {
			return false;
		}
	}
}