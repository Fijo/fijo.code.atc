using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Predicates {
	public class IterationIgnorePredicatesService : IIterationIgnorePredicatesService {
		private readonly ICollection<IterationIgnorePredicates> _iterationIgnorePredicateses;

		public IterationIgnorePredicatesService(IEnumerable<IterationIgnorePredicates> iterationPredicates) {
			_iterationIgnorePredicateses = iterationPredicates.Execute();
		}

		public bool Required(Model.Base.Code code) {
			return _iterationIgnorePredicateses.AsParallel().All(x => x.Required(code));
		}
	}
}