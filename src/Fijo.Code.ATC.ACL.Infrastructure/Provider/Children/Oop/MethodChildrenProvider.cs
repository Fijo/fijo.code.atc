using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Model.Oop;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Oop {
	public class MethodChildrenProvider : ChildrenProvider<Method> {
		#region Overrides of ChildrenProvider<Method>
		protected override Method Cast(Model.Base.Code obj) {
			return (Method) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(Method obj) {
			return obj.Parameters.Cast<Model.Base.Code>().AddReturn(obj.ReturnType);
		}
		#endregion
	}
}