using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Oop {
	public class TypeChildrenProvider : ChildrenProvider<Type> {
		#region Overrides of ChildrenProvider<Type>
		protected override Type Cast(Model.Base.Code obj) {
			return (Type) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(Type obj) {
			var members = obj.Members;
			var baseType = obj.BaseTypes;
			return baseType != null
				       ? members.Cast<Model.Base.Code>().Concat(baseType)
				       : members;
		}
		#endregion
	}
}