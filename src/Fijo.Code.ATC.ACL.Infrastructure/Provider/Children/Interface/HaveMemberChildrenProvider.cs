using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveMemberChildrenProvider : ChildrenProvider<IHaveMember> {
		#region Overrides of ChildrenProvider<IHaveMember>
		protected override IHaveMember Cast(Model.Base.Code obj) {
			return (IHaveMember) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveMember obj) {
			yield return obj.Member;
		}
		#endregion
	}
}