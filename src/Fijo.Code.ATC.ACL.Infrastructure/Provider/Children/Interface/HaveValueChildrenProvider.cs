using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveValueChildrenProvider : ChildrenProvider<IHaveValue> {
		#region Overrides of ChildrenProviderHandling<IHaveValue,Code,Code>
		protected override IHaveValue Cast(Model.Base.Code source) {
			return (IHaveValue) source;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveValue obj) {
			yield return obj.Value;
		}
		#endregion
	}
}