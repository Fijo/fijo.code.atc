using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveBodyChildrenProvider : ChildrenProvider<IHaveBody> {
		#region Overrides of ChildrenProvider<IHaveBody>
		protected override IHaveBody Cast(Model.Base.Code obj) {
			return (IHaveBody) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveBody obj) {
			yield return obj.Body;
		}
		#endregion
	}
}