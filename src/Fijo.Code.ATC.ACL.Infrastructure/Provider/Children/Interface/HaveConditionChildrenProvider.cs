using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveConditionChildrenProvider : ChildrenProvider<IHaveCondition> {
		#region Overrides of ChildrenProvider<IHaveCondition>
		protected override IHaveCondition Cast(Model.Base.Code obj) {
			return (IHaveCondition) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveCondition obj) {
			yield return obj.Condition;
		}
		#endregion
	}
}