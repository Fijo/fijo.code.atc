using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveNamespaceChildrenProvider : ChildrenProvider<IHaveNamespace> {
		#region Overrides of ChildrenProvider<IHaveNamespace>
		protected override IHaveNamespace Cast(Model.Base.Code obj) {
			return (IHaveNamespace) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveNamespace obj) {
			yield return obj.Namespace;
		}
		#endregion
	}
}