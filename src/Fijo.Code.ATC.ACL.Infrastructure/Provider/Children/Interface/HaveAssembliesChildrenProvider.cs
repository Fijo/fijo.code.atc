using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveAssembliesChildrenProvider : ChildrenProvider<IHaveAssemblies> {
		#region Overrides of ChildrenProvider<IHaveAssemblies>
		protected override IHaveAssemblies Cast(Model.Base.Code obj) {
			return (IHaveAssemblies) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveAssemblies obj) {
			return obj.Assemblies;
		}
		#endregion
	}
}