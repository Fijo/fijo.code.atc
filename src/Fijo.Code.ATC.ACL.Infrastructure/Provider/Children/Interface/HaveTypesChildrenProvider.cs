using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveTypesChildrenProvider : ChildrenProvider<IHaveTypes> {
		#region Overrides of ChildrenProvider<IHaveTypes>
		protected override IHaveTypes Cast(Model.Base.Code obj) {
			return (IHaveTypes) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveTypes obj) {
			return obj.Types;
		}
		#endregion
	}
}