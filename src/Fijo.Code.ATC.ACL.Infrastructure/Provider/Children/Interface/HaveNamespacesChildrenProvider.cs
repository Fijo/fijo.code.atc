using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveNamespacesChildrenProvider : ChildrenProvider<IHaveNamespaces> {
		#region Overrides of ChildrenProvider<IHaveNamespaces>
		protected override IHaveNamespaces Cast(Model.Base.Code obj) {
			return (IHaveNamespaces) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveNamespaces obj) {
			return obj.Namespaces;
		}
		#endregion
	}
}