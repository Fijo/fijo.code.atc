using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveVariableChildrenProvider : ChildrenProvider<IHaveVariable> {
		#region Overrides of ChildrenProvider<IHaveVariable>
		protected override IHaveVariable Cast(Model.Base.Code obj) {
			return (IHaveVariable) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveVariable obj) {
			yield return obj.Variable;
		}
		#endregion
	}
}