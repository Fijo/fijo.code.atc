using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveTypeChildrenProvider : ChildrenProvider<IHaveType> {
		#region Overrides of ChildrenProvider<IHaveType>
		protected override IHaveType Cast(Model.Base.Code obj) {
			return (IHaveType) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveType obj) {
			yield return obj.Type;
		}
		#endregion
	}
}