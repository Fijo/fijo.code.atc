using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Interface {
	public class HaveInstanceChildrenProvider : ChildrenProvider<IHaveInstance> {
		#region Overrides of ChildrenProvider<IHaveInstance>
		protected override IHaveInstance Cast(Model.Base.Code obj) {
			return (IHaveInstance) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IHaveInstance obj) {
			yield return obj.Instance;
		}
		#endregion
	}
}