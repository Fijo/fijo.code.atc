using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class GotoExpressionChildrenProvider : ChildrenProvider<GotoExpression> {
		#region Overrides of ChildrenProviderHandling<GotoExpression,Code,Code>
		protected override GotoExpression Cast(Model.Base.Code source) {
			return (GotoExpression) source;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(GotoExpression obj) {
			yield return obj.Target;
		}
		#endregion
	}
}