using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class IfExpressionChildrenProvider : ChildrenProvider<IfExpression> {
		#region Overrides of ChildrenProvider<IfExpression>
		protected override IfExpression Cast(Model.Base.Code obj) {
			return (IfExpression) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(IfExpression obj) {
			yield return obj.TrueBody;
			yield return obj.FalseBody;
		}
		#endregion
	}
}