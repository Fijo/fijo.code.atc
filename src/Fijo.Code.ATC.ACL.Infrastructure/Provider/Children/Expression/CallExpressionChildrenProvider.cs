using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Model.Expressions;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class CallExpressionChildrenProvider : ChildrenProvider<CallExpression> {
		#region Overrides of ChildrenProvider<CallExpression>
		protected override CallExpression Cast(Model.Base.Code obj) {
			return (CallExpression) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(CallExpression obj) {
			return obj.ArgumentInstances.Cast<Model.Base.Code>().AddReturn(obj.Target);
		}
		#endregion
	}
}