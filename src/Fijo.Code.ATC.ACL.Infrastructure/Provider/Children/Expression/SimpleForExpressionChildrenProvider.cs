using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class SimpleForExpressionChildrenProvider : ChildrenProvider<SimpleForExpression> {
		#region Overrides of ChildrenProvider<SimpleForExpression>
		protected override SimpleForExpression Cast(Model.Base.Code obj) {
			return (SimpleForExpression) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(SimpleForExpression obj) {
			yield return obj.AfterThroughtOperation;
		}
		#endregion
	}
}