using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class SetExpressionChildrenProvider : ChildrenProvider<SetExpression> {
		#region Overrides of ChildrenProviderHandling<SetExpression,Code,Code>
		protected override SetExpression Cast(Model.Base.Code source) {
			return (SetExpression) source;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(SetExpression obj) {
			yield return obj.Target;
		}
		#endregion
	}
}