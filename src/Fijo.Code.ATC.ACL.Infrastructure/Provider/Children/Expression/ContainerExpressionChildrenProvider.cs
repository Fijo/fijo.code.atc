using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class ContainerExpressionChildrenProvider : ChildrenProvider<ContainerExpression> {
		#region Overrides of ChildrenProvider<ContainerExpression>
		protected override ContainerExpression Cast(Model.Base.Code obj) {
			return (ContainerExpression) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(ContainerExpression obj) {
			return obj;
		}
		#endregion
	}
}