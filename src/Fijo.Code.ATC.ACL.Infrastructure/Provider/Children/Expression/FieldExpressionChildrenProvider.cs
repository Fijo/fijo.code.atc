using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class FieldExpressionChildrenProvider : ChildrenProvider<FieldExpression> {
		#region Overrides of ChildrenProviderHandling<FieldExpression,Code,Code>
		protected override FieldExpression Cast(Model.Base.Code source) {
			return (FieldExpression) source;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(FieldExpression obj) {
			var instance = obj.Instance;
			if(instance != null) yield return instance;
			yield return obj.Target;
		}
		#endregion
	}
}