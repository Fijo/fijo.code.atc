using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class ForExpressionChildrenProvider : ChildrenProvider<ForExpression> {
		#region Overrides of ChildrenProvider<ForExpression>
		protected override ForExpression Cast(Model.Base.Code obj) {
			return (ForExpression) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(ForExpression obj) {
			yield return obj.Initialization;
		}
		#endregion
	}
}