using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class TryExpressionChildrenProvider : ChildrenProvider<TryExpression> {
		#region Overrides of ChildrenProviderHandling<TryExpression,Code,Code>
		protected override TryExpression Cast(Model.Base.Code source) {
			return (TryExpression) source;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(TryExpression obj) {
			return obj.Catches.AddReturn(obj.Finally);
		}
		#endregion
	}
}