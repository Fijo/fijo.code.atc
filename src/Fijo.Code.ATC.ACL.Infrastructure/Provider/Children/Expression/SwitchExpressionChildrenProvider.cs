using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class SwitchExpressionChildrenProvider : ChildrenProvider<SwitchExpression> {
		#region Overrides of ChildrenProvider<SwitchExpression>
		protected override SwitchExpression Cast(Model.Base.Code obj) {
			return (SwitchExpression) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(SwitchExpression obj) {
			return obj.Cases.AddReturn(obj.Input);
		}
		#endregion
	}
}