using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class CatchExpressionChildrenProvider : ChildrenProvider<CatchExpression> {
		#region Overrides of ChildrenProviderHandling<CatchExpression,Code,Code>
		protected override CatchExpression Cast(Model.Base.Code source) {
			return (CatchExpression) source;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(CatchExpression obj) {
			yield return obj.ExceptionType;
		}
		#endregion
	}
}