using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Expression {
	public class CaseExpressionChildrenProvider : ChildrenProvider<CaseExpression> {
		#region Overrides of ChildrenProvider<CaseExpression>
		protected override CaseExpression Cast(Model.Base.Code obj) {
			return (CaseExpression) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(CaseExpression obj) {
			yield return obj.Key;
		}
		#endregion
	}
}