﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Predicates;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.CGAll.Interface;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children {
	public class ChildrenService : IChildrenService {
		private readonly ICGAllHandlingProcessor<Model.Base.Code, IEnumerable<Model.Base.Code>> _childrenProviderProcessor;
		private readonly IIterationIgnorePredicatesService _iterationIgnorePredicates;
		private readonly IEqualityComparer<Model.Base.Code> _codeInstanceEqualityComparer;

		public ChildrenService(ICGAllHandlingProcessor<Model.Base.Code, IEnumerable<Model.Base.Code>> childrenProviderProcessor, IIterationIgnorePredicatesService iterationIgnorePredicates, IEqualityComparerFactory equalityComparerFactory) {
			_childrenProviderProcessor = childrenProviderProcessor;
			_iterationIgnorePredicates = iterationIgnorePredicates;
			_codeInstanceEqualityComparer = equalityComparerFactory.Instance<Model.Base.Code>();
		}

		#region Implementation of IChildrenService
		private IEnumerable<IEnumerable<Model.Base.Code>> InternGetChildren(Model.Base.Code obj) {
			return _iterationIgnorePredicates.Required(obj)
				       ? Enumerable.Empty<IEnumerable<Model.Base.Code>>()
				       : _childrenProviderProcessor.Get(obj);
		}

		public IEnumerable<Model.Base.Code> GetChildren(Model.Base.Code obj, Func<Model.Base.Code, bool> ignorePredicte = null) {
			return ignorePredicte != null && ignorePredicte(obj)
				       ? Enumerable.Empty<Model.Base.Code>()
				       : InternGetChildren(obj).Flat();
		}

		public IEnumerable<Model.Base.Code> GetAllChildrenFlatten(Model.Base.Code obj, Func<Model.Base.Code, bool> ignorePredicte = null, bool includeObj = false) {
			var set = new HashSet<Model.Base.Code>(_codeInstanceEqualityComparer);
			InternGetAllChildrenFlatten(set, obj, ignorePredicte, includeObj);
			return set;
		}

		private void InternGetAllChildrenFlatten(ISet<Model.Base.Code> set, Model.Base.Code obj, Func<Model.Base.Code, bool> ignorePredicte, bool includeObj) {
			if (includeObj ? !set.Add(obj) : set.Contains(obj)) return;
			GetChildren(obj, ignorePredicte).ForEach(child => InternGetAllChildrenFlatten(set, child, ignorePredicte, true));
		}
		#endregion
	}
}