using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children {
	public interface IChildrenService {
		IEnumerable<Model.Base.Code> GetChildren(Model.Base.Code obj, [CanBeNull] Func<Model.Base.Code, bool> ignorePredicte = null);
		IEnumerable<Model.Base.Code> GetAllChildrenFlatten(Model.Base.Code obj, [CanBeNull] Func<Model.Base.Code, bool> ignorePredicte = null, bool includeObj = false);
	}
}