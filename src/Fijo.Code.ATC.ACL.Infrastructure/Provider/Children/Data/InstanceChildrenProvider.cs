using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Data;

namespace Fijo.Code.ATC.ACL.Infrastructure.Provider.Children.Data {
	public class InstanceChildrenProvider : ChildrenProvider<Instance> {
		#region Overrides of ChildrenProvider<Instance>
		protected override Instance Cast(Model.Base.Code obj) {
			return (Instance) obj;
		}

		protected override IEnumerable<Model.Base.Code> GetChildren(Instance obj) {
			return obj.MemberInstances;
		}
		#endregion
	}
}