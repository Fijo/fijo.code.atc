using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	public class AssemblyFile {
		public string Path { get; set; }
		public AssemblyDefinition AssemblyDefinition { get; set; }
	}
}