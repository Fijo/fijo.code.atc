using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Assemblies.GAC.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Assembly;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	[UsedImplicitly]
	public class AssemblyService : IAssemblyService {
		private readonly ICodeService _codeService;
		private readonly IAssemblyProvider _assemblyProvider;

		public AssemblyService(IGAC gac, AssembliesProvider assembliesProvider, ICodeService codeService, IAssemblyProvider assemblyProvider) {
			_codeService = codeService;
			_assemblyProvider = assemblyProvider;
		}
		#region Implementation of IAssemblyService
		#region GetReferencedAssemblies
		public IEnumerable<AssemblyDefinition> GetReferencedAssemblies(AssemblyDefinition assembly) {
			return assembly.Modules.AsParallel().SelectMany(x => x.AssemblyReferences.Select(y => LoadAssembly(y, x, assembly)));
		}

		public AssemblyDefinition GetAssembly(string path) {
			return _assemblyProvider.GetAssembly(path);
		}

		private AssemblyDefinition LoadAssembly(AssemblyNameReference assemblyName, ModuleDefinition contextModule, AssemblyDefinition contextAssembly) {
			var loaded = _assemblyProvider.Get(new AssemblyResolveContext(assemblyName, contextModule, contextAssembly));

			var name = loaded.MainModule.Assembly.Name;
			Trace.WriteLineIf(!name.Version.Equals(assemblyName.Version), string.Format("referenced �{0}� in {{Assembly: �{1}�, Module: �{2}�}} but will use �{3}�", assemblyName.FullName, contextAssembly.FullName, contextModule.Name, name.FullName));
			return loaded;
		}

		#endregion

		public IEnumerable<string> GetNamespaces(ModuleDefinition module) {
			return _codeService.GetTypeDefs(module)
				.Select(x => x.Namespace)
				.Distinct();
		}
		
		public IEnumerable<TypeDefinition> GetTypes(ModuleDefinition module, string @namespace) {
			return _codeService.GetTypeDefs(module)
				.AsParallel()
				.Where(x => x.Namespace == @namespace);
		}
		#endregion
	}
}