using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	public class AssemblyResolveContext {
		public readonly AssemblyNameReference ToLoad;
		public readonly ModuleDefinition ReferencedFrom;
		public readonly AssemblyDefinition ReferencedFromAssembly;

		public AssemblyResolveContext(AssemblyNameReference toLoad, ModuleDefinition referencedFrom, AssemblyDefinition referencedFromAssembly) {
			ToLoad = toLoad;
			ReferencedFrom = referencedFrom;
			ReferencedFromAssembly = referencedFromAssembly;
		}
	}
}