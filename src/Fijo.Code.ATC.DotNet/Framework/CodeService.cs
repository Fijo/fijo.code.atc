using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	[UsedImplicitly]
	public class CodeService : ICodeService {
		#region Implementation of ICodeService
		public IEnumerable<TypeDefinition> GetTypeDefs(AssemblyDefinition assembly, bool includeNested = true) {
			return GetModuleDefs(assembly).SelectMany(x => GetTypeDefs(x, includeNested));
		}

		public IEnumerable<TypeDefinition> GetTypeDefs(ModuleDefinition assembly, bool includeNested = true) {
			var internTypeDefinitions = InternTypeDefinitions(assembly);
			if(includeNested) internTypeDefinitions = InternTypeDefinitionsWithNestedTypes(internTypeDefinitions);
			return internTypeDefinitions;
		}

		private IEnumerable<TypeDefinition> InternTypeDefinitionsWithNestedTypes(IEnumerable<TypeDefinition> typeDefentions) {
			return typeDefentions
				.SelectMany(x => x.IntoEnumerable()
					                 .Concat(InternTypeDefinitionsWithNestedTypes(x.NestedTypes)));
		}

		private IEnumerable<TypeDefinition> InternTypeDefinitions(ModuleDefinition module) {
			var typeDefinitions = module.Types.Where(x => !IgnoreTypeDef(x));
			return typeDefinitions;
		}

		public IEnumerable<IMetadataTokenProvider> Members(TypeDefinition typeDefinition) {
			var methodDefinitions = typeDefinition.Methods;
			return typeDefinition.Fields.Concat(typeDefinition.Properties, methodDefinitions, typeDefinition.Events, methodDefinitions.SelectMany(GetTypesFromMethodBody));
		}

		private static IEnumerable<IMetadataTokenProvider> GetTypesFromMethodBody(MethodDefinition methodDef) {
			var body = methodDef.Body;
			if (body == null) return Enumerable.Empty<IMetadataTokenProvider>();
			return body.Variables.Select(x => x.VariableType).Where(x => x.IsFunctionPointer);
		}


		public IEnumerable<ModuleDefinition> GetModuleDefs(AssemblyDefinition assembly) {
			return assembly.Modules;
		}

		private bool IgnoreTypeDef(TypeDefinition typeDef) {
			return typeDef.Name == "<Module>" && typeDef.Namespace == "";
		}
		#endregion
	}
}