using System.Collections.Generic;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	public interface ICodeService {
		IEnumerable<TypeDefinition> GetTypeDefs(AssemblyDefinition assembly, bool includeNested = true);
		IEnumerable<TypeDefinition> GetTypeDefs(ModuleDefinition assembly, bool includeNested = true);
		IEnumerable<IMetadataTokenProvider> Members(TypeDefinition typeDefinition);
	}
}