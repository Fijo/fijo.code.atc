using System.Collections.Generic;
using Fijo.Assemblies.GAC.Dto;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Framework {
	[PublicAPI]
	public class GacAssemblyEqualityComparer : IEqualityComparer<GacAssembly> {
		#region Implementation of IEqualityComparer<in GacAssembly>
		public bool Equals(GacAssembly x, GacAssembly y) {
			return x.AssemblyName.FullName == y.AssemblyName.FullName;
		}

		public int GetHashCode(GacAssembly obj) {
			return obj.AssemblyName.FullName.GetHashCode();
		}
		#endregion
	}
}