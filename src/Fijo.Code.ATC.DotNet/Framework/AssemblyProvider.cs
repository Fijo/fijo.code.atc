using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Fijo.Assemblies.GAC.Dto;
using Fijo.Assemblies.GAC.Interface;
using Fijo.Infrastructure.DesignPattern.Store;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Extentions.Object;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	public class AssemblyResolver : DefaultAssemblyResolver {
		private readonly ILoadedAssemblyStore _loadedAssemblyStore;

		public AssemblyResolver(ILoadedAssemblyStore loadedAssemblyStore) {
			_loadedAssemblyStore = loadedAssemblyStore;
		}

		public override AssemblyDefinition Resolve(string fullName, ReaderParameters parameters) {
			AssemblyFile result;
			return _loadedAssemblyStore.TryGetByFullName(fullName, out result)
				       ? result.AssemblyDefinition
				       : WrapLoad(base.Resolve(fullName, parameters));
		}

		public override AssemblyDefinition Resolve(AssemblyNameReference name) {
			if (name == null) throw new ArgumentNullException("name");
			AssemblyFile result;
			return _loadedAssemblyStore.TryGetByFullName(name.FullName, out result)
				       ? result.AssemblyDefinition
				       : WrapLoad(base.Resolve(name));
		}

		private AssemblyDefinition WrapLoad(AssemblyDefinition ad) {
			Trace.WriteLine("--* loaded " + ad.FullName + " from " + ad.MainModule.FullyQualifiedName);
			return ad;
		}
	}


	public interface ILoadedAssemblyStore {
		bool TryGetByPath(string path, out AssemblyFile result);
		bool TryGetByFullName(string fullName, out AssemblyFile result);
		void Store(AssemblyFile assemblyFile);
	}
	
	public class LoadedAssemblyStore : ILoadedAssemblyStore {
		private readonly IDictionary<string, AssemblyFile> _byPathDict = new Dictionary<string, AssemblyFile>();
		private readonly IDictionary<string, AssemblyFile> _byFullNameDict = new Dictionary<string, AssemblyFile>();

		public bool TryGetByPath(string path, out AssemblyFile result) {
			lock (_byPathDict) return _byPathDict.TryGetValue(path, out result);
		}

		public bool TryGetByFullName(string fullName, out AssemblyFile result) {
			lock (_byPathDict) return _byFullNameDict.TryGetValue(fullName, out result);
		}

		public void Store(AssemblyFile assemblyFile) {
			lock (_byPathDict) {
				_byPathDict.Add(assemblyFile.Path, assemblyFile);
				_byFullNameDict.Add(assemblyFile.AssemblyDefinition.FullName, assemblyFile);
			}
		}
	}

	[UsedImplicitly]
	public class AssemblyProvider : IAssemblyProvider {
		private readonly ILoadedAssemblyStore _loadedAssemblyStore;
		private readonly IDictionary<string, GacAssembly> _gacAssemblies;

		public AssemblyProvider(IGAC gac, IEqualityComparer<GacAssembly> gacAssemblyEqualityComparer, ILoadedAssemblyStore loadedAssemblyStore) {
			_loadedAssemblyStore = loadedAssemblyStore;
			_gacAssemblies = gac.Get()
			                    .DistinctMerge((x, y) => x.AssemblyName.ProcessorArchitecture.IsIn(ProcessorArchitecture.MSIL, ProcessorArchitecture.None) ? x : y,
			                                   gacAssemblyEqualityComparer).ToDict(x => x.AssemblyName.FullName, x => x);
		}
		#region Implementation of IAssemblyProvider
		public AssemblyDefinition Get(AssemblyResolveContext assemblyResolveContext) {
			return GetLoadedAssembly(assemblyResolveContext.ToLoad, assemblyResolveContext);
		}
		#endregion

		private AssemblyDefinition GetLoadedAssembly(AssemblyNameReference assemblyName, AssemblyResolveContext assemblyResolveContext) {
			var fullName = assemblyName.FullName;
			lock (_loadedAssemblyStore) {
				AssemblyFile result;
				if (_loadedAssemblyStore.TryGetByFullName(fullName, out result)) return result.AssemblyDefinition;
				var loaded = LoadAssembly(assemblyName, assemblyResolveContext);
				_loadedAssemblyStore.Store(loaded);
				return loaded.AssemblyDefinition;
			}
		}

		private AssemblyFile LoadAssembly(AssemblyNameReference assemblyName, AssemblyResolveContext assemblyResolveContext) {
			var assemblyPath = GetAssemblyName(Path.GetDirectoryName(GetByFullName(assemblyResolveContext.ReferencedFromAssembly.Name.FullName).Path), assemblyName);
			var toLoad = File.Exists(assemblyPath) ? assemblyPath : _gacAssemblies[assemblyName.FullName].AssemblyName.CodeBase;

			var loaded = new AssemblyFile {AssemblyDefinition = ReadAssembly(toLoad), Path = toLoad};
			return loaded;
		}

		private AssemblyFile GetByFullName(string fullName) {
			AssemblyFile result;
			Debug.Assert(_loadedAssemblyStore.TryGetByFullName(fullName, out result));
			return result;
		}

		public AssemblyDefinition GetAssembly(string path) {
			lock(_loadedAssemblyStore) {
				var assemblyDefinition = ReadAssembly(path);
				var assembly = new AssemblyFile {AssemblyDefinition = assemblyDefinition, Path = path};
				_loadedAssemblyStore.Store(assembly);
				return assemblyDefinition;
			}
		}

		private AssemblyDefinition ReadAssembly(string path) {
			lock(typeof(ModuleDefinition)) {
				var assemblyResolver = new AssemblyResolver(_loadedAssemblyStore);
				var assemblyDefinition = ModuleDefinition.ReadModule(path, new ReaderParameters(ReadingMode.Immediate) {AssemblyResolver = assemblyResolver}).Assembly;
				AssemblyDefenitionValidation(assemblyDefinition, assemblyResolver);
				Debug.Assert(assemblyDefinition.MainModule.FullyQualifiedName == path);
				return assemblyDefinition;
			}
		}

		[Conditional("DEBUG")]
		private static void AssemblyDefenitionValidation(AssemblyDefinition assemblyDefinition, AssemblyResolver assemblyResolver) {
			#if DEBUG
			assemblyDefinition.Modules.ForEach(module => Debug.Assert(module.AssemblyResolver.InstanceEquals(assemblyResolver)));
			#endif
		}

		private string GetAssemblyName(string dir, AssemblyNameReference assemblyName) {
			return Path.Combine(dir, string.Format("{0}.dll", assemblyName.Name));
		}
	}
}