using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	public interface IAssemblyProvider {
		AssemblyDefinition Get(AssemblyResolveContext assemblyResolveContext);
		AssemblyDefinition GetAssembly(string path);
	}
}