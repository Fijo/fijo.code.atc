using System.Collections.Generic;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Framework {
	public interface IAssemblyService {
		IEnumerable<AssemblyDefinition> GetReferencedAssemblies(AssemblyDefinition assembly);
		AssemblyDefinition GetAssembly(string path);
		IEnumerable<string> GetNamespaces(ModuleDefinition module);
		IEnumerable<TypeDefinition> GetTypes(ModuleDefinition module, string @namespace);
	}
}