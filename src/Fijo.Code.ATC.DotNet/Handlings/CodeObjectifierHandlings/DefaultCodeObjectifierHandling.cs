using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifierHandlings.Base;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Requests;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifierHandlings {
	public class DefaultCodeObjectifierHandling : ICodeObjectifierHandling {
		private readonly IFinder<ICodeObjectifier, TreatedCode> _codeObjectifierFinder;
		private readonly IOut _out;

		public DefaultCodeObjectifierHandling(IFinder<ICodeObjectifier, TreatedCode> codeObjectifierFinder, IOut @out) {
			_codeObjectifierFinder = codeObjectifierFinder;
			_out = @out;
		}

		#region Implementation of ITryGetResultHandling<in CodeObjectifierRequest,Expression>
		public bool TryProcess(CodeObjectifierRequest request, out Expression result) {
			var instruction = request.Source;
			return _out.True(out result, _codeObjectifierFinder.Get(instruction.Code).Handle(instruction, request.Context));
		}
		#endregion
	}
}