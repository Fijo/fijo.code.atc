using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Model.Requests;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifierHandlings.Base {
	public interface ICodeObjectifierHandling : ITryGetResultHandling<CodeObjectifierRequest, Expression> {}
}