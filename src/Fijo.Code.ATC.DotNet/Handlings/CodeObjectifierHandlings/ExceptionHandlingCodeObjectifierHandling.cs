using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifierHandlings.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Dto;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Object;
using Mono.Cecil.Cil;
using ExceptionHandler = Fijo.Code.CecilContrib.Dto.ExceptionHandler;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifierHandlings {
	public class ExceptionHandlingCodeObjectifierHandling : ICodeObjectifierHandling {
		private readonly IList<ExceptionHandlerType> _supportedExceptionHandlerTypes = new[] {ExceptionHandlerType.Catch, ExceptionHandlerType.Finally};

		private readonly ITypeMappingService _typeMappingService;
		private readonly IDotNetCodeFactory _codeFactory;
		private readonly ICodeObjectifierService _codeObjectifierService;
		private readonly IOut _out;

		public ExceptionHandlingCodeObjectifierHandling(ITypeMappingService typeMappingService, IDotNetCodeFactory codeFactory, ICodeObjectifierService codeObjectifierService, IOut @out) {
			_typeMappingService = typeMappingService;
			_codeFactory = codeFactory;
			_codeObjectifierService = codeObjectifierService;
			_out = @out;
		}

		#region Implementation of ITryGetResultHandling<in CodeObjectifierRequest,Expression>
		public bool TryProcess(CodeObjectifierRequest request, out Expression result) {
			var context = request.Context;
			var source = request.Source;
			HandledExceptionBlock exceptionHandler;
			return _out.Generic(out result, context.Context.ExceptionHandlers.TryGetValue(source.Source, out exceptionHandler),
			                    () => GetHandledExceptionExpression(context, exceptionHandler));
		}
		#endregion

		private Expression GetHandledExceptionExpression(CodeObjectifierCurrentContext context, HandledExceptionBlock exceptionHandler) {
			var exceptionHandlers = exceptionHandler.ExceptionHandler;

			// todo check what to do with the FilterStart (that may even not be at the correct place in the ExceptionHandler)
			// they seem to be used by vb.net but not by c#

			ValidateExceptionHandler(exceptionHandlers);

			return _codeFactory.CreateTryExpression(
				GetHandledExceptionPartContainer(exceptionHandler.TryStart, exceptionHandler.TryEnd, context),
				GetCatches(context, exceptionHandlers),
				GetFinallyContainer(context, exceptionHandlers)); // todo look how to detect a finally 
		}
		#region Exception Handlers
		private void ValidateExceptionHandler(ICollection<ExceptionHandler> exceptionHandlers) {
			// ensure that no code is lost without noting it
			var notSupportedHandlers = exceptionHandlers.Where(x => !IsInExtention.IsIn<ExceptionHandlerType>(x.HandlerType, _supportedExceptionHandlerTypes)).Execute();
			if (notSupportedHandlers.Any())
				// todo may improve that exception type
				throw new NotSupportedException(string.Format("The exeption type(s) {0} of the given exeption handler(s) are currently not supported. Currently only the following exception types are supported: {1}",
				                                              string.Join(", ", notSupportedHandlers.Select(x => x.HandlerType)), string.Join(", ", _supportedExceptionHandlerTypes)));
		}

		private Expression GetFinallyContainer(CodeObjectifierCurrentContext context, ICollection<ExceptionHandler> exceptionHandlers) {
			var finallyHandlers = exceptionHandlers.Where(x => x.HandlerType == ExceptionHandlerType.Finally).Execute();
			switch (finallyHandlers.Count) {
				case 0:
					return null;
				case 1:
					var finallyHandler = finallyHandlers.Single();
					return GetHandledExceptionContainer(context, finallyHandler);
				default:
					// todo may improve that exception type
					throw new InvalidOperationException("Invalid HandledExeptionBlock (try, catch/ finally block). There cannot be more than one finally per try, catch/ finally block.");
			}
		}

		private Catches GetCatches(CodeObjectifierCurrentContext context, ICollection<ExceptionHandler> exceptionHandlers) {
			var catchHandlers = exceptionHandlers.Where(x => x.HandlerType == ExceptionHandlerType.Catch);
			return new Catches(catchHandlers.Select(x => CreateCatchExpression(context, x)));
		}

		private CatchExpression CreateCatchExpression(CodeObjectifierCurrentContext context, ExceptionHandler exceptionHandler) {
			return _codeFactory.CreateCatchExpression(GetCatchType(context, exceptionHandler),
			                                          GetHandledExceptionContainer(context, exceptionHandler));
		}

		private Expression GetHandledExceptionContainer(CodeObjectifierCurrentContext context, ExceptionHandler exceptionHandler) {
			return GetHandledExceptionPartContainer(exceptionHandler.HandlerStart, exceptionHandler.HandlerEnd, context);
		}

		private Type GetCatchType(CodeObjectifierCurrentContext context, ExceptionHandler x) {
			return _typeMappingService.Map(context.Context.Context, x.CatchType);
		}

		private Expression GetHandledExceptionPartContainer(Instruction fromInstruction, Instruction toInstruction, CodeObjectifierCurrentContext context) {
			var underlyingThreadInstructionDict = context.Context.TreateInstructionDict;
			return GetHandledExceptionPartContainer(underlyingThreadInstructionDict[fromInstruction],
			                                        underlyingThreadInstructionDict[toInstruction], context);
		}

		private Expression GetHandledExceptionPartContainer(TreateInstruction fromInstruction, TreateInstruction toInstruction, CodeObjectifierCurrentContext context) {
			// fixme is that correct or do I have to clear the stack
			if (!context.Stack.None()) throw new Exception("The stack has to be empty at the point where a new try, catch or finally block starts"); // todo improve the exception type

			var instructions = context.Context.Instructions;
			var fromIndex = instructions.IndexOf(fromInstruction);
			if (fromIndex == -1) throw new InvalidOperationException("The ´fromInstruction´ have to be in the in the instruction list of the Context.");
			var toIndex = instructions.IndexOf(toInstruction);
			if (toIndex == -1) throw new InvalidOperationException("The ´toInstruction´ have to be in the in the instruction list of the Context.");
			var instructionsToTake = toIndex - fromIndex;
			if (instructionsToTake < 0) throw new InvalidOperationException("The ´fromInstruction´ has to occur before ´toInstruction´ in the instruction list of the context");

			var treateInstructions = instructions.Skip(fromIndex - 1).Take(instructionsToTake + 1);
			// ReSharper disable PossibleMultipleEnumeration
#if DEBUG
			treateInstructions = treateInstructions.ToArray();
			Debug.Assert(treateInstructions.First().InstanceEquals(fromInstruction));
			Debug.Assert(treateInstructions.Last().InstanceEquals(toInstruction));
#endif
			return _codeObjectifierService.GetContainer(context, treateInstructions);
			// ReSharper restore PossibleMultipleEnumeration
		}
		#endregion
	}
}