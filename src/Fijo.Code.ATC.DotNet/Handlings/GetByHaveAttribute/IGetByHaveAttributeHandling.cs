using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;

namespace Fijo.Code.ATC.DotNet.Handlings.GetByHaveAttribute {
	public interface IGetByHaveAttributeHandling : ITryGetResultHandling<HaveAttributeRequest, OopCode>{}
}