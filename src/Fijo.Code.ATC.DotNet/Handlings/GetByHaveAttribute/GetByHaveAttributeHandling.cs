using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Handlings.GetByHaveAttribute {
	public abstract class GetByHaveAttributeHandling<TSource, TResult> : GetByHaveAttributeHandling<TSource, TSource, TResult> where TSource : class, ICustomAttributeProvider
	                                                                                                                           where TResult : OopCode {
		protected GetByHaveAttributeHandling(IOut @out) : base(@out) {}

		protected override TSource KeySelector(TSource source) {
			return source;
		}
	}

	public abstract class GetByHaveAttributeHandling<TSource, TKey, TResult> : IGetByHaveAttributeHandling where TResult : OopCode where TSource : class, ICustomAttributeProvider {
		protected readonly IOut Out;

		protected GetByHaveAttributeHandling (IOut @out) {
			Out = @out;
		}

		public bool TryProcess(HaveAttributeRequest request, out OopCode result) {
			var source = request.HaveAttribute as TSource;
			TResult innerResult = null;
			return Out.Generic(out result, source != null && DictSelector(request.Context).TryGetValue(KeySelector(source), out innerResult),
			                   () => innerResult);
		}

		protected abstract TKey KeySelector(TSource source);

		protected abstract IDictionary<TKey, TResult> DictSelector(MappingContext context);
	}
}