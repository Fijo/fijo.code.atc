using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Handlings.GetByHaveAttribute {
	public class GetMemberByHaveAttributeHandling : GetByHaveAttributeHandling<IMemberDefinition, Member> {
		public GetMemberByHaveAttributeHandling(IOut @out) : base(@out) {}

		protected override IDictionary<IMemberDefinition, Member> DictSelector(MappingContext context) {
			return context.MembersDefs;
		}
	}
}