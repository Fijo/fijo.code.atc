using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Handlings.GetByHaveAttribute {
	public class GetGenericParameterByHaveAttributeHandling : GetByHaveAttributeHandling<GenericParameter, Type> {
		public GetGenericParameterByHaveAttributeHandling(IOut @out) : base(@out) {}

		protected override IDictionary<GenericParameter, Type> DictSelector(MappingContext context) {
			return context.GenericParameters;
		}
	}
}