using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Handlings.GetByHaveAttribute {
	public class GetReturnTypeByHaveAttributeHandling : GetByHaveAttributeHandling<MethodReturnType, ReturnType> {
		public GetReturnTypeByHaveAttributeHandling(IOut @out) : base(@out) {}

		protected override IDictionary<MethodReturnType, ReturnType> DictSelector(MappingContext context) {
			return context.ReturnTypes;
		}
	}
}