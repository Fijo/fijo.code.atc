using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class LdTokenCodeObjectifier : CodeObjectifier<LdTokenInstruction> {
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.LdToken; } }
		#endregion
		#region Overrides of CodeObjectifier<LdTokenInstruction>
		protected override Expression Handle(LdTokenInstruction instruction, CodeObjectifierCurrentContext context) {
			throw new System.NotImplementedException();
		}
		#endregion
	}
}