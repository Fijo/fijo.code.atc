using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class NopCodeObjectifier : CodeObjectifier<NopInstruction> {
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Nop; } }
		#endregion
		#region Overrides of CodeObjectifier<NopInstruction>
		protected override Expression Handle(NopInstruction instruction, CodeObjectifierCurrentContext context) {
			var expressions = context.Stack;
			return expressions.Count != 0 ? expressions.Pop() : null;
		}
		#endregion
	}
}