using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class ThrowObjectifier : CodeObjectifier<ThrowInstruction> {
		private readonly IDotNetCodeFactory _codeFactory;
		public ThrowObjectifier(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Throw; } }
		#endregion

		#region Overrides of CodeObjectifier<RetInstruction>
		protected override Expression Handle(ThrowInstruction instruction, CodeObjectifierCurrentContext context) {
			return _codeFactory.CreateThrowExpression(context.Stack.Pop());
		}
		#endregion
	}
}