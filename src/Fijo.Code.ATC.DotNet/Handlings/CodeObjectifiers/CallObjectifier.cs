using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class CallObjectifier : CodeObjectifier<CallInstruction> {
		private readonly ICodeObjectifierUtil _utli;
		public CallObjectifier(ICodeObjectifierUtil utli) {
			_utli = utli;
		}

		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Call; } }
		#endregion

		#region Overrides of CodeObjectifier<CallInstruction>
		protected override Expression Handle(CallInstruction instruction, CodeObjectifierCurrentContext context) {
			// ToDo may handle instances
			var target = instruction.Target;
			var methodRef = target as MethodReference;
			return _utli.GetCall(context, context.Context.Context.Methods[methodRef != null ? methodRef.Resolve() ?? methodRef : target]);
		}
		#endregion
	}
}