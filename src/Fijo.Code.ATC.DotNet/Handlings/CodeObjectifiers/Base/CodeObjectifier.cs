﻿using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base {
	public abstract class CodeObjectifier : ICodeObjectifier {
		#region Implementation of ICodeObjectifier
		public abstract TreatedCode Code { get; }
		public abstract Expression Handle(TreateInstruction instruction, CodeObjectifierCurrentContext context);
		#endregion
	}

	public abstract class CodeObjectifier<T> : CodeObjectifier where T : TreateInstruction {
		#region Implementation of ICodeObjectifier
		public override Expression Handle(TreateInstruction instruction, CodeObjectifierCurrentContext context) {
			return Handle((T) instruction, context);
		}

		protected abstract Expression Handle(T instruction, CodeObjectifierCurrentContext context);
		#endregion
	}
}