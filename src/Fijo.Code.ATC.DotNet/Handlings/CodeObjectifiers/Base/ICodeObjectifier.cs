using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base {
	public interface ICodeObjectifier {
		TreatedCode Code { get; }
		Expression Handle(TreateInstruction instruction, CodeObjectifierCurrentContext context);
	}
}