using System;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class NewArrCodeObjectifier : CodeObjectifier<NewArrInstruction> {
		private readonly IDotNetACLContextService _aclContextService;
		private readonly IGenericTypeService _genericTypeService;
		private readonly ITypeMappingService _typeMappingService;
		private readonly IDotNetCodeFactory _codeFactory;
		public NewArrCodeObjectifier(IDotNetACLContextService aclContextService, IGenericTypeService genericTypeService, ITypeMappingService typeMappingService, IDotNetCodeFactory codeFactory) {
			_aclContextService = aclContextService;
			_genericTypeService = genericTypeService;
			_typeMappingService = typeMappingService;
			_codeFactory = codeFactory;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.NewArr; } }
		#endregion
		#region Overrides of CodeObjectifier<NewArrInstruction>
		protected override Expression Handle(NewArrInstruction instruction, CodeObjectifierCurrentContext context) {

			//// ToDo may correct this ctor call after impl some template member stuff
			//context.Stack.Push(
			//_codeFactory.CreateCallExpression(_typeMappingService.Map(context.Context, instruction.Target));
			throw new NotImplementedException();
			
		}
		#endregion
	}
}