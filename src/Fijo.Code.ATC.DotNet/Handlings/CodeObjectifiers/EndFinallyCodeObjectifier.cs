using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class EndFinallyCodeObjectifier : CodeObjectifier<EndFinallyInstruction> {
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.EndFinally; } }
		#endregion
		#region Overrides of CodeObjectifier<EndFinallyInstruction>
		protected override Expression Handle(EndFinallyInstruction instruction, CodeObjectifierCurrentContext context) {
			// do nothing
			// exception handling is not implemented as CodeObjectifier
			// that is done in the service somewhere in the CodeObjectifierService or something that is used it in.
			return null;
		}
		#endregion
	}
}