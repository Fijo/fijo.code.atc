using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class LdLocCodeObjectifier : CodeObjectifier<LdLocInstruction> {
		private readonly ICodeObjectifierUtil _utli;

		public LdLocCodeObjectifier(ICodeObjectifierUtil utli) {
			_utli = utli;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.LdLoc; } }
		#endregion
		#region Overrides of CodeObjectifier<LdLocInstruction>
		protected override Expression Handle(LdLocInstruction instruction, CodeObjectifierCurrentContext context) {
			return _utli.GetVariable(context, context.Context.Variables[instruction.Target]);
		}
		#endregion
	}
}