using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Infrastructure.DesignPattern.Mapping;
using CILExpression = Fijo.Code.CecilContrib.Cil.Model.Expr.Expression;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class ConditionCodeObjectifier : CodeObjectifier<ConditionInstruction> {
		private readonly IMapping<ACLContextContainer, CILExpression, Method> _operatorMapping;
		private readonly ICodeObjectifierUtil _utli;

		public ConditionCodeObjectifier(IMapping<ACLContextContainer, CILExpression, Method> operatorMapping, ICodeObjectifierUtil utli) {
			_operatorMapping = operatorMapping;
			_utli = utli;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Condition; } }
		#endregion
		#region Overrides of CodeObjectifier<ConditionInstruction>
		protected override Expression Handle(ConditionInstruction instruction, CodeObjectifierCurrentContext context) {
			return _utli.GetCall(context, _operatorMapping.Map(context.Context.Context.ContextContainer, instruction.Expression));
		}
		#endregion
	}
}