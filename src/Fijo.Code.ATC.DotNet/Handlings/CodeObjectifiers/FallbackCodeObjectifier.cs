using System;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class FallbackCodeObjectifier : CodeObjectifier {
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { throw new NotSupportedException(); } }
		public override Expression Handle(TreateInstruction instruction, CodeObjectifierCurrentContext context) {
			return null;
		}
		#endregion
	}
}