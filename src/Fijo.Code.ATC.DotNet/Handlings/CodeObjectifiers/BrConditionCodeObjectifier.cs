using System;
using System.Diagnostics;
using System.Linq;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Extentions;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.LightContrib.Extentions.Object;
using Mono.Cecil.Cil;
using CILExpression = Fijo.Code.CecilContrib.Cil.Model.Expr.Expression;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class BrConditionCodeObjectifier : CodeObjectifier<BrConditionalInstruction> {
		private readonly IMapping<ACLContextContainer, CILExpression, Method> _operatorMapping;
		private readonly ICodeObjectifierUtil _utli;
		private readonly IDotNetCodeFactory _codeFactory;

		public BrConditionCodeObjectifier(IMapping<ACLContextContainer, CILExpression, Method> operatorMapping, ICodeObjectifierUtil utli, IDotNetCodeFactory codeFactory) {
			_operatorMapping = operatorMapping;
			_utli = utli;
			_codeFactory = codeFactory;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.BrConditional; } }
		#endregion
		#region Overrides of CodeObjectifier<BrConditionalInstruction>
		protected override Expression Handle(BrConditionalInstruction instruction, CodeObjectifierCurrentContext context) {
			var expression = instruction.Expression;
			// ToDo hacky using the stack for to build the instruction
			var currentContext = context.Context;
			var objectifierContext = currentContext.Context;
			if (expression.HasConstantRightValue)
				context.Stack.Push(_codeFactory.CreateValueExpression(
					_codeFactory.CreateNativeInstance(currentContext.DotNetACLContext.TypeSystem[expression.RightValueType],
					                                  expression.RightValue)));
			var target = _operatorMapping.Map(objectifierContext.ContextContainer, expression);
			var call = _utli.GetCall(context, target);
			Debug.Assert(call == null, "The called Function should not have the return type �void�");
			var condition = context.Stack.Pop();
			Debug.Assert(condition is CallExpression && ((CallExpression) condition).Target.InstanceEquals(target));
			return _codeFactory.CreateIfExpression(condition, _codeFactory.GotoExpression(GetTargetInstruction(instruction.Target, context)));
		}

		private Expression GetTargetInstruction(Instruction target, CodeObjectifierCurrentContext context) {
			var targetInstruction = context.Context.InstructionDict[target];
			// dict mit stack und operation die den stack ...
			// wenn keine targetInstruction und auch keine als einstiegspunkt mehr folgend
			// unrolled eine stack operation bauen

			Debug.Assert(targetInstruction != null);
			return targetInstruction;
		}
		#endregion
	}
}