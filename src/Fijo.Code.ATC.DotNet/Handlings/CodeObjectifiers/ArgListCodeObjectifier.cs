using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class ArgListCodeObjectifier : CodeObjectifier<ArgListInstruction> {
		private readonly IDotNetACLContextService _aclContextService;
		private readonly IGenericTypeService _genericTypeService;
		public ArgListCodeObjectifier(IGenericTypeService genericTypeService, IDotNetACLContextService aclContextService) {
			_genericTypeService = genericTypeService;
			_aclContextService = aclContextService;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.ArgList; } }
		#endregion
		#region Overrides of CodeObjectifier<ArgListInstruction>
		protected override Expression Handle(ArgListInstruction instruction, CodeObjectifierCurrentContext context) {
			return null;
		}
		#endregion
	}
}