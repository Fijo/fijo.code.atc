using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class DupCodeObjectifier : CodeObjectifier<DupInstruction> {
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Dup; } }
		#endregion
		#region Overrides of CodeObjectifier<DupInstruction>
		protected override Expression Handle(DupInstruction instruction, CodeObjectifierCurrentContext context) {
			var stack = context.Stack;
			var value = stack.Pop();
			stack.Push(value);
			stack.Push(value);
			return null;
		}
		#endregion
	}
}