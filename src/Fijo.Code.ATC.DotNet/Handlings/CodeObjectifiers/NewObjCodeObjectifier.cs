using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class NewObjCodeObjectifier : CodeObjectifier<NewObjInstruction> {
		private readonly ICodeObjectifierUtil _utli;
		public NewObjCodeObjectifier(ICodeObjectifierUtil utli) {
			_utli = utli;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.NewObj; } }
		#endregion
		#region Overrides of CodeObjectifier<NewObjInstruction>
		protected override Expression Handle(NewObjInstruction instruction, CodeObjectifierCurrentContext context) {
			var methodReference = instruction.Target;
			return _utli.GetCall(context, context.Context.Context.Methods[methodReference.Resolve() ?? methodReference]);
		}
		#endregion
	}
}