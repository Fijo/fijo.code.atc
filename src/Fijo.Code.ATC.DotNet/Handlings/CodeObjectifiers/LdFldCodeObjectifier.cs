using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class LdFldCodeObjectifier : CodeObjectifier<LdFldInstruction> {
		private readonly ICodeObjectifierUtil _utli;
		public LdFldCodeObjectifier(ICodeObjectifierUtil utli) {
			_utli = utli;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.LdFld; } }
		#endregion
		#region Overrides of CodeObjectifier<LdFldInstruction>
		protected override Expression Handle(LdFldInstruction instruction, CodeObjectifierCurrentContext context) {
			return _utli.GetField(instruction.Target, context);
		}
		#endregion
	}
}