using System.Diagnostics;
using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Data;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Extentions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class LdcCodeObjectifier : CodeObjectifier<LdcInstruction> {
		private readonly IDotNetCodeFactory _codeFactory;
		public LdcCodeObjectifier(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Ldc; } }
		#endregion
		#region Overrides of CodeObjectifier<LdcInstruction>
		protected override Expression Handle(LdcInstruction instruction, CodeObjectifierCurrentContext context) {
			Debug.Assert(instruction.Type.IsCoreType());
			context.Stack.Push(_codeFactory.CreateValueExpression(GetInstance(instruction, context.Context)));
			return null;
		}

		private Instance GetInstance(LdcInstruction instruction, CodeObjectifierContext context) {
			return _codeFactory.CreateNativeInstance(context.Context.Types[instruction.Type], instruction.Target);
		}
		#endregion
	}
}