using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class ReadonlyCodeObjectifier : CodeObjectifier<ReadonlyInstruction> {
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Readonly; } }
		#endregion
		#region Overrides of CodeObjectifier<ReadonlyInstruction>
		protected override Expression Handle(ReadonlyInstruction instruction, CodeObjectifierCurrentContext context) {
			throw new System.NotImplementedException();
		}
		#endregion
	}
}