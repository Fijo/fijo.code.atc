using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Code.CecilContrib.Enums;
using Fijo.Code.CecilContrib.Extentions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class RetObjectifier : CodeObjectifier<RetInstruction> {
		private readonly IDotNetCodeFactory _codeFactory;
		public RetObjectifier(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.Ret; } }
		#endregion
		#region Overrides of CodeObjectifier<RetInstruction>
		protected override Expression Handle(RetInstruction instruction, CodeObjectifierCurrentContext context) {
			return GetReturnExpression(GetReturnValue(context));
		}

		private Expression GetReturnValue(CodeObjectifierCurrentContext context) {
			return context.Context.ReturnType.IsCoreType(CoreTypes.Void) ? null : context.Stack.Pop();
		}

		private ReturnExpression GetReturnExpression(Expression value) {
			return _codeFactory.CreateReturnExpression(value);
		}
		#endregion
	}
}