using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;

namespace Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers {
	public class LdArgCodeObjectifier : CodeObjectifier<LdArgInstruction> {
		private readonly ICodeObjectifierUtil _utli;

		public LdArgCodeObjectifier(ICodeObjectifierUtil utli) {
			_utli = utli;
		}
		#region Overrides of CodeObjectifier
		public override TreatedCode Code { get { return TreatedCode.LdArg; } }
		#endregion
		#region Overrides of CodeObjectifier<LdArgInstruction>
		protected override Expression Handle(LdArgInstruction instruction, CodeObjectifierCurrentContext context) {
			return _utli.GetVariable(context, context.Context.Parameters[instruction.Target]);
		}
		#endregion
	}
}