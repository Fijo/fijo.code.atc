using System.Linq;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.DotNet.Model.Attr;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Infrastructure.DesignPattern.Mapping;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class GenericParamsAttrMapping : IMapping<InternGenericParamContext, GenericParamsAttr> {
		#region Implementation of IMapping<in InternGenericParamContext,out GenericParamsAttr>
		public GenericParamsAttr Map(InternGenericParamContext source) {
			var typeByGenericParameters = source.TypeByGenericParameters;
			return GetGenericParamsAttr(new Types(source.GenericParameters.AsParallel().Select(y => typeByGenericParameters[y])));
		}

		private GenericParamsAttr GetGenericParamsAttr(Types types) {
			return new GenericParamsAttr {Types = types};
		}
		#endregion
	}
}