using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class ModuleAssemblyMapping : IMapping<ModuleDefinition, Assembly> {
		private readonly IDotNetCodeFactory _codeFactory;

		public ModuleAssemblyMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}

		public Assembly Map(ModuleDefinition source) {
			return _codeFactory.CreateModule(source.Name, null);
		}
	}
}