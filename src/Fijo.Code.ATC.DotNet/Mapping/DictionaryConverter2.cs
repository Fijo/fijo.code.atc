﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Fijo.Infrastructure.Model;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Typing.DictionaryType;
using FijoCore.Infrastructure.LightContrib.Module.Typing.KeyValuePairType;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Fijo.Code.ATC.DotNet.Mapping {
		[Serializable]
		public class SerializationDict {
			public KeyValuePair[] Content{get;set;}
			public Type DictionaryType{get;set;}
			public string TypeName{get;set;}

			public SerializationDict() {
			}

			public SerializationDict(KeyValuePair[] content, Type dictionaryType, string typeName) {
				Content = content;
				DictionaryType = dictionaryType;
				TypeName = typeName;
			}
		}
	public class DictionaryConverter2 : JsonConverter {

		private readonly IDictionary<Type, ConstructorInfo> _constructorCache = new Dictionary<Type, ConstructorInfo>();
		private readonly IDictionary<Type, Type> _dictionaryToKeyValuePair = new Dictionary<Type, Type>();
		private readonly IDictionaryTypeService _dictionaryTypeService;
		private readonly IGenericKeyValuePairAccessorProvider _genericKeyValuePairAccessorProvider;
		private readonly Type[] _constructorTypeArray = new Type[0];

		public override bool CanRead { get { return true; } }

		public DictionaryConverter2(IDictionaryTypeService dictionaryTypeService, IGenericKeyValuePairAccessorProvider genericKeyValuePairAccessorProvider) {
			_dictionaryTypeService = dictionaryTypeService;
			_genericKeyValuePairAccessorProvider = genericKeyValuePairAccessorProvider;
		}

		#region Overrides of JsonConverter
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
			serializer.Serialize(writer, MapForSerialize((IEnumerable) value));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
			return MapDeserialized((SerializationDict) serializer.Deserialize(reader, typeof (SerializationDict)));
		}

		public override bool CanConvert(System.Type objectType) {
			return _dictionaryTypeService.IsDictionary(objectType);
		}
		#endregion
		
		private object MapForSerialize(IEnumerable value) {
			var type = value.GetType();
			var accessor = _genericKeyValuePairAccessorProvider.Get(GetKeyValuePairType(_dictionaryTypeService.GetDictionaryType(type)));
			return new SerializationDict(value.Cast<object>().Select(x => new KeyValuePair(accessor.Value(x), accessor.Key(x))).ToArray(), type, type.FullName);
		}

		private Type GetKeyValuePairType([NotNull] Type dictionaryType) {
			return _dictionaryToKeyValuePair.GetOrCreate(dictionaryType, () => GetInternKeyValuePairType(dictionaryType));
		}

		private Type GetInternKeyValuePairType([NotNull] Type dictionaryType) {
			if (dictionaryType == typeof (IDictionary)) return typeof (KeyValuePair<object, object>);
			Debug.Assert(dictionaryType.IsGenericType && !dictionaryType.IsGenericTypeDefinition && dictionaryType.GetGenericTypeDefinition() == typeof (IDictionary<,>));
			return typeof (KeyValuePair<,>).MakeGenericType(dictionaryType.GetGenericArguments());
		}

		private object MapDeserialized(SerializationDict value) {
			if (value == null) return null;

			var dictionaryType = value.DictionaryType;
			var dict = (IDictionary) GetConstructor(dictionaryType).Invoke(null);
			value.Content.ForEach(x => dict.Add(x.Key, x.Value));
			return dict;
		}

		private ConstructorInfo GetConstructor(Type dictionaryType) {
			return _constructorCache.GetOrCreate(dictionaryType, () => dictionaryType.GetConstructor(_constructorTypeArray));
		}
	}
}