using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;
using Assembly = Fijo.Code.ATC.ACL.Model.Oop.Assembly;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class AssemblyMapping : IMapping<AssemblyDefinition, Assembly> {
		private readonly IDotNetCodeFactory _codeFactory;
		private readonly IMapping<AssemblyNameDefinition, AssemblyName> _assemblyNameMapping;

		public AssemblyMapping(IDotNetAttributeFactory attributeFactory, IDotNetCodeFactory codeFactory, IMapping<AssemblyNameDefinition, AssemblyName> assemblyNameMapping) {
			_codeFactory = codeFactory;
			_assemblyNameMapping = assemblyNameMapping;
		}
		#region Implementation of IMapping<in AssemblyDefinition,out Assembly>
		public Assembly Map(AssemblyDefinition source) {
			return _codeFactory.CreateAssembly(_assemblyNameMapping.Map(source.Name), null);
		}
		#endregion
	}
}