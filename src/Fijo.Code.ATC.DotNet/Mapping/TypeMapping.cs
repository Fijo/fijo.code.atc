using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class TypeMapping : IMapping<TypeDefinition, Type> {
		private readonly IMapping<TypeDefinition, Modifier> _modifierMapping;
		private readonly IDotNetCodeFactory _codeFactory;

		public TypeMapping(IMapping<TypeDefinition, Modifier> modifierMapping, IDotNetCodeFactory codeFactory) {
			_modifierMapping = modifierMapping;
			_codeFactory = codeFactory;
		}
		#region Implementation of IMapping<in TypeDefinition,out Namespaces>
		public Type Map(TypeDefinition source) {
			return _codeFactory.CreateType(_modifierMapping.Map(source), source.Name);
		}
		#endregion
	}
}