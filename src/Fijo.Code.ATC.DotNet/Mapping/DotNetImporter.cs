﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.Common.Infrastructure.Factories.Interface;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.Core.Base;
using Fijo.Code.ATC.DotNet.Adjustments;
using Fijo.Code.ATC.DotNet.Factory;
using Fijo.Code.ATC.DotNet.Factory.Interfaces;
using Fijo.Code.ATC.DotNet.Framework;
using Fijo.Code.ATC.DotNet.Model;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Dto;
using Fijo.Code.CecilContrib.Extentions;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Newtonsoft.Json;
using Assembly = Fijo.Code.ATC.ACL.Model.Oop.Assembly;
using Attribute = Fijo.Code.ATC.ACL.AdvancedOop.Dto.Attribute;
using Module = Fijo.Code.ATC.ACL.Model.Oop.Module;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;
#if ImporterCache
using Fijo.Infrastructure.DesignPattern.Mapping.Impl;
using ICustomAttributeProvider = Mono.Cecil.ICustomAttributeProvider;
using MethodBody = Mono.Cecil.Cil.MethodBody;
#endif

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class JsonSettings : SingletonRepositoryBase<JsonSerializerSettings>, IJsonSettings {
		private readonly DictionaryConverter2 _dictionaryConverter;

		public JsonSettings(DictionaryConverter2 dictionaryConverter) {
			_dictionaryConverter = dictionaryConverter;
		}

		protected override JsonSerializerSettings Create() {
			var jsonSerializerSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.All,
				PreserveReferencesHandling = PreserveReferencesHandling.Objects
			};
			jsonSerializerSettings.Converters.Add(_dictionaryConverter);
			return jsonSerializerSettings;
		}
	}

	public class DotNetImporter : CodeImporter<DotNetModule> {
		protected class InternContext {
			public MappingContext MappingContext { get; set; }
			public bool UseNullMapping { get; set; }
			public ICollection<ModuleDefinition> AllModules { get; set; }
			public ICollection<IMetadataTokenProvider> MemberDefs { get; set; }
			public ICollection<IMethodSignature> MethodDefs { get; set; }
		}

		private readonly IMappingObjectFactory _mappingObjectsFactory;
		private readonly IAssemblyService _assemblyService;
		private readonly IMapping<AssemblyDefinition, Assembly> _assemblyMapping;
		private readonly IMapping<TypeDefinition, Type> _typeMapping;
		private readonly IMapping<InternNamespace, Namespace> _namespaceMapping;
		private readonly IMapping<IMetadataTokenProvider, ACL.Model.Oop.Member> _memberMapping;
		private readonly IMapping<ParameterDefinition, Parameter> _parameterMapping;
		private readonly IMapping<GenericParameter, Type> _genericParameterMapping;
		private readonly IMapping<ICustomAttribute, Attribute> _attributeMapping;
		private readonly IMapping<ModuleDefinition, Assembly> _moduleAssemblyMapping;
		private readonly IMapping<MethodReturnType, ReturnType> _returnTypeMapping;
		private readonly IMapping<VariableDefinition, Variable> _variableMapping;
		private readonly ICodeService _codeService;
		private readonly IEqualityComparer<TypeDefinition> _typeDefentionEqualityComparer;
		private readonly IPairFactory _pairFactory;
		private readonly IACLContextContainerFactory _containerFactory;
		private readonly IDotNetACLContextFactory _dotNetContextAclFactory;
		private readonly IEqualityComparer<AssemblyNameDefinition> _assemblyNameDefinitionEqualityComparer;
		private readonly IEqualityComparer<AssemblyNameReference> _assemblyNameReferenceEqualityComparer;
		private readonly IEqualityComparer<AssemblyDefinition> _assemblyDefenitionEqualityComparer;
		private readonly IEqualityComparer<ModuleDefinition> _moduleDefenitionEqualityComparer;
		private readonly ISerializationFacade _serializationFacade;
		private readonly JsonSettings _jsonSettings;
		private readonly IBinarySettings _binarySettings;
		private readonly IEnumerable<IAdjustment<MappingContext>> _adjustments;
		private readonly MD5 _md5Service = new MD5CryptoServiceProvider();

		public DotNetImporter(IMappingObjectFactory mappingObjectsFactory, IAssemblyService assemblyService, IMapping<AssemblyDefinition, Assembly> assemblyMapping, IAdjustment<MappingContext>[] adjustments, IMapping<TypeDefinition, Type> typeMapping, IMapping<InternNamespace, Namespace> namespaceMapping, IMapping<IMetadataTokenProvider, ACL.Model.Oop.Member> memberMapping, IMapping<ParameterDefinition, Parameter> parameterMapping, ICodeService codeService, IEqualityComparer<TypeDefinition> typeDefentionEqualityComparer, IMapping<GenericParameter, Type> genericParameterMapping, IPairFactory pairFactory, IACLContextContainerFactory containerFactory, IDotNetACLContextFactory dotNetContextAclFactory, IMapping<ICustomAttribute, Attribute> attributeMapping, IMapping<ModuleDefinition, Assembly> moduleAssemblyMapping, IEqualityComparer<AssemblyNameDefinition> assemblyNameDefinitionEqualityComparer, IEqualityComparer<AssemblyNameReference> assemblyNameReferenceEqualityComparer, IEqualityComparer<AssemblyDefinition> assemblyDefenitionEqualityComparer, IEqualityComparer<ModuleDefinition> moduleDefenitionEqualityComparer, IMapping<MethodReturnType, ReturnType> returnTypeMapping, IMapping<VariableDefinition, Variable> variableMapping, ISerializationFacade serializationFacade, IOut @out, IBinarySettings binarySettings, JsonSettings jsonSettings) {
			_mappingObjectsFactory = mappingObjectsFactory;
			_assemblyService = assemblyService;
			_assemblyMapping = assemblyMapping;
			_adjustments = adjustments;
			_typeMapping = typeMapping;
			_namespaceMapping = namespaceMapping;
			_memberMapping = memberMapping;
			_parameterMapping = parameterMapping;
			_codeService = codeService;
			_typeDefentionEqualityComparer = typeDefentionEqualityComparer;
			_genericParameterMapping = genericParameterMapping;
			_pairFactory = pairFactory;
			_containerFactory = containerFactory;
			_dotNetContextAclFactory = dotNetContextAclFactory;
			_attributeMapping = attributeMapping;
			_moduleAssemblyMapping = moduleAssemblyMapping;
			_assemblyNameDefinitionEqualityComparer = assemblyNameDefinitionEqualityComparer;
			_assemblyNameReferenceEqualityComparer = assemblyNameReferenceEqualityComparer;
			_assemblyDefenitionEqualityComparer = assemblyDefenitionEqualityComparer;
			_moduleDefenitionEqualityComparer = moduleDefenitionEqualityComparer;
			_returnTypeMapping = returnTypeMapping;
			_variableMapping = variableMapping;
			_serializationFacade = serializationFacade;
			_jsonSettings = jsonSettings;
			_binarySettings = null;
		}

		#region Overrides of CodeImporter<DotNetModule>
		public override Module Map(DotNetModule source) {
			var context = InternMap(source);
			return GetModule(source, source.Assemblies, context);
		}

		private void AdjustContext(InternContext innerContext) {
			var context = innerContext.MappingContext;
			context.Assemblies = context.SourceAssemblies.Select(x => x.Value).ToDict(x => x, GetMapping(_assemblyMapping, innerContext).Map, _assemblyDefenitionEqualityComparer);
			context.Modules = innerContext.AllModules.ToDict(x => x, GetMapping(_moduleAssemblyMapping, innerContext).Map, _moduleDefenitionEqualityComparer);
			context.Namespaces = context.SourceNamespaces.AsParallel().SelectMany(x => x.Value.Select(y => _mappingObjectsFactory.GetInternNamespace(x.Key, y))).ToDict(x => x, _namespaceMapping.Map);
			context.Types = context.SourceTypes.ToDict(x => x, GetMapping(_typeMapping, innerContext).Map, _typeDefentionEqualityComparer);
			context.Members = innerContext.MemberDefs.ToDict(x => x, GetMapping(_memberMapping, innerContext).Map);
			context.MembersDefs = context.Members.AsParallel().Where(x => x.Key is IMemberDefinition).ToDict(x => (IMemberDefinition) x.Key, x => x.Value);
			context.Fields = context.MembersDefs.AsParallel().Where(x => x.Key is FieldDefinition).ToDict(x => (FieldDefinition) x.Key, x => (Field) x.Value);
			context.Methods = context.Members.AsParallel().Where(x => x.Key is MethodDefinition).ToDict(x => (IMethodSignature) x.Key, x => (Method) x.Value);
			context.MethodsDefs = context.Methods.AsParallel().Where(x => x.Key is MethodDefinition).ToDict(x => (MethodDefinition) x.Key, x => x.Value);
			context.ReturnTypes = context.SourceReturnTypes.Select(x => x.Value).ToDict(x => x, GetMapping(_returnTypeMapping, innerContext).Map);
			context.GenericParameters = context.SourceGenericParameters.SelectMany(x => x.Value).ToDict(x => x, GetMapping(_genericParameterMapping, innerContext).Map);
			context.Parameters = context.SourceParameters.SelectMany(x => x.Value).ToDict(x => x, GetMapping(_parameterMapping, innerContext).Map);
			context.Attributes = context.SourceAttributesByProvider.SelectMany(x => x.Value).ToDict(x => x, GetMapping(_attributeMapping, innerContext).Map);
			context.Variables = context.SourceVariables.SelectMany(x => x.Value).ToDict(x => x, GetMapping(_variableMapping, innerContext).Map);
			//var test = context.Methods.Select(x => x.Key.Body).Where(x => x != null).OrderByDescending(x => x.CodeSize).SkipWhile(x => !x.HasVariables || !x.HasExceptionHandlers).First();
		}

		private InternContext GetContext(DotNetModule source) {
			var context = GetContext();
			var innerContext = GetInnerContext(context);
			var allAssemblies = GetAllAssemblies(source.Assemblies).Execute();

			context.SourceAssemblies = allAssemblies.ToDict(x => (AssemblyNameReference) x.Name, x => x, _assemblyNameReferenceEqualityComparer);
			context.SourceModulesByAssembly = allAssemblies.ToDict(x => x.Name, x => (IList<ModuleDefinition>) x.Modules, _assemblyNameDefinitionEqualityComparer);
			context.SourceModule = context.SourceModulesByAssembly.SelectMany(x => x.Value).ToDict(x => (ModuleReference) x, x => x);
			innerContext.AllModules = context.SourceModule.Select(x => x.Value).Execute();
			context.SourceRefAssembliesByModule = innerContext.AllModules.ToDict(x => x, x => (IList<AssemblyDefinition>) x.AssemblyReferences.Select(y => context.SourceAssemblies[y]).ToList());
			context.SourceNamespaces = innerContext.AllModules.ToDict(x => x, x => (IList<string>) _assemblyService.GetNamespaces(x).ToList(), _moduleDefenitionEqualityComparer);
			context.SourceTypeByModule = innerContext.AllModules.ToDict(x => x, x => (IList<TypeDefinition>) _codeService.GetTypeDefs(x).ToList(), _moduleDefenitionEqualityComparer);
			context.SourceTypes = context.SourceTypeByModule.AsParallel().SelectMany(x => x.Value).Distinct().ToList();
			context.SourceMembers = context.SourceTypes.ToDict(x => x, x => (IList<IMetadataTokenProvider>) _codeService.Members(x).ToList(), _typeDefentionEqualityComparer);
			context.SourceMembersDefs = context.SourceMembers.ToDict(x => x.Key, x => (IList<IMemberDefinition>) x.Value.AsParallel().OfType<IMemberDefinition>().ToList());
			innerContext.MemberDefs = context.SourceMembers.SelectMany(x => x.Value).Execute();
			context.SourceParameters = innerContext.MemberDefs.AsParallel().OfType<IMethodSignature>().ToDict(x => x, GetParameters);
			innerContext.MethodDefs = context.SourceParameters.AsParallel().Select(x => x.Key).Execute();
			context.SourceReturnTypes = innerContext.MethodDefs.ToDict(x => x, x => x.MethodReturnType);
			context.SourceMethodByParam = context.SourceParameters.AsParallel().SelectMany(x => x.Value.Select(y => _pairFactory.KeyValuePair(y, x.Key))).ToDict(x => x.Key, x => x.Value);
			context.SourceGenericParameters = innerContext.MethodDefs.Cast<IGenericParameterProvider>().AsEnumerable().Concat(context.SourceTypes).ToDict(x => x, x => (IList<GenericParameter>) x.GenericParameters);
			context.SourceAttributesByProvider = GetCustomAttributeProvider(allAssemblies, innerContext.AllModules, context).ToDict(x => x, x => x.CustomAttributes);
			context.SourceVariables = innerContext.MethodDefs.AsParallel().OfType<MethodDefinition>().ToDict(x => x, GetVariables);
			return innerContext;
		}

		[OnlyPublicForTesting]
		public MappingContext InternMap(DotNetModule source) {
			var innerContext = GetContext(source);
			var context = innerContext.MappingContext;

			#if ImporterCache
			var cachePath = GetCachePath(source);
			if (TryAdjustFromCache(innerContext, cachePath)) return context;
			#endif

			AdjustContext(innerContext);
			AdjustContext(context);

			#if ImporterCache
			PersistToCache(cachePath, context);
			#endif
			return context;
		}

		private void AdjustContext(MappingContext context) {
			Parallel.ForEach(_adjustments, x => {
				var watch = new Stopwatch();
				watch.Stop();
				watch.Reset();
				watch.Start();
				x.Adjust(context);
				watch.Stop();
				Trace.WriteLine(string.Format("{0} took {1} milliseconds", x.GetType().Name,
				                              watch.ElapsedMilliseconds));
			});
		}

		#region Caching
		#if ImporterCache
		private bool TryAdjustFromCache(InternContext innerContext, string cachePath) {
			if (!CacheExists(cachePath)) return false;
			var context = innerContext.MappingContext;
			innerContext.UseNullMapping = true;
			AdjustContext(innerContext);

			var deserialize = ReadCacheableMappingContext(cachePath);
			context.ContextContainer = deserialize.ContextContainer;
			SetValues(context.Assemblies, deserialize.Assemblies, x => x.GetFullToken());
			SetValues(context.Attributes, deserialize.Attributes, GetAttributeTokenResolver(context));
			SetValues(context.Fields, deserialize.Fields, x => x.GetFullToken());
			SetValues(context.GenericParameters, deserialize.GenericParameters, x => x.GetFullToken());
			SetValues(context.Members, deserialize.Members, x => x.GetFullToken());
			SetValues(context.MembersDefs, deserialize.MembersDefs, x => x.GetFullToken());
			SetValues(context.Methods, deserialize.Methods, x => x.GetFullToken());
			SetValues(context.MethodsDefs, deserialize.MethodsDefs, x => x.GetFullToken());
			SetValues(context.Modules, deserialize.Modules, x => x.GetFullToken());
			SetValues(context.Namespaces, deserialize.Namespaces, GetFullToken);
			SetValues(context.Parameters, deserialize.Parameters, GetParameterTokenResolver(context));
			SetValues(context.ReturnTypes, deserialize.ReturnTypes, x => x.GetFullToken());
			SetValues(context.Types, deserialize.Types, x => x.GetFullToken());
			SetValues(context.Variables, deserialize.Variables, GetVariableTokenResolver(context));
			return true;
		}

		private bool CacheExists(string cachePath) {
			return File.Exists(cachePath)
			#if DEBUG
			// just for easier development
			&& File.OpenRead(cachePath).Length != 0
			#endif
			;
		}

		private void PersistToCache(string cachePath, MappingContext context) {
			using (var fileStream = File.OpenWrite(cachePath)) {
				_serializationFacade.Serialize(SerializationFormat.JSON, GetCacheableMappingContext(context), fileStream, serializationSettings: _jsonSettings);
				fileStream.Close();
			}
		}

		private CacheableMappingContext ReadCacheableMappingContext(string cachePath) {
			CacheableMappingContext deserialize;
			var sw = new Stopwatch();
			sw.Stop();
			sw.Reset();
			sw.Start();
			using (var fileStream = File.OpenRead(cachePath)) {
				deserialize = _serializationFacade.Deserialize<CacheableMappingContext>(SerializationFormat.JSON, fileStream, serializationSettings: _jsonSettings);
				fileStream.Close();
			}
			sw.Stop();
			Trace.WriteLine(string.Format("Cached parts object loaded (took {0} ms)", sw.ElapsedMilliseconds));
			//Trace.WriteLine(string.Format("DictionaryConverter consumed {0} ms of that", DictionaryConverter.Stopwatch.ElapsedMilliseconds));
			return deserialize;
		}

		private CacheableMappingContext GetCacheableMappingContext(MappingContext context) {
			return new CacheableMappingContext
			{
				Assemblies = GetCachableDict(context.Assemblies, x => x.GetFullToken()),
				ContextContainer = context.ContextContainer,
				Attributes = GetCachableDict(context.Attributes, GetAttributeTokenResolver(context)),
				Fields = GetCachableDict(context.Fields, x => x.GetFullToken()),
				GenericParameters = GetCachableDict(context.GenericParameters, x => x.GetFullToken()),
				Members = GetCachableDict(context.Members, x => x.GetFullToken()),
				MembersDefs = GetCachableDict(context.MembersDefs, x => x.GetFullToken()),
				MethodsDefs = GetCachableDict(context.MethodsDefs, x => x.GetFullToken()),
				Methods = GetCachableDict(context.Methods, x => x.GetFullToken()),
				Modules = GetCachableDict(context.Modules, x => x.GetFullToken()),
				Namespaces = GetCachableDict(context.Namespaces, GetFullToken),
				Parameters = GetCachableDict(context.Parameters, GetParameterTokenResolver(context)),
				ReturnTypes = GetCachableDict(context.ReturnTypes, x => x.GetFullToken()),
				Types = GetCachableDict(context.Types, x => x.GetFullToken()),
				Variables = GetCachableDict(context.Variables, GetVariableTokenResolver(context))
			};
		}

		private Func<CustomAttribute, FullToken> GetAttributeTokenResolver(MappingContext context) {
			var sourceAttributesByProvider = context.SourceAttributesByProvider.AsParallel().SelectMany(x => x.Value.Select(y => _pairFactory.KeyValuePair(y, x.Key))).ToDict();
			return x => x.GetFullToken(sourceAttributesByProvider[x]);
		}
		
		private Func<ParameterDefinition, FullToken> GetParameterTokenResolver(MappingContext context) {
			var methodByParam = context.SourceMethodByParam;
			return x => x.GetFullToken(methodByParam[x]);
		}

		private Func<VariableDefinition, FullToken> GetVariableTokenResolver(MappingContext context) {
			var methodByVariable = context.MethodsDefs.AsParallel().Select(x => x.Key).Where(x => x.HasBody).SelectMany(x => x.Body.Variables.Select(y => _pairFactory.KeyValuePair(y, x))).ToDict();
			return x => x.GetFullToken(methodByVariable[x]);
		}

		private FullToken GetFullToken(InternNamespace x) {
			return new SerializableNamespaceName(x.Name).GetFullToken(x.Module);
		}

		private void SetValues<TKey, TValue>(IDictionary<TKey, TValue> target, IList<KeyValuePair<FullToken, TValue>> toSet, Func<TKey, FullToken> tokenResolver) {
			Debug.Assert(target.Keys.Count == toSet.Count, "Invalid cache -> entry count is incorrect");
			var keys = target.Keys.Execute();
			var toSetDict = toSet.ToDict(x => x.Key, x => x.Value);
			keys.ForEach((x, i) => target[x] = toSetDict[tokenResolver(x)]);
		}

		private IList<KeyValuePair<FullToken, TValue>> GetCachableDict<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> source, Func<TKey, FullToken> tokenResolver) {
			return source.Select(pair => _pairFactory.KeyValuePair(tokenResolver(pair.Key), pair.Value)).ToList();
		}

		private string GetCachePath(DotNetModule source) {
			return string.Format(@"C:\DotNetModuleAdjustmentCache\{0}", GetCacheKey(source));
		}

		private string GetCacheKey(DotNetModule source) {
			return BitConverter.ToString(_md5Service.ComputeHash(Encoding.Default.GetBytes(
				string.Format("{0}#{1}",
				              string.Join("|", _adjustments.Select(x => x.GetType().Name)),
				              string.Join("|", source.Assemblies.Select(x => x.FullName))))));
		}
		#endif
		#endregion
		
		[Note("will only affect when compiled with ´ImporterCache´")]
		private IMapping<TSource, TResult> GetMapping<TSource, TResult>(IMapping<TSource, TResult> mapping, InternContext innerContext) where TResult : class {
			#if ImporterCache
			return innerContext.UseNullMapping
				       ? NullMapping<TSource, TResult>.Default
				       : mapping;
			#else
			#region PreCondition
			Debug.Assert(!innerContext.UseNullMapping, "UseNullMapping should only be set to true when compiled with ´ImporterCache´");
			#endregion
			return mapping;
			#endif
		}

		private IList<ParameterDefinition> GetParameters(IMethodSignature method) {
			var methodDef = method as MethodDefinition;
			MethodBody body;
			if (methodDef == null || !methodDef.HasThis || (body = methodDef.Body) == null) return method.Parameters;
			return body.ThisParameter.IntoEnumerable().Concat(method.Parameters).ToArray();
		}

		private IList<VariableDefinition> GetVariables(MethodDefinition x) {
			var methodBody = x.Body;
			if (methodBody == null) return new VariableDefinition[0];
			return methodBody.Variables;
		}

		private IEnumerable<ICustomAttributeProvider> GetCustomAttributeProvider(IEnumerable<AssemblyDefinition> allAssemblies, IEnumerable<ModuleDefinition> allModules, MappingContext context) {
			return allAssemblies.Cast<ICustomAttributeProvider>()
			                    .Concat(allModules,
			                            context.SourceMembersDefs.SelectMany(x => x.Value),
			                            context.SourceGenericParameters.SelectMany(x => x.Value),
			                            context.SourceParameters.SelectMany(x => x.Value),
			                            context.SourceTypes,
			                            context.SourceMembersDefs.AsParallel().SelectMany(x => x.Value).OfType<MethodDefinition>().Select(x => x.MethodReturnType));
		}

		private InternContext GetInnerContext(MappingContext context) {
			return new InternContext {MappingContext = context};
		}

		private MappingContext GetContext() {
			return new MappingContext {ContextContainer = GetACLContextContainer()};
		}

		private ACLContextContainer GetACLContextContainer() {
			var container = _containerFactory.Create();
			container.Add(_dotNetContextAclFactory.Create());
			return container;
		}

		private IEnumerable<AssemblyDefinition> GetAllAssemblies(IEnumerable<AssemblyDefinition> sourceAssemblies) {
			#if TRACE
			var watch = new Stopwatch();
			watch.Stop();
			watch.Reset();
			watch.Start();
			Trace.WriteLine("Loading Assemblies...");
			#endif
			var allAssemblies = new HashSet<AssemblyDefinition>(sourceAssemblies, _assemblyDefenitionEqualityComparer);
			while (allAssemblies.AsParallel().SelectMany(_assemblyService.GetReferencedAssemblies).Execute().Select(x => {
				lock (allAssemblies) return allAssemblies.Add(x);
			}).Execute().Any(x => x)) {}
			#if TRACE
			watch.Stop();
			Trace.WriteLine(string.Format("Assemblies loaded (took {0} ms)", watch.ElapsedMilliseconds));
			#endif
			return allAssemblies;
		}

		private Module GetModule(DotNetModule source, IEnumerable<AssemblyDefinition> sourceAssemblies, MappingContext context) {
			return new Module
			{
				Name = source.Name,
				Assemblies = new ACL.Model.Collections.Assemblies(GetResultAssemblies(sourceAssemblies, context))
			};
		}

		private IEnumerable<Assembly> GetResultAssemblies(IEnumerable<AssemblyDefinition> sourceAssemblies, MappingContext context) {
			var assemblies = context.Assemblies;
			return sourceAssemblies.AsParallel().Select(x => assemblies[x]);
		}
		#endregion
	}
}