using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class ModifierMapping : IMapping<TypeDefinition, Modifier> {
		#region Implementation of IMapping<in TypeDefinition,out Modifier>
		public Modifier Map(TypeDefinition source) {
			if(source.IsPublic) return Modifier.Public;
			if(source.IsNestedPrivate) return Modifier.Private;
			if(source.IsNestedFamilyAndAssembly) return Modifier.ProtectedInternal;
			if(source.IsNestedFamily) return Modifier.Protected;
			return Modifier.Internal;
		}
		#endregion
	}
}