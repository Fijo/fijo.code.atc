using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping.Member {
	public class MemberMapping : IMapping<IMetadataTokenProvider, ACL.Model.Oop.Member> {
		private readonly IMapping<IMethodSignature, Method> _methodMapping;
		private readonly IMapping<IMemberDefinition, Field> _fieldMapping;

		public MemberMapping(IMapping<IMethodSignature, Method> methodMapping, IMapping<IMemberDefinition, Field> fieldMapping) {
			_methodMapping = methodMapping;
			_fieldMapping = fieldMapping;
		}
		#region Implementation of IMapping<in IMetadataTokenProvider,out Member>
		public ACL.Model.Oop.Member Map(IMetadataTokenProvider source) {
			var methodDefinition = source as IMethodSignature;
			return methodDefinition != null
				       ? (ACL.Model.Oop.Member) _methodMapping.Map(methodDefinition)
				       : _fieldMapping.Map((IMemberDefinition) source);
		}
		#endregion
	}
}