using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping.Member {
	public class MethodMapping : IMapping<IMethodSignature, Method> {
		private readonly IDotNetCodeFactory _codeFactory;

		public MethodMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}

		#region Implementation of IMapping<in IMethodSignature,out Method>
		public Method Map(IMethodSignature source) {
			return _codeFactory.CreateMethod(GetName(source));
		}

		private string GetName(IMethodSignature source) {
			var definition = source as MethodDefinition;
			return definition != null ? definition.Name : ((FunctionPointerType) source).Name;
		}
		#endregion
	}
}