using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping.Member {
	public class FieldMapping : IMapping<IMemberDefinition, Field> {
		private readonly IDotNetCodeFactory _codeFactory;

		public FieldMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}

		#region Implementation of IMapping<in IMemberDefinition,out Field>
		public Field Map(IMemberDefinition source) {
			return _codeFactory.CreateField(source.Name);
		}
		#endregion
	}
}