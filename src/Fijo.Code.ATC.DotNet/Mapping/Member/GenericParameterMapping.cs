using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping.Member {
	[About("GenericTypeParameterMapping")]
	public class GenericParameterMapping : IMapping<GenericParameter, Type> {
		private readonly IDotNetCodeFactory _codeFactory;

		public GenericParameterMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}

		#region Implementation of IMapping<in GenericParameter,out Type>
		public Type Map(GenericParameter source) {
			// ToDo impl Modifier
			var type = _codeFactory.CreateType(Modifier.Public, source.Name);
			type.OopAttrs.Add(new GenericTypeParamAttr());
			return type;
		}
		#endregion
	}
}