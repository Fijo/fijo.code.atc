using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class ReturnTypeMapping : IMapping<MethodReturnType, ReturnType> {
		private readonly IDotNetCodeFactory _codeFactory;
		public ReturnTypeMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Implementation of IMapping<in MethodReturnType,out ReturnType>
		public ReturnType Map(MethodReturnType source) {
			return _codeFactory.CreateReturnType();
		}
		#endregion
	}
}