using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Infrastructure.DesignPattern.Mapping;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class NamespaceMapping : IMapping<InternNamespace, Namespace> {
		private readonly IDotNetCodeFactory _codeFactory;
		public NamespaceMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Implementation of IMapping<in InternNamespace,out Namespace>
		public Namespace Map(InternNamespace source) {
			return _codeFactory.CreateNamespace(source.Name);
		}
		#endregion
	}
}