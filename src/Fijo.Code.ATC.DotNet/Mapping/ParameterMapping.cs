using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class ParameterMapping : IMapping<ParameterDefinition, Parameter> {
		private readonly IDotNetCodeFactory _codeFactory;
		public ParameterMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Implementation of IMapping<in ParameterDefinition,out Parameter>
		public Parameter Map(ParameterDefinition source) {
			return _codeFactory.CreateParameter(source.Name);
		}
		#endregion
	}
}