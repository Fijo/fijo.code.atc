using Fijo.Code.ATC.ACL.AdvancedOop.Dto;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class AttributeMapping : IMapping<ICustomAttribute, Attribute> {
		private readonly IDotNetCodeFactory _codeFactory;

		public AttributeMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}

		public Attribute Map(ICustomAttribute source) {
			return _codeFactory.Attribute();
		}
	}
}