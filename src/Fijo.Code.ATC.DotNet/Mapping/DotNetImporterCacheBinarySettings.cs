﻿using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;

namespace Fijo.Code.ATC.DotNet.Mapping {
	public class DotNetImporterCacheBinarySettings : SingletonRepositoryBase<BinarySerializerSettings>, IBinarySettings {
		#region Overrides of SingletonRepositoryBase<BinarySerializerSettings>
		protected override BinarySerializerSettings Create() {
			return new BinarySerializerSettings
			{
				UseUnsafeDeserialize = true
			};
		}
		#endregion
	}
}