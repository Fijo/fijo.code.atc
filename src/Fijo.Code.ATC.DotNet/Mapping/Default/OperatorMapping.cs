﻿using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Infrastructure.DesignPattern.Mapping;

namespace Fijo.Code.ATC.DotNet.Mapping.Default {
	public class OperatorMapping : IMapping<ACLContextContainer, Expression, Method> {
		private readonly IDictionary<OperatorCode, AtcCilOperators> _operatorMapping = new Dictionary<OperatorCode, AtcCilOperators>
		{
			{OperatorCode.Not, AtcCilOperators.Not},
			{OperatorCode.Equal, AtcCilOperators.Equal},
			{OperatorCode.NotEqual, AtcCilOperators.NotEqual},
			{OperatorCode.Greater, AtcCilOperators.Greater},
			{OperatorCode.GreaterUnsigned, AtcCilOperators.GreaterUnsigned},
			{OperatorCode.GreaterOrEqual, AtcCilOperators.GreaterThan},
			{OperatorCode.GreaterOrEqualUnsigned, AtcCilOperators.GreaterThanUnsigned},
			{OperatorCode.Less, AtcCilOperators.Less},
			{OperatorCode.LessUnsigned, AtcCilOperators.LessUnsigned},
			{OperatorCode.LessOrEqual, AtcCilOperators.LessThan},
			{OperatorCode.LessOrEqualUnsigned, AtcCilOperators.LessThanUnsigned},
			// ToDo finish that
		};

		#region Implementation of IMapping<in ACLContextContainer,in Expression,out Method>
		public Method Map(ACLContextContainer context, Expression source) {
			return context.OfType<DotNetACLContext>().Single().OperatorMethods[_operatorMapping[source.Code]];
		}
		#endregion
	}
}