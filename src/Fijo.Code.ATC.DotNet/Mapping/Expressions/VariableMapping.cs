using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Mono.Cecil.Cil;

namespace Fijo.Code.ATC.DotNet.Mapping.Expressions {
	public class VariableMapping : IMapping<VariableDefinition, Variable> {
		private readonly IDotNetCodeFactory _codeFactory;

		public VariableMapping(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}

		#region Implementation of IMapping<in VariableDefinition,out Variable>
		public Variable Map(VariableDefinition source) {
			return _codeFactory.CreateVariable(source.Name);
		}
		#endregion
	}
}