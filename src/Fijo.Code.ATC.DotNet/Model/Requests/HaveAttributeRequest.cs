using Fijo.Code.ATC.DotNet.Model.Intern;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Model.Requests {
	public class HaveAttributeRequest {
		public readonly MappingContext Context;
		public readonly ICustomAttributeProvider HaveAttribute;

		public HaveAttributeRequest(MappingContext context, ICustomAttributeProvider haveAttribute) {
			Context = context;
			HaveAttribute = haveAttribute;
		}
	}
}