using Fijo.Code.ATC.DotNet.Model.Intern;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Model.Requests {
	public class TypeMappingRequest {
		public readonly MappingContext Context;
		public readonly TypeReference Source;

		public TypeMappingRequest(MappingContext context, TypeReference source) {
			Context = context;
			Source = source;
		}
	}
}