using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;

namespace Fijo.Code.ATC.DotNet.Model.Requests {
	public class CodeObjectifierRequest {
		public readonly TreateInstruction Source;
		public readonly CodeObjectifierCurrentContext Context;

		public CodeObjectifierRequest(TreateInstruction source, CodeObjectifierCurrentContext context) {
			Source = source;
			Context = context;
		}
	}
}