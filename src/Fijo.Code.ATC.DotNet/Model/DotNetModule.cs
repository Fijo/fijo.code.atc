using System.Collections.Generic;
using System.Diagnostics;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Model {
	[DebuggerDisplay("DotNetModule: {Name}")]
	public class DotNetModule {
		public string Name { get; set; }
		public IList<AssemblyDefinition> Assemblies { get; set; }
	}
}