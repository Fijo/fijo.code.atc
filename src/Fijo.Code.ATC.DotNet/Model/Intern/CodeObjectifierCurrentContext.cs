using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Model.Expressions;

namespace Fijo.Code.ATC.DotNet.Model.Intern {
	public class CodeObjectifierCurrentContext {
		public CodeObjectifierContext Context { get; set; }
		public Stack<Expression> Stack { get; set; }
	}
}