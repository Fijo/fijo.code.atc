using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Model.Intern {
	[DebuggerDisplay("InternNamespace: {Name}")]
	public class InternNamespace {
		private readonly IEqualityComparer<ModuleDefinition> _moduleDefinitionEqualityComparer;
		public readonly string Name;
		public readonly ModuleDefinition Module;

		public InternNamespace(IEqualityComparer<ModuleDefinition> moduleDefinitionEqualityComparer, string name, ModuleDefinition module) {
			_moduleDefinitionEqualityComparer = moduleDefinitionEqualityComparer;
			Name = name;
			Module = module;
		}

		#region Equality
		protected bool Equals(InternNamespace other) {
			return Equals(Name, other.Name) &&
			       _moduleDefinitionEqualityComparer.Equals(Module, other.Module);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<InternNamespace>(obj, Equals);
		}

		public override int GetHashCode() {
			return Name.NullableGetHashCode().GetHashCodeAlgorithm(_moduleDefinitionEqualityComparer.GetHashCode(Module));
		}
		#endregion
	}
}