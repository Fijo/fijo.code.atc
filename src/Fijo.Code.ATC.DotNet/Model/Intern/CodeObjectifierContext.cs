using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Dto;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.ATC.DotNet.Model.Intern {
	public class CodeObjectifierContext {
		public IList<TreateInstruction> Instructions { get; set; }
		public IDictionary<Instruction, TreateInstruction> TreateInstructionDict { get; set; }
		public IDictionary<Instruction, Expression> InstructionDict { get; set; }
		public MethodBody Body { get; set; }
		public Method Method { get; set; }
		public TypeReference ReturnType { get; set; }
		public MappingContext Context { get; set; }
		public DotNetACLContext DotNetACLContext { get; set; }
		public IDictionary<VariableDefinition, ACL.Model.Oop.Variable> Variables { get; set; }
		public IDictionary<ParameterDefinition, ACL.Model.Oop.Parameter> Parameters { get; set; }
		public IDictionary<Instruction, HandledExceptionBlock> ExceptionHandlers { get; set; }
	}
}