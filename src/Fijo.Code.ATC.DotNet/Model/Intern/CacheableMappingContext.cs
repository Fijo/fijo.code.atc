﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.CecilContrib.Dto;
using Fijo.Infrastructure.Model.Pair;
using Mono.Cecil;
using Attribute = Fijo.Code.ATC.ACL.AdvancedOop.Dto.Attribute;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.DotNet.Model.Intern {
	[Serializable, DataContract]
	public class CacheableMappingContext {
		[DataMember(Order = 1)]
		public ACLContextContainer ContextContainer { get; set; }
		[DataMember(Order = 2)]
		public IList<KeyValuePair<FullToken, Assembly>> Assemblies { get; set; }
		[DataMember(Order = 3)]
		public IList<KeyValuePair<FullToken, Assembly>> Modules { get; set; }
		[DataMember(Order = 4)]
		public IList<KeyValuePair<FullToken, Namespace>> Namespaces { get; set; }
		[DataMember(Order = 5)]
		public IList<KeyValuePair<FullToken, Type>> Types { get; set; }
		[DataMember(Order = 6)]
		public IList<KeyValuePair<FullToken, Member>> Members { get; set; }
		[DataMember(Order = 7)]
		public IList<KeyValuePair<FullToken, Member>> MembersDefs { get; set; }
		[DataMember(Order = 8)]
		public IList<KeyValuePair<FullToken, Field>> Fields { get; set; }
		[DataMember(Order = 9)]
		public IList<KeyValuePair<FullToken, Method>> Methods { get; set; }
		[DataMember(Order = 10)]
		public IList<KeyValuePair<FullToken, Method>> MethodsDefs{ get; set; }
		[DataMember(Order = 11)]
		public IList<KeyValuePair<FullToken, ReturnType>> ReturnTypes { get; set; }
		[DataMember(Order = 12)]
		public IList<KeyValuePair<FullToken, Type>> GenericParameters { get; set; }
		[DataMember(Order = 13)]
		public IList<KeyValuePair<FullToken, Parameter>> Parameters { get; set; }
		[DataMember(Order = 14)]
		public IList<KeyValuePair<FullToken, Attribute>> Attributes { get; set; }
		[DataMember(Order = 15)]
		public IList<KeyValuePair<FullToken, Variable>> Variables { get; set; }
	}
}