using System;
using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.Model.Oop;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Collections.Generic;
using Assembly = Fijo.Code.ATC.ACL.Model.Oop.Assembly;
using Attribute = Fijo.Code.ATC.ACL.AdvancedOop.Dto.Attribute;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.DotNet.Model.Intern {
	public class MappingContext {
		public ACLContextContainer ContextContainer { get; set; }
		public IDictionary<AssemblyNameReference, AssemblyDefinition> SourceAssemblies { get; set; }
		public IDictionary<ModuleReference, ModuleDefinition> SourceModule { get; set; }
		public IDictionary<AssemblyNameDefinition, IList<ModuleDefinition>> SourceModulesByAssembly { get; set; }
		public IDictionary<ModuleDefinition, IList<AssemblyDefinition>> SourceRefAssembliesByModule { get; set; }
		public IDictionary<ModuleDefinition, IList<string>> SourceNamespaces { get; set; }
		public IDictionary<ModuleDefinition, IList<TypeDefinition>> SourceTypeByModule { get; set; }
		public IList<TypeDefinition> SourceTypes { get; set; }
		public IDictionary<TypeDefinition, IList<IMetadataTokenProvider>> SourceMembers { get; set; }
		public IDictionary<TypeDefinition, IList<IMemberDefinition>> SourceMembersDefs { get; set; }
		public IDictionary<IGenericParameterProvider, IList<GenericParameter>> SourceGenericParameters { get; set; }
		public IDictionary<IMethodSignature, IList<ParameterDefinition>> SourceParameters { get; set; }
		public IDictionary<IMethodSignature, MethodReturnType> SourceReturnTypes { get; set; }
		public IDictionary<ParameterDefinition, IMethodSignature> SourceMethodByParam { get; set; }
		public IDictionary<ICustomAttributeProvider, Collection<CustomAttribute>> SourceAttributesByProvider { get; set; }
		public IDictionary<MethodDefinition, IList<VariableDefinition>> SourceVariables { get; set; }

		public IDictionary<AssemblyDefinition, Assembly> Assemblies { get; set; }
		public IDictionary<ModuleDefinition, Assembly> Modules { get; set; }
		public IDictionary<InternNamespace, Namespace> Namespaces { get; set; }
		public IDictionary<TypeDefinition, Type> Types { get; set; }
		public IDictionary<IMetadataTokenProvider, Member> Members { get; set; }
		public IDictionary<IMemberDefinition, Member> MembersDefs { get; set; }
		public IDictionary<FieldDefinition, Field> Fields { get; set; }
		public IDictionary<IMethodSignature, Method> Methods { get; set; }
		public IDictionary<MethodDefinition, Method> MethodsDefs{ get; set; }
		public IDictionary<MethodReturnType, ReturnType> ReturnTypes { get; set; }
		public IDictionary<GenericParameter, Type> GenericParameters { get; set; }
		public IDictionary<ParameterDefinition, Parameter> Parameters { get; set; }
		public IDictionary<CustomAttribute, Attribute> Attributes { get; set; }
		public IDictionary<VariableDefinition, Variable> Variables { get; set; }
	}
}