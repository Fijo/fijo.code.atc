using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class ByReferenceTypeMapper : SimpleWrapperTypeMapper<ByReferenceType> {
		public ByReferenceTypeMapper(IGenericTypeService genericTypeService, ITypeMappingService typeMappingService, IOut @out) : base(genericTypeService, typeMappingService, @out) {}

		#region Overrides of SimpleWrapperTypeMapper<ByReferenceType>
		protected override TypeReference GetGenericTypeArgument(ByReferenceType source) {
			return source.ElementType;
		}

		protected override Type GetSpecialType(DotNetACLContext context) {
			return context.ByReferenceType;
		}
		#endregion
	}
}