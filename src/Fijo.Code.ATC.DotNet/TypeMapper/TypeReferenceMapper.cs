using System;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class TypeReferenceMapper : TypeMapper {
		private readonly ITypeMappingService _typeMappingService;
		private readonly IOut _out;

		public TypeReferenceMapper(ITypeMappingService typeMappingService, IOut @out) {
			_typeMappingService = typeMappingService;
			_out = @out;
		}

		#region Overrides of TypeMapper
		protected override bool IsFor(Type type) {
			return GetForType() == type;
		}

		protected override Type GetForType() {
			return typeof (TypeReference);
		}

		protected override bool TryMap(MappingContext context, TypeReference source, out ACL.Model.Oop.Type result) {
			return _out.True(out result, _typeMappingService.Map(context, source.Resolve()));
		}
		#endregion
	}
}