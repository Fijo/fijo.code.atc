using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;
using ArrayDimension = Fijo.Code.ATC.ACL.DotNet.Dto.ArrayDimension;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class ArrayTypeMapper : TypeMapper<ArrayType> {
		private readonly IDotNetACLContextService _aclContextService;
		private readonly IMapping<Mono.Cecil.ArrayDimension, ArrayDimension> _arrayDimensionMapping;
		private readonly IOut _out;
		private readonly IGenericTypeService _genericTypeService;
		private readonly ITypeMappingService _typeMappingService;

		public ArrayTypeMapper(IDotNetACLContextService aclContextService, IMapping<Mono.Cecil.ArrayDimension, ArrayDimension> arrayDimensionMapping, IOut @out, IGenericTypeService genericTypeService, ITypeMappingService typeMappingService) {
			_aclContextService = aclContextService;
			_arrayDimensionMapping = arrayDimensionMapping;
			_out = @out;
			_genericTypeService = genericTypeService;
			_typeMappingService = typeMappingService;
		}
		#region Overrides of TypeMapper<ArrayType>
		protected override bool TryMap(MappingContext context, ArrayType source, out Type result) {
			return _out.True(out result, CreateSpecificType(context, source));
		}

		private Type CreateSpecificType(MappingContext context, ArrayType source) {
			return _genericTypeService.CreateSpecificType(context.ContextContainer, GetGenericArrayType(context, source),
			                                              new[] {_typeMappingService.Map(context, source.ElementType)});
		}

		private Type GetGenericArrayType(MappingContext context, ArrayType source) {
			return _aclContextService.GetArrayType(context.ContextContainer.OfType<DotNetACLContext>().Single(),
			                                       source.Dimensions.AsParallel().Select(_arrayDimensionMapping.Map));
		}
		#endregion
	}
}