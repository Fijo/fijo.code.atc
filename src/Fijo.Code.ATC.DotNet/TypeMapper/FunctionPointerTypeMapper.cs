using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class FunctionPointerTypeMapper : TypeMapper<FunctionPointerType> {
		private readonly IDotNetACLContextService _contextService;
		private readonly IOut _out;

		public FunctionPointerTypeMapper(IDotNetACLContextService contextService, IOut @out) {
			_contextService = contextService;
			_out = @out;
		}
		#region Overrides of TypeMapper<FunctionPointerType>
		protected override bool TryMap(MappingContext context, FunctionPointerType source, out Type result) {
			return _out.True(out result, CreateSpecificType(context, source));
		}

		private Type CreateSpecificType(MappingContext context, FunctionPointerType source) {
			return _contextService.GetFunctionPointerType(GetDotNetACLContext(context), GetTargetMethod(context, source));
		}

		private Method GetTargetMethod(MappingContext context, FunctionPointerType source) {
			var targetMethod = context.Methods[source];
			return targetMethod;
		}

		private DotNetACLContext GetDotNetACLContext(MappingContext context) {
			return context.ContextContainer.OfType<DotNetACLContext>().Single();
		}
		#endregion
	}
}