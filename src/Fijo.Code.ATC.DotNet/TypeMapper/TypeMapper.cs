using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public abstract class TypeMapper : ITryGetResultHandling<TypeMappingRequest, Type> {
		protected virtual bool IsFor(System.Type type) {
			return type.IsAssignableFrom(GetForType());
		}

		protected abstract System.Type GetForType();

		#region Implementation of ITypeMapper
		protected abstract bool TryMap(MappingContext context, TypeReference source, out Type result);
		#endregion

		#region Implementation of ITryGetResultHandling<in TypeMappingRequest,Type>
		public bool TryProcess(TypeMappingRequest source, out Type result) {
			if(!IsFor(source.Source.GetType())) {
				result = null;
				return false;
			}
			return TryMap(source.Context, source.Source, out result);
		}
		#endregion
	}

	public abstract class TypeMapper<TSource> : TypeMapper where TSource : TypeReference {
		protected abstract bool TryMap(MappingContext context, TSource source, out Type result);

		protected override System.Type GetForType() {
			return typeof (TSource);
		}

		#region Implementation of ITypeMapper
		protected override bool TryMap(MappingContext context, TypeReference source, out Type result) {
			return TryMap(context, (TSource)source, out result);
		}
		#endregion
	}
}