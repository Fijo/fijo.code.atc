using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class TypeDefenitionMapper : TypeMapper<TypeDefinition> {
		private readonly IOut _out;

		public TypeDefenitionMapper(IOut @out) {
			_out = @out;
		}

		#region Overrides of TypeMapper<TypeDefinition>
		protected override bool TryMap(MappingContext context, TypeDefinition source, out Type result) {
			return _out.True(out result, context.Types[source]);
		}
		#endregion
	}
}