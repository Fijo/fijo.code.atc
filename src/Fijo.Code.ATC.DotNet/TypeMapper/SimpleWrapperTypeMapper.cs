using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public abstract class SimpleWrapperTypeMapper<T> : TypeMapper<T> where T : TypeReference {
		protected readonly IGenericTypeService GenericTypeService;
		protected readonly ITypeMappingService TypeMappingService;
		protected readonly IOut Out;

		protected SimpleWrapperTypeMapper(IGenericTypeService genericTypeService, ITypeMappingService typeMappingService, IOut @out) {
			GenericTypeService = genericTypeService;
			TypeMappingService = typeMappingService;
			Out = @out;
		}

		#region Overrides of TypeMapper
		protected override bool TryMap(MappingContext context, T source, out Type result) {
			return Out.True(out result, CreateSpecificType(context, source));
		}

		private Type CreateSpecificType(MappingContext context, T source) {
			return GenericTypeService.CreateSpecificType(context.ContextContainer, GetGenericSpecialType(context),
			                                             new[] {TypeMappingService.Map(context, GetGenericTypeArgument(source))});
		}

		protected abstract TypeReference GetGenericTypeArgument(T source);

		private Type GetGenericSpecialType(MappingContext context) {
			return GetSpecialType(context.ContextContainer.OfType<DotNetACLContext>().Single());
		}

		protected abstract Type GetSpecialType(DotNetACLContext context);
		#endregion
	}
}