using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class RequiredModifierTypeMapper : ModifierTypeMapper<RequiredModifierType> {
		public RequiredModifierTypeMapper(IDotNetACLContextService aclContextService, IOut @out, ITypeMappingService typeMappingService) : base(aclContextService, @out, typeMappingService) {}

		protected override bool IsOptional { get { return false; } }
		protected override TypeReference GetModifierType(RequiredModifierType source) {
			return source.ModifierType;
		}

		protected override TypeReference GetElementType(RequiredModifierType source) {
			return source.ElementType;
		}
	}
}