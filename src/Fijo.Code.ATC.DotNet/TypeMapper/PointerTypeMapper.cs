using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class PointerTypeMapper : SimpleWrapperTypeMapper<PointerType> {
		public PointerTypeMapper(IGenericTypeService genericTypeService, ITypeMappingService typeMappingService, IOut @out) : base(genericTypeService, typeMappingService, @out) {}

		#region Overrides of SimpleWrapperTypeMapper<PointerType>
		protected override TypeReference GetGenericTypeArgument(PointerType source) {
			return source.ElementType;
		}

		protected override Type GetSpecialType(DotNetACLContext context) {
			return context.PointerType;
		}
		#endregion
	}
}