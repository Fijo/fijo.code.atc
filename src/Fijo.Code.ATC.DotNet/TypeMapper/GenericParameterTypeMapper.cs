using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class GenericParameterTypeMapper : TypeMapper<GenericParameter> {
		#region Overrides of TypeMapper<GenericParameter>
		protected override bool TryMap(MappingContext context, GenericParameter source, out Type result) {
			return context.GenericParameters.TryGetValue(source, out result);
		}
		#endregion
	}
}