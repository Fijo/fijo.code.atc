using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class OptionalModifierTypeMapper : ModifierTypeMapper<OptionalModifierType> {
		public OptionalModifierTypeMapper(IDotNetACLContextService aclContextService, IOut @out, ITypeMappingService typeMappingService) : base(aclContextService, @out, typeMappingService) {}
		protected override bool IsOptional { get { return true; } }
		protected override TypeReference GetModifierType(OptionalModifierType source) {
			return source.ModifierType;
		}

		protected override TypeReference GetElementType(OptionalModifierType source) {
			return source.ElementType;
		}
	}
}