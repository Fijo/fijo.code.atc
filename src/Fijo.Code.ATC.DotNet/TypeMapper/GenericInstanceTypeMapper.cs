using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public class GenericInstanceTypeMapper : TypeMapper<GenericInstanceType> {
		private readonly IOut _out;
		private readonly IGenericTypeService _genericTypeService;
		private readonly ITypeMappingService _typeMappingService;

		public GenericInstanceTypeMapper(IOut @out, IGenericTypeService genericTypeService, ITypeMappingService typeMappingService) {
			_out = @out;
			_genericTypeService = genericTypeService;
			_typeMappingService = typeMappingService;
		}

		#region Overrides of TypeMapper<GenericInstanceType>
		protected override bool TryMap(MappingContext context, GenericInstanceType source, out Type result) {
			return _out.True(out result, CreateSpecificType(context, source));
		}

		private Type CreateSpecificType(MappingContext context, GenericInstanceType source) {
			return _genericTypeService.CreateSpecificType(context.ContextContainer,
			                                              _typeMappingService.Map(context, source.ElementType),
			                                              GetGenericTypeArguments(context, source));
		}

		private IEnumerable<Type> GetGenericTypeArguments(MappingContext context, GenericInstanceType source) {
			// executing this parallel will cause issues with the threadsync. -> deadlock
			// GetOrCreate(array /* <- will be locked */, func) 
			// --> parallel - another thread
			// --> GetOrCreate(array /* <- is already locked by another thread ... try lock ... wait endless */, func) 
			return source.GenericArguments.Select(x => _typeMappingService.Map(context, x));
		}
		#endregion
	}
}