using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.TypeMapper {
	public abstract class ModifierTypeMapper<T> : TypeMapper<T>
		where T : TypeReference {
		protected readonly IDotNetACLContextService ACLContextService;
		protected readonly IOut Out;
		protected readonly ITypeMappingService TypeMappingService;

		protected ModifierTypeMapper(IDotNetACLContextService aclContextService, IOut @out, ITypeMappingService typeMappingService) {
			ACLContextService = aclContextService;
			Out = @out;
			TypeMappingService = typeMappingService;
		}

		#region Overrides of TypeMapper<OptionalModifierType>
		protected override bool TryMap(MappingContext context, T source, out Type result) {
			return Out.True(out result, GetRequiredModifierType(context, source));
		}

		private Type GetRequiredModifierType(MappingContext context, T source) {
			return ACLContextService.GetRequiredModifierType(GetDotNetACLContext(context),
			                                                 TypeMappingService.Map(context, GetElementType(source)),
			                                                 TypeMappingService.Map(context, GetModifierType(source)), IsOptional);
		}

		protected abstract bool IsOptional { get; }

		protected abstract TypeReference GetModifierType(T source);

		protected abstract TypeReference GetElementType(T source);

		private static DotNetACLContext GetDotNetACLContext(MappingContext context) {
			return context.ContextContainer.OfType<DotNetACLContext>().Single();
		}
		#endregion
	}
}