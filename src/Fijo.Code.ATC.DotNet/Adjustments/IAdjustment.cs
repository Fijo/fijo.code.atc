namespace Fijo.Code.ATC.DotNet.Adjustments {
	public interface IAdjustment<in T> {
		void Adjust(T obj);
	}
}