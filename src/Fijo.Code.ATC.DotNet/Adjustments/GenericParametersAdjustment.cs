using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Adjustments {
	[UsedImplicitly]
	public class GenericParametersAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var types = obj.Types;
			var methods = obj.Methods;
			var typeByGenericParameters = obj.GenericParameters;
			obj.SourceGenericParameters.ForEach(x => {
				var genericParameterProvider = x.Key;
				GetOopCode(genericParameterProvider, types, methods).OopAttrs.Add(GetGenericParamsAttr(new Types(x.Value.AsParallel().Select(y => typeByGenericParameters[y]))));
			});
			IsReady.Set();
		}

		private GenericParamsAttr GetGenericParamsAttr(Types types) {
			return new GenericParamsAttr {Types = types};
		}

		private OopCode GetOopCode(IGenericParameterProvider genericParameterProvider, IDictionary<TypeDefinition, ACL.Model.Oop.Type> types, IDictionary<IMethodSignature, Method> methods) {
			var typeDef = genericParameterProvider as TypeDefinition;
			return typeDef != null
				       ? (OopCode) types[typeDef]
				       : methods[(IMethodSignature) genericParameterProvider];
		}
		#endregion
	}
}