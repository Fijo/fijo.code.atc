using System.Threading.Tasks;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.DotNet.Adjustments.Namespaces {
	[UsedImplicitly]
	public class NamespaceModuleAdjustment : IMappingAdjustment {
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var modules = obj.Modules;
			Parallel.ForEach(obj.Namespaces, x => {
				x.Value.Assembly = modules[x.Key.Module];
			});
		}
		#endregion
	}
}