using System.Linq;
using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.DotNet.Adjustments.Namespaces {
	[UsedImplicitly]
	public class NamespaceTypesAdjustment : IMappingAdjustment {
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var types = obj.Types;
			var sourceTypeByAssembly = obj.SourceTypeByModule;
			obj.Namespaces.ForEach(x => {
				var ns = x.Key;
				var nsName = ns.Name;
				x.Value.Types = new Types(sourceTypeByAssembly[ns.Module].AsParallel().Where(y => y.Namespace == nsName).Select(y => types[y]));
			});
		}
		#endregion
	}
}