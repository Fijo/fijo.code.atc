using System.Collections.Generic;
using Fijo.Code.ATC.ACL.AdvancedOop.Attr;
using Fijo.Code.ATC.ACL.AdvancedOop.Collections;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using System.Linq;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Adjustments.HaveAttribute {
	[UsedImplicitly]
	public class HaveAttributeAttributeAdjustment : IMappingAdjustment {
		private readonly IMappingContextService _mappingContextService;

		public HaveAttributeAttributeAdjustment(IMappingContextService mappingContextService) {
			_mappingContextService = mappingContextService;
		}

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var attributesDict = obj.Attributes;
			obj.SourceAttributesByProvider.ForEach(x => _mappingContextService.GetByHaveAttribute(obj, x.Key)
			                                                                  .OopAttrs.Add(GetAttributeAttr(new Attributes(GetMappedAttributes(x.Value, attributesDict)))));
		}

		private IEnumerable<ACL.AdvancedOop.Dto.Attribute> GetMappedAttributes(IEnumerable<CustomAttribute> attributes, IDictionary<CustomAttribute, ACL.AdvancedOop.Dto.Attribute> attributeDict) {
			return attributes.AsParallel().Select(y => attributeDict[y]);
		}

		private AttributeAttr GetAttributeAttr(Attributes attributes) {
			return new AttributeAttr{Attributes = attributes};
		}
		#endregion
	}
}