using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.DotNet.Adjustments {
	[Note("specific")]
	public interface IMappingAdjustment : IAdjustment<MappingContext> {}
}