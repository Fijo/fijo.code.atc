using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Attribute {
	[UsedImplicitly]
	public class AttributeCallExpressionAdjustment : IMappingAdjustment {
		private readonly IDotNetCodeFactory _codeFactory;
		public AttributeCallExpressionAdjustment(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var methods = obj.Methods;
			Parallel.ForEach(obj.Attributes, x => {
				var methodReference = x.Key.Constructor;
				x.Value.CallExpression = _codeFactory.CreateCallExpression(methods[methodReference.Resolve()], null);
			});
		}
		#endregion
	}
}