using System.Threading.Tasks;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.DotNet.Adjustments.Attribute {
	[UsedImplicitly]
	public class AttributeTypeAdjustment : IMappingAdjustment {
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var types = obj.Types;
			Parallel.ForEach(obj.Attributes, x => {
				x.Value.Type = types[x.Key.AttributeType.Resolve()];
			});
		}
		#endregion
	}
}