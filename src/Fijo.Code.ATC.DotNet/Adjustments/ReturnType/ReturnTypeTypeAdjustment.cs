using System.Threading;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.ReturnType {
	[UsedImplicitly]
	public class ReturnTypeTypeAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);
		private readonly ITypeMappingService _typeMappingService;

		public ReturnTypeTypeAdjustment(ITypeMappingService typeMappingService) {
			_typeMappingService = typeMappingService;
		}
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			GenericParametersAdjustment.IsReady.WaitOne();
			obj.ReturnTypes.ForEach(x => {
				x.Value.Type = _typeMappingService.Map(obj, x.Key.ReturnType);
			});
			IsReady.Set();
		}
		#endregion
	}
}