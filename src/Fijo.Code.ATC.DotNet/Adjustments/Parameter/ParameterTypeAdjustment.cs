using System.Threading;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Adjustments.ReturnType;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Parameter {
	[UsedImplicitly]
	public class ParameterTypeAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);
		private readonly ITypeMappingService _typeMappingService;

		public ParameterTypeAdjustment(ITypeMappingService typeMappingService) {
			_typeMappingService = typeMappingService;
		}
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			GenericParametersAdjustment.IsReady.WaitOne();
			obj.Parameters.ForEach(x => {
				var parameterType = x.Key.ParameterType;
				x.Value.Type = _typeMappingService.Map(obj, parameterType);
			});
			IsReady.Set();
		}
		#endregion
	}
}