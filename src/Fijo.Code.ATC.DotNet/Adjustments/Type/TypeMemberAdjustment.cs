using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.DotNet.Adjustments.Type {
	[UsedImplicitly]
	public class TypeMemberAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var members = obj.MembersDefs;
			var sourceMembers = obj.SourceMembersDefs;
			obj.Types.ForEach(x => {
				x.Value.Members = new ACL.Model.Collections.Members(sourceMembers[x.Key].AsParallel().Select(y => members[y]));
			});
			IsReady.Set();
		}
		#endregion
	}
}