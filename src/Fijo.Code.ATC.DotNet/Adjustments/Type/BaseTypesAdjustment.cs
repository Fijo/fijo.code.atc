using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;
using Mono.Cecil;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.DotNet.Adjustments.Type {
	[UsedImplicitly]
	public class BaseTypesAdjustment : IMappingAdjustment {
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var dictionary = obj.Types;
			dictionary.ForEach(x => {
				x.Value.BaseTypes = new Types(GetMappedBaseTypes(GetBaseTypes(x.Key), dictionary));
			});
		}

		private static IEnumerable<ACL.Model.Oop.Type> GetMappedBaseTypes(IEnumerable<TypeReference> baseTypes, IDictionary<TypeDefinition, ACL.Model.Oop.Type> dictionary) {
			return baseTypes.AsParallel()
				.Where(y => y != null)
				.Select(baseType => dictionary[baseType.Resolve()]);
		}

		private IEnumerable<TypeReference> GetBaseTypes(TypeDefinition typeDef) {
			return typeDef.BaseType.IntoEnumerable()
			              .Concat(typeDef.Interfaces);
		}
		#endregion
	}
}