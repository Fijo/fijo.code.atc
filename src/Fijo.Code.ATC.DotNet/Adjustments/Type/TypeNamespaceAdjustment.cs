using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.DotNet.Factory;
using Fijo.Code.ATC.DotNet.Factory.Interfaces;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Adjustments.Type {
	[UsedImplicitly]
	public class TypeNamespaceAdjustment : IMappingAdjustment {
		private readonly IMappingObjectFactory _mappingObjectsFactory;

		public TypeNamespaceAdjustment(IMappingObjectFactory mappingObjectsFactory) {
			_mappingObjectsFactory = mappingObjectsFactory;
		}
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var namespaces = obj.Namespaces;
			Parallel.ForEach(obj.Types, x => {
				var type = x.Key;
				x.Value.Namespace = namespaces[_mappingObjectsFactory.GetInternNamespace(type.Module, type.Namespace)];
			});
		}
		#endregion
	}
	
	//[UsedImplicitly]
	//public class TypeGenericParamsAdjustment : IMappingAdjustment {
	//	private readonly IMappingObjectFactory _mappingObjectsFactory;

	//	public TypeGenericParamsAdjustment(IMappingObjectFactory mappingObjectsFactory) {
	//		_mappingObjectsFactory = mappingObjectsFactory;
	//	}

	//	#region Implementation of IAdjustment<in MappingContext>
	//	public void Adjust(MappingContext obj) {
	//		var namespaces = obj.Namespaces;
	//		Parallel.ForEach(obj.Types, x => {
	//			var type = x.Key;
	//			type.GenericParameters[0].gener
	//		});
	//	}

	//	private GeneralAttr GetInCompleteAttr(ModuleDefinition moduleDefinition) {
	//		return new InCompleteAttr{Reason = string.Format("The Assembly �{0}� is used in a reference but is not referenced. So it is not loaded, and it cannot be inspected because of this. (The maybe incorrect path to the assembly: �{1}�)", moduleDefinition.Name, moduleDefinition.FullyQualifiedName)};
	//	}
	//	#endregion
	//}
}