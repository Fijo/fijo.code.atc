using System.Threading;
using Fijo.Code.ATC.ACL.DotNet.Attr;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Adjustments.Parameter;
using Fijo.Code.ATC.DotNet.Adjustments.ReturnType;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments {
	[UsedImplicitly]
	public class GenericTypeAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);
		private readonly IGenericTypeService _genericTypeService;

		public GenericTypeAdjustment(IGenericTypeService genericTypeService) {
			_genericTypeService = genericTypeService;
		}
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			ParameterTypeAdjustment.IsReady.WaitOne();
			ReturnTypeTypeAdjustment.IsReady.WaitOne();
			GenericParametersAdjustment.IsReady.WaitOne();

			_genericTypeService.AdjustSpecificTypes(obj.ContextContainer);

			IsReady.Set();
		}
		#endregion
	}
}