using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.ACL.DotNet.Enums;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Enums;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Adjustments.Assemblies {
	[UsedImplicitly]
	public class SpecialAssemblyAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);
		private const string AssemblyName = "AtcCilCore";
		private readonly AssemblyNameDefinition _assemblyNameDef;
		private readonly IDotNetCodeFactory _codeFactory;
		private readonly ICilTypeSystemService _typeSystemService;
		private readonly IMapping<AssemblyNameDefinition, AssemblyName> _assemblyNameMapping;

		public SpecialAssemblyAdjustment(IDotNetCodeFactory codeFactory, ICilTypeSystemService typeSystemService, IMapping<AssemblyNameDefinition, AssemblyName> assemblyNameMapping) {
			_codeFactory = codeFactory;
			_typeSystemService = typeSystemService;
			_assemblyNameMapping = assemblyNameMapping;
			_assemblyNameDef = GetAssemblyNameDef();
		}

		private AssemblyNameDefinition GetAssemblyNameDef() {
			return new AssemblyNameDefinition(AssemblyName, new Version(1, 0))
			{
				PublicKeyToken = new byte[] {228, 78, 184, 8, 93, 116, 12, 174}
			};
		}

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			TypeSystemAdjustment.IsReady.WaitOne();
			var dotNetACLContext = obj.ContextContainer.OfType<DotNetACLContext>().Single();
			var assembly = GetAssembly(obj);
			dotNetACLContext.CoreAssembly = assembly;
			var ns = assembly.Assemblies.Single(x => x.Name == AssemblyName).Namespaces.Single(x => x.Name == AssemblyName);
			dotNetACLContext.OperatorMethods = GetOperatorMethodDict(ns);
			IsReady.Set();
		}

		private IDictionary<AtcCilOperators, Method> GetOperatorMethodDict(Namespace ns) {
			return GetType(ns, AtcCilCoreTypes.Operators)
				.Members.OfType<Method>()
				.ToDict(x => (AtcCilOperators) Enum.Parse(typeof (AtcCilOperators), x.Name), x => x);
		}

		private ACL.Model.Oop.Type GetType(Namespace ns, AtcCilCoreTypes type) {
			var typeString = type.ToString();
			return ns.Types.Single(x => x.Name == typeString);
		}

		private Func<ACL.Model.Base.Code, bool> GetIgnorePredicate(MappingContext context) {
			var defaultTypes = _typeSystemService.Get(context.ContextContainer).Values.Execute();
			return code => {
				var type = code as ACL.Model.Oop.Type;
				return type != null && defaultTypes.Contains(type);
			};
		}

		private Assembly GetAssembly(MappingContext obj) {
			return _codeFactory.CreateAssembly(_assemblyNameMapping.Map(_assemblyNameDef), new ACL.Model.Collections.Assemblies(GetModules(obj)));
		}

		private IEnumerable<Assembly> GetModules(MappingContext context) {
			yield return _codeFactory.CreateModule(AssemblyName, null, new ACL.Model.Collections.Namespaces(GetNamespaces(context)));
		}

		private IEnumerable<Namespace> GetNamespaces(MappingContext context) {
			yield return _codeFactory.CreateNamespace(AssemblyName, null, new Types(GetTypes(context)));
		}

		private IEnumerable<ACL.Model.Oop.Type> GetTypes(MappingContext context) {
			yield return _codeFactory.CreateType(Modifier.Public, AtcCilCoreTypes.Operators.ToString(), null, new ACL.Model.Collections.Members(GetMembers(context)));
		}

		private IEnumerable<Member> GetMembers(MappingContext context) {
			var typeSystem = _typeSystemService.Get(context.ContextContainer);
			yield return GetNotMethod(typeSystem);
			yield return GetEqualMethod(typeSystem);
			yield return GetNotEqualMethod(typeSystem);
			yield return GetGreaterMethod(typeSystem);
			yield return GetGreaterUnsignedMethod(typeSystem);
			yield return GetGreaterThanMethod(typeSystem);
			yield return GetGreaterThanUnsignedMethod(typeSystem);
			yield return GetLessMethod(typeSystem);
			yield return GetLessUnsignedMethod(typeSystem);
			yield return GetLessThanMethod(typeSystem);
			yield return GetLessThanUnsignedMethod(typeSystem);
		}

		private Method GetMethod(AtcCilOperators functionName, Parameters parameters, ACL.Model.Oop.Type returnType) {
			return _codeFactory.CreateMethod(functionName.ToString(), parameters,
			                                 null, _codeFactory.CreateReturnType(returnType));
		}

		private ACL.Model.Oop.Parameter GetParameter(string name, ACL.Model.Oop.Type type) {
			return _codeFactory.CreateParameter(name, type);
		}

		private Method GetNotMethod(CilTypeSystem typeSystem) {
			return GetMethod(AtcCilOperators.Not, new Parameters(GetParameter("source", typeSystem[CoreTypes.Bool])),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetEqualMethod(CilTypeSystem typeSystem) {
			var obj = typeSystem[CoreTypes.Object];
			return GetMethod(AtcCilOperators.Equal, new Parameters(GetParameter("obj1", obj), GetParameter("obj2", obj)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetNotEqualMethod(CilTypeSystem typeSystem) {
			var obj = typeSystem[CoreTypes.Object];
			return GetMethod(AtcCilOperators.NotEqual, new Parameters(GetParameter("obj1", obj), GetParameter("obj2", obj)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetGreaterMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.Greater, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetGreaterUnsignedMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.GreaterUnsigned, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetGreaterThanMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.GreaterThan, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetGreaterThanUnsignedMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.GreaterThanUnsigned, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetLessMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.Less, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetLessUnsignedMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.LessUnsigned, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetLessThanMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.LessThan, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		private Method GetLessThanUnsignedMethod(CilTypeSystem typeSystem) {
			var interger = typeSystem[CoreTypes.Int32];
			return GetMethod(AtcCilOperators.LessThanUnsigned, new Parameters(GetParameter("a", interger), GetParameter("b", interger)),
			                 typeSystem[CoreTypes.Bool]);
		}

		// ToDo may impl the short form too... or may handle it in another way 
		#endregion
	}
}