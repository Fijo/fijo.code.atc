using System.Linq;
using System.Threading;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Service.Interface;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Assemblies {
	[UsedImplicitly]
	public class TypeSystemAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);
		private readonly ICilTypeSystemService _typeSystemService;

		public TypeSystemAdjustment(ICilTypeSystemService typeSystemService) {
			_typeSystemService = typeSystemService;
		}
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			_typeSystemService.Adjust(obj.ContextContainer, obj.Modules.First().Key, typeRef => obj.Types[typeRef.Resolve()]);
			IsReady.Set();
		}
		#endregion
	}
}