using System.Linq;
using System.Threading.Tasks;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Assemblies {
	[UsedImplicitly]
	public class AssemblyModuleReferenceAdjustment : IMappingAdjustment {
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var modules = obj.Modules;
			var sourceModulesByAssembly = obj.SourceModulesByAssembly;
			Parallel.ForEach(obj.Assemblies, x => {
				x.Value.Assemblies = new ACL.Model.Collections.Assemblies(sourceModulesByAssembly[x.Key.Name].AsParallel().Select(y => modules[y]));
			});
		}
		#endregion
	}
}