using System.Linq;
using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Adjustments.ReturnType;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Variable {
	[UsedImplicitly]
	public class VariableTypeAdjustment : IMappingAdjustment {
		private readonly ITypeMappingService _typeMappingService;

		public VariableTypeAdjustment(ITypeMappingService typeMappingService) {
			_typeMappingService = typeMappingService;
		}

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			GenericParametersAdjustment.IsReady.WaitOne();
			obj.Variables.ForEach(x => {
				x.Value.Type = _typeMappingService.Map(obj, x.Key.VariableType);
			});
		}
		#endregion
	}
}