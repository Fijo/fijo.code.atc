using System.Linq;
using System.Threading.Tasks;
using Fijo.Code.ATC.DotNet.Factory.Interfaces;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Module {
	[UsedImplicitly]
	public class ModuleNamespaceAdjustment : IMappingAdjustment {
		private readonly IMappingObjectFactory _mappingObjectsFactory;
		public ModuleNamespaceAdjustment(IMappingObjectFactory mappingObjectsFactory) {
			_mappingObjectsFactory = mappingObjectsFactory;
		}
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var namespaces = obj.Namespaces;
			var sourceNamespaces = obj.SourceNamespaces;
			Parallel.ForEach(obj.Modules, x => {
				var module = x.Key;
				x.Value.Namespaces = new ACL.Model.Collections.Namespaces(sourceNamespaces[module].AsParallel().Select(y => namespaces[_mappingObjectsFactory.GetInternNamespace(module, y)]));
			});
		}
		#endregion
	}
}