using System.Linq;
using System.Threading.Tasks;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Module {
	[UsedImplicitly]
	public class ModuleAssemblyReferenceAdjustment : IMappingAdjustment {
		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var sourceRefAssembliesByModule = obj.SourceRefAssembliesByModule;
			var assemblies = obj.Assemblies;
			Parallel.ForEach(obj.Modules, x => {
				x.Value.Assemblies = new ACL.Model.Collections.Assemblies(sourceRefAssembliesByModule[x.Key].AsParallel().Select(y => assemblies[y]));
			});
		}
		#endregion
	}
}