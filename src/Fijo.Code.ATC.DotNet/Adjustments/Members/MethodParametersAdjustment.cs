using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.DotNet.Adjustments.Members {
	[UsedImplicitly]
	public class MethodParametersAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var methodParameters = obj.SourceParameters;
			var parameters = obj.Parameters;
			obj.Methods.ForEach(x => {
				x.Value.Parameters = new Parameters(methodParameters[x.Key].AsParallel().Select(y => parameters[y]));
			});
			IsReady.Set();
		}
		#endregion
	}
}