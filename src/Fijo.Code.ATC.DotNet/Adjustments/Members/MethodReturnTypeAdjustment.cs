using System.Threading;
using System.Threading.Tasks;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.DotNet.Adjustments.Members {
	[UsedImplicitly]
	public class MethodReturnTypeAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var returnTypes = obj.ReturnTypes;
			Parallel.ForEach(obj.Methods, x => {
				x.Value.ReturnType = returnTypes[x.Key.MethodReturnType];
			});
			IsReady.Set();
		}
		#endregion
	}
}