using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Adjustments.Assemblies;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Code.CecilContrib.Cil.Services.Interfaces;
using Fijo.Code.CecilContrib.Dto;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.ATC.DotNet.Adjustments.Members {
	[UsedImplicitly]
	public class MethodBodyAdjustment : IMappingAdjustment {
		private readonly IInstructionTreadService _instructionTreadService;
		private readonly ICodeObjectifierService _codeObjectifierService;
		private readonly IBodyMetaCodeService _bodyMetaCodeService;

		public MethodBodyAdjustment(IInstructionTreadService instructionTreadService, ICodeObjectifierService codeObjectifierService, IBodyMetaCodeService bodyMetaCodeService) {
			_instructionTreadService = instructionTreadService;
			_codeObjectifierService = codeObjectifierService;
			_bodyMetaCodeService = bodyMetaCodeService;
		}

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			GenericTypeAdjustment.IsReady.WaitOne();
			SpecialAssemblyAdjustment.IsReady.WaitOne();
			obj.MethodsDefs.ForEach(x => {
				var methodDef = x.Key;
				var body = methodDef.Body;
				if (body == null) return;
				var treateInstructions = GetTreateInstructions(methodDef, body).ToList();
				var method = x.Value;

				var context = GetCodeObjectifierContext(obj, treateInstructions, body, method);
				
				method.Body = _codeObjectifierService.GetContainer(context);
			});
		}

		private IEnumerable<TreateInstruction> GetTreateInstructions(MethodDefinition methodDef, MethodBody body) {
			var module = methodDef.Module;
			return body.Instructions.AsParallel()
				    .SelectMany(instruction => _instructionTreadService.Get(body, module, instruction));
		}

		private CodeObjectifierContext GetCodeObjectifierContext(MappingContext context, IList<TreateInstruction> treateInstructions, MethodBody body, Method method) {
			return new CodeObjectifierContext
			{
				Instructions = treateInstructions,
				TreateInstructionDict = treateInstructions.ToDict(x => x.Source, x => x),
				InstructionDict = treateInstructions.ToDict(x => x.Source, x => default(Expression)),
				Body = body,
				Method = method,
				Context = context,
				DotNetACLContext = context.ContextContainer.OfType<DotNetACLContext>().Single(),
				ReturnType = GetReturnType(context, body.Method),
				Variables = GetVariables(context, body),
				Parameters = GetParameters(context, body),
				ExceptionHandlers = GetExceptionHandlers(body)
			};
		}

		private IDictionary<Instruction, HandledExceptionBlock> GetExceptionHandlers(MethodBody body) {
			return _bodyMetaCodeService.GetHandledExceptionBlocks(body).ToDict(x => x.TryStart, x => x);
		}

		private IDictionary<VariableDefinition, ACL.Model.Oop.Variable> GetVariables(MappingContext context, MethodBody body) {
			var varDict = context.Variables;
			return context.SourceVariables[body.Method].ToDict(x => x, x => varDict[x]);
		}

		private IDictionary<ParameterDefinition, ACL.Model.Oop.Parameter> GetParameters(MappingContext context, MethodBody body) {
			var paramDict = context.Parameters;
			return context.SourceParameters[body.Method].ToDict(x => x, x => paramDict[x]);
		}

		private TypeReference GetReturnType(MappingContext context, IMethodSignature targetMethod) {
			return context.SourceReturnTypes[targetMethod].ReturnType;
		}
		#endregion
	}
}