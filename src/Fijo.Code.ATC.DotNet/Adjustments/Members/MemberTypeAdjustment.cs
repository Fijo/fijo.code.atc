using System.Threading;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Code.ATC.DotNet.Adjustments.Members {
	[UsedImplicitly]
	public class MemberTypeAdjustment : IMappingAdjustment {
		internal static readonly ManualResetEvent IsReady = new ManualResetEvent(false);

		#region Implementation of IAdjustment<in MappingContext>
		public void Adjust(MappingContext obj) {
			var types = obj.Types;
			obj.MembersDefs.ForEach(x => {
				x.Value.Type = types[x.Key.DeclaringType.Resolve()];
			});
			IsReady.Set();
		}
		#endregion
	}
}