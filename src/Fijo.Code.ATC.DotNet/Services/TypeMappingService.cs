using System.Diagnostics;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Fijo.Code.ATC.DotNet.Services {
	public class TypeMappingService : ITypeMappingService {
		private readonly ILazy<ITryGetResultHandlingProcessor<TypeMappingRequest, Type>> _typeMapperProcessor;
		public TypeMappingService(ILazy<ITryGetResultHandlingProcessor<TypeMappingRequest, Type>> typeMapperProcessor) {
			_typeMapperProcessor = typeMapperProcessor;
		}
		#region Implementation of ITypeMappingService
		public Type Map(MappingContext context, TypeReference source) {
			return _typeMapperProcessor.Get().Process(new TypeMappingRequest(context, source));
		}
		#endregion
	}
}