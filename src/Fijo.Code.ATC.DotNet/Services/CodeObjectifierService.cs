using System.Collections.Generic;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;

namespace Fijo.Code.ATC.DotNet.Services {
	public class CodeObjectifierService : ICodeObjectifierService {
		private readonly IDotNetCodeFactory _codeFactory;
		private readonly ILazy<ITryGetResultHandlingProcessor<CodeObjectifierRequest, Expression>> _codeObjectifierHandlingProcessor;

		public CodeObjectifierService(IDotNetCodeFactory codeFactory, ILazy<ITryGetResultHandlingProcessor<CodeObjectifierRequest, Expression>> codeObjectifierHandlingProcessor) {
			_codeFactory = codeFactory;
			_codeObjectifierHandlingProcessor = codeObjectifierHandlingProcessor;
		}

		#region Implementation of ICodeObjectifierService
		private Expression GetInternExpression(TreateInstruction source, CodeObjectifierCurrentContext context) {
			return _codeObjectifierHandlingProcessor.Get().Process(new CodeObjectifierRequest(source, context));
		}

		private Expression GetExpression(TreateInstruction source, CodeObjectifierCurrentContext context) {
			return GetInternExpression(source, context);
		}

		public Expression Get(TreateInstruction source, CodeObjectifierCurrentContext context) {
			var expression = GetExpression(source, context);
			context.Context.InstructionDict[source.Source] = expression;
			return expression;
		}

		public ContainerExpression GetContainer(CodeObjectifierCurrentContext context) {
			return GetContainer(context, context.Context.Instructions);
		}

		public ContainerExpression GetContainer(CodeObjectifierCurrentContext context, IEnumerable<TreateInstruction> treateInstructions) {
			var bodyExpr = _codeFactory.CreateContainerExpression();
			foreach (var treateInstruction in treateInstructions) {
				bodyExpr.Add(Get(treateInstruction, context));
			}
			return bodyExpr;
		}

		public ContainerExpression GetContainer(CodeObjectifierContext context) {
			return GetContainer(CreateCurrentContext(context));
		}

		public CodeObjectifierCurrentContext CreateCurrentContext(CodeObjectifierContext context) {
			var currentContext = new CodeObjectifierCurrentContext
			{
				Stack = new Stack<Expression>(),
				Context = context
			};
			//Enumerable.Range(0, 1000).ForEach(y => currentContext.Stack.Push(new ValueExpression()));
			return currentContext;
		}
		#endregion
	}
}