using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.CecilContrib.Enums;
using Fijo.Code.CecilContrib.Extentions;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Services {
	public class CodeObjectifierUtil : ICodeObjectifierUtil {
		private readonly IDotNetCodeFactory _codeFactory;
		public CodeObjectifierUtil(IDotNetCodeFactory codeFactory) {
			_codeFactory = codeFactory;
		}
		#region Implementation of ICodeObjectifierUtil
		public Expression SetVariable(CodeObjectifierCurrentContext context, Variable variable) {
			var expressions = context.Stack;
			expressions.Push(_codeFactory.CreateSetExpression(_codeFactory.CreateVariableExpression(variable),
			                                                  expressions.Pop()));
			return null;
		}

		public Expression GetVariable(CodeObjectifierCurrentContext context, Variable variable) {
			context.Stack.Push(_codeFactory.CreateVariableExpression(variable));
			return null;
		}

		public Expression SetField(FieldReference field, CodeObjectifierCurrentContext context) {
			var stack = context.Stack;
			var value = stack.Pop();
			return _codeFactory.CreateSetExpression(InternalGetField(field, context), value);
		}

		public Expression GetField(FieldReference field, CodeObjectifierCurrentContext context) {
			context.Stack.Push(InternalGetField(field, context));
			return null;
		}

		private FieldExpression InternalGetField(FieldReference field, CodeObjectifierCurrentContext context) {
			var fieldDef = field.Resolve();
			return _codeFactory.CreateFieldExpression(context.Context.Context.Fields[fieldDef], GetInstance(context.Stack, fieldDef));
		}

		private Expression GetInstance(Stack<Expression> stack, FieldDefinition fieldDefinition) {
			return fieldDefinition.IsStatic ? null : stack.Pop();
		}

		public Expression GetCall(CodeObjectifierCurrentContext context, Method target) {
			var call = GetCallExpression(context, target);
			if (context.Context.ReturnType.IsCoreType(CoreTypes.Void)) return call;
			context.Stack.Push(call);
			return null;
		}

		private Expression GetCallExpression(CodeObjectifierCurrentContext context, Method target) {
			return _codeFactory.CreateCallExpression(target, new Expressions(GetExpressions(target, context.Stack)));
		}

		private IEnumerable<Expression> GetExpressions(Method target, Stack<Expression> stack) {
			return ((IEnumerable<Parameter>) target.Parameters).Reverse()
				.Select(x => stack.Pop());
		}
		#endregion
	}
}