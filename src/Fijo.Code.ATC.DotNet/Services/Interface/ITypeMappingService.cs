using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Services.Interface {
	public interface ITypeMappingService {
		Type Map(MappingContext context, TypeReference source);
	}
}