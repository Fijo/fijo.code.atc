using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Services.Interface {
	public interface ICodeObjectifierUtil {
		Expression SetVariable(CodeObjectifierCurrentContext context, Variable variable);
		Expression GetVariable(CodeObjectifierCurrentContext context, Variable variable);
		Expression SetField(FieldReference field, CodeObjectifierCurrentContext context);
		Expression GetField(FieldReference field, CodeObjectifierCurrentContext context);
		Expression GetCall(CodeObjectifierCurrentContext context, Method target);
	}
}