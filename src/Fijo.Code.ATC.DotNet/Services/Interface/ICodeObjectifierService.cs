using System.Collections.Generic;
using System.ComponentModel;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.DotNet.Services.Interface {
	public interface ICodeObjectifierService {
		Expression Get(TreateInstruction source, CodeObjectifierCurrentContext context);
		[EditorBrowsable(EditorBrowsableState.Advanced), Note("more advanced function that is considered to be used mostly only for internal use in implementations of ICodeObjectifierHandling(s)")]
		ContainerExpression GetContainer(CodeObjectifierCurrentContext context, IEnumerable<TreateInstruction> treateInstructions);
		ContainerExpression GetContainer(CodeObjectifierCurrentContext context);
		ContainerExpression GetContainer(CodeObjectifierContext context);
		CodeObjectifierCurrentContext CreateCurrentContext(CodeObjectifierContext context);
	}
}