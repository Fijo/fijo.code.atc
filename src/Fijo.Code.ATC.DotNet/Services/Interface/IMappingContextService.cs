using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Services.Interface {
	public interface IMappingContextService {
		OopCode GetByHaveAttribute(MappingContext context, ICustomAttributeProvider haveAttribute);
	}
}