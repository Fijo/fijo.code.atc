using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using Fijo.Code.ATC.DotNet.Services.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Services {
	public class MappingContextService : IMappingContextService {
		private readonly ITryGetResultHandlingProcessor<HaveAttributeRequest, OopCode> _getByHaveAttributeProcessor;

		public MappingContextService(ITryGetResultHandlingProcessor<HaveAttributeRequest, OopCode> getByHaveAttributeProcessor) {
			_getByHaveAttributeProcessor = getByHaveAttributeProcessor;
		}

		public OopCode GetByHaveAttribute(MappingContext context, ICustomAttributeProvider haveAttribute) {
			return _getByHaveAttributeProcessor.Process(new HaveAttributeRequest(context, haveAttribute));
		}
	}
}