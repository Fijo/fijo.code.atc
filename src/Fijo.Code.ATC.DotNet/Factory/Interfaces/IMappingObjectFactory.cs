using Fijo.Code.ATC.DotNet.Model.Intern;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Factory.Interfaces {
	public interface IMappingObjectFactory {
		InternNamespace GetInternNamespace(ModuleDefinition moduleDef, string name);
	}
}