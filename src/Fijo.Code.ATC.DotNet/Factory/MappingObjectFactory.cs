using System.Collections.Generic;
using Fijo.Code.ATC.DotNet.Factory.Interfaces;
using Fijo.Code.ATC.DotNet.Model.Intern;
using JetBrains.Annotations;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNet.Factory {
	[PublicAPI]
	public class MappingObjectFactory : IMappingObjectFactory {
		private readonly IEqualityComparer<ModuleDefinition> _moduleDefinitionEqualityComparer;

		public MappingObjectFactory(IEqualityComparer<AssemblyNameReference> asssemblyNameReferenceEqualityComparer, IEqualityComparer<ModuleDefinition> moduleDefinitionEqualityComparer) {
			_moduleDefinitionEqualityComparer = moduleDefinitionEqualityComparer;
		}

		public InternNamespace GetInternNamespace(ModuleDefinition moduleDef, string name) {
			return new InternNamespace(_moduleDefinitionEqualityComparer, name, moduleDef);
		}
	}
}