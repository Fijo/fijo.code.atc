﻿using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;
using Fijo.Code.CecilContrib.Cil.Enums;

namespace Fijo.Code.ATC.DotNet.Providers {
	public class CodeObjectifierProvider : FallbackInjectionAlgorithmProvider<ICodeObjectifier, TreatedCode> {
		#region Overrides of InjectionAlgorithmProviderBase<ICodeObjectifier,TreatedCode>
		protected override TreatedCode KeySelector(ICodeObjectifier algorithm) {
			return algorithm.Code;
		}
		#endregion
		#region Overrides of FallbackInjectionAlgorithmProvider<ICodeObjectifier,TreatedCode>
		protected override bool IsFallback(ICodeObjectifier algorithm) {
			return algorithm is FallbackCodeObjectifier;
		}
		#endregion
	}
}