﻿using System.Collections.Generic;
using Fijo.Assemblies.GAC.Dto;
using Fijo.Assemblies.GAC.Properties;
using Fijo.Code.ATC.ACL.AdvancedOop.Dto;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Code.ATC.ACL.DotNet.Properties;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.Core.Base;
using Fijo.Code.ATC.DotNet.Adjustments;
using Fijo.Code.ATC.DotNet.Adjustments.Assemblies;
using Fijo.Code.ATC.DotNet.Adjustments.Attribute;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Adjustments.Namespaces;
using Fijo.Code.ATC.DotNet.Adjustments.Parameter;
using Fijo.Code.ATC.DotNet.Adjustments.ReturnType;
using Fijo.Code.ATC.DotNet.Adjustments.Type;
using Fijo.Code.ATC.DotNet.Adjustments.Variable;
using Fijo.Code.ATC.DotNet.EqualityComparers;
using Fijo.Code.ATC.DotNet.Factory;
using Fijo.Code.ATC.DotNet.Factory.Interfaces;
using Fijo.Code.ATC.DotNet.Framework;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifierHandlings;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifierHandlings.Base;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Handlings.GetByHaveAttribute;
using Fijo.Code.ATC.DotNet.Mapping;
using Fijo.Code.ATC.DotNet.Mapping.Default;
using Fijo.Code.ATC.DotNet.Mapping.Expressions;
using Fijo.Code.ATC.DotNet.Mapping.Member;
using Fijo.Code.ATC.DotNet.Model;
using Fijo.Code.ATC.DotNet.Model.Intern;
using Fijo.Code.ATC.DotNet.Model.Requests;
using Fijo.Code.ATC.DotNet.Providers;
using Fijo.Code.ATC.DotNet.Services;
using Fijo.Code.ATC.DotNet.Services.Interface;
using Fijo.Code.ATC.DotNet.TypeMapper;
using Fijo.Code.CecilContrib.Cil.Enums;
using Fijo.Code.CecilContrib.Cil.Model.Expr;
using Fijo.Code.CecilContrib.Properties;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Interface;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.Base.Provider;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult;
using FijoCore.Infrastructure.LightContrib.Module.HandlingProcessing.TryGetResult.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;
using Mono.Cecil;
using Mono.Cecil.Cil;
using CecilContribExpression = Fijo.Code.CecilContrib.Cil.Model.Expr.Expression;
using Expression = Fijo.Code.ATC.ACL.Model.Expressions.Expression;
using Assembly = Fijo.Code.ATC.ACL.Model.Oop.Assembly;

namespace Fijo.Code.ATC.DotNet.Properties {
	[PublicAPI]
	public class DotNetATCInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
			kernel.Load(new GACInjectionModule());
			kernel.Load(new CecilContribInjectionModule());
			kernel.Load(new DotNetACLInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<ICodeService>().To<CodeService>().InSingletonScope();
			kernel.Bind<IAssemblyProvider>().To<AssemblyProvider>().InSingletonScope();
			kernel.Bind<IAssemblyService>().To<AssemblyService>().InSingletonScope();
			kernel.Bind<ILoadedAssemblyStore>().To<LoadedAssemblyStore>().InSingletonScope();

			BindTypeMapping(kernel);

			BindGetByHaveAttribute(kernel);

			kernel.Bind<IEqualityComparer<GacAssembly>>().To<GacAssemblyEqualityComparer>().InSingletonScope();

			kernel.Bind<IMappingObjectFactory>().To<MappingObjectFactory>().InSingletonScope();
			kernel.Bind<IMappingContextService>().To<MappingContextService>().InSingletonScope();

			BindMappings(kernel);
			BindAdjustments(kernel);

			//kernel.Bind<ITryGetResultHandlingProcessor<CodeObjectifierRequest, Expression>>().To<TryGetResultHandlingProcessor<CodeObjectifierRequest, Expression>>().InSingletonScope();
			kernel.Bind<IHandlingProvider<ITryGetResultHandling<CodeObjectifierRequest, Expression>>>().To<InjectionHandlingProvider<ITryGetResultHandling<CodeObjectifierRequest, Expression>, ICodeObjectifierHandling>>().InSingletonScope();
			kernel.Bind<ICodeObjectifierHandling>().To<ExceptionHandlingCodeObjectifierHandling>().InSingletonScope();
			kernel.Bind<ICodeObjectifierHandling>().To<DefaultCodeObjectifierHandling>().InSingletonScope();

			//kernel.Bind<IFinder<ICodeObjectifier, TreatedCode>>().To<Finder<ICodeObjectifier, TreatedCode>>().InSingletonScope();
			kernel.Bind<IAlgorithmProvider<ICodeObjectifier, TreatedCode>>().To<CodeObjectifierProvider>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<ArgListCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<BrConditionCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<EndFinallyCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<LdArgCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<LdcCodeObjectifier>().InSingletonScope();
			//Kernel.Bind<ICodeObjectifier>().To<LdFldCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<LdLocCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<CallObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<ConditionCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<DupCodeObjectifier>().InSingletonScope();
			//Kernel.Bind<ICodeObjectifier>().To<NewArrCodeObjectifier>().InSingletonScope();
			//Kernel.Bind<ICodeObjectifier>().To<NewObjCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<NopCodeObjectifier>().InSingletonScope();
			//Kernel.Bind<ICodeObjectifier>().To<ReadonlyCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<RetObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<StArgCodeObjectifier>().InSingletonScope();
			//Kernel.Bind<ICodeObjectifier>().To<StFldCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<StLocCodeObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<ThrowObjectifier>().InSingletonScope();
			Kernel.Bind<ICodeObjectifier>().To<FallbackCodeObjectifier>().InSingletonScope();

			Kernel.Bind<ICodeObjectifierService>().To<CodeObjectifierService>().InSingletonScope();
			Kernel.Bind<ICodeObjectifierUtil>().To<CodeObjectifierUtil>().InSingletonScope();

			kernel.Bind<IBinarySettings>().To<DotNetImporterCacheBinarySettings>().WhenInjectedInto<DotNetImporter>().InSingletonScope();
		}

		private void BindTypeMapping(IKernel kernel) {
			kernel.Bind<ITypeMappingService>().To<TypeMappingService>().InSingletonScope();

			kernel.Bind<IHandlingProvider<ITryGetResultHandling<TypeMappingRequest, Type>>>().To<InjectionHandlingProvider<ITryGetResultHandling<TypeMappingRequest, Type>, TypeMapper.TypeMapper>>().InSingletonScope();
			//kernel.Bind<ITryGetResultHandlingProcessor<TypeMappingRequest, Type>>().To<TryGetResultHandlingProcessor<TypeMappingRequest, Type>>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<TypeReferenceMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<ArrayTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<GenericParameterTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<TypeDefenitionMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<GenericInstanceTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<RequiredModifierTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<OptionalModifierTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<PointerTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<ByReferenceTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<PinnedTypeMapper>().InSingletonScope();
			kernel.Bind<TypeMapper.TypeMapper>().To<FunctionPointerTypeMapper>().InSingletonScope();
		}

		private void BindGetByHaveAttribute(IKernel kernel) {
			kernel.Bind<IHandlingProvider<ITryGetResultHandling<HaveAttributeRequest, OopCode>>>().To<InjectionHandlingProvider<ITryGetResultHandling<HaveAttributeRequest, OopCode>, IGetByHaveAttributeHandling>>().InSingletonScope();
			//kernel.Bind<ITryGetResultHandlingProcessor<HaveAttributeRequest, OopCode>>().To<TryGetResultHandlingProcessor<HaveAttributeRequest, OopCode>>().InSingletonScope();
			kernel.Bind<IGetByHaveAttributeHandling>().To<GetAssemblyByHaveAttributeHandling>().InSingletonScope();
			kernel.Bind<IGetByHaveAttributeHandling>().To<GetGenericParameterByHaveAttributeHandling>().InSingletonScope();
			kernel.Bind<IGetByHaveAttributeHandling>().To<GetMemberByHaveAttributeHandling>().InSingletonScope();
			kernel.Bind<IGetByHaveAttributeHandling>().To<GetParameterByHaveAttributeHandling>().InSingletonScope();
			kernel.Bind<IGetByHaveAttributeHandling>().To<GetTypeByHaveAttributeHandling>().InSingletonScope();
			kernel.Bind<IGetByHaveAttributeHandling>().To<GetModuleByHaveAttributeHandling>().InSingletonScope();
			kernel.Bind<IGetByHaveAttributeHandling>().To<GetReturnTypeByHaveAttributeHandling>().InSingletonScope();
		}

		private void BindAdjustments(IKernel kernel) {
			//kernel.Bind<IAdjustment<MappingContext>>().To<ModuleNamespaceAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<AssemblyModuleReferenceAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<ModuleAssemblyReferenceAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<BaseTypesAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<NamespaceModuleAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<NamespaceTypesAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<TypeMemberAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<TypeNamespaceAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<MemberTypeAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<MethodParametersAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<GenericParametersAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<MethodReturnTypeAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<ReturnTypeTypeAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<ParameterTypeAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<AttributeTypeAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<AttributeCallExpressionAdjustment>().InSingletonScope();
			//kernel.Bind<IAdjustment<MappingContext>>().To<HaveAttributeAttributeAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<VariableTypeAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<SpecialAssemblyAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<TypeSystemAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<GenericTypeAdjustment>().InSingletonScope();
			kernel.Bind<IAdjustment<MappingContext>>().To<MethodBodyAdjustment>().InSingletonScope();
		}

		private void BindMappings(IKernel kernel) {
			kernel.Bind<CodeImporter<DotNetModule>>().To<DotNetImporter>().InSingletonScope();
			kernel.Bind<IMapping<AssemblyDefinition, Assembly>>().To<AssemblyMapping>().InSingletonScope();
			kernel.Bind<IMapping<ModuleDefinition, Assembly>>().To<ModuleAssemblyMapping>().InSingletonScope();
			kernel.Bind<IMapping<InternNamespace, Namespace>>().To<NamespaceMapping>().InSingletonScope();
			kernel.Bind<IMapping<TypeDefinition, Type>>().To<TypeMapping>().InSingletonScope();
			kernel.Bind<IMapping<TypeDefinition, Modifier>>().To<ModifierMapping>().InSingletonScope();

			kernel.Bind<IMapping<IMemberDefinition, Field>>().To<FieldMapping>().InSingletonScope();
			kernel.Bind<IMapping<IMethodSignature, Method>>().To<MethodMapping>().InSingletonScope();
			kernel.Bind<IMapping<IMetadataTokenProvider, Member>>().To<MemberMapping>().InSingletonScope();

			kernel.Bind<IMapping<MethodReturnType, ReturnType>>().To<ReturnTypeMapping>().InSingletonScope();

			kernel.Bind<IMapping<ParameterDefinition, Parameter>>().To<ParameterMapping>().InSingletonScope();

			kernel.Bind<IMapping<GenericParameter, Type>>().To<GenericParameterMapping>().InSingletonScope();
			
			kernel.Bind<IMapping<ICustomAttribute, Attribute>>().To<AttributeMapping>().InSingletonScope();

			kernel.Bind<IMapping<VariableDefinition, Variable>>().To<VariableMapping>().InSingletonScope();


			kernel.Bind<IMapping<ACLContextContainer, CecilContribExpression, Method>>().To<OperatorMapping>().InSingletonScope();
		}
	}
}