using System;

namespace Fijo.Code.ATC.ACL.Common.Enums {
	[Flags]
	public enum Modifier {
		Public,
		Private,
		Protected,
		ProtectedInternal,
		Internal,
	}
}