using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Common.Enums;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Oop;
using JetBrains.Annotations;
using Type = System.Type;

namespace Fijo.Code.ATC.ACL.Common.Attrs {
	[Serializable, DataContract]
	[PublicAPI]
	[DebuggerDisplay("ModifierAttr: {Modifier}")]
	public class ModifierAttr : OopAttr {
		[DataMember(Order = 1)]
		public Modifier Modifier { get; set; }

		public override string ToString() {
			return string.Format("ModifierAttr: {0}", Modifier);
		}

		[PublicAPI]
		public class Validation : AttrValidation<ModifierAttr> {
			private readonly IList<Type> _allowedTypes = new[]
			{
				typeof (Model.Oop.Type),
				typeof (Member)
			};

			public override bool IsValidFor(Type codeObjType) {
				return base.IsValidFor(codeObjType) && _allowedTypes.Any(codeObjType.IsAssignableFrom);
			}
		}
	}
}