using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Common.Attrs {
	[Note("marks something as in most case optional thing that do not need to be handled in the most cases.")]
	public interface IOptionalAnotation {}
}