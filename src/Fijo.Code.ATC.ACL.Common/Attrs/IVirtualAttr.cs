using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Common.Attrs {
	[Note("Virtual as simulated/ not (always) really existing. There is no meaning connection to the virtual keyword.")]
	[Desc("Means, that the obj with that attr has to be handled differently/ ignored is some cases, because it is kinda �fake�.")]
	public interface IVirtualAttr {}
}