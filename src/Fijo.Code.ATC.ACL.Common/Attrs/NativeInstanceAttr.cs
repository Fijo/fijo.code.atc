using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;
using Fijo.Code.ATC.ACL.Model.Data;
using Fijo.Code.ATC.ACL.Model.Expressions;
using JetBrains.Annotations;
using Type = System.Type;

namespace Fijo.Code.ATC.ACL.Common.Attrs {
	[Serializable, DataContract]
	[PublicAPI]
	[DebuggerDisplay("NativeInstanceAttr: {Instance}")]
	public class NativeInstanceAttr : DataAttr, IVirtualAttr {
		[DataMember(Order = 1)]
		public object Value { get; set; }

		[PublicAPI]
		public class Validation : AttrValidation<NativeInstanceAttr> {
			public override bool IsValidFor(Type codeObjType) {
				return base.IsValidFor(codeObjType) && codeObjType.IsAssignableFrom(typeof (Instance));
			}
		}
	}
}