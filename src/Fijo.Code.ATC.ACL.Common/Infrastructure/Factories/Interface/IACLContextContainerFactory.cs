﻿using Fijo.Code.ATC.ACL.Common.Context;

namespace Fijo.Code.ATC.ACL.Common.Infrastructure.Factories.Interface {
	public interface IACLContextContainerFactory {
		ACLContextContainer Create();
	}
}