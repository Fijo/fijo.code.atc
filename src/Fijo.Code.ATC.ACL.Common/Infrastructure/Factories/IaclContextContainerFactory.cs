using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.Common.Infrastructure.Factories.Interface;

namespace Fijo.Code.ATC.ACL.Common.Infrastructure.Factories {
	public class ACLContextContainerFactory : IACLContextContainerFactory {
		#region Implementation of IACLContextContainerFactory
		public ACLContextContainer Create() {
			return new ACLContextContainer();
		}
		#endregion
	}
}