﻿using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.Common.Infrastructure.Factories;
using Fijo.Code.ATC.ACL.Common.Infrastructure.Factories.Interface;
using Fijo.Code.ATC.ACL.Infrastructure.Properties;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Attrs;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Common.Properties {
	[PublicAPI]
	public class ACLCommonInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new ACLInfrastructureInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<AttrValidation>().To<ModifierAttr.Validation>().InSingletonScope();
			kernel.Bind<AttrValidation>().To<NativeInstanceAttr.Validation>().InSingletonScope();

			kernel.Bind<IACLContextContainerFactory>().To<ACLContextContainerFactory>().InSingletonScope();
		}
	}
}