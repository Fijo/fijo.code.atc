using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.Common.Context {
	[Serializable, DataContract]
	public class ACLContextContainer : List<ACLContext> {
		public ACLContextContainer() {}
		public ACLContextContainer(int capacity) : base(capacity) {}
		public ACLContextContainer(IEnumerable<ACLContext> collection) : base(collection) {}
		public ACLContextContainer(params ACLContext[] collection) : base(collection) {}
	}
}