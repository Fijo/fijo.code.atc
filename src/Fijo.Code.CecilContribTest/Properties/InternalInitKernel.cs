﻿using Fijo.Code.CecilContrib.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.Code.CecilContribTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new CecilContribInjectionModule());
		}
	}
}