﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Code.CecilContrib.Dto;
using Fijo.Code.CecilContrib.Extentions;
using Fijo.Code.CecilContrib.Extentions.ModuleDefinition;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Collections.Generic;
using NUnit.Framework;
using MetadataToken = Fijo.Code.CecilContrib.Dto.MetadataToken;

namespace Fijo.Code.CecilContribTest.Extentions {
	[TestFixture]
	public class FullTokenExtentionTest {
		protected const string TestModuleFullyQualifiedName = @"Q:\NotExisting\test.dll";

		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		public void Variable_GetFullToken_MustEqualExpected(int index) {
			var moduleDef = GetModule();

			var typeReference = new TypeReference("System", "Int32", moduleDef, null);
			var methodDef = new MethodDefinition("testMethod", MethodAttributes.Public, typeReference);
			methodDef.DeclaringType = TypeDefinition.CreateType("test", "MyType", TypeAttributes.Class | TypeAttributes.Public, null, moduleDef);
			var methodBody = new MethodBody(methodDef);
			var variableDefinitions = Enumerable.Range(0, 3).Select(x => new VariableDefinition("test_" + x, typeReference)).ToList();
			methodBody.Variables.AddRange(variableDefinitions);
			methodDef.Body = methodBody;

			var fullToken = variableDefinitions[index].GetFullToken(methodDef);
			var expected = new FullToken(
				new VariableToken(new MetadataToken(new Mono.Cecil.MetadataToken(100663296)), index),
				TestModuleFullyQualifiedName);
			Assert.AreEqual(expected, fullToken);
		}

		[Test]
		public void Module_GetFullToken_MustEqualExpected() {
			var moduleDef = GetModule();
			moduleDef.MetadataToken = new Mono.Cecil.MetadataToken(123);

			var fullToken = moduleDef.GetFullToken();
			var expected = new FullToken(new MetadataToken(new Mono.Cecil.MetadataToken(123)), TestModuleFullyQualifiedName);
			Assert.AreEqual(expected, fullToken);
		}

		[Test]
		public void Assembly_GetFullToken_MustEqualExpected() {
			var assemblyDef = AssemblyDefinition.CreateAssembly(new AssemblyNameDefinition("test", new System.Version(1, 0)), "test", ModuleKind.Dll);
			assemblyDef.MetadataToken = new Mono.Cecil.MetadataToken(123);

			var fullToken = assemblyDef.GetFullToken();
			var expected = new FullToken(new SerializableAssemblyName("test, Version=1.0, Culture=neutral, PublicKeyToken=null"), null);
			Assert.AreEqual(expected, fullToken);
		}

		[Test]
		public void GenericParameter_GetFullToken_MustEqualExpected() {
			var moduleDef = GetModule();
			
			var typeReference = new TypeReference("System", "Int32", moduleDef, null);
			var methodDef = new MethodDefinition("testMethod", MethodAttributes.Public, typeReference);
			methodDef.DeclaringType = TypeDefinition.CreateType("test", "MyType", TypeAttributes.Class | TypeAttributes.Public, null, moduleDef);

			var genericParameter = new GenericParameter("T", methodDef);
			
				
			var fullToken = genericParameter.GetFullToken();
			var expected = new FullToken(new MetadataToken(new Mono.Cecil.MetadataToken(100663296)), TestModuleFullyQualifiedName);
			Assert.AreEqual(expected, fullToken);
		}

		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		public void FieldDefinition_GetFullToken_MustEqualExpected(int index) {
			FullTokenBaseTest(index, MemberType.Field, (i, typeRef, declaringType) => {
				var fieldDef = new FieldDefinition("Value_" + i, FieldAttributes.Public, typeRef);
				fieldDef.DeclaringType = declaringType;
				return fieldDef;
			}, x => x.Fields, x => x.GetFullToken());
		}

		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		public void MethodDefinition_GetFullToken_MustEqualExpected(int index) {
			FullTokenBaseTest(index, MemberType.Method, (i, typeRef, declaringType) => {
				var fieldDef = new MethodDefinition("Value_" + i, MethodAttributes.Public, typeRef);
				fieldDef.DeclaringType = declaringType;
				return fieldDef;
			}, x => x.Methods, x => x.GetFullToken());
		}

		private void FullTokenBaseTest<T>(int index, MemberType memberType, Func<int, TypeReference, TypeDefinition, T> defCreator, Func<TypeDefinition, IList<T>> getCollection, Func<T, FullToken> fullTokenResolver) {
			var moduleDef = GetModule();

			var typeReference = new TypeReference("System", "Int32", moduleDef, null);

			var typeDefinition = TypeDefinition.CreateType("test", "MyType", TypeAttributes.Class | TypeAttributes.Public, null, moduleDef);

			var fieldDefinitions = Enumerable.Range(0, 3).Select(x => {
				var fieldDef = defCreator(x, typeReference, typeDefinition);
				return fieldDef;
			}).ToList();
			getCollection(typeDefinition).AddRange(fieldDefinitions);


			var fullToken = fullTokenResolver(fieldDefinitions[index]);
			var expected = new FullToken(new MemberToken(new MetadataToken(new Mono.Cecil.MetadataToken(33554432)), memberType, index), TestModuleFullyQualifiedName);
			Assert.AreEqual(expected, fullToken);
		}

		private ModuleDefinition GetModule() {
			var moduleDef = ModuleDefinition.CreateModule("test", ModuleKind.Dll);
			moduleDef.SetFullyQualifiedName(TestModuleFullyQualifiedName);
			return moduleDef;
		}
	}
}