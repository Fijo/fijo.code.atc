﻿using System;

namespace Fijo.Code.ATC.TestAssembly.CILTest {
	public class Test {
		public void SimpleTryFinallyTest() {
			int i;
			try {
				i = 0;
			}
			finally {
				i = 4;
			}
		}

		public void SimpleTryCatchTest() {
			int i;
			try {
				i = 0;
			}
			catch (ArgumentException) {
				i = 1;
			}
			finally {
				i = 4;
			}
		}

		public void TryCatchTest() {
			int i;
			try {
				i = 0;
			}
			catch (ArgumentException) {
				i = 1;
			}
			catch (OverflowException) {
				i = 2;
			}
			finally {
				i = 4;
			}
		}

		public void MultibleTryCatchTest() {
			int i;
			try {
				try {
					i = 0;
				}
				catch (NotFiniteNumberException) {
					i = 3;
				}
			}
			catch (ArgumentException) {
				i = 1;
			}
			catch (OverflowException) {
				i = 2;
			}
			finally {
				i = 4;
			}
		}

	}
}
