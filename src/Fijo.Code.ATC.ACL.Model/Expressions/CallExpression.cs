using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Expressions {
	[Serializable, DataContract]
	[DebuggerDisplay("CallExpression: {Target}(Count= {Expressions.Count})")]
	public class CallExpression : Expression {
		[DataMember(Order = 1)]
		public Method Target { get; set; }
		[DataMember(Order = 2)]
		public Collections.Expressions ArgumentInstances { get; set; }
	}
}