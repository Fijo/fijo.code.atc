using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.Model.Expressions {
	[Serializable, DataContract]
	[DebuggerDisplay("ContainerExpression: Count = {Count}")]
	public class ContainerExpression : Expression, IList<Expression> {
		[DataMember(Order = 1)]
		protected readonly IList<Expression> Expressions = new List<Expression>();

		#region Implementation of IEnumerable
		public IEnumerator<Expression> GetEnumerator() {
			return Expressions.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
		#endregion
		#region Implementation of ICollection<Expression>
		public void Add(Expression item) {
			Expressions.Add(item);
		}

		public void Clear() {
			Expressions.Clear();
		}

		public bool Contains(Expression item) {
			return Expressions.Contains(item);
		}

		public void CopyTo(Expression[] array, int arrayIndex) {
			Expressions.CopyTo(array, arrayIndex);
		}

		public bool Remove(Expression item) {
			return Expressions.Remove(item);
		}

		public int Count { get { return Expressions.Count; } }
		public bool IsReadOnly { get { return Expressions.IsReadOnly; } }
		#endregion
		#region Implementation of IList<Expression>
		public int IndexOf(Expression item) {
			return Expressions.IndexOf(item);
		}

		public void Insert(int index, Expression item) {
			Expressions.Insert(index, item);
		}

		public void RemoveAt(int index) {
			Expressions.RemoveAt(index);
		}

		public Expression this[int index] { get { return Expressions[index]; } set { Expressions[index] = value; } }
		#endregion
	}
}