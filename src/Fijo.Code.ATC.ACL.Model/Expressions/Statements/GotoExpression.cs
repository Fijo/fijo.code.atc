using System;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements {
	[Serializable, DataContract]
	public class GotoExpression : Expression {
		[DataMember(Order = 1)]
		public Expression Target { get; set; }
	}
}