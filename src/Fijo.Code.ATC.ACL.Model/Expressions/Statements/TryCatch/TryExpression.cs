using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch {
	[Serializable, DataContract]
	public class TryExpression : Expression, IHaveBody {
		#region Implementation of IHaveBody
		[DataMember(Order = 1)]
		public Expression Body { get; set; }
		#endregion
		[DataMember(Order = 2)]
		public Catches Catches { get; set; }
		[DataMember(Order = 3)]
		public Expression Finally { get; set; }
	}
}