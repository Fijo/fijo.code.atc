using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch {
	[Serializable, DataContract]
	public class CatchExpression : Expression, IHaveBody {
		[DataMember(Order = 1)]
		public Type ExceptionType { get; set; }
		#region Implementation of IHaveBody
		[DataMember(Order = 2)]
		public Expression Body { get; set; }
		#endregion
	}
}