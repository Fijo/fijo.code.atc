using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements {
	[Serializable, DataContract]
	public class ReturnExpression : Expression, IHaveValue {
		[DataMember(Order = 1)]
		public Expression Value { get; set; }
	}

	[Serializable, DataContract]
	public class ThrowExpression : Expression, IHaveValue {
		[DataMember(Order = 1)]
		public Expression Value { get; set; }
	}
}