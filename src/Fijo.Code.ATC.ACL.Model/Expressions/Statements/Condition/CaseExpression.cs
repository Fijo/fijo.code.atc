using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition {
	[Serializable, DataContract]
	[PublicAPI]
	public class CaseExpression : Expression, IHaveBody {
		[DataMember(Order = 1)]
		public Expression Key { get; set; }
		#region Implementation of IHaveBody
		[DataMember(Order = 2)]
		public Expression Body { get; set; }
		#endregion
		[IgnoreDataMember]
		[JsonIgnore]
		public bool IsDefault { get { return Key == null; } }
	}
}