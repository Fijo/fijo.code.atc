using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition {
	[Serializable, DataContract]
	public class SwitchExpression : Expression {
		[DataMember(Order = 1)]
		public Expression Input { get; set; }
		[DataMember(Order = 2)]
		public Cases Cases { get; set; }
	}
}