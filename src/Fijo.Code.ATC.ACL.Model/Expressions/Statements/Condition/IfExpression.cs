using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition {
	[Serializable, DataContract]
	public class IfExpression : Expression, IHaveCondition {
		#region Implementation of IHaveCondition
		[DataMember(Order = 1)]
		public Expression Condition { get; set; }
		#endregion
		[DataMember(Order = 2)]
		public Expression TrueBody { get; set; }
		[DataMember(Order = 3)]
		public Expression FalseBody { get; set; }
	}
}