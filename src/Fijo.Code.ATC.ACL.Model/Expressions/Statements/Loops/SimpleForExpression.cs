using System;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops {
	[Serializable, DataContract]
	public class SimpleForExpression : LoopExpression {
		[DataMember(Order = 1)]
		public Expression AfterThroughtOperation { get; set; }
	}
}