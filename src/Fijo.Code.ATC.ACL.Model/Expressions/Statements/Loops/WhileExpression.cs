using System;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops {
	[Serializable, DataContract]
	public class WhileExpression : LoopExpression {}
}