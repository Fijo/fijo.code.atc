using System;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops {
	[Serializable, DataContract]
	public class ForExpression : SimpleForExpression {
		[DataMember(Order = 1)]
		public Expression Initialization { get; set; }
	}
}