using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Statements.Loops {
	[Serializable, DataContract]
	public abstract class LoopExpression : Expression, IHaveCondition, IHaveBody {
		#region Implementation of IHaveCondition
		[DataMember(Order = 1)]
		public Expression Condition { get; set; }
		#endregion
		#region Implementation of IHaveBody
		[DataMember(Order = 2)]
		public Expression Body { get; set; }
		#endregion
	}
}