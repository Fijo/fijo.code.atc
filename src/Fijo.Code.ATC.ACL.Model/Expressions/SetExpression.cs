using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Model.Expressions {
	[Serializable, DataContract]
	[About("AssignmentExpression")]
	public class SetExpression : Expression, IHaveValue {
		[About("TargetObject")]
		[About("TargetVariable")]
		[DataMember(Order = 1)]
		public Expression Target { get; set; }
		#region Implementation of IHaveValue
		[DataMember(Order = 2)]
		public Expression Value { get; set; }
		#endregion
	}
}