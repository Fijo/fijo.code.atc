using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Expressions {
	[Serializable, DataContract]
	public abstract class Expression : Base.Code {
		[DataMember(Order = 1)]
		public ExpressionAttrs ExpressionAttrs { get; private set; }

		protected Expression() {
			ExpressionAttrs = new ExpressionAttrs();
		}
	}
}