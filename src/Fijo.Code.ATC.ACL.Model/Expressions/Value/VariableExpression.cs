using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Value {
	[Serializable, DataContract]
	[DebuggerDisplay("VariableExpression: Variable.Name")]
	public class VariableExpression : Expression, IHaveVariable, IValueResolver {
		#region Implementation of IHaveVariable
		[DataMember(Order = 1)]
		public Variable Variable { get; set; }
		#endregion
	}
}