using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Data;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Value {
	[Serializable, DataContract]
	[About("ConstantExpression")]
	public class ValueExpression : Expression, IHaveInstance, IValueResolver {
		#region Implementation of IHaveInstance
		[DataMember(Order = 1)]
		public Instance Instance { get; set; }
		#endregion
	}
}