using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Expressions.Value {
	[Serializable, DataContract]
	public class FieldExpression : Expression, IValueResolver {
		[DataMember(Order = 1)]
		public Expression Instance { get; set; }
		[DataMember(Order = 2)]
		public Field Target { get; set; }
	}
}