using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Oop;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class Parameters : List<Parameter> {
		public Parameters() {}
		public Parameters(int capacity) : base(capacity) {}
		public Parameters(IEnumerable<Parameter> collection) : base(collection) {}
		public Parameters(params Parameter[] collection) : base(collection) {}
	}
}