using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Data;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class MemberInstances : List<MemberInstance> {
		public MemberInstances() {}
		public MemberInstances(int capacity) : base(capacity) {}
		public MemberInstances(IEnumerable<MemberInstance> collection) : base(collection) {}
		public MemberInstances(params MemberInstance[] collection) : base(collection) {}
	}
}