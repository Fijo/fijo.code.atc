using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Oop;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class Variables : List<Variable> {
		public Variables() {}
		public Variables(int capacity) : base(capacity) {}
		public Variables(IEnumerable<Variable> collection) : base(collection) {}
		public Variables(params Variable[] collection) : base(collection) {}
	}
}