using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Attrs;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class GeneralAttrs : List<GeneralAttr> {
		public GeneralAttrs() {}
		public GeneralAttrs(int capacity) : base(capacity) {}
		public GeneralAttrs(IEnumerable<GeneralAttr> collection) : base(collection) {}
		public GeneralAttrs(params GeneralAttr[] collection) : base(collection) {}
	}
}