using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.Condition;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class Cases : List<CaseExpression> {
		public Cases() {}
		public Cases(int capacity) : base(capacity) {}
		public Cases(IEnumerable<CaseExpression> collection) : base(collection) {}
		public Cases(params CaseExpression[] collection) : base(collection) {}
	}
}