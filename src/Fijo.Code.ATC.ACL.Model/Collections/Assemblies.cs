using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	public class Assemblies : List<Assembly> {
		public Assemblies() {}
		public Assemblies(int capacity) : base(capacity) {}
		public Assemblies(IEnumerable<Assembly> collection) : base(collection) {}
		public Assemblies(params Assembly[] collection) : base(collection) {}
	}
}