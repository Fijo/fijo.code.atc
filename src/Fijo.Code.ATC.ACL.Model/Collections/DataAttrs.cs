using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Attrs;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class DataAttrs : List<DataAttr>{
		public DataAttrs() {}
		public DataAttrs(int capacity) : base(capacity) {}
		public DataAttrs(IEnumerable<DataAttr> collection) : base(collection) {}
		public DataAttrs(params DataAttr[] collection) : base(collection) {}
	}
}