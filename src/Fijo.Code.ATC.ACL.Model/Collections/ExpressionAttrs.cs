using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Attrs;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class ExpressionAttrs : List<ExpressionAttr> {
		public ExpressionAttrs() {}
		public ExpressionAttrs(int capacity) : base(capacity) {}
		public ExpressionAttrs(IEnumerable<ExpressionAttr> collection) : base(collection) {}
		public ExpressionAttrs(params ExpressionAttr[] collection) : base(collection) {}
	}
}