using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	public class Members : List<Member> {
		public Members() {}
		public Members(int capacity) : base(capacity) {}
		public Members(IEnumerable<Member> collection) : base(collection) {}
		public Members(params Member[] collection) : base(collection) {}
	}
}