using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements.TryCatch;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	public class Catches : List<CatchExpression> {
		public Catches() {}
		public Catches(int capacity) : base(capacity) {}
		public Catches(IEnumerable<CatchExpression> collection) : base(collection) {}
		public Catches(params CatchExpression[] collection) : base(collection) {}
	}
}