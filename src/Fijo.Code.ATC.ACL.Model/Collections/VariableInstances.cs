using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Data;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	[PublicAPI]
	public class VariableInstances : List<VariableInstance> {
		public VariableInstances() {}
		public VariableInstances(int capacity) : base(capacity) {}
		public VariableInstances(IEnumerable<VariableInstance> collection) : base(collection) {}
		public VariableInstances(params VariableInstance[] collection) : base(collection) {}
	}
}