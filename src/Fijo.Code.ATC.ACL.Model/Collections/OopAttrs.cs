using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Attrs;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	public class OopAttrs : List<OopAttr> {
		public OopAttrs() {}
		public OopAttrs(int capacity) : base(capacity) {}
		public OopAttrs(IEnumerable<OopAttr> collection) : base(collection) {}
		public OopAttrs(params OopAttr[] collection) : base(collection) {}
	}
}