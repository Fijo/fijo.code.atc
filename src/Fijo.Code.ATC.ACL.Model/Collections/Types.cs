using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	//[KnownType(typeof(Type[]))]
	public class Types : List<Type> {
		public Types() {}
		public Types(int capacity) : base(capacity) {}
		public Types(IEnumerable<Type> collection) : base(collection) {}
		public Types(params Type[] collection) : base(collection) {}
	}
}