using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Expressions;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	public class Expressions : List<Expression> {
		public Expressions() {}
		public Expressions(int capacity) : base(capacity) {}
		public Expressions(IEnumerable<Expression> collection) : base(collection) {}
		public Expressions(params Expression[] collection) : base(collection) {}
	}
}