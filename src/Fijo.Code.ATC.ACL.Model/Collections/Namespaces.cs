using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Collections {
	[Serializable, DataContract]
	public class Namespaces : List<Namespace> {
		public Namespaces() {}
		public Namespaces(int capacity) : base(capacity) {}
		public Namespaces(IEnumerable<Namespace> collection) : base(collection) {}
		public Namespaces(params Namespace[] collection) : base(collection) {}
	}
}