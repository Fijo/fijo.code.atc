using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[DebuggerDisplay("Field: {Name}")]
	public class Field : Member {
		public override string ToString() {
			return string.Format("Field: {0}", Name);
		}
	}
}