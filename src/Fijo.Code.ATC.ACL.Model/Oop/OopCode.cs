using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	public abstract class OopCode : Base.Code {
		[DataMember(Order = 1)]
		public OopAttrs OopAttrs { get; private set; }

		protected OopCode() {
			OopAttrs = new OopAttrs();
		}
	}
}