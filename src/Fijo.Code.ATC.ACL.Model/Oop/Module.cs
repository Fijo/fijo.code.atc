using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[DebuggerDisplay("Module: {Name}")]
	[Note("This module has nothing to do with the Modules in DotNet. This is just a bundle of assemblies")]
	public class Module : OopCode, IHaveName, IHaveAssemblies {
		#region Implementation of IHaveName
		[DataMember(Order = 1)]
		public string Name { get; set; }
		#endregion
		#region Implementation of IHaveAssemblies
		[DataMember(Order = 2)]
		public Assemblies Assemblies { get; set; }
		#endregion

		public override string ToString() {
			return string.Format("Module: {0}", Name);
		}
	}
}