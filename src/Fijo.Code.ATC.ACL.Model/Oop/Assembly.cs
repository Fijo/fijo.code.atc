using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[DebuggerDisplay("Assembly: {Name}")]
	public class Assembly : OopCode, IHaveName, IHaveAssemblies, IHaveNamespaces {
		#region Implementation of IHaveName
		[DataMember(Order = 1)]
		public string Name { get; set; }
		#endregion
		#region Implementation of IHaveAssemblies
		[DataMember(Order = 2)]
		public Assemblies Assemblies { get; set; }
		#endregion
		#region Implementation of IHaveNamespaces
		[DataMember(Order = 3)]
		public Namespaces Namespaces { get; set; }
		#endregion
		public override string ToString() {
			return string.Format("Assembly: {0}", Name);
		}
	}
}