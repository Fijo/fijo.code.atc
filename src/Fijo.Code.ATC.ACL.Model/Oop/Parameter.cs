using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[PublicAPI]
	[DebuggerDisplay("Parameter: {Name}")]
	public class Parameter : Variable {
		public override string ToString() {
			return string.Format("Parameter: {0}", Name);
		}
	}
}