using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[DebuggerDisplay("Member: {Name}")]
	public class Member : OopCode, IHaveName, IHaveType  {
		#region Implementation of IHaveName
		[DataMember(Order = 1)]
		public string Name { get; set; }
		#endregion
		#region Implementation of IHaveType
		[DataMember(Order = 2)]
		public Type Type { get; set; }
		#endregion

		public override string ToString() {
			return "Member: " + Name;
		}
	}
}