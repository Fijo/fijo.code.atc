using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[DebuggerDisplay("Namespace: {Name}")]
	public class Namespace : OopCode, IHaveName, IHaveAssembly, IHaveTypes {
		#region Implementation of IHaveName
		[DataMember(Order = 1)]
		public string Name { get; set; }
		#endregion
		#region Implementation of IHaveAssembly
		[DataMember(Order = 2)]
		public Assembly Assembly { get; set; }
		#endregion
		#region Implementation of IHaveTypes
		[DataMember(Order = 3)]
		public Types Types { get; set; }
		#endregion

		public override string ToString() {
			return string.Format("Namespace: {0}", Name);
		}
	}
}