﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[DebuggerDisplay("ReturnType: {Type.Name}")]
	public class ReturnType : OopCode, IHaveType {
		[DataMember(Order = 1)]
		public Type Type { get; set; }

		public override string ToString() {
			return string.Format("ReturnType: {0}", Type.Name);
		}
	}
}