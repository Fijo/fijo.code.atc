using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[PublicAPI]
	[DebuggerDisplay("Method: {Name}")]
	public class Method : Member, IHaveBody  {
		[DataMember(Order = 1)]
		public Parameters Parameters { get; set; }
		#region Implementation of IHaveBody
		[DataMember(Order = 2)]
		public Expression Body { get; set; }
		#endregion
		[DataMember(Order = 3)]
		public ReturnType ReturnType { get; set; }

		public override string ToString() {
			return "Method: " + Name;
		}
	}
}