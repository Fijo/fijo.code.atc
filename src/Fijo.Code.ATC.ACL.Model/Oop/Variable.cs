using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[PublicAPI]
	[DebuggerDisplay("Variable: {Name}")]
	public class Variable : OopCode, IHaveType, IHaveName {
		#region Implementation of IHaveType
		[DataMember(Order = 1)]
		public Type Type { get; set; }
		#endregion
		#region Implementation of IHaveName
		[DataMember(Order = 2)]
		public string Name { get; set; }
		#endregion

		public override string ToString() {
			return string.Format("Variable: {0}", Name);
		}
	}
}