using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Interfaces;

namespace Fijo.Code.ATC.ACL.Model.Oop {
	[Serializable, DataContract]
	[DebuggerDisplay("Type: {Name}")]
	public class Type : OopCode, IHaveName, IHaveNamespace {
		#region Implementation of IHaveName
		[DataMember(Order = 1)]
		public string Name { get; set; }
		#endregion
		#region Implementation of IHaveNamespace
		[DataMember(Order = 2)]
		public Namespace Namespace { get; set; }
		#endregion
		[DataMember(Order = 3)]
		public Members Members { get; set; }
		[DataMember(Order = 4)]
		public Types BaseTypes { get; set; }

		public Type() {
			BaseTypes = new Types();
		}

		public override string ToString() {
			return string.Format("Type: {0}", Name);
		}
	}
}