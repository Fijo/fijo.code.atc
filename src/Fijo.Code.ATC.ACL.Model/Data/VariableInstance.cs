using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Fijo.Code.ATC.ACL.Model.Oop;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Data {
	[Serializable, DataContract]
	[PublicAPI]
	public class VariableInstance : DataCode, IHaveVariable, IHaveInstance {
		#region Implementation of IHaveVariable
		[DataMember(Order = 1)]
		public virtual Variable Variable { get; set; }
		#endregion
		#region Implementation of IHaveInstance
		[DataMember(Order = 2)]
		public Instance Instance { get; set; }
		#endregion
	}
}