using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Data {
	[Serializable, DataContract]
	public abstract class DataCode : Base.Code {
		[DataMember(Order = 1)]
		public DataAttrs DataAttrs { get; private set; }

		protected DataCode() {
			DataAttrs = new DataAttrs();
		}
	}
}