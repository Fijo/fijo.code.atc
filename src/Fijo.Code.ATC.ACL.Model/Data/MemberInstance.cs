using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using Fijo.Code.ATC.ACL.Model.Oop;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.Model.Data {
	[Serializable, DataContract]
	[PublicAPI]
	[DebuggerDisplay("MemberInstance: {Member.Name} -> {Instance.Type.Name}")]
	public class MemberInstance : DataCode, IHaveMember, IHaveInstance {
		#region Implementation of IHaveMember
		[DataMember(Order = 1)]
		public Member Member { get; set; }
		#endregion
		#region Implementation of IHaveInstance
		[DataMember(Order = 2)]
		public Instance Instance { get; set; }
		#endregion

		public override string ToString() {
			return string.Format("MemberInstance: {0} -> {1}", Member.Name, Instance.Type.Name);
		}
	}
}