using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Interfaces;
using JetBrains.Annotations;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.Model.Data {
	[Serializable, DataContract]
	[PublicAPI]
	[DebuggerDisplay("Instance: {Type.Name}")]
	public class Instance : DataCode, IHaveType {
		#region Implementation of IHaveType
		[DataMember(Order = 1)]
		public Type Type { get; set; }
		#endregion
		[DataMember(Order = 2)]
		public MemberInstances MemberInstances { get; set; }

		public override string ToString() {
			return string.Format("Instance: {0}", Type.Name);
		}
	}
}