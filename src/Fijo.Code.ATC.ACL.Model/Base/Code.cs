using System;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Base {
	[Serializable, DataContract]
	public abstract class Code {
		[DataMember(Order = 1)]
		public GeneralAttrs GeneralAttrs { get; private set; }

		protected Code() {
			GeneralAttrs = new GeneralAttrs();
		}
	}
}