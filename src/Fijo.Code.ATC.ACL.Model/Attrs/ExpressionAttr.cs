using System;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Model.Attrs {
	[Serializable, DataContract]
	[Desc("Attributes for types that inherit from Expression.")]
	public abstract class ExpressionAttr : Attr {}
}