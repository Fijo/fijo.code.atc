using System;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Model.Attrs {
	[Serializable, DataContract]
	[Desc("Attributes for all types of Code Objects.")]
	public abstract class GeneralAttr : Attr {}
}