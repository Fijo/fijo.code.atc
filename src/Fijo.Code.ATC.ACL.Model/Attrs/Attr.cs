using System;
using System.Runtime.Serialization;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Code.ATC.ACL.Model.Attrs {
	[Serializable, DataContract]
	[AboutName("Attr", "Attribute")]
	[About("CodeAttribute")]
	[About("CodeProperty")]
	public abstract class Attr {}
}