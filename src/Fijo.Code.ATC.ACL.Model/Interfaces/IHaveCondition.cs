using Fijo.Code.ATC.ACL.Model.Expressions;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveCondition {
		Expression Condition { get; set; }
	}
}