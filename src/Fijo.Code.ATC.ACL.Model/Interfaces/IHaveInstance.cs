using Fijo.Code.ATC.ACL.Model.Data;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveInstance {
		Instance Instance { get; set; }
	}
}