using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveAssemblies {
		Assemblies Assemblies { get; set; }
	}
}