using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveVariable {
		Variable Variable { get; set; }
	}
}