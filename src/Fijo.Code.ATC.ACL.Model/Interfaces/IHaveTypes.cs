using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveTypes {
		Types Types { get; set; }
	}
}