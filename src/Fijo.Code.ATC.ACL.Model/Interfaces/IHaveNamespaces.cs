using Fijo.Code.ATC.ACL.Model.Collections;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveNamespaces {
		Namespaces Namespaces { get; set; }
	}
}