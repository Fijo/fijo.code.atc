namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveName {
		string Name { get; set; }
	}
}