using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveNamespace {
		Namespace Namespace { get; set; }
	}
}