using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveType {
		Type Type { get; set; }
	}
}