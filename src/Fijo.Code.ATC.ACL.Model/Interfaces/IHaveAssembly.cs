using Fijo.Code.ATC.ACL.Model.Oop;

namespace Fijo.Code.ATC.ACL.Model.Interfaces {
	public interface IHaveAssembly {
		Assembly Assembly { get; set; }
	}
}