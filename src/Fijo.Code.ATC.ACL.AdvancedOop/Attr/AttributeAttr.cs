using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.AdvancedOop.Collections;
using Fijo.Code.ATC.ACL.Common.Attrs;
using Fijo.Code.ATC.ACL.Model.Attrs;

namespace Fijo.Code.ATC.ACL.AdvancedOop.Attr {
	[Serializable, DataContract]
	[DisplayName("AttributeAttr: {Type}")]
	public class AttributeAttr : OopAttr, IOptionalAnotation {
		[DataMember(Order = 1)]
		public Attributes Attributes { get; set; }

		public AttributeAttr() {
			Attributes = new Attributes();
		}
	}
}