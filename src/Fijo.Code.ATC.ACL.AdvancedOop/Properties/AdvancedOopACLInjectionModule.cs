﻿using Fijo.Code.ATC.ACL.Common.Properties;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace Fijo.Code.ATC.ACL.AdvancedOop.Properties {
	[PublicAPI]
	public class AdvancedOopACLInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
			kernel.Load(new ACLCommonInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
		}
	}
}