﻿using System.Collections.Generic;
using Fijo.Code.ATC.ACL.AdvancedOop.Dto;

namespace Fijo.Code.ATC.ACL.AdvancedOop.Collections {
	public class Attributes : List<Attribute> {
		public Attributes() {}
		public Attributes(int capacity) : base(capacity) {}
		public Attributes(IEnumerable<Attribute> collection) : base(collection) {}
	}
}