using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.ACL.AdvancedOop.Dto {
	[Serializable, DataContract]
	[DebuggerDisplay("Attribute: {Type.Name}")]
	public class Attribute : OopCode {
		[DataMember(Order = 1)]
		public Type Type { get; set; }
		[Note("should target the ctor of the Type")]
		[DataMember(Order = 2)]
		public CallExpression CallExpression { get; set; }
	}
}