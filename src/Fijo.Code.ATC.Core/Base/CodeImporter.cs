﻿using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Infrastructure.DesignPattern.Mapping;

namespace Fijo.Code.ATC.Core.Base {
	public abstract class CodeImporter<T> : IMapping<T, Module> {
		#region Implementation of IMapping<in T,out Module>
		public abstract Module Map(T source);
		#endregion
	}
}