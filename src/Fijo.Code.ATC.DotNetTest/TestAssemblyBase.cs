﻿using Fijo.Code.ATC.DotNetTest.Data;
using Fijo.Code.ATC.DotNetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using Mono.Cecil;
using NUnit.Framework;

namespace Fijo.Code.ATC.DotNetTest {
	[TestFixture]
	public abstract class TestAssemblyBase {
		protected AssemblyDefinition TestAssembly;
		protected ModuleDefinition TestModule;
		protected TypeSystem TypeSystem;

		[SetUp]
		public virtual void SetUp() {
			InitDependencyInjection();
			
			var testAssemblyProvider = Kernel.Resolve<TestAssemblyProvider>();
			TestAssembly = testAssemblyProvider.Get();
			TestModule = TestAssembly.MainModule;
			TypeSystem = TestModule.TypeSystem;
		}

		protected virtual void InitDependencyInjection() {
			new InternalInitKernel().Init();
		}
	}
}