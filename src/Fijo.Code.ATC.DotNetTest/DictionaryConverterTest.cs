﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using Fijo.Code.ATC.DotNet.Mapping;
using Fijo.Code.ATC.DotNetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl.Json.Converters;
using NUnit.Framework;
using Newtonsoft.Json;

namespace Fijo.Code.ATC.DotNetTest {
	[TestFixture]
	public class DictionaryConverterTest {
		private class TestObj {}

		private DictionaryConverter2 _dictionaryConverter;
		private JsonSerializerSettings _jsonSerializerSettings;
		private const string TestJson = @"{""$id"":""1"",""Content"":[{""$id"":""2"",""Key"":""hello"",""Value"":{""$id"":""3"",""$type"":""Fijo.Code.ATC.DotNetTest.DictionaryConverterTest+TestObj, Fijo.Code.ATC.DotNetTest""}}],""DictionaryType"":""System.Collections.Generic.Dictionary`2[[Fijo.Code.ATC.DotNetTest.DictionaryConverterTest+TestObj, Fijo.Code.ATC.DotNetTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null],[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089""}";

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();

			_dictionaryConverter = Kernel.Resolve<DictionaryConverter2>();
			_jsonSerializerSettings = GetJSONSerializerSettings();
		}

		[Test]
		public void WriteJsonTest() {
			var actual = JsonConvert.SerializeObject(GetTestDict(), _jsonSerializerSettings);
			var expected = TestJson;
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void ReadJsonTest() {
			var actual = JsonConvert.DeserializeObject<Dictionary<TestObj, string>>(TestJson, _jsonSerializerSettings);
			Assert.AreEqual(1, actual.Count);
			var first = actual.First();
			Assert.IsNotNull(first.Key);
			Assert.AreEqual("hello", first.Value);
		}

		[TestCase(typeof(Dictionary<TestObj, string>), true)]
		[TestCase(typeof(TestObj), false)]
		public void CanSerializeTest(Type type, bool expected) {
			Assert.AreEqual(expected, _dictionaryConverter.CanConvert(type));
		}

		private static Dictionary<TestObj, string> GetTestDict() {
			return new Dictionary<TestObj, string>
			{
				{new TestObj(), "hello"}
			};
		}

		private JsonSerializerSettings GetJSONSerializerSettings() {
			var jsonSerializerSettings = new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Auto,
				PreserveReferencesHandling = PreserveReferencesHandling.Objects,
				TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple
			};
			jsonSerializerSettings.Converters.Add(_dictionaryConverter);
			return jsonSerializerSettings;
		}
	}
}