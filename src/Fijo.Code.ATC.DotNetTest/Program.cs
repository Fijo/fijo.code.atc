﻿namespace Fijo.Code.ATC.DotNetTest {
	class Program {
		 static void Main(string[] args) {
			 var dotNetImporterTest = new DotNetImporterTest();
			 dotNetImporterTest.SetUp();
			 dotNetImporterTest.ImportTest();
		 }
	}
}