﻿using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.Infrastructure.Provider.Children;
using Fijo.Code.ATC.ACL.Infrastructure.Validations.Interfaces;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.Core.Base;
using Fijo.Code.ATC.DotNet.Framework;
using Fijo.Code.ATC.DotNet.Model;
using Fijo.Code.ATC.DotNetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Mono.Cecil;
using NUnit.Framework;

namespace Fijo.Code.ATC.DotNetTest {
	[TestFixture]
	public class DotNetImporterTest {
		private CodeImporter<DotNetModule> _codeImporter;
		private IAssemblyService _assemblyService;
		private IChildrenService _childrenService;
		private IValidationService _validationService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_codeImporter = Kernel.Resolve<CodeImporter<DotNetModule>>();
			_assemblyService = Kernel.Resolve<IAssemblyService>();
			_childrenService = Kernel.Resolve<IChildrenService>();
			_validationService = Kernel.Resolve<IValidationService>();
		}

		[Test]
		public void ImportTest() {
			var module = _codeImporter.Map(GetDotNetModule());

			var children = _childrenService.GetAllChildrenFlatten(module).ToList();
			var type = children.OfType<Type>().First();
			var validationResult = _validationService.GetValidationResult(type).ToList();
		}

		private DotNetModule GetDotNetModule() {
			return new DotNetModule
			{
				Name = "TestModule",
				Assemblies = GetAssemblies().ToList()
			};
		}

		private IEnumerable<AssemblyDefinition> GetAssemblies() {
			yield return _assemblyService.GetAssembly(@"R:\FijoCore.Infrastructure.LightContrib\Ninject.dll");
		}

		
		[Test]
		public void Test() {
			var test = new int[,] {};
		}
	}
}