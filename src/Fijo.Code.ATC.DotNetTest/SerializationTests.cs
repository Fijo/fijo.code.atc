﻿using System.Collections.Generic;
using Fijo.Code.ATC.ACL.Common.Context;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Dto;
using Fijo.Code.ATC.DotNetTest.Properties;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Impl;
using FijoCore.Infrastructure.LightContribTest.Module.Serialization.Impl;
using NUnit.Framework;
using Newtonsoft.Json;
using Type = Fijo.Code.ATC.ACL.Model.Oop.Type;

namespace Fijo.Code.ATC.DotNetTest {
	[TestFixture]
	public class JsonSerializationTests : SerializationTestBase<JSONSerialization> {
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}


		[Test]
		public void ACLContextContainer_JsonSerialize() {
			var aclContextContainer = new ACLContextContainer();

			aclContextContainer.Add(new DotNetACLContext
			{
				Content = new Dictionary<IEnumerable<string>, Type>
				{
					{new[] {"test", "test2"}, null}
				},
				ArrayTypes = new Dictionary<ArrayDimension[], Type>
				{
					//{new []{new ArrayDimension(0, 2)}, new Type{Name = "[]"}},
					//{new []{new ArrayDimension(0, 2),new ArrayDimension(0, 2)}, new Type{Name = "[,]"}},
					//{new []{new ArrayDimension(0, 2),new ArrayDimension(0, 2), new ArrayDimension(0, 2)}, new Type{Name = "[,,]"}},
				}
			});


			var test = SerializeDeserializeTest(aclContextContainer);
		}

		[Test]
		public void Issue() {
			var serializedObject = JsonConvert.SerializeObject(new Dictionary<string[], Type>
			{
				{new[] {"hello", "world"}, null}
			});
			// A Dictionary with an Array/ List/ IEnumerable<>/ ICollection<> etc as key is not serialized correctly
			// the code above produces the following json {"String[]-Array":null}
			Assert.DoesNotThrow(() => JsonConvert.DeserializeObject(serializedObject, typeof (Dictionary<string[], Type>)));
		}
	}
}