﻿using Fijo.Code.ATC.DotNet.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.Code.ATC.DotNetTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new DotNetATCInjectionModule());
		}
	}
}