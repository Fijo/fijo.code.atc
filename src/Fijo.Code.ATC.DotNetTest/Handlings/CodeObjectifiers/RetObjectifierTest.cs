using System.Linq;
using Fijo.Code.ATC.ACL.Model.Expressions.Statements;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using Mono.Cecil;
using NUnit.Framework;

namespace Fijo.Code.ATC.DotNetTest.Handlings.CodeObjectifiers {
	[TestFixture]
	public class RetObjectifierTest : ObjectifierTestBase<RetObjectifier> {
		private TypeDefinition _testType;

		public override void SetUp() {
			base.SetUp();
			_testType = ObjectifierNamespace.Single(x => x.Name == "RetTest");
		}

		[Test]
		public void VoidMethod_Handle_HasNoReturnValue() {
			InitMethod(_testType.Methods.Single(x => x.Name == "VoidMethod" && x.Parameters.None()));

			var ret = GetReturn();
			Assert.IsNull(ret.Value);
		}

		[Test]
		public void VoidMethod_SomethingOnStack_Handle_HasNoReturnValue() {
			InitMethod(_testType.Methods.Single(x => x.Name == "VoidMethod" && x.Parameters.None()));
			
			var valueExpression = GetNewType(_testType);
			ObjectifierContext.Stack.Push(valueExpression);

			var ret = GetReturn();
			Assert.IsNull(ret.Value);
		}

		[Test]
		public void GetRetTestMethod_RetTestObjectOnStack_Handle_HasNoReturnValue() {
			InitMethod(_testType.Methods.Single(x => x.Name == "GetRetTestMethod" && x.Parameters.None()));

			var valueExpression = GetNewType(_testType);
			ObjectifierContext.Stack.Push(valueExpression);

			var ret = GetReturn();
			Assert.AreSame(valueExpression, ret.Value);
		}

		private ReturnExpression GetReturn() {
			return (ReturnExpression) CurrentObjectifier.Handle(GetRetInstruction(), ObjectifierContext);
		}

		private RetInstruction GetRetInstruction() {
			return new RetInstruction(null);
		}
	}
}