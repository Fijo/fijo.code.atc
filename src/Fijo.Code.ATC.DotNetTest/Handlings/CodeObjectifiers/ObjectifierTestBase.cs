﻿using System;
using System.Diagnostics;
using Fijo.Code.ATC.ACL.Model.Collections;
using Fijo.Code.ATC.ACL.Model.Expressions.Value;
using Fijo.Code.ATC.DotNet.Adjustments.Assemblies;
using Fijo.Code.ATC.DotNet.Adjustments;
using Fijo.Code.ATC.DotNet.Adjustments.Attribute;
using Fijo.Code.ATC.DotNet.Adjustments.HaveAttribute;
using Fijo.Code.ATC.DotNet.Adjustments.Module;
using Fijo.Code.ATC.DotNet.Adjustments.Namespaces;
using Fijo.Code.ATC.DotNet.Adjustments.ReturnType;
using Fijo.Code.ATC.DotNet.Adjustments.Parameter;
using Fijo.Code.ATC.DotNet.Adjustments.Type;
using Fijo.Code.ATC.DotNet.Adjustments.Variable;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Fijo.Code.ATC.ACL.DotNet.Context;
using Fijo.Code.ATC.ACL.DotNet.Infrastructure.Factory.Interface;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.ACL.Model.Oop;
using Fijo.Code.ATC.Core.Base;
using Fijo.Code.ATC.DotNet.Adjustments.Members;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers.Base;
using Fijo.Code.ATC.DotNet.Mapping;
using Fijo.Code.ATC.DotNet.Model;
using Fijo.Code.ATC.DotNet.Model.Intern;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Ninject;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;
using SType = System.Type;

namespace Fijo.Code.ATC.DotNetTest.Handlings.CodeObjectifiers {
	[TestFixture]
	public abstract class ObjectifierTestBase<T> : TestAssemblyBase where T : ICodeObjectifier {
		protected Stopwatch Stopwatch;
		protected ICodeObjectifier CurrentObjectifier;
		protected ICollection<TypeDefinition> ObjectifierNamespace;
		protected IDotNetCodeFactory CodeFactory;
		protected Method Method;
		protected MethodDefinition MethodDef;
		protected MappingContext MappingContext;
		protected CodeObjectifierCurrentContext ObjectifierContext;
		private DotNetImporter _dotNetImporter;

		[SetUp]
		public virtual void SetUp() {
			base.SetUp();
			CodeFactory = Kernel.Resolve<IDotNetCodeFactory>();
			CurrentObjectifier = Kernel.Inject.GetAll<ICodeObjectifier>().OfType<T>().Single();
			
			ObjectifierNamespace = TestModule.Types.Where(x => x.Namespace == "Fijo.Code.ATC.TestAssembly.CodeObjectifiers").Execute();
			_dotNetImporter = GetDotNetImporter();
			
			MappingContext = GetContext();
		}

		protected override void InitDependencyInjection() {
			base.InitDependencyInjection();
			Stopwatch = new Stopwatch();
			Stopwatch.Stop();
			Stopwatch.Reset();
			Stopwatch.Start();
		}

		private DotNetImporter GetDotNetImporter() {
			var inject = Kernel.Inject;
			var usedAdjustments = new[]
			{
				typeof(AssemblyModuleReferenceAdjustment),
				typeof(AttributeCallExpressionAdjustment),
				typeof(AttributeTypeAdjustment),
				typeof(HaveAttributeAttributeAdjustment),
				typeof(MethodReturnTypeAdjustment),
				typeof(ModuleAssemblyReferenceAdjustment),
				typeof(ModuleNamespaceAdjustment),
				typeof(NamespaceModuleAdjustment),
				typeof(NamespaceTypesAdjustment),
				typeof(BaseTypesAdjustment),
				typeof(TypeNamespaceAdjustment),
				typeof(VariableTypeAdjustment),
				
				typeof(MemberTypeAdjustment),
				typeof(MethodParametersAdjustment),
				typeof(TypeMemberAdjustment),

				typeof(SpecialAssemblyAdjustment),
				typeof(GenericParametersAdjustment),
				typeof(GenericTypeAdjustment),
				typeof(ReturnTypeTypeAdjustment),
				typeof(ParameterTypeAdjustment),
				typeof(TypeSystemAdjustment)
			};
			Debug.Assert(usedAdjustments.IsUnique());
			
			var adjustmentsTypes = inject.GetAll<IAdjustment<MappingContext>>().Select(x => x.GetType()).Execute();
			var adjustmentsTypesUsed = adjustmentsTypes.Where(x => x.IsIn(usedAdjustments)).Execute();
			inject.Unbind<IAdjustment<MappingContext>>();
			adjustmentsTypesUsed.ForEach(BindAdjustment(inject));
			var toReturn = (DotNetImporter) Kernel.Resolve<CodeImporter<DotNetModule>>();
			adjustmentsTypes.Except(adjustmentsTypesUsed).ForEach(BindAdjustment(inject));
			return toReturn;
		}

		private Action<SType> BindAdjustment(IKernel inject) {
			return x => inject.Bind<IAdjustment<MappingContext>>().To(x).InSingletonScope();
		}

		protected void InitMethod(MethodDefinition methodDef) {
			MethodDef = methodDef;
			Method = MappingContext.MethodsDefs[methodDef];
			ObjectifierContext = GetCodeObjectifierCurrentContext(GetCodeObjectifierContext(methodDef));
		}


		protected CodeObjectifierCurrentContext GetCodeObjectifierCurrentContext(CodeObjectifierContext objectifierContext) {
			return new CodeObjectifierCurrentContext
			{
				Context = objectifierContext,
				Stack = new Stack<Expression>()
			};
		}

		protected CodeObjectifierContext GetCodeObjectifierContext(MethodDefinition methodDef) {
			var method = MappingContext.MethodsDefs[methodDef];
			var methodBody = new MethodBody(methodDef);
			return new CodeObjectifierContext
			{
				Body = methodBody,
				Context = MappingContext,
				DotNetACLContext = MappingContext.ContextContainer.OfType<DotNetACLContext>().Single(),
				InstructionDict = null,
				Instructions = null,
				Method = method,
				Parameters = GetParameters(MappingContext, methodBody),
				ReturnType = GetReturnType(MappingContext, methodDef),
				Variables = GetVariables(MappingContext, methodBody)
			};
		}

		private IDictionary<VariableDefinition, Variable> GetVariables(MappingContext context, MethodBody body) {
			var varDict = context.Variables;
			return context.SourceVariables[body.Method].ToDict(x => x, x => varDict[x]);
		}

		private IDictionary<ParameterDefinition, Parameter> GetParameters(MappingContext context, MethodBody body) {
			var paramDict = context.Parameters;
			return context.SourceParameters[body.Method].ToDict(x => x, x => paramDict[x]);
		}

		private TypeReference GetReturnType(MappingContext context, IMethodSignature targetMethod) {
			return context.SourceReturnTypes[targetMethod].ReturnType;
		}

		private MappingContext GetContext() {
			return _dotNetImporter.InternMap(new DotNetModule
			{
				Assemblies = new List<AssemblyDefinition>{TestAssembly},
				Name = "TestAssemblyModule"
			});
		}

		#region Utli
		protected ValueExpression GetNewType(TypeDefinition typeForInstance) {
			var type = MappingContext.Types[typeForInstance];
			Debug.Assert(type.Members.OfType<Field>().None());
			return CodeFactory.CreateValueExpression(CodeFactory.CreateInstance(
				type, new MemberInstances()));
		}
		#endregion

	}
}