using System;
using System.Linq;
using Fijo.Code.ATC.ACL.Model.Expressions;
using Fijo.Code.ATC.DotNet.Handlings.CodeObjectifiers;
using Fijo.Code.CecilContrib.Cil.Model.TreateInstructions;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using Mono.Cecil;
using Mono.Cecil.Cil;
using NUnit.Framework;

namespace Fijo.Code.ATC.DotNetTest.Handlings.CodeObjectifiers {
	[TestFixture]
	public class CallObjectifierTest : ObjectifierTestBase<CallObjectifier> {
		private TypeDefinition _testType;

		public override void SetUp() {
			base.SetUp();
			_testType = ObjectifierNamespace.Single(x => x.Name == "CallTest");
		}

		[Test]
		[Note("Method should have the the arguments ( )")]
		public void SimpleStaticMethod_Handle_HasTargetAndEmptyArgumentInstances() {
			InitMethod(_testType.Methods.Single(x => x.Name == "SimpleStaticMethod" && x.Parameters.None()));

			var call = GetCall();
			Assert.AreSame(Method, call.Target);
			Assert.IsEmpty(call.ArgumentInstances);
			
			Stopwatch.Stop();

			Console.WriteLine("took {0}", Stopwatch.ElapsedMilliseconds);
		}

		[Test]
		[Note("Method should have the the arguments ( CallTest this )")]
		public void SimpleInstanceMethod_Handle_HasTargetAnd1ThisArgumentInstance() {
			InitMethod(_testType.Methods.Single(x => x.Name == "SimpleInstanceMethod" && x.Parameters.None()));

			var valueExpression = GetNewType(_testType);
			ObjectifierContext.Stack.Push(valueExpression);

			var call = GetCall();
			Assert.AreSame(Method, call.Target);
			Assert.AreEqual(1, call.ArgumentInstances.Count);
			Assert.AreSame(valueExpression, call.ArgumentInstances.Single());
		}

		

		private CallExpression GetCall() {
			return (CallExpression) CurrentObjectifier.Handle(GetCallInstruction(MethodDef), ObjectifierContext);
		}

		private CallInstruction GetCallInstruction(MethodDefinition methodDefinition) {
			return new CallInstruction(methodDefinition, Instruction.Create(OpCodes.Call, methodDefinition));
		}
	}
}