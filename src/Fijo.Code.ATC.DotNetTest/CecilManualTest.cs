﻿using System;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Mono.Cecil;
using NUnit.Framework;

namespace Fijo.Code.ATC.DotNetTest {
	[TestFixture]
	public class CecilManualTest : TestAssemblyBase {
		private TypeDefinition _typeDefinition;
		protected ICollection<TypeDefinition> CILTestNamespace { get; set; }

		[SetUp]
		public void SetUp() {
			base.SetUp();
			
			CILTestNamespace = TestAssembly.MainModule.Types.Where(x => x.Namespace == "Fijo.Code.ATC.TestAssembly.CILTest").Execute();
			_typeDefinition = CILTestNamespace.Single(x => x.Name == "Test");
		}

		[Test]
		public void TryCatchTest() {
			var tryCatchTestClass = GetMethod("SimpleTryCatchTest");
			var methodBody = tryCatchTestClass.Body;

			foreach (var exceptionHandler in methodBody.ExceptionHandlers) {
				
				Console.WriteLine(exceptionHandler);

			}
			var instructions = methodBody.Instructions;



			foreach (var instruction in instructions) {
				Console.WriteLine(instruction);
			}

		}

		private MethodDefinition GetMethod(string name) {
			return _typeDefinition.Methods.Single(x => x.Name == name);
		}
	}
}