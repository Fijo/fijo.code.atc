using System;
using Fijo.Code.ATC.DotNet.Framework;
using Mono.Cecil;

namespace Fijo.Code.ATC.DotNetTest.Data {
	public enum TestAssemblies {
		Default
	}

	public class TestAssemblyProvider {
		private readonly IAssemblyService _assemblyService;

		public TestAssemblyProvider(IAssemblyService assemblyService) {
			_assemblyService = assemblyService;
		}

		public AssemblyDefinition Get(TestAssemblies testAssemblies = TestAssemblies.Default) {
			switch (testAssemblies) {
				case TestAssemblies.Default:
					return _assemblyService.GetAssembly(@"R:\Fijo.Code.ATC.TestAssembly\Fijo.Code.ATC.TestAssembly.dll");
			}
			throw new ArgumentException("testAssemblies");
		}

	}
}